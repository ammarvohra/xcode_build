﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChartboostExample
struct ChartboostExample_t2783486883;
// System.String
struct String_t;
// ChartboostSDK.CBLocation
struct CBLocation_t2073599518;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBLocation2073599518.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBImpressionError4105614948.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBClickError3356089303.h"

// System.Void ChartboostExample::.ctor()
extern "C"  void ChartboostExample__ctor_m853900622 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::OnEnable()
extern "C"  void ChartboostExample_OnEnable_m3127290730 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::Start()
extern "C"  void ChartboostExample_Start_m1081502314 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::SetupDelegates()
extern "C"  void ChartboostExample_SetupDelegates_m630877257 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::Update()
extern "C"  void ChartboostExample_Update_m470565731 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::UpdateScrolling()
extern "C"  void ChartboostExample_UpdateScrolling_m2676250094 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::AddLog(System.String)
extern "C"  void ChartboostExample_AddLog_m2102277569 (ChartboostExample_t2783486883 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::OnGUI()
extern "C"  void ChartboostExample_OnGUI_m4046813898 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::LayoutHeader()
extern "C"  void ChartboostExample_LayoutHeader_m3066265315 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::LayoutToggles()
extern "C"  void ChartboostExample_LayoutToggles_m3988910333 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::LayoutButtons()
extern "C"  void ChartboostExample_LayoutButtons_m980221533 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::LayoutAgeGate(System.Int32)
extern "C"  void ChartboostExample_LayoutAgeGate_m1338158037 (ChartboostExample_t2783486883 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::OnDisable()
extern "C"  void ChartboostExample_OnDisable_m540965003 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didInitialize(System.Boolean)
extern "C"  void ChartboostExample_didInitialize_m433054556 (ChartboostExample_t2783486883 * __this, bool ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didFailToLoadInterstitial(ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError)
extern "C"  void ChartboostExample_didFailToLoadInterstitial_m1284899614 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, int32_t ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didDismissInterstitial(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didDismissInterstitial_m437158988 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didCloseInterstitial(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didCloseInterstitial_m969686608 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didClickInterstitial(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didClickInterstitial_m1157863128 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didCacheInterstitial(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didCacheInterstitial_m694656090 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostExample::shouldDisplayInterstitial(ChartboostSDK.CBLocation)
extern "C"  bool ChartboostExample_shouldDisplayInterstitial_m98653208 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didDisplayInterstitial(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didDisplayInterstitial_m3971991534 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didFailToLoadMoreApps(ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError)
extern "C"  void ChartboostExample_didFailToLoadMoreApps_m877039461 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, int32_t ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didDismissMoreApps(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didDismissMoreApps_m140441489 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didCloseMoreApps(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didCloseMoreApps_m1685405789 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didClickMoreApps(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didClickMoreApps_m2192966389 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didCacheMoreApps(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didCacheMoreApps_m427853103 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostExample::shouldDisplayMoreApps(ChartboostSDK.CBLocation)
extern "C"  bool ChartboostExample_shouldDisplayMoreApps_m2541266965 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didDisplayMoreApps(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didDisplayMoreApps_m28907785 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didFailToRecordClick(ChartboostSDK.CBLocation,ChartboostSDK.CBClickError)
extern "C"  void ChartboostExample_didFailToRecordClick_m3173773854 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, int32_t ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didFailToLoadRewardedVideo(ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError)
extern "C"  void ChartboostExample_didFailToLoadRewardedVideo_m1710685433 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, int32_t ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didDismissRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didDismissRewardedVideo_m2940336601 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didCloseRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didCloseRewardedVideo_m1396366877 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didClickRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didClickRewardedVideo_m1634250293 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didCacheRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didCacheRewardedVideo_m466503291 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostExample::shouldDisplayRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  bool ChartboostExample_shouldDisplayRewardedVideo_m3235457429 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didCompleteRewardedVideo(ChartboostSDK.CBLocation,System.Int32)
extern "C"  void ChartboostExample_didCompleteRewardedVideo_m1614527803 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, int32_t ___reward1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didDisplayRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didDisplayRewardedVideo_m3037420257 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didCacheInPlay(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_didCacheInPlay_m1598630931 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didFailToLoadInPlay(ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError)
extern "C"  void ChartboostExample_didFailToLoadInPlay_m2692330517 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, int32_t ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didPauseClickForConfirmation()
extern "C"  void ChartboostExample_didPauseClickForConfirmation_m4115251359 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::willDisplayVideo(ChartboostSDK.CBLocation)
extern "C"  void ChartboostExample_willDisplayVideo_m1057585976 (ChartboostExample_t2783486883 * __this, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::didCompleteAppStoreSheetFlow()
extern "C"  void ChartboostExample_didCompleteAppStoreSheetFlow_m2443858517 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostExample::TrackIAP()
extern "C"  void ChartboostExample_TrackIAP_m1117185349 (ChartboostExample_t2783486883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
