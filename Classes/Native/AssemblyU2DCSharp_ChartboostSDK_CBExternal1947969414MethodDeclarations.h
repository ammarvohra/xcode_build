﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChartboostSDK.CBExternal
struct CBExternal_t1947969414;
// System.String
struct String_t;
// ChartboostSDK.CBLocation
struct CBLocation_t2073599518;
// ChartboostSDK.CBInPlay
struct CBInPlay_t2057847888;
// ChartboostSDK.CBMediation
struct CBMediation_t669766003;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBStatusBarBehavio2216036290.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBLocation2073599518.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBLevelType2978753963.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBMediation669766003.h"

// System.Void ChartboostSDK.CBExternal::.ctor()
extern "C"  void CBExternal__ctor_m1153208534 (CBExternal_t1947969414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::Log(System.String)
extern "C"  void CBExternal_Log_m1941809742 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::isInitialized()
extern "C"  bool CBExternal_isInitialized_m3387635922 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::checkInitialized()
extern "C"  bool CBExternal_checkInitialized_m1220755428 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostInit(System.String,System.String,System.String)
extern "C"  void CBExternal__chartBoostInit_m3156554028 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, String_t* ___appSignature1, String_t* ___unityVersion2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::_chartBoostIsAnyViewVisible()
extern "C"  bool CBExternal__chartBoostIsAnyViewVisible_m2172527577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostCacheInterstitial(System.String)
extern "C"  void CBExternal__chartBoostCacheInterstitial_m71603328 (Il2CppObject * __this /* static, unused */, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::_chartBoostHasInterstitial(System.String)
extern "C"  bool CBExternal__chartBoostHasInterstitial_m2158121080 (Il2CppObject * __this /* static, unused */, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostShowInterstitial(System.String)
extern "C"  void CBExternal__chartBoostShowInterstitial_m4042543637 (Il2CppObject * __this /* static, unused */, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostCacheRewardedVideo(System.String)
extern "C"  void CBExternal__chartBoostCacheRewardedVideo_m488696663 (Il2CppObject * __this /* static, unused */, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::_chartBoostHasRewardedVideo(System.String)
extern "C"  bool CBExternal__chartBoostHasRewardedVideo_m4254842113 (Il2CppObject * __this /* static, unused */, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostShowRewardedVideo(System.String)
extern "C"  void CBExternal__chartBoostShowRewardedVideo_m971852966 (Il2CppObject * __this /* static, unused */, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostCacheInPlay(System.String)
extern "C"  void CBExternal__chartBoostCacheInPlay_m995822899 (Il2CppObject * __this /* static, unused */, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::_chartBoostHasInPlay(System.String)
extern "C"  bool CBExternal__chartBoostHasInPlay_m1408085013 (Il2CppObject * __this /* static, unused */, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ChartboostSDK.CBExternal::_chartBoostGetInPlay(System.String)
extern "C"  IntPtr_t CBExternal__chartBoostGetInPlay_m1001858632 (Il2CppObject * __this /* static, unused */, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostSetCustomId(System.String)
extern "C"  void CBExternal__chartBoostSetCustomId_m4210088070 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostDidPassAgeGate(System.Boolean)
extern "C"  void CBExternal__chartBoostDidPassAgeGate_m2111371337 (Il2CppObject * __this /* static, unused */, bool ___pass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBExternal::_chartBoostGetCustomId()
extern "C"  String_t* CBExternal__chartBoostGetCustomId_m982246007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostHandleOpenURL(System.String,System.String)
extern "C"  void CBExternal__chartBoostHandleOpenURL_m1192111563 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___sourceApp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostSetShouldPauseClickForConfirmation(System.Boolean)
extern "C"  void CBExternal__chartBoostSetShouldPauseClickForConfirmation_m4017463396 (Il2CppObject * __this /* static, unused */, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostSetShouldRequestInterstitialsInFirstSession(System.Boolean)
extern "C"  void CBExternal__chartBoostSetShouldRequestInterstitialsInFirstSession_m2496405053 (Il2CppObject * __this /* static, unused */, bool ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostShouldDisplayInterstitialCallbackResult(System.Boolean)
extern "C"  void CBExternal__chartBoostShouldDisplayInterstitialCallbackResult_m2616503584 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostShouldDisplayRewardedVideoCallbackResult(System.Boolean)
extern "C"  void CBExternal__chartBoostShouldDisplayRewardedVideoCallbackResult_m142108905 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::_chartBoostGetAutoCacheAds()
extern "C"  bool CBExternal__chartBoostGetAutoCacheAds_m1243166629 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostSetAutoCacheAds(System.Boolean)
extern "C"  void CBExternal__chartBoostSetAutoCacheAds_m3763286512 (Il2CppObject * __this /* static, unused */, bool ___autoCacheAds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostSetShouldPrefetchVideoContent(System.Boolean)
extern "C"  void CBExternal__chartBoostSetShouldPrefetchVideoContent_m166546681 (Il2CppObject * __this /* static, unused */, bool ___shouldDisplay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostTrackInAppPurchaseEvent(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void CBExternal__chartBoostTrackInAppPurchaseEvent_m4030172844 (Il2CppObject * __this /* static, unused */, String_t* ___receipt0, String_t* ___productTitle1, String_t* ___productDescription2, String_t* ___productPrice3, String_t* ___productCurrency4, String_t* ___productIdentifier5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostSetGameObjectName(System.String)
extern "C"  void CBExternal__chartBoostSetGameObjectName_m2985222106 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostSetStatusBarBehavior(ChartboostSDK.CBStatusBarBehavior)
extern "C"  void CBExternal__chartBoostSetStatusBarBehavior_m421614898 (Il2CppObject * __this /* static, unused */, int32_t ___statusBarBehavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostTrackLevelInfo(System.String,System.Int32,System.Int32,System.Int32,System.String)
extern "C"  void CBExternal__chartBoostTrackLevelInfo_m1172830328 (Il2CppObject * __this /* static, unused */, String_t* ___eventLabel0, int32_t ___levelType1, int32_t ___mainLevel2, int32_t ___subLevel3, String_t* ___description4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::_chartBoostSetMediation(System.Int32,System.String)
extern "C"  void CBExternal__chartBoostSetMediation_m4033780359 (Il2CppObject * __this /* static, unused */, int32_t ___mediator0, String_t* ___version1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::init()
extern "C"  void CBExternal_init_m1939467030 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::initWithAppId(System.String,System.String)
extern "C"  void CBExternal_initWithAppId_m786627240 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, String_t* ___appSignature1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::destroy()
extern "C"  void CBExternal_destroy_m157132036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::isAnyViewVisible()
extern "C"  bool CBExternal_isAnyViewVisible_m1561264507 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::cacheInterstitial(ChartboostSDK.CBLocation)
extern "C"  void CBExternal_cacheInterstitial_m2531379637 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::hasInterstitial(ChartboostSDK.CBLocation)
extern "C"  bool CBExternal_hasInterstitial_m3590921843 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::showInterstitial(ChartboostSDK.CBLocation)
extern "C"  void CBExternal_showInterstitial_m489821314 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::cacheInPlay(ChartboostSDK.CBLocation)
extern "C"  void CBExternal_cacheInPlay_m3964418888 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::hasInPlay(ChartboostSDK.CBLocation)
extern "C"  bool CBExternal_hasInPlay_m3026842704 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChartboostSDK.CBInPlay ChartboostSDK.CBExternal::getInPlay(ChartboostSDK.CBLocation)
extern "C"  CBInPlay_t2057847888 * CBExternal_getInPlay_m4196426326 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::cacheRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  void CBExternal_cacheRewardedVideo_m3891739282 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::hasRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  bool CBExternal_hasRewardedVideo_m1208474390 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::showRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  void CBExternal_showRewardedVideo_m1663985287 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::chartBoostShouldDisplayInterstitialCallbackResult(System.Boolean)
extern "C"  void CBExternal_chartBoostShouldDisplayInterstitialCallbackResult_m2015397891 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::chartBoostShouldDisplayRewardedVideoCallbackResult(System.Boolean)
extern "C"  void CBExternal_chartBoostShouldDisplayRewardedVideoCallbackResult_m2401264708 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::setCustomId(System.String)
extern "C"  void CBExternal_setCustomId_m2717185926 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBExternal::getCustomId()
extern "C"  String_t* CBExternal_getCustomId_m3369927817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::didPassAgeGate(System.Boolean)
extern "C"  void CBExternal_didPassAgeGate_m3217500035 (Il2CppObject * __this /* static, unused */, bool ___pass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::handleOpenURL(System.String,System.String)
extern "C"  void CBExternal_handleOpenURL_m557791341 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___sourceApp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::setShouldPauseClickForConfirmation(System.Boolean)
extern "C"  void CBExternal_setShouldPauseClickForConfirmation_m3322090156 (Il2CppObject * __this /* static, unused */, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::setShouldRequestInterstitialsInFirstSession(System.Boolean)
extern "C"  void CBExternal_setShouldRequestInterstitialsInFirstSession_m1658596539 (Il2CppObject * __this /* static, unused */, bool ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::setGameObjectName(System.String)
extern "C"  void CBExternal_setGameObjectName_m928216090 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBExternal::getAutoCacheAds()
extern "C"  bool CBExternal_getAutoCacheAds_m3154251559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::setAutoCacheAds(System.Boolean)
extern "C"  void CBExternal_setAutoCacheAds_m1290836360 (Il2CppObject * __this /* static, unused */, bool ___autoCacheAds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::setStatusBarBehavior(ChartboostSDK.CBStatusBarBehavior)
extern "C"  void CBExternal_setStatusBarBehavior_m27114370 (Il2CppObject * __this /* static, unused */, int32_t ___statusBarBehavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::setShouldPrefetchVideoContent(System.Boolean)
extern "C"  void CBExternal_setShouldPrefetchVideoContent_m124202127 (Il2CppObject * __this /* static, unused */, bool ___shouldPrefetch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::trackLevelInfo(System.String,ChartboostSDK.CBLevelType,System.Int32,System.Int32,System.String)
extern "C"  void CBExternal_trackLevelInfo_m1089453179 (Il2CppObject * __this /* static, unused */, String_t* ___eventLabel0, int32_t ___type1, int32_t ___mainLevel2, int32_t ___subLevel3, String_t* ___description4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::trackLevelInfo(System.String,ChartboostSDK.CBLevelType,System.Int32,System.String)
extern "C"  void CBExternal_trackLevelInfo_m3235834528 (Il2CppObject * __this /* static, unused */, String_t* ___eventLabel0, int32_t ___type1, int32_t ___mainLevel2, String_t* ___description3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::trackInAppAppleStorePurchaseEvent(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void CBExternal_trackInAppAppleStorePurchaseEvent_m1854128053 (Il2CppObject * __this /* static, unused */, String_t* ___receipt0, String_t* ___productTitle1, String_t* ___productDescription2, String_t* ___productPrice3, String_t* ___productCurrency4, String_t* ___productIdentifier5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::setMediation(ChartboostSDK.CBMediation,System.String)
extern "C"  void CBExternal_setMediation_m1771799630 (Il2CppObject * __this /* static, unused */, CBMediation_t669766003 * ___mediator0, String_t* ___version1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBExternal::.cctor()
extern "C"  void CBExternal__cctor_m3449186311 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
