﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RatePanel
struct  RatePanel_t560543380  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject RatePanel::RateContactButton
	GameObject_t1756533147 * ___RateContactButton_2;
	// UnityEngine.Sprite RatePanel::Rate
	Sprite_t309593783 * ___Rate_3;
	// UnityEngine.Sprite RatePanel::Contact
	Sprite_t309593783 * ___Contact_4;
	// UnityEngine.GameObject RatePanel::Star1
	GameObject_t1756533147 * ___Star1_5;
	// UnityEngine.GameObject RatePanel::Star2
	GameObject_t1756533147 * ___Star2_6;
	// UnityEngine.GameObject RatePanel::Star3
	GameObject_t1756533147 * ___Star3_7;
	// UnityEngine.GameObject RatePanel::Star4
	GameObject_t1756533147 * ___Star4_8;
	// UnityEngine.GameObject RatePanel::Star5
	GameObject_t1756533147 * ___Star5_9;
	// UnityEngine.GameObject RatePanel::Close
	GameObject_t1756533147 * ___Close_10;
	// UnityEngine.Sprite RatePanel::FillStar
	Sprite_t309593783 * ___FillStar_11;
	// UnityEngine.Sprite RatePanel::BlankStar
	Sprite_t309593783 * ___BlankStar_12;
	// System.Single RatePanel::FillStarY
	float ___FillStarY_13;
	// System.Single RatePanel::BlankStarY
	float ___BlankStarY_14;
	// UnityEngine.GameObject RatePanel::SelectedStar
	GameObject_t1756533147 * ___SelectedStar_15;

public:
	inline static int32_t get_offset_of_RateContactButton_2() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___RateContactButton_2)); }
	inline GameObject_t1756533147 * get_RateContactButton_2() const { return ___RateContactButton_2; }
	inline GameObject_t1756533147 ** get_address_of_RateContactButton_2() { return &___RateContactButton_2; }
	inline void set_RateContactButton_2(GameObject_t1756533147 * value)
	{
		___RateContactButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___RateContactButton_2, value);
	}

	inline static int32_t get_offset_of_Rate_3() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___Rate_3)); }
	inline Sprite_t309593783 * get_Rate_3() const { return ___Rate_3; }
	inline Sprite_t309593783 ** get_address_of_Rate_3() { return &___Rate_3; }
	inline void set_Rate_3(Sprite_t309593783 * value)
	{
		___Rate_3 = value;
		Il2CppCodeGenWriteBarrier(&___Rate_3, value);
	}

	inline static int32_t get_offset_of_Contact_4() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___Contact_4)); }
	inline Sprite_t309593783 * get_Contact_4() const { return ___Contact_4; }
	inline Sprite_t309593783 ** get_address_of_Contact_4() { return &___Contact_4; }
	inline void set_Contact_4(Sprite_t309593783 * value)
	{
		___Contact_4 = value;
		Il2CppCodeGenWriteBarrier(&___Contact_4, value);
	}

	inline static int32_t get_offset_of_Star1_5() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___Star1_5)); }
	inline GameObject_t1756533147 * get_Star1_5() const { return ___Star1_5; }
	inline GameObject_t1756533147 ** get_address_of_Star1_5() { return &___Star1_5; }
	inline void set_Star1_5(GameObject_t1756533147 * value)
	{
		___Star1_5 = value;
		Il2CppCodeGenWriteBarrier(&___Star1_5, value);
	}

	inline static int32_t get_offset_of_Star2_6() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___Star2_6)); }
	inline GameObject_t1756533147 * get_Star2_6() const { return ___Star2_6; }
	inline GameObject_t1756533147 ** get_address_of_Star2_6() { return &___Star2_6; }
	inline void set_Star2_6(GameObject_t1756533147 * value)
	{
		___Star2_6 = value;
		Il2CppCodeGenWriteBarrier(&___Star2_6, value);
	}

	inline static int32_t get_offset_of_Star3_7() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___Star3_7)); }
	inline GameObject_t1756533147 * get_Star3_7() const { return ___Star3_7; }
	inline GameObject_t1756533147 ** get_address_of_Star3_7() { return &___Star3_7; }
	inline void set_Star3_7(GameObject_t1756533147 * value)
	{
		___Star3_7 = value;
		Il2CppCodeGenWriteBarrier(&___Star3_7, value);
	}

	inline static int32_t get_offset_of_Star4_8() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___Star4_8)); }
	inline GameObject_t1756533147 * get_Star4_8() const { return ___Star4_8; }
	inline GameObject_t1756533147 ** get_address_of_Star4_8() { return &___Star4_8; }
	inline void set_Star4_8(GameObject_t1756533147 * value)
	{
		___Star4_8 = value;
		Il2CppCodeGenWriteBarrier(&___Star4_8, value);
	}

	inline static int32_t get_offset_of_Star5_9() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___Star5_9)); }
	inline GameObject_t1756533147 * get_Star5_9() const { return ___Star5_9; }
	inline GameObject_t1756533147 ** get_address_of_Star5_9() { return &___Star5_9; }
	inline void set_Star5_9(GameObject_t1756533147 * value)
	{
		___Star5_9 = value;
		Il2CppCodeGenWriteBarrier(&___Star5_9, value);
	}

	inline static int32_t get_offset_of_Close_10() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___Close_10)); }
	inline GameObject_t1756533147 * get_Close_10() const { return ___Close_10; }
	inline GameObject_t1756533147 ** get_address_of_Close_10() { return &___Close_10; }
	inline void set_Close_10(GameObject_t1756533147 * value)
	{
		___Close_10 = value;
		Il2CppCodeGenWriteBarrier(&___Close_10, value);
	}

	inline static int32_t get_offset_of_FillStar_11() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___FillStar_11)); }
	inline Sprite_t309593783 * get_FillStar_11() const { return ___FillStar_11; }
	inline Sprite_t309593783 ** get_address_of_FillStar_11() { return &___FillStar_11; }
	inline void set_FillStar_11(Sprite_t309593783 * value)
	{
		___FillStar_11 = value;
		Il2CppCodeGenWriteBarrier(&___FillStar_11, value);
	}

	inline static int32_t get_offset_of_BlankStar_12() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___BlankStar_12)); }
	inline Sprite_t309593783 * get_BlankStar_12() const { return ___BlankStar_12; }
	inline Sprite_t309593783 ** get_address_of_BlankStar_12() { return &___BlankStar_12; }
	inline void set_BlankStar_12(Sprite_t309593783 * value)
	{
		___BlankStar_12 = value;
		Il2CppCodeGenWriteBarrier(&___BlankStar_12, value);
	}

	inline static int32_t get_offset_of_FillStarY_13() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___FillStarY_13)); }
	inline float get_FillStarY_13() const { return ___FillStarY_13; }
	inline float* get_address_of_FillStarY_13() { return &___FillStarY_13; }
	inline void set_FillStarY_13(float value)
	{
		___FillStarY_13 = value;
	}

	inline static int32_t get_offset_of_BlankStarY_14() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___BlankStarY_14)); }
	inline float get_BlankStarY_14() const { return ___BlankStarY_14; }
	inline float* get_address_of_BlankStarY_14() { return &___BlankStarY_14; }
	inline void set_BlankStarY_14(float value)
	{
		___BlankStarY_14 = value;
	}

	inline static int32_t get_offset_of_SelectedStar_15() { return static_cast<int32_t>(offsetof(RatePanel_t560543380, ___SelectedStar_15)); }
	inline GameObject_t1756533147 * get_SelectedStar_15() const { return ___SelectedStar_15; }
	inline GameObject_t1756533147 ** get_address_of_SelectedStar_15() { return &___SelectedStar_15; }
	inline void set_SelectedStar_15(GameObject_t1756533147 * value)
	{
		___SelectedStar_15 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedStar_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
