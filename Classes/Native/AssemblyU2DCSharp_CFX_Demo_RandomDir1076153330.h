﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_RandomDir
struct  CFX_Demo_RandomDir_t1076153330  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 CFX_Demo_RandomDir::min
	Vector3_t2243707580  ___min_2;
	// UnityEngine.Vector3 CFX_Demo_RandomDir::max
	Vector3_t2243707580  ___max_3;

public:
	inline static int32_t get_offset_of_min_2() { return static_cast<int32_t>(offsetof(CFX_Demo_RandomDir_t1076153330, ___min_2)); }
	inline Vector3_t2243707580  get_min_2() const { return ___min_2; }
	inline Vector3_t2243707580 * get_address_of_min_2() { return &___min_2; }
	inline void set_min_2(Vector3_t2243707580  value)
	{
		___min_2 = value;
	}

	inline static int32_t get_offset_of_max_3() { return static_cast<int32_t>(offsetof(CFX_Demo_RandomDir_t1076153330, ___max_3)); }
	inline Vector3_t2243707580  get_max_3() const { return ___max_3; }
	inline Vector3_t2243707580 * get_address_of_max_3() { return &___max_3; }
	inline void set_max_3(Vector3_t2243707580  value)
	{
		___max_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
