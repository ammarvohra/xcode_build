﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback
struct GADUNativeExpressAdViewWillPresentScreenCallback_t3478706784;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient
struct RewardBasedVideoAdClient_t2282664017;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>
struct EventHandler_1_t347919082;
// System.EventHandler`1<GoogleMobileAds.Api.Reward>
struct EventHandler_1_t344857101;
// GoogleMobileAds.Api.AdRequest
struct AdRequest_t3179524098;
// System.String
struct String_t;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback
struct GADURewardBasedVideoAdDidCloseCallback_t2453903099;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback
struct GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback
struct GADURewardBasedVideoAdDidOpenCallback_t587935421;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback
struct GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback
struct GADURewardBasedVideoAdDidRewardCallback_t129051320;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback
struct GADURewardBasedVideoAdDidStartCallback_t25341677;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback
struct GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867;
// GoogleMobileAds.iOS.Utils
struct Utils_t984021165;
// HandLegAnim
struct HandLegAnim_t1741127542;
// KittyHandNFootManager
struct KittyHandNFootManager_t1686788969;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// BubbleWaterTalkMoving
struct BubbleWaterTalkMoving_t3963847219;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t948534547;
// UnityEngine.Animator
struct Animator_t69676727;
// ToolButton
struct ToolButton_t3848681854;
// KittyHandNFootManager/<DeleteSpray>c__Iterator5
struct U3CDeleteSprayU3Ec__Iterator5_t3757503118;
// KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9
struct U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216;
// KittyHandNFootManager/<LoadNextLevel>c__Iterator7
struct U3CLoadNextLevelU3Ec__Iterator7_t3146601861;
// KittyHandNFootManager/<MakeBigger>c__Iterator1
struct U3CMakeBiggerU3Ec__Iterator1_t2709881044;
// KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6
struct U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083;
// KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3
struct U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693;
// KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8
struct U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244;
// KittyHandNFootManager/<NailCuttingAnim>c__Iterator2
struct U3CNailCuttingAnimU3Ec__Iterator2_t2399846568;
// KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4
struct U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482;
// KittyHandNFootManager/<RestartButtonUp>c__IteratorA
struct U3CRestartButtonUpU3Ec__IteratorA_t2431284672;
// KittyHandNFootManager/<ShareScreenshot>c__IteratorC
struct U3CShareScreenshotU3Ec__IteratorC_t3190744391;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4251328308;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// KittyHandNFootManager/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3126961537;
// KittyHandNFootManager/<TakeSnap>c__IteratorB
struct U3CTakeSnapU3Ec__IteratorB_t2011450030;
// LiveVideo
struct LiveVideo_t867630853;
// UnityEngine.Renderer
struct Renderer_t257310565;
// MaskCamera
struct MaskCamera_t4194554627;
// UnityEngine.Camera
struct Camera_t189460977;
// MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t1970456718;
// System.Type
struct Type_t;
// RatePanel
struct RatePanel_t560543380;
// SoundManager
struct SoundManager_t654432262;
// SoundManager/<CloseMouth>c__Iterator0
struct U3CCloseMouthU3Ec__Iterator0_t2695835239;
// SoundManager/<DestroyBubbleSound>c__Iterator1
struct U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612;
// StyleManager
struct StyleManager_t3102803280;
// StyleManager/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3971177230;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t3220183178;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t13116344;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// ToolButton/<ButtonBackOnPosition>c__Iterator0
struct U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669;
// TouchOnStar
struct TouchOnStar_t2108028178;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeExpres3478706784.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeExpres3478706784MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2282664017.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2282664017MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen1880931879.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "mscorlib_System_EventHandler_1_gen347919082.h"
#include "mscorlib_System_EventHandler_1_gen344857101.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Externs2948936873MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV4169257859MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi862929376MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi587935421MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVid25341677MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2453903099MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi129051320MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2167763867MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV4169257859.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi862929376.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi587935421.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVid25341677.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2453903099.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi129051320.h"
#include "mscorlib_System_Double4078015681.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2167763867.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdRequest3179524098.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Utils984021165MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen1880931879MethodDeclarations.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "mscorlib_System_EventArgs3289624707MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdFailedToLo1756611910MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen347919082MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdFailedToLo1756611910.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Reward1753549929MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen344857101MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Reward1753549929.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Utils984021165.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdRequest3179524098MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge362681087MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1010328439MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Mediation_Me1641207307MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3145964225.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Nullable_1_gen1791139578.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Mediation_Me1641207307.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat545058113.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge362681087.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3145964225MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3251239280MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Nullable_1_gen1791139578MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Gender3528073263.h"
#include "mscorlib_System_Nullable_1_gen2088641033MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1010328439.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat545058113MethodDeclarations.h"
#include "AssemblyU2DCSharp_HandLegAnim1741127542.h"
#include "AssemblyU2DCSharp_HandLegAnim1741127542MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager1686788969.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager1686788969MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CStartU33126961537MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CStartU33126961537.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharp_BubbleWaterTalkMoving3963847219.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundManager654432262.h"
#include "AssemblyU2DCSharp_SoundManager654432262MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_BoxCollider2D948534547.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DCSharp_ToolButton3848681854.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "AssemblyU2DCSharp_AdsManager2564570159MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "AssemblyU2DCSharp_AdsManager2564570159.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeBig2709881044MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeBig2709881044.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CNailCut2399846568MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CNailCut2399846568.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeNai1756903693MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeNai1756903693.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CPrepareF599836482MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CPrepareF599836482.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CDeleteS3757503118MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CDeleteS3757503118.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeCur3508152083MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeCur3508152083.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CLoadNex3146601861MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CLoadNex3146601861.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeSty3184490244MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeSty3184490244.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CFbLikeB1718721216MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CFbLikeB1718721216.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CRestart2431284672MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CRestart2431284672.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CTakeSna2011450030MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CTakeSna2011450030.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CShareSc3190744391MethodDeclarations.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CShareSc3190744391.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_IO_Path41728875MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "AssemblyU2DCSharp_LiveVideo867630853.h"
#include "AssemblyU2DCSharp_LiveVideo867630853MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture1079476942MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamDevice3983871389.h"
#include "UnityEngine_UnityEngine_WebCamTexture1079476942.h"
#include "UnityEngine_UnityEngine_WebCamDevice3983871389MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "AssemblyU2DCSharp_MaskCamera4194554627.h"
#include "AssemblyU2DCSharp_MaskCamera4194554627MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GL1765937205MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3360518468.h"
#include "UnityEngine_UnityEngine_TextureWrapMode3683976566.h"
#include "UnityEngine_UnityEngine_FilterMode10814199.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen506773894.h"
#include "mscorlib_System_Nullable_1_gen506773894MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoPInvokeCallbackAttribute1970456718.h"
#include "AssemblyU2DCSharp_MonoPInvokeCallbackAttribute1970456718MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "AssemblyU2DCSharp_RatePanel560543380.h"
#include "AssemblyU2DCSharp_RatePanel560543380MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "AssemblyU2DCSharp_SoundManager_U3CCloseMouthU3Ec__2695835239MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundManager_U3CCloseMouthU3Ec__2695835239.h"
#include "AssemblyU2DCSharp_SoundManager_U3CDestroyBubbleSou1692068612MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundManager_U3CDestroyBubbleSou1692068612.h"
#include "AssemblyU2DCSharp_StyleManager3102803280.h"
#include "AssemblyU2DCSharp_StyleManager3102803280MethodDeclarations.h"
#include "AssemblyU2DCSharp_StyleManager_U3CStartU3Ec__Itera3971177230MethodDeclarations.h"
#include "AssemblyU2DCSharp_StyleManager_U3CStartU3Ec__Itera3971177230.h"
#include "AssemblyU2DCSharp_ToolButton3848681854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_PolygonCollider2D3220183178.h"
#include "UnityEngine_UnityEngine_CircleCollider2D13116344.h"
#include "AssemblyU2DCSharp_ToolButton_ButtonType1054768361.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754MethodDeclarations.h"
#include "AssemblyU2DCSharp_ToolButton_U3CButtonBackOnPositi3433254669MethodDeclarations.h"
#include "AssemblyU2DCSharp_ToolButton_U3CButtonBackOnPositi3433254669.h"
#include "AssemblyU2DCSharp_ToolButton_ButtonType1054768361MethodDeclarations.h"
#include "AssemblyU2DCSharp_TouchOnStar2108028178.h"
#include "AssemblyU2DCSharp_TouchOnStar2108028178MethodDeclarations.h"

// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.EventHandler`1<System.EventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEventHandler_1_t1880931879_m3275209341(__this /* static, unused */, p0, p1, p2, method) ((  EventHandler_1_t1880931879 * (*) (Il2CppObject * /* static, unused */, EventHandler_1_t1880931879 **, EventHandler_1_t1880931879 *, EventHandler_1_t1880931879 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEventHandler_1_t347919082_m259117074(__this /* static, unused */, p0, p1, p2, method) ((  EventHandler_1_t347919082 * (*) (Il2CppObject * /* static, unused */, EventHandler_1_t347919082 **, EventHandler_1_t347919082 *, EventHandler_1_t347919082 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.EventHandler`1<GoogleMobileAds.Api.Reward>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEventHandler_1_t344857101_m705530765(__this /* static, unused */, p0, p1, p2, method) ((  EventHandler_1_t344857101 * (*) (Il2CppObject * /* static, unused */, EventHandler_1_t344857101 **, EventHandler_1_t344857101 *, EventHandler_1_t344857101 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<BubbleWaterTalkMoving>()
#define GameObject_AddComponent_TisBubbleWaterTalkMoving_t3963847219_m3008669749(__this, method) ((  BubbleWaterTalkMoving_t3963847219 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
#define Component_GetComponent_TisTextMesh_t1641806576_m4177416886(__this, method) ((  TextMesh_t1641806576 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, method) ((  SpriteRenderer_t1209076198 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2650145732(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider2D>()
#define GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(__this, method) ((  BoxCollider2D_t948534547 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t69676727_m2717502299(__this, method) ((  Animator_t69676727 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ToolButton>()
#define GameObject_GetComponent_TisToolButton_t3848681854_m3256490429(__this, method) ((  ToolButton_t3848681854 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.AndroidJavaObject::GetStatic<System.Object>(System.String)
extern "C"  Il2CppObject * AndroidJavaObject_GetStatic_TisIl2CppObject_m1003560681_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, const MethodInfo* method);
#define AndroidJavaObject_GetStatic_TisIl2CppObject_m1003560681(__this, p0, method) ((  Il2CppObject * (*) (AndroidJavaObject_t4251328308 *, String_t*, const MethodInfo*))AndroidJavaObject_GetStatic_TisIl2CppObject_m1003560681_gshared)(__this, p0, method)
// !!0 UnityEngine.AndroidJavaObject::GetStatic<System.String>(System.String)
#define AndroidJavaObject_GetStatic_TisString_t_m422203629(__this, p0, method) ((  String_t* (*) (AndroidJavaObject_t4251328308 *, String_t*, const MethodInfo*))AndroidJavaObject_GetStatic_TisIl2CppObject_m1003560681_gshared)(__this, p0, method)
// !!0 UnityEngine.AndroidJavaObject::Call<System.Object>(System.String,System.Object[])
extern "C"  Il2CppObject * AndroidJavaObject_Call_TisIl2CppObject_m1398489478_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, ObjectU5BU5D_t3614634134* p1, const MethodInfo* method);
#define AndroidJavaObject_Call_TisIl2CppObject_m1398489478(__this, p0, p1, method) ((  Il2CppObject * (*) (AndroidJavaObject_t4251328308 *, String_t*, ObjectU5BU5D_t3614634134*, const MethodInfo*))AndroidJavaObject_Call_TisIl2CppObject_m1398489478_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::Call<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
#define AndroidJavaObject_Call_TisAndroidJavaObject_t4251328308_m4199176621(__this, p0, p1, method) ((  AndroidJavaObject_t4251328308 * (*) (AndroidJavaObject_t4251328308 *, String_t*, ObjectU5BU5D_t3614634134*, const MethodInfo*))AndroidJavaObject_Call_TisIl2CppObject_m1398489478_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::CallStatic<System.Object>(System.String,System.Object[])
extern "C"  Il2CppObject * AndroidJavaObject_CallStatic_TisIl2CppObject_m1295142028_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, ObjectU5BU5D_t3614634134* p1, const MethodInfo* method);
#define AndroidJavaObject_CallStatic_TisIl2CppObject_m1295142028(__this, p0, p1, method) ((  Il2CppObject * (*) (AndroidJavaObject_t4251328308 *, String_t*, ObjectU5BU5D_t3614634134*, const MethodInfo*))AndroidJavaObject_CallStatic_TisIl2CppObject_m1295142028_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::CallStatic<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
#define AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m3890456357(__this, p0, p1, method) ((  AndroidJavaObject_t4251328308 * (*) (AndroidJavaObject_t4251328308 *, String_t*, ObjectU5BU5D_t3614634134*, const MethodInfo*))AndroidJavaObject_CallStatic_TisIl2CppObject_m1295142028_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::GetStatic<UnityEngine.AndroidJavaObject>(System.String)
#define AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m273477134(__this, p0, method) ((  AndroidJavaObject_t4251328308 * (*) (AndroidJavaObject_t4251328308 *, String_t*, const MethodInfo*))AndroidJavaObject_GetStatic_TisIl2CppObject_m1003560681_gshared)(__this, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t257310565_m1312615893(__this, method) ((  Renderer_t257310565 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, method) ((  Rigidbody2D_t502193897 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody2D>()
#define GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396(__this, method) ((  Rigidbody2D_t502193897 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t948534547_m324820273(__this, method) ((  BoxCollider2D_t948534547 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.PolygonCollider2D>()
#define Component_GetComponent_TisPolygonCollider2D_t3220183178_m191733878(__this, method) ((  PolygonCollider2D_t3220183178 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CircleCollider2D>()
#define Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442(__this, method) ((  CircleCollider2D_t13116344 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.BoxCollider2D>()
#define GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550(__this, method) ((  BoxCollider2D_t948534547 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GADUNativeExpressAdViewWillPresentScreenCallback__ctor_m3053910633 (GADUNativeExpressAdViewWillPresentScreenCallback_t3478706784 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback::Invoke(System.IntPtr)
extern "C"  void GADUNativeExpressAdViewWillPresentScreenCallback_Invoke_m3061695959 (GADUNativeExpressAdViewWillPresentScreenCallback_t3478706784 * __this, IntPtr_t ___nativeExpressAdClient0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GADUNativeExpressAdViewWillPresentScreenCallback_Invoke_m3061695959((GADUNativeExpressAdViewWillPresentScreenCallback_t3478706784 *)__this->get_prev_9(),___nativeExpressAdClient0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___nativeExpressAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___nativeExpressAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___nativeExpressAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___nativeExpressAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_GADUNativeExpressAdViewWillPresentScreenCallback_t3478706784 (GADUNativeExpressAdViewWillPresentScreenCallback_t3478706784 * __this, IntPtr_t ___nativeExpressAdClient0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___nativeExpressAdClient0).get_m_value_0()));

}
// System.IAsyncResult GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t GADUNativeExpressAdViewWillPresentScreenCallback_BeginInvoke_m1243072646_MetadataUsageId;
extern "C"  Il2CppObject * GADUNativeExpressAdViewWillPresentScreenCallback_BeginInvoke_m1243072646 (GADUNativeExpressAdViewWillPresentScreenCallback_t3478706784 * __this, IntPtr_t ___nativeExpressAdClient0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GADUNativeExpressAdViewWillPresentScreenCallback_BeginInvoke_m1243072646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___nativeExpressAdClient0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GADUNativeExpressAdViewWillPresentScreenCallback_EndInvoke_m1113384527 (GADUNativeExpressAdViewWillPresentScreenCallback_t3478706784 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::.ctor()
extern "C"  void RewardBasedVideoAdClient__ctor_m3480651414 (RewardBasedVideoAdClient_t2282664017 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_add_OnAdLoaded_m2360608895_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_add_OnAdLoaded_m2360608895 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_add_OnAdLoaded_m2360608895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdLoaded_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdLoaded_2();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_remove_OnAdLoaded_m361986058_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_remove_OnAdLoaded_m361986058 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_remove_OnAdLoaded_m361986058_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdLoaded_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdLoaded_2();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern Il2CppClass* EventHandler_1_t347919082_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_add_OnAdFailedToLoad_m3360906945_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_add_OnAdFailedToLoad_m3360906945 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t347919082 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_add_OnAdFailedToLoad_m3360906945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t347919082 * V_0 = NULL;
	EventHandler_1_t347919082 * V_1 = NULL;
	{
		EventHandler_1_t347919082 * L_0 = __this->get_OnAdFailedToLoad_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t347919082 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t347919082 ** L_2 = __this->get_address_of_OnAdFailedToLoad_3();
		EventHandler_1_t347919082 * L_3 = V_1;
		EventHandler_1_t347919082 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t347919082 * L_6 = V_0;
		EventHandler_1_t347919082 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t347919082 *>(L_2, ((EventHandler_1_t347919082 *)CastclassSealed(L_5, EventHandler_1_t347919082_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t347919082 * L_8 = V_0;
		EventHandler_1_t347919082 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t347919082 *)L_8) == ((Il2CppObject*)(EventHandler_1_t347919082 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern Il2CppClass* EventHandler_1_t347919082_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_remove_OnAdFailedToLoad_m1695697336_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_remove_OnAdFailedToLoad_m1695697336 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t347919082 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_remove_OnAdFailedToLoad_m1695697336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t347919082 * V_0 = NULL;
	EventHandler_1_t347919082 * V_1 = NULL;
	{
		EventHandler_1_t347919082 * L_0 = __this->get_OnAdFailedToLoad_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t347919082 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t347919082 ** L_2 = __this->get_address_of_OnAdFailedToLoad_3();
		EventHandler_1_t347919082 * L_3 = V_1;
		EventHandler_1_t347919082 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t347919082 * L_6 = V_0;
		EventHandler_1_t347919082 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t347919082 *>(L_2, ((EventHandler_1_t347919082 *)CastclassSealed(L_5, EventHandler_1_t347919082_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t347919082 * L_8 = V_0;
		EventHandler_1_t347919082 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t347919082 *)L_8) == ((Il2CppObject*)(EventHandler_1_t347919082 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_add_OnAdOpening_m1443827366_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_add_OnAdOpening_m1443827366 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_add_OnAdOpening_m1443827366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdOpening_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdOpening_4();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_remove_OnAdOpening_m2766087639_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_remove_OnAdOpening_m2766087639 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_remove_OnAdOpening_m2766087639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdOpening_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdOpening_4();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdStarted(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_add_OnAdStarted_m2292558145_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_add_OnAdStarted_m2292558145 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_add_OnAdStarted_m2292558145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdStarted_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdStarted_5();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdStarted(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_remove_OnAdStarted_m3745925218_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_remove_OnAdStarted_m3745925218 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_remove_OnAdStarted_m3745925218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdStarted_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdStarted_5();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_add_OnAdClosed_m1044374692_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_add_OnAdClosed_m1044374692 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_add_OnAdClosed_m1044374692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdClosed_6();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdClosed_6();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_remove_OnAdClosed_m1339443377_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_remove_OnAdClosed_m1339443377 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_remove_OnAdClosed_m1339443377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdClosed_6();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdClosed_6();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern Il2CppClass* EventHandler_1_t344857101_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_add_OnAdRewarded_m3090518136_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_add_OnAdRewarded_m3090518136 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t344857101 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_add_OnAdRewarded_m3090518136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t344857101 * V_0 = NULL;
	EventHandler_1_t344857101 * V_1 = NULL;
	{
		EventHandler_1_t344857101 * L_0 = __this->get_OnAdRewarded_7();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t344857101 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t344857101 ** L_2 = __this->get_address_of_OnAdRewarded_7();
		EventHandler_1_t344857101 * L_3 = V_1;
		EventHandler_1_t344857101 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t344857101 * L_6 = V_0;
		EventHandler_1_t344857101 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t344857101 *>(L_2, ((EventHandler_1_t344857101 *)CastclassSealed(L_5, EventHandler_1_t344857101_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t344857101 * L_8 = V_0;
		EventHandler_1_t344857101 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t344857101 *)L_8) == ((Il2CppObject*)(EventHandler_1_t344857101 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern Il2CppClass* EventHandler_1_t344857101_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_remove_OnAdRewarded_m1609402645_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_remove_OnAdRewarded_m1609402645 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t344857101 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_remove_OnAdRewarded_m1609402645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t344857101 * V_0 = NULL;
	EventHandler_1_t344857101 * V_1 = NULL;
	{
		EventHandler_1_t344857101 * L_0 = __this->get_OnAdRewarded_7();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t344857101 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t344857101 ** L_2 = __this->get_address_of_OnAdRewarded_7();
		EventHandler_1_t344857101 * L_3 = V_1;
		EventHandler_1_t344857101 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t344857101 * L_6 = V_0;
		EventHandler_1_t344857101 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t344857101 *>(L_2, ((EventHandler_1_t344857101 *)CastclassSealed(L_5, EventHandler_1_t344857101_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t344857101 * L_8 = V_0;
		EventHandler_1_t344857101 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t344857101 *)L_8) == ((Il2CppObject*)(EventHandler_1_t344857101 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_add_OnAdLeavingApplication_m1189931146_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_add_OnAdLeavingApplication_m1189931146 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_add_OnAdLeavingApplication_m1189931146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdLeavingApplication_8();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdLeavingApplication_8();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern Il2CppClass* EventHandler_1_t1880931879_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_remove_OnAdLeavingApplication_m801192143_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_remove_OnAdLeavingApplication_m801192143 (RewardBasedVideoAdClient_t2282664017 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_remove_OnAdLeavingApplication_m801192143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1880931879 * V_0 = NULL;
	EventHandler_1_t1880931879 * V_1 = NULL;
	{
		EventHandler_1_t1880931879 * L_0 = __this->get_OnAdLeavingApplication_8();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1880931879 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1880931879 ** L_2 = __this->get_address_of_OnAdLeavingApplication_8();
		EventHandler_1_t1880931879 * L_3 = V_1;
		EventHandler_1_t1880931879 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1880931879 * L_6 = V_0;
		EventHandler_1_t1880931879 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1880931879 *>(L_2, ((EventHandler_1_t1880931879 *)CastclassSealed(L_5, EventHandler_1_t1880931879_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1880931879 * L_8 = V_0;
		EventHandler_1_t1880931879 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1880931879 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1880931879 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.IntPtr GoogleMobileAds.iOS.RewardBasedVideoAdClient::get_RewardBasedVideoAdPtr()
extern "C"  IntPtr_t RewardBasedVideoAdClient_get_RewardBasedVideoAdPtr_m2843237604 (RewardBasedVideoAdClient_t2282664017 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_rewardBasedVideoAdPtr_0();
		return L_0;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::set_RewardBasedVideoAdPtr(System.IntPtr)
extern "C"  void RewardBasedVideoAdClient_set_RewardBasedVideoAdPtr_m3075055733 (RewardBasedVideoAdClient_t2282664017 * __this, IntPtr_t ___value0, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_rewardBasedVideoAdPtr_0();
		Externs_GADURelease_m239039392(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___value0;
		__this->set_rewardBasedVideoAdPtr_0(L_1);
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::CreateRewardBasedVideoAd()
extern Il2CppClass* RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var;
extern Il2CppClass* GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859_il2cpp_TypeInfo_var;
extern Il2CppClass* GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376_il2cpp_TypeInfo_var;
extern Il2CppClass* GADURewardBasedVideoAdDidOpenCallback_t587935421_il2cpp_TypeInfo_var;
extern Il2CppClass* GADURewardBasedVideoAdDidStartCallback_t25341677_il2cpp_TypeInfo_var;
extern Il2CppClass* GADURewardBasedVideoAdDidCloseCallback_t2453903099_il2cpp_TypeInfo_var;
extern Il2CppClass* GADURewardBasedVideoAdDidRewardCallback_t129051320_il2cpp_TypeInfo_var;
extern Il2CppClass* GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867_il2cpp_TypeInfo_var;
extern const MethodInfo* RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_m1731704502_MethodInfo_var;
extern const MethodInfo* RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m3620490801_MethodInfo_var;
extern const MethodInfo* RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m1872094434_MethodInfo_var;
extern const MethodInfo* RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_m724316502_MethodInfo_var;
extern const MethodInfo* RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m3954160878_MethodInfo_var;
extern const MethodInfo* RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_m4093671146_MethodInfo_var;
extern const MethodInfo* RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_m1901340942_MethodInfo_var;
extern const uint32_t RewardBasedVideoAdClient_CreateRewardBasedVideoAd_m2854290848_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_CreateRewardBasedVideoAd_m2854290848 (RewardBasedVideoAdClient_t2282664017 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_CreateRewardBasedVideoAd_m2854290848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t G_B2_0;
	memset(&G_B2_0, 0, sizeof(G_B2_0));
	IntPtr_t G_B1_0;
	memset(&G_B1_0, 0, sizeof(G_B1_0));
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B4_0 = NULL;
	IntPtr_t G_B4_1;
	memset(&G_B4_1, 0, sizeof(G_B4_1));
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B3_0 = NULL;
	IntPtr_t G_B3_1;
	memset(&G_B3_1, 0, sizeof(G_B3_1));
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B6_0 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B6_1 = NULL;
	IntPtr_t G_B6_2;
	memset(&G_B6_2, 0, sizeof(G_B6_2));
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B5_0 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B5_1 = NULL;
	IntPtr_t G_B5_2;
	memset(&G_B5_2, 0, sizeof(G_B5_2));
	GADURewardBasedVideoAdDidOpenCallback_t587935421 * G_B8_0 = NULL;
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B8_1 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B8_2 = NULL;
	IntPtr_t G_B8_3;
	memset(&G_B8_3, 0, sizeof(G_B8_3));
	GADURewardBasedVideoAdDidOpenCallback_t587935421 * G_B7_0 = NULL;
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B7_1 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B7_2 = NULL;
	IntPtr_t G_B7_3;
	memset(&G_B7_3, 0, sizeof(G_B7_3));
	GADURewardBasedVideoAdDidStartCallback_t25341677 * G_B10_0 = NULL;
	GADURewardBasedVideoAdDidOpenCallback_t587935421 * G_B10_1 = NULL;
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B10_2 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B10_3 = NULL;
	IntPtr_t G_B10_4;
	memset(&G_B10_4, 0, sizeof(G_B10_4));
	GADURewardBasedVideoAdDidStartCallback_t25341677 * G_B9_0 = NULL;
	GADURewardBasedVideoAdDidOpenCallback_t587935421 * G_B9_1 = NULL;
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B9_2 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B9_3 = NULL;
	IntPtr_t G_B9_4;
	memset(&G_B9_4, 0, sizeof(G_B9_4));
	GADURewardBasedVideoAdDidCloseCallback_t2453903099 * G_B12_0 = NULL;
	GADURewardBasedVideoAdDidStartCallback_t25341677 * G_B12_1 = NULL;
	GADURewardBasedVideoAdDidOpenCallback_t587935421 * G_B12_2 = NULL;
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B12_3 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B12_4 = NULL;
	IntPtr_t G_B12_5;
	memset(&G_B12_5, 0, sizeof(G_B12_5));
	GADURewardBasedVideoAdDidCloseCallback_t2453903099 * G_B11_0 = NULL;
	GADURewardBasedVideoAdDidStartCallback_t25341677 * G_B11_1 = NULL;
	GADURewardBasedVideoAdDidOpenCallback_t587935421 * G_B11_2 = NULL;
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B11_3 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B11_4 = NULL;
	IntPtr_t G_B11_5;
	memset(&G_B11_5, 0, sizeof(G_B11_5));
	GADURewardBasedVideoAdDidRewardCallback_t129051320 * G_B14_0 = NULL;
	GADURewardBasedVideoAdDidCloseCallback_t2453903099 * G_B14_1 = NULL;
	GADURewardBasedVideoAdDidStartCallback_t25341677 * G_B14_2 = NULL;
	GADURewardBasedVideoAdDidOpenCallback_t587935421 * G_B14_3 = NULL;
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B14_4 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B14_5 = NULL;
	IntPtr_t G_B14_6;
	memset(&G_B14_6, 0, sizeof(G_B14_6));
	GADURewardBasedVideoAdDidRewardCallback_t129051320 * G_B13_0 = NULL;
	GADURewardBasedVideoAdDidCloseCallback_t2453903099 * G_B13_1 = NULL;
	GADURewardBasedVideoAdDidStartCallback_t25341677 * G_B13_2 = NULL;
	GADURewardBasedVideoAdDidOpenCallback_t587935421 * G_B13_3 = NULL;
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * G_B13_4 = NULL;
	GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * G_B13_5 = NULL;
	IntPtr_t G_B13_6;
	memset(&G_B13_6, 0, sizeof(G_B13_6));
	{
		GCHandle_t3409268066  L_0 = GCHandle_Alloc_m3171748614(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		IntPtr_t L_1 = GCHandle_op_Explicit_m1252045235(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_rewardBasedVideoAdClientPtr_1(L_1);
		IntPtr_t L_2 = __this->get_rewardBasedVideoAdClientPtr_1();
		IntPtr_t L_3 = Externs_GADUCreateRewardBasedVideoAd_m3334359302(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		RewardBasedVideoAdClient_set_RewardBasedVideoAdPtr_m3075055733(__this, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = RewardBasedVideoAdClient_get_RewardBasedVideoAdPtr_m2843237604(__this, /*hidden argument*/NULL);
		GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * L_5 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_9();
		G_B1_0 = L_4;
		if (L_5)
		{
			G_B2_0 = L_4;
			goto IL_0040;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_m1731704502_MethodInfo_var);
		GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * L_7 = (GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 *)il2cpp_codegen_object_new(GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859_il2cpp_TypeInfo_var);
		GADURewardBasedVideoAdDidReceiveAdCallback__ctor_m977464374(L_7, NULL, L_6, /*hidden argument*/NULL);
		((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_9(L_7);
		G_B2_0 = G_B1_0;
	}

IL_0040:
	{
		GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * L_8 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_9();
		GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * L_9 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_10();
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
		if (L_9)
		{
			G_B4_0 = L_8;
			G_B4_1 = G_B2_0;
			goto IL_005d;
		}
	}
	{
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m3620490801_MethodInfo_var);
		GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * L_11 = (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 *)il2cpp_codegen_object_new(GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376_il2cpp_TypeInfo_var);
		GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback__ctor_m60345993(L_11, NULL, L_10, /*hidden argument*/NULL);
		((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1_10(L_11);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_005d:
	{
		GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * L_12 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_10();
		GADURewardBasedVideoAdDidOpenCallback_t587935421 * L_13 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_11();
		G_B5_0 = L_12;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
		if (L_13)
		{
			G_B6_0 = L_12;
			G_B6_1 = G_B4_0;
			G_B6_2 = G_B4_1;
			goto IL_007a;
		}
	}
	{
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m1872094434_MethodInfo_var);
		GADURewardBasedVideoAdDidOpenCallback_t587935421 * L_15 = (GADURewardBasedVideoAdDidOpenCallback_t587935421 *)il2cpp_codegen_object_new(GADURewardBasedVideoAdDidOpenCallback_t587935421_il2cpp_TypeInfo_var);
		GADURewardBasedVideoAdDidOpenCallback__ctor_m4117226438(L_15, NULL, L_14, /*hidden argument*/NULL);
		((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache2_11(L_15);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_007a:
	{
		GADURewardBasedVideoAdDidOpenCallback_t587935421 * L_16 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_11();
		GADURewardBasedVideoAdDidStartCallback_t25341677 * L_17 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_12();
		G_B7_0 = L_16;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		G_B7_3 = G_B6_2;
		if (L_17)
		{
			G_B8_0 = L_16;
			G_B8_1 = G_B6_0;
			G_B8_2 = G_B6_1;
			G_B8_3 = G_B6_2;
			goto IL_0097;
		}
	}
	{
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_m724316502_MethodInfo_var);
		GADURewardBasedVideoAdDidStartCallback_t25341677 * L_19 = (GADURewardBasedVideoAdDidStartCallback_t25341677 *)il2cpp_codegen_object_new(GADURewardBasedVideoAdDidStartCallback_t25341677_il2cpp_TypeInfo_var);
		GADURewardBasedVideoAdDidStartCallback__ctor_m572849034(L_19, NULL, L_18, /*hidden argument*/NULL);
		((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache3_12(L_19);
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
		G_B8_2 = G_B7_2;
		G_B8_3 = G_B7_3;
	}

IL_0097:
	{
		GADURewardBasedVideoAdDidStartCallback_t25341677 * L_20 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_12();
		GADURewardBasedVideoAdDidCloseCallback_t2453903099 * L_21 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_13();
		G_B9_0 = L_20;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
		G_B9_4 = G_B8_3;
		if (L_21)
		{
			G_B10_0 = L_20;
			G_B10_1 = G_B8_0;
			G_B10_2 = G_B8_1;
			G_B10_3 = G_B8_2;
			G_B10_4 = G_B8_3;
			goto IL_00b4;
		}
	}
	{
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m3954160878_MethodInfo_var);
		GADURewardBasedVideoAdDidCloseCallback_t2453903099 * L_23 = (GADURewardBasedVideoAdDidCloseCallback_t2453903099 *)il2cpp_codegen_object_new(GADURewardBasedVideoAdDidCloseCallback_t2453903099_il2cpp_TypeInfo_var);
		GADURewardBasedVideoAdDidCloseCallback__ctor_m586528734(L_23, NULL, L_22, /*hidden argument*/NULL);
		((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache4_13(L_23);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
	}

IL_00b4:
	{
		GADURewardBasedVideoAdDidCloseCallback_t2453903099 * L_24 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_13();
		GADURewardBasedVideoAdDidRewardCallback_t129051320 * L_25 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_14();
		G_B11_0 = L_24;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
		G_B11_5 = G_B10_4;
		if (L_25)
		{
			G_B12_0 = L_24;
			G_B12_1 = G_B10_0;
			G_B12_2 = G_B10_1;
			G_B12_3 = G_B10_2;
			G_B12_4 = G_B10_3;
			G_B12_5 = G_B10_4;
			goto IL_00d1;
		}
	}
	{
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_m4093671146_MethodInfo_var);
		GADURewardBasedVideoAdDidRewardCallback_t129051320 * L_27 = (GADURewardBasedVideoAdDidRewardCallback_t129051320 *)il2cpp_codegen_object_new(GADURewardBasedVideoAdDidRewardCallback_t129051320_il2cpp_TypeInfo_var);
		GADURewardBasedVideoAdDidRewardCallback__ctor_m2993363035(L_27, NULL, L_26, /*hidden argument*/NULL);
		((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache5_14(L_27);
		G_B12_0 = G_B11_0;
		G_B12_1 = G_B11_1;
		G_B12_2 = G_B11_2;
		G_B12_3 = G_B11_3;
		G_B12_4 = G_B11_4;
		G_B12_5 = G_B11_5;
	}

IL_00d1:
	{
		GADURewardBasedVideoAdDidRewardCallback_t129051320 * L_28 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_14();
		GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 * L_29 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_15();
		G_B13_0 = L_28;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
		G_B13_3 = G_B12_2;
		G_B13_4 = G_B12_3;
		G_B13_5 = G_B12_4;
		G_B13_6 = G_B12_5;
		if (L_29)
		{
			G_B14_0 = L_28;
			G_B14_1 = G_B12_0;
			G_B14_2 = G_B12_1;
			G_B14_3 = G_B12_2;
			G_B14_4 = G_B12_3;
			G_B14_5 = G_B12_4;
			G_B14_6 = G_B12_5;
			goto IL_00ee;
		}
	}
	{
		IntPtr_t L_30;
		L_30.set_m_value_0((void*)(void*)RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_m1901340942_MethodInfo_var);
		GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 * L_31 = (GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 *)il2cpp_codegen_object_new(GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867_il2cpp_TypeInfo_var);
		GADURewardBasedVideoAdWillLeaveApplicationCallback__ctor_m794305038(L_31, NULL, L_30, /*hidden argument*/NULL);
		((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache6_15(L_31);
		G_B14_0 = G_B13_0;
		G_B14_1 = G_B13_1;
		G_B14_2 = G_B13_2;
		G_B14_3 = G_B13_3;
		G_B14_4 = G_B13_4;
		G_B14_5 = G_B13_5;
		G_B14_6 = G_B13_6;
	}

IL_00ee:
	{
		GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 * L_32 = ((RewardBasedVideoAdClient_t2282664017_StaticFields*)RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_15();
		Externs_GADUSetRewardBasedVideoAdCallbacks_m761410542(NULL /*static, unused*/, G_B14_6, G_B14_5, G_B14_4, G_B14_3, G_B14_2, G_B14_1, G_B14_0, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::LoadAd(GoogleMobileAds.Api.AdRequest,System.String)
extern "C"  void RewardBasedVideoAdClient_LoadAd_m1476455624 (RewardBasedVideoAdClient_t2282664017 * __this, AdRequest_t3179524098 * ___request0, String_t* ___adUnitId1, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		AdRequest_t3179524098 * L_0 = ___request0;
		IntPtr_t L_1 = Utils_BuildAdRequest_m1864044758(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = RewardBasedVideoAdClient_get_RewardBasedVideoAdPtr_m2843237604(__this, /*hidden argument*/NULL);
		IntPtr_t L_3 = V_0;
		String_t* L_4 = ___adUnitId1;
		Externs_GADURequestRewardBasedVideoAd_m2185212556(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		IntPtr_t L_5 = V_0;
		Externs_GADURelease_m239039392(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::ShowRewardBasedVideoAd()
extern "C"  void RewardBasedVideoAdClient_ShowRewardBasedVideoAd_m3339963799 (RewardBasedVideoAdClient_t2282664017 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = RewardBasedVideoAdClient_get_RewardBasedVideoAdPtr_m2843237604(__this, /*hidden argument*/NULL);
		Externs_GADUShowRewardBasedVideoAd_m1811537128(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GoogleMobileAds.iOS.RewardBasedVideoAdClient::IsLoaded()
extern "C"  bool RewardBasedVideoAdClient_IsLoaded_m2826368451 (RewardBasedVideoAdClient_t2282664017 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = RewardBasedVideoAdClient_get_RewardBasedVideoAdPtr_m2843237604(__this, /*hidden argument*/NULL);
		bool L_1 = Externs_GADURewardBasedVideoAdReady_m3999277472(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::DestroyRewardedVideoAd()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_DestroyRewardedVideoAd_m785823586_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_DestroyRewardedVideoAd_m785823586 (RewardBasedVideoAdClient_t2282664017 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_DestroyRewardedVideoAd_m785823586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		RewardBasedVideoAdClient_set_RewardBasedVideoAdPtr_m3075055733(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::Dispose()
extern "C"  void RewardBasedVideoAdClient_Dispose_m4078026337 (RewardBasedVideoAdClient_t2282664017 * __this, const MethodInfo* method)
{
	GCHandle_t3409268066  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RewardBasedVideoAdClient_DestroyRewardedVideoAd_m785823586(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = __this->get_rewardBasedVideoAdClientPtr_1();
		GCHandle_t3409268066  L_1 = GCHandle_op_Explicit_m1207158571(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GCHandle_Free_m1639542352((&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::Finalize()
extern "C"  void RewardBasedVideoAdClient_Finalize_m2957310878 (RewardBasedVideoAdClient_t2282664017 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		RewardBasedVideoAdClient_Dispose_m4078026337(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidReceiveAdCallback(System.IntPtr)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1652260904_MethodInfo_var;
extern const uint32_t RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_m1731704502_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_m1731704502 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_m1731704502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RewardBasedVideoAdClient_t2282664017 * V_0 = NULL;
	{
		IntPtr_t L_0 = ___rewardBasedVideoAdClient0;
		RewardBasedVideoAdClient_t2282664017 * L_1 = RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RewardBasedVideoAdClient_t2282664017 * L_2 = V_0;
		NullCheck(L_2);
		EventHandler_1_t1880931879 * L_3 = L_2->get_OnAdLoaded_2();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		RewardBasedVideoAdClient_t2282664017 * L_4 = V_0;
		NullCheck(L_4);
		EventHandler_1_t1880931879 * L_5 = L_4->get_OnAdLoaded_2();
		RewardBasedVideoAdClient_t2282664017 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs_t3289624707 * L_7 = ((EventArgs_t3289624707_StaticFields*)EventArgs_t3289624707_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_5);
		EventHandler_1_Invoke_m1652260904(L_5, L_6, L_7, /*hidden argument*/EventHandler_1_Invoke_m1652260904_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_m1731704502(intptr_t ___rewardBasedVideoAdClient0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___rewardBasedVideoAdClient0' to managed representation
	IntPtr_t ____rewardBasedVideoAdClient0_unmarshaled;
	____rewardBasedVideoAdClient0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___rewardBasedVideoAdClient0)));

	// Managed method invocation
	::RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_m1731704502(NULL, ____rewardBasedVideoAdClient0_unmarshaled, NULL);

}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback(System.IntPtr,System.String)
extern Il2CppClass* AdFailedToLoadEventArgs_t1756611910_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m728413707_MethodInfo_var;
extern const uint32_t RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m3620490801_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m3620490801 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rewardBasedVideoAdClient0, String_t* ___error1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m3620490801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RewardBasedVideoAdClient_t2282664017 * V_0 = NULL;
	AdFailedToLoadEventArgs_t1756611910 * V_1 = NULL;
	AdFailedToLoadEventArgs_t1756611910 * V_2 = NULL;
	{
		IntPtr_t L_0 = ___rewardBasedVideoAdClient0;
		RewardBasedVideoAdClient_t2282664017 * L_1 = RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RewardBasedVideoAdClient_t2282664017 * L_2 = V_0;
		NullCheck(L_2);
		EventHandler_1_t347919082 * L_3 = L_2->get_OnAdFailedToLoad_3();
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		AdFailedToLoadEventArgs_t1756611910 * L_4 = (AdFailedToLoadEventArgs_t1756611910 *)il2cpp_codegen_object_new(AdFailedToLoadEventArgs_t1756611910_il2cpp_TypeInfo_var);
		AdFailedToLoadEventArgs__ctor_m804439342(L_4, /*hidden argument*/NULL);
		V_2 = L_4;
		AdFailedToLoadEventArgs_t1756611910 * L_5 = V_2;
		String_t* L_6 = ___error1;
		NullCheck(L_5);
		AdFailedToLoadEventArgs_set_Message_m431997948(L_5, L_6, /*hidden argument*/NULL);
		AdFailedToLoadEventArgs_t1756611910 * L_7 = V_2;
		V_1 = L_7;
		RewardBasedVideoAdClient_t2282664017 * L_8 = V_0;
		NullCheck(L_8);
		EventHandler_1_t347919082 * L_9 = L_8->get_OnAdFailedToLoad_3();
		RewardBasedVideoAdClient_t2282664017 * L_10 = V_0;
		AdFailedToLoadEventArgs_t1756611910 * L_11 = V_1;
		NullCheck(L_9);
		EventHandler_1_Invoke_m728413707(L_9, L_10, L_11, /*hidden argument*/EventHandler_1_Invoke_m728413707_MethodInfo_var);
	}

IL_002e:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m3620490801(intptr_t ___rewardBasedVideoAdClient0, char* ___error1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___rewardBasedVideoAdClient0' to managed representation
	IntPtr_t ____rewardBasedVideoAdClient0_unmarshaled;
	____rewardBasedVideoAdClient0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___rewardBasedVideoAdClient0)));

	// Marshaling of parameter '___error1' to managed representation
	String_t* ____error1_unmarshaled = NULL;
	____error1_unmarshaled = il2cpp_codegen_marshal_string_result(___error1);

	// Managed method invocation
	::RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m3620490801(NULL, ____rewardBasedVideoAdClient0_unmarshaled, ____error1_unmarshaled, NULL);

}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidOpenCallback(System.IntPtr)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1652260904_MethodInfo_var;
extern const uint32_t RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m1872094434_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m1872094434 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m1872094434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RewardBasedVideoAdClient_t2282664017 * V_0 = NULL;
	{
		IntPtr_t L_0 = ___rewardBasedVideoAdClient0;
		RewardBasedVideoAdClient_t2282664017 * L_1 = RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RewardBasedVideoAdClient_t2282664017 * L_2 = V_0;
		NullCheck(L_2);
		EventHandler_1_t1880931879 * L_3 = L_2->get_OnAdOpening_4();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		RewardBasedVideoAdClient_t2282664017 * L_4 = V_0;
		NullCheck(L_4);
		EventHandler_1_t1880931879 * L_5 = L_4->get_OnAdOpening_4();
		RewardBasedVideoAdClient_t2282664017 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs_t3289624707 * L_7 = ((EventArgs_t3289624707_StaticFields*)EventArgs_t3289624707_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_5);
		EventHandler_1_Invoke_m1652260904(L_5, L_6, L_7, /*hidden argument*/EventHandler_1_Invoke_m1652260904_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m1872094434(intptr_t ___rewardBasedVideoAdClient0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___rewardBasedVideoAdClient0' to managed representation
	IntPtr_t ____rewardBasedVideoAdClient0_unmarshaled;
	____rewardBasedVideoAdClient0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___rewardBasedVideoAdClient0)));

	// Managed method invocation
	::RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m1872094434(NULL, ____rewardBasedVideoAdClient0_unmarshaled, NULL);

}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidStartCallback(System.IntPtr)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1652260904_MethodInfo_var;
extern const uint32_t RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_m724316502_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_m724316502 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_m724316502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RewardBasedVideoAdClient_t2282664017 * V_0 = NULL;
	{
		IntPtr_t L_0 = ___rewardBasedVideoAdClient0;
		RewardBasedVideoAdClient_t2282664017 * L_1 = RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RewardBasedVideoAdClient_t2282664017 * L_2 = V_0;
		NullCheck(L_2);
		EventHandler_1_t1880931879 * L_3 = L_2->get_OnAdStarted_5();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		RewardBasedVideoAdClient_t2282664017 * L_4 = V_0;
		NullCheck(L_4);
		EventHandler_1_t1880931879 * L_5 = L_4->get_OnAdStarted_5();
		RewardBasedVideoAdClient_t2282664017 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs_t3289624707 * L_7 = ((EventArgs_t3289624707_StaticFields*)EventArgs_t3289624707_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_5);
		EventHandler_1_Invoke_m1652260904(L_5, L_6, L_7, /*hidden argument*/EventHandler_1_Invoke_m1652260904_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_m724316502(intptr_t ___rewardBasedVideoAdClient0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___rewardBasedVideoAdClient0' to managed representation
	IntPtr_t ____rewardBasedVideoAdClient0_unmarshaled;
	____rewardBasedVideoAdClient0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___rewardBasedVideoAdClient0)));

	// Managed method invocation
	::RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_m724316502(NULL, ____rewardBasedVideoAdClient0_unmarshaled, NULL);

}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidCloseCallback(System.IntPtr)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1652260904_MethodInfo_var;
extern const uint32_t RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m3954160878_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m3954160878 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m3954160878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RewardBasedVideoAdClient_t2282664017 * V_0 = NULL;
	{
		IntPtr_t L_0 = ___rewardBasedVideoAdClient0;
		RewardBasedVideoAdClient_t2282664017 * L_1 = RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RewardBasedVideoAdClient_t2282664017 * L_2 = V_0;
		NullCheck(L_2);
		EventHandler_1_t1880931879 * L_3 = L_2->get_OnAdClosed_6();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		RewardBasedVideoAdClient_t2282664017 * L_4 = V_0;
		NullCheck(L_4);
		EventHandler_1_t1880931879 * L_5 = L_4->get_OnAdClosed_6();
		RewardBasedVideoAdClient_t2282664017 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs_t3289624707 * L_7 = ((EventArgs_t3289624707_StaticFields*)EventArgs_t3289624707_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_5);
		EventHandler_1_Invoke_m1652260904(L_5, L_6, L_7, /*hidden argument*/EventHandler_1_Invoke_m1652260904_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m3954160878(intptr_t ___rewardBasedVideoAdClient0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___rewardBasedVideoAdClient0' to managed representation
	IntPtr_t ____rewardBasedVideoAdClient0_unmarshaled;
	____rewardBasedVideoAdClient0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___rewardBasedVideoAdClient0)));

	// Managed method invocation
	::RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m3954160878(NULL, ____rewardBasedVideoAdClient0_unmarshaled, NULL);

}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidRewardUserCallback(System.IntPtr,System.String,System.Double)
extern Il2CppClass* Reward_t1753549929_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1689613570_MethodInfo_var;
extern const uint32_t RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_m4093671146_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_m4093671146 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rewardBasedVideoAdClient0, String_t* ___rewardType1, double ___rewardAmount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_m4093671146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RewardBasedVideoAdClient_t2282664017 * V_0 = NULL;
	Reward_t1753549929 * V_1 = NULL;
	Reward_t1753549929 * V_2 = NULL;
	{
		IntPtr_t L_0 = ___rewardBasedVideoAdClient0;
		RewardBasedVideoAdClient_t2282664017 * L_1 = RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RewardBasedVideoAdClient_t2282664017 * L_2 = V_0;
		NullCheck(L_2);
		EventHandler_1_t344857101 * L_3 = L_2->get_OnAdRewarded_7();
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		Reward_t1753549929 * L_4 = (Reward_t1753549929 *)il2cpp_codegen_object_new(Reward_t1753549929_il2cpp_TypeInfo_var);
		Reward__ctor_m2828810995(L_4, /*hidden argument*/NULL);
		V_2 = L_4;
		Reward_t1753549929 * L_5 = V_2;
		String_t* L_6 = ___rewardType1;
		NullCheck(L_5);
		Reward_set_Type_m3504343776(L_5, L_6, /*hidden argument*/NULL);
		Reward_t1753549929 * L_7 = V_2;
		double L_8 = ___rewardAmount2;
		NullCheck(L_7);
		Reward_set_Amount_m1945389112(L_7, L_8, /*hidden argument*/NULL);
		Reward_t1753549929 * L_9 = V_2;
		V_1 = L_9;
		RewardBasedVideoAdClient_t2282664017 * L_10 = V_0;
		NullCheck(L_10);
		EventHandler_1_t344857101 * L_11 = L_10->get_OnAdRewarded_7();
		RewardBasedVideoAdClient_t2282664017 * L_12 = V_0;
		Reward_t1753549929 * L_13 = V_1;
		NullCheck(L_11);
		EventHandler_1_Invoke_m1689613570(L_11, L_12, L_13, /*hidden argument*/EventHandler_1_Invoke_m1689613570_MethodInfo_var);
	}

IL_0035:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_m4093671146(intptr_t ___rewardBasedVideoAdClient0, char* ___rewardType1, double ___rewardAmount2)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___rewardBasedVideoAdClient0' to managed representation
	IntPtr_t ____rewardBasedVideoAdClient0_unmarshaled;
	____rewardBasedVideoAdClient0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___rewardBasedVideoAdClient0)));

	// Marshaling of parameter '___rewardType1' to managed representation
	String_t* ____rewardType1_unmarshaled = NULL;
	____rewardType1_unmarshaled = il2cpp_codegen_marshal_string_result(___rewardType1);

	// Managed method invocation
	::RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_m4093671146(NULL, ____rewardBasedVideoAdClient0_unmarshaled, ____rewardType1_unmarshaled, ___rewardAmount2, NULL);

}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdWillLeaveApplicationCallback(System.IntPtr)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1652260904_MethodInfo_var;
extern const uint32_t RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_m1901340942_MetadataUsageId;
extern "C"  void RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_m1901340942 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_m1901340942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RewardBasedVideoAdClient_t2282664017 * V_0 = NULL;
	{
		IntPtr_t L_0 = ___rewardBasedVideoAdClient0;
		RewardBasedVideoAdClient_t2282664017 * L_1 = RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RewardBasedVideoAdClient_t2282664017 * L_2 = V_0;
		NullCheck(L_2);
		EventHandler_1_t1880931879 * L_3 = L_2->get_OnAdLeavingApplication_8();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		RewardBasedVideoAdClient_t2282664017 * L_4 = V_0;
		NullCheck(L_4);
		EventHandler_1_t1880931879 * L_5 = L_4->get_OnAdLeavingApplication_8();
		RewardBasedVideoAdClient_t2282664017 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs_t3289624707 * L_7 = ((EventArgs_t3289624707_StaticFields*)EventArgs_t3289624707_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_5);
		EventHandler_1_Invoke_m1652260904(L_5, L_6, L_7, /*hidden argument*/EventHandler_1_Invoke_m1652260904_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_m1901340942(intptr_t ___rewardBasedVideoAdClient0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___rewardBasedVideoAdClient0' to managed representation
	IntPtr_t ____rewardBasedVideoAdClient0_unmarshaled;
	____rewardBasedVideoAdClient0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___rewardBasedVideoAdClient0)));

	// Managed method invocation
	::RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_m1901340942(NULL, ____rewardBasedVideoAdClient0_unmarshaled, NULL);

}
// GoogleMobileAds.iOS.RewardBasedVideoAdClient GoogleMobileAds.iOS.RewardBasedVideoAdClient::IntPtrToRewardBasedVideoClient(System.IntPtr)
extern Il2CppClass* RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var;
extern const uint32_t RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138_MetadataUsageId;
extern "C"  RewardBasedVideoAdClient_t2282664017 * RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_m4215489138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GCHandle_t3409268066  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = ___rewardBasedVideoAdClient0;
		GCHandle_t3409268066  L_1 = GCHandle_op_Explicit_m1207158571(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = GCHandle_get_Target_m2327042781((&V_0), /*hidden argument*/NULL);
		return ((RewardBasedVideoAdClient_t2282664017 *)IsInstClass(L_2, RewardBasedVideoAdClient_t2282664017_il2cpp_TypeInfo_var));
	}
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidCloseCallback__ctor_m586528734 (GADURewardBasedVideoAdDidCloseCallback_t2453903099 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback::Invoke(System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidCloseCallback_Invoke_m1488908142 (GADURewardBasedVideoAdDidCloseCallback_t2453903099 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GADURewardBasedVideoAdDidCloseCallback_Invoke_m1488908142((GADURewardBasedVideoAdDidCloseCallback_t2453903099 *)__this->get_prev_9(),___rewardBasedVideoAdClient0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidCloseCallback_t2453903099 (GADURewardBasedVideoAdDidCloseCallback_t2453903099 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___rewardBasedVideoAdClient0).get_m_value_0()));

}
// System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t GADURewardBasedVideoAdDidCloseCallback_BeginInvoke_m1358395807_MetadataUsageId;
extern "C"  Il2CppObject * GADURewardBasedVideoAdDidCloseCallback_BeginInvoke_m1358395807 (GADURewardBasedVideoAdDidCloseCallback_t2453903099 * __this, IntPtr_t ___rewardBasedVideoAdClient0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GADURewardBasedVideoAdDidCloseCallback_BeginInvoke_m1358395807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___rewardBasedVideoAdClient0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GADURewardBasedVideoAdDidCloseCallback_EndInvoke_m1040237292 (GADURewardBasedVideoAdDidCloseCallback_t2453903099 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback__ctor_m60345993 (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback::Invoke(System.IntPtr,System.String)
extern "C"  void GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_Invoke_m2266136717 (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * __this, IntPtr_t ___rewardBasedVideoClient0, String_t* ___error1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_Invoke_m2266136717((GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 *)__this->get_prev_9(),___rewardBasedVideoClient0, ___error1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___rewardBasedVideoClient0, String_t* ___error1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___rewardBasedVideoClient0, ___error1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___rewardBasedVideoClient0, String_t* ___error1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___rewardBasedVideoClient0, ___error1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * __this, IntPtr_t ___rewardBasedVideoClient0, String_t* ___error1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___error1' to native representation
	char* ____error1_marshaled = NULL;
	____error1_marshaled = il2cpp_codegen_marshal_string(___error1);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___rewardBasedVideoClient0).get_m_value_0()), ____error1_marshaled);

	// Marshaling cleanup of parameter '___error1' native representation
	il2cpp_codegen_marshal_free(____error1_marshaled);
	____error1_marshaled = NULL;

}
// System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_BeginInvoke_m4131598438_MetadataUsageId;
extern "C"  Il2CppObject * GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_BeginInvoke_m4131598438 (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * __this, IntPtr_t ___rewardBasedVideoClient0, String_t* ___error1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_BeginInvoke_m4131598438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___rewardBasedVideoClient0);
	__d_args[1] = ___error1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_EndInvoke_m1623676623 (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidOpenCallback__ctor_m4117226438 (GADURewardBasedVideoAdDidOpenCallback_t587935421 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback::Invoke(System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidOpenCallback_Invoke_m2197471706 (GADURewardBasedVideoAdDidOpenCallback_t587935421 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GADURewardBasedVideoAdDidOpenCallback_Invoke_m2197471706((GADURewardBasedVideoAdDidOpenCallback_t587935421 *)__this->get_prev_9(),___rewardBasedVideoAdClient0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidOpenCallback_t587935421 (GADURewardBasedVideoAdDidOpenCallback_t587935421 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___rewardBasedVideoAdClient0).get_m_value_0()));

}
// System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t GADURewardBasedVideoAdDidOpenCallback_BeginInvoke_m3998677961_MetadataUsageId;
extern "C"  Il2CppObject * GADURewardBasedVideoAdDidOpenCallback_BeginInvoke_m3998677961 (GADURewardBasedVideoAdDidOpenCallback_t587935421 * __this, IntPtr_t ___rewardBasedVideoAdClient0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GADURewardBasedVideoAdDidOpenCallback_BeginInvoke_m3998677961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___rewardBasedVideoAdClient0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GADURewardBasedVideoAdDidOpenCallback_EndInvoke_m3086024828 (GADURewardBasedVideoAdDidOpenCallback_t587935421 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidReceiveAdCallback__ctor_m977464374 (GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback::Invoke(System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidReceiveAdCallback_Invoke_m1945634230 (GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GADURewardBasedVideoAdDidReceiveAdCallback_Invoke_m1945634230((GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 *)__this->get_prev_9(),___rewardBasedVideoAdClient0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 (GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___rewardBasedVideoAdClient0).get_m_value_0()));

}
// System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t GADURewardBasedVideoAdDidReceiveAdCallback_BeginInvoke_m2622081015_MetadataUsageId;
extern "C"  Il2CppObject * GADURewardBasedVideoAdDidReceiveAdCallback_BeginInvoke_m2622081015 (GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * __this, IntPtr_t ___rewardBasedVideoAdClient0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GADURewardBasedVideoAdDidReceiveAdCallback_BeginInvoke_m2622081015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___rewardBasedVideoAdClient0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GADURewardBasedVideoAdDidReceiveAdCallback_EndInvoke_m2659240196 (GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidRewardCallback__ctor_m2993363035 (GADURewardBasedVideoAdDidRewardCallback_t129051320 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback::Invoke(System.IntPtr,System.String,System.Double)
extern "C"  void GADURewardBasedVideoAdDidRewardCallback_Invoke_m1262324133 (GADURewardBasedVideoAdDidRewardCallback_t129051320 * __this, IntPtr_t ___rewardBasedVideoAdClient0, String_t* ___rewardType1, double ___rewardAmount2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GADURewardBasedVideoAdDidRewardCallback_Invoke_m1262324133((GADURewardBasedVideoAdDidRewardCallback_t129051320 *)__this->get_prev_9(),___rewardBasedVideoAdClient0, ___rewardType1, ___rewardAmount2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___rewardBasedVideoAdClient0, String_t* ___rewardType1, double ___rewardAmount2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___rewardBasedVideoAdClient0, ___rewardType1, ___rewardAmount2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___rewardBasedVideoAdClient0, String_t* ___rewardType1, double ___rewardAmount2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___rewardBasedVideoAdClient0, ___rewardType1, ___rewardAmount2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidRewardCallback_t129051320 (GADURewardBasedVideoAdDidRewardCallback_t129051320 * __this, IntPtr_t ___rewardBasedVideoAdClient0, String_t* ___rewardType1, double ___rewardAmount2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, char*, double);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___rewardType1' to native representation
	char* ____rewardType1_marshaled = NULL;
	____rewardType1_marshaled = il2cpp_codegen_marshal_string(___rewardType1);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___rewardBasedVideoAdClient0).get_m_value_0()), ____rewardType1_marshaled, ___rewardAmount2);

	// Marshaling cleanup of parameter '___rewardType1' native representation
	il2cpp_codegen_marshal_free(____rewardType1_marshaled);
	____rewardType1_marshaled = NULL;

}
// System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback::BeginInvoke(System.IntPtr,System.String,System.Double,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t GADURewardBasedVideoAdDidRewardCallback_BeginInvoke_m2287780726_MetadataUsageId;
extern "C"  Il2CppObject * GADURewardBasedVideoAdDidRewardCallback_BeginInvoke_m2287780726 (GADURewardBasedVideoAdDidRewardCallback_t129051320 * __this, IntPtr_t ___rewardBasedVideoAdClient0, String_t* ___rewardType1, double ___rewardAmount2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GADURewardBasedVideoAdDidRewardCallback_BeginInvoke_m2287780726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___rewardBasedVideoAdClient0);
	__d_args[1] = ___rewardType1;
	__d_args[2] = Box(Double_t4078015681_il2cpp_TypeInfo_var, &___rewardAmount2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GADURewardBasedVideoAdDidRewardCallback_EndInvoke_m2877166625 (GADURewardBasedVideoAdDidRewardCallback_t129051320 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidStartCallback__ctor_m572849034 (GADURewardBasedVideoAdDidStartCallback_t25341677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback::Invoke(System.IntPtr)
extern "C"  void GADURewardBasedVideoAdDidStartCallback_Invoke_m3538955206 (GADURewardBasedVideoAdDidStartCallback_t25341677 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GADURewardBasedVideoAdDidStartCallback_Invoke_m3538955206((GADURewardBasedVideoAdDidStartCallback_t25341677 *)__this->get_prev_9(),___rewardBasedVideoAdClient0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidStartCallback_t25341677 (GADURewardBasedVideoAdDidStartCallback_t25341677 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___rewardBasedVideoAdClient0).get_m_value_0()));

}
// System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t GADURewardBasedVideoAdDidStartCallback_BeginInvoke_m1535589781_MetadataUsageId;
extern "C"  Il2CppObject * GADURewardBasedVideoAdDidStartCallback_BeginInvoke_m1535589781 (GADURewardBasedVideoAdDidStartCallback_t25341677 * __this, IntPtr_t ___rewardBasedVideoAdClient0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GADURewardBasedVideoAdDidStartCallback_BeginInvoke_m1535589781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___rewardBasedVideoAdClient0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GADURewardBasedVideoAdDidStartCallback_EndInvoke_m3825854400 (GADURewardBasedVideoAdDidStartCallback_t25341677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GADURewardBasedVideoAdWillLeaveApplicationCallback__ctor_m794305038 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback::Invoke(System.IntPtr)
extern "C"  void GADURewardBasedVideoAdWillLeaveApplicationCallback_Invoke_m3929478590 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GADURewardBasedVideoAdWillLeaveApplicationCallback_Invoke_m3929478590((GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 *)__this->get_prev_9(),___rewardBasedVideoAdClient0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___rewardBasedVideoAdClient0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 * __this, IntPtr_t ___rewardBasedVideoAdClient0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___rewardBasedVideoAdClient0).get_m_value_0()));

}
// System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t GADURewardBasedVideoAdWillLeaveApplicationCallback_BeginInvoke_m1864660607_MetadataUsageId;
extern "C"  Il2CppObject * GADURewardBasedVideoAdWillLeaveApplicationCallback_BeginInvoke_m1864660607 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 * __this, IntPtr_t ___rewardBasedVideoAdClient0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GADURewardBasedVideoAdWillLeaveApplicationCallback_BeginInvoke_m1864660607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___rewardBasedVideoAdClient0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GADURewardBasedVideoAdWillLeaveApplicationCallback_EndInvoke_m121451212 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GoogleMobileAds.iOS.Utils::.ctor()
extern "C"  void Utils__ctor_m3454559032 (Utils_t984021165 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr GoogleMobileAds.iOS.Utils::BuildAdRequest(GoogleMobileAds.Api.AdRequest)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_GetEnumerator_m1136260840_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2543007644_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1968630970_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3599713869_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m804483696_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m870713862_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4175023932_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2205157096_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m3795771450_MethodInfo_var;
extern const MethodInfo* Nullable_1_GetValueOrDefault_m706662709_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2852364250_MethodInfo_var;
extern const MethodInfo* Nullable_1_GetValueOrDefault_m278040267_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m1733730025_MethodInfo_var;
extern const MethodInfo* Nullable_1_GetValueOrDefault_m2607283502_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m195867968_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m893463330_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m192190662_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1007348211_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m391049161_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m882561911_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3808422820_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4132625270_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3623238892_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2627989796_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1200626320;
extern const uint32_t Utils_BuildAdRequest_m1864044758_MetadataUsageId;
extern "C"  IntPtr_t Utils_BuildAdRequest_m1864044758 (Il2CppObject * __this /* static, unused */, AdRequest_t3179524098 * ___request0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_BuildAdRequest_m1864044758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	String_t* V_1 = NULL;
	Enumerator_t3145964225  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Enumerator_t933071039  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Nullable_1_t3251239280  V_5;
	memset(&V_5, 0, sizeof(V_5));
	DateTime_t693205669  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Nullable_1_t3251239280  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Nullable_1_t1791139578  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Nullable_1_t1791139578  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Nullable_1_t2088641033  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Nullable_1_t2088641033  V_11;
	memset(&V_11, 0, sizeof(V_11));
	KeyValuePair_2_t1701344717  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Enumerator_t969056901  V_13;
	memset(&V_13, 0, sizeof(V_13));
	MediationExtras_t1641207307 * V_14 = NULL;
	Enumerator_t545058113  V_15;
	memset(&V_15, 0, sizeof(V_15));
	IntPtr_t V_16;
	memset(&V_16, 0, sizeof(V_16));
	KeyValuePair_2_t1701344717  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Enumerator_t969056901  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0 = Externs_GADUCreateRequest_m186728553(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		AdRequest_t3179524098 * L_1 = ___request0;
		NullCheck(L_1);
		HashSet_1_t362681087 * L_2 = AdRequest_get_Keywords_m1030121356(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Enumerator_t3145964225  L_3 = HashSet_1_GetEnumerator_m1136260840(L_2, /*hidden argument*/HashSet_1_GetEnumerator_m1136260840_MethodInfo_var);
		V_2 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0017:
		{
			String_t* L_4 = Enumerator_get_Current_m2543007644((&V_2), /*hidden argument*/Enumerator_get_Current_m2543007644_MethodInfo_var);
			V_1 = L_4;
			IntPtr_t L_5 = V_0;
			String_t* L_6 = V_1;
			Externs_GADUAddKeyword_m2695335653(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		}

IL_0026:
		{
			bool L_7 = Enumerator_MoveNext_m1968630970((&V_2), /*hidden argument*/Enumerator_MoveNext_m1968630970_MethodInfo_var);
			if (L_7)
			{
				goto IL_0017;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x45, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3599713869((&V_2), /*hidden argument*/Enumerator_Dispose_m3599713869_MethodInfo_var);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0045:
	{
		AdRequest_t3179524098 * L_8 = ___request0;
		NullCheck(L_8);
		List_1_t1398341365 * L_9 = AdRequest_get_TestDevices_m908117651(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Enumerator_t933071039  L_10 = List_1_GetEnumerator_m804483696(L_9, /*hidden argument*/List_1_GetEnumerator_m804483696_MethodInfo_var);
		V_4 = L_10;
	}

IL_0052:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0066;
		}

IL_0057:
		{
			String_t* L_11 = Enumerator_get_Current_m870713862((&V_4), /*hidden argument*/Enumerator_get_Current_m870713862_MethodInfo_var);
			V_3 = L_11;
			IntPtr_t L_12 = V_0;
			String_t* L_13 = V_3;
			Externs_GADUAddTestDevice_m4092615724(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		}

IL_0066:
		{
			bool L_14 = Enumerator_MoveNext_m4175023932((&V_4), /*hidden argument*/Enumerator_MoveNext_m4175023932_MethodInfo_var);
			if (L_14)
			{
				goto IL_0057;
			}
		}

IL_0072:
		{
			IL2CPP_LEAVE(0x85, FINALLY_0077);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0077;
	}

FINALLY_0077:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2205157096((&V_4), /*hidden argument*/Enumerator_Dispose_m2205157096_MethodInfo_var);
		IL2CPP_END_FINALLY(119)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(119)
	{
		IL2CPP_JUMP_TBL(0x85, IL_0085)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0085:
	{
		AdRequest_t3179524098 * L_15 = ___request0;
		NullCheck(L_15);
		Nullable_1_t3251239280  L_16 = AdRequest_get_Birthday_m3522519806(L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		bool L_17 = Nullable_1_get_HasValue_m3795771450((&V_5), /*hidden argument*/Nullable_1_get_HasValue_m3795771450_MethodInfo_var);
		if (!L_17)
		{
			goto IL_00c5;
		}
	}
	{
		AdRequest_t3179524098 * L_18 = ___request0;
		NullCheck(L_18);
		Nullable_1_t3251239280  L_19 = AdRequest_get_Birthday_m3522519806(L_18, /*hidden argument*/NULL);
		V_7 = L_19;
		DateTime_t693205669  L_20 = Nullable_1_GetValueOrDefault_m706662709((&V_7), /*hidden argument*/Nullable_1_GetValueOrDefault_m706662709_MethodInfo_var);
		V_6 = L_20;
		IntPtr_t L_21 = V_0;
		int32_t L_22 = DateTime_get_Year_m1985210972((&V_6), /*hidden argument*/NULL);
		int32_t L_23 = DateTime_get_Month_m1464831817((&V_6), /*hidden argument*/NULL);
		int32_t L_24 = DateTime_get_Day_m2066530041((&V_6), /*hidden argument*/NULL);
		Externs_GADUSetBirthday_m1736627929(NULL /*static, unused*/, L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		AdRequest_t3179524098 * L_25 = ___request0;
		NullCheck(L_25);
		Nullable_1_t1791139578  L_26 = AdRequest_get_Gender_m3375617580(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		bool L_27 = Nullable_1_get_HasValue_m2852364250((&V_8), /*hidden argument*/Nullable_1_get_HasValue_m2852364250_MethodInfo_var);
		if (!L_27)
		{
			goto IL_00ee;
		}
	}
	{
		IntPtr_t L_28 = V_0;
		AdRequest_t3179524098 * L_29 = ___request0;
		NullCheck(L_29);
		Nullable_1_t1791139578  L_30 = AdRequest_get_Gender_m3375617580(L_29, /*hidden argument*/NULL);
		V_9 = L_30;
		int32_t L_31 = Nullable_1_GetValueOrDefault_m278040267((&V_9), /*hidden argument*/Nullable_1_GetValueOrDefault_m278040267_MethodInfo_var);
		Externs_GADUSetGender_m1291605563(NULL /*static, unused*/, L_28, L_31, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		AdRequest_t3179524098 * L_32 = ___request0;
		NullCheck(L_32);
		Nullable_1_t2088641033  L_33 = AdRequest_get_TagForChildDirectedTreatment_m1568472663(L_32, /*hidden argument*/NULL);
		V_10 = L_33;
		bool L_34 = Nullable_1_get_HasValue_m1733730025((&V_10), /*hidden argument*/Nullable_1_get_HasValue_m1733730025_MethodInfo_var);
		if (!L_34)
		{
			goto IL_0117;
		}
	}
	{
		IntPtr_t L_35 = V_0;
		AdRequest_t3179524098 * L_36 = ___request0;
		NullCheck(L_36);
		Nullable_1_t2088641033  L_37 = AdRequest_get_TagForChildDirectedTreatment_m1568472663(L_36, /*hidden argument*/NULL);
		V_11 = L_37;
		bool L_38 = Nullable_1_GetValueOrDefault_m2607283502((&V_11), /*hidden argument*/Nullable_1_GetValueOrDefault_m2607283502_MethodInfo_var);
		Externs_GADUTagForChildDirectedTreatment_m781435999(NULL /*static, unused*/, L_35, L_38, /*hidden argument*/NULL);
	}

IL_0117:
	{
		AdRequest_t3179524098 * L_39 = ___request0;
		NullCheck(L_39);
		Dictionary_2_t3943999495 * L_40 = AdRequest_get_Extras_m472816900(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		Enumerator_t969056901  L_41 = Dictionary_2_GetEnumerator_m195867968(L_40, /*hidden argument*/Dictionary_2_GetEnumerator_m195867968_MethodInfo_var);
		V_13 = L_41;
	}

IL_0124:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0146;
		}

IL_0129:
		{
			KeyValuePair_2_t1701344717  L_42 = Enumerator_get_Current_m893463330((&V_13), /*hidden argument*/Enumerator_get_Current_m893463330_MethodInfo_var);
			V_12 = L_42;
			IntPtr_t L_43 = V_0;
			String_t* L_44 = KeyValuePair_2_get_Key_m192190662((&V_12), /*hidden argument*/KeyValuePair_2_get_Key_m192190662_MethodInfo_var);
			String_t* L_45 = KeyValuePair_2_get_Value_m1007348211((&V_12), /*hidden argument*/KeyValuePair_2_get_Value_m1007348211_MethodInfo_var);
			Externs_GADUSetExtra_m1949804207(NULL /*static, unused*/, L_43, L_44, L_45, /*hidden argument*/NULL);
		}

IL_0146:
		{
			bool L_46 = Enumerator_MoveNext_m391049161((&V_13), /*hidden argument*/Enumerator_MoveNext_m391049161_MethodInfo_var);
			if (L_46)
			{
				goto IL_0129;
			}
		}

IL_0152:
		{
			IL2CPP_LEAVE(0x165, FINALLY_0157);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0157;
	}

FINALLY_0157:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m882561911((&V_13), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
		IL2CPP_END_FINALLY(343)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(343)
	{
		IL2CPP_JUMP_TBL(0x165, IL_0165)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0165:
	{
		AdRequest_t3179524098 * L_47 = ___request0;
		NullCheck(L_47);
		List_1_t1010328439 * L_48 = AdRequest_get_MediationExtras_m4190608797(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Enumerator_t545058113  L_49 = List_1_GetEnumerator_m3808422820(L_48, /*hidden argument*/List_1_GetEnumerator_m3808422820_MethodInfo_var);
		V_15 = L_49;
	}

IL_0172:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01f7;
		}

IL_0177:
		{
			MediationExtras_t1641207307 * L_50 = Enumerator_get_Current_m4132625270((&V_15), /*hidden argument*/Enumerator_get_Current_m4132625270_MethodInfo_var);
			V_14 = L_50;
			IntPtr_t L_51 = Externs_GADUCreateMutableDictionary_m3061167884(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_16 = L_51;
			IntPtr_t L_52 = V_16;
			IntPtr_t L_53 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_54 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
			if (!L_54)
			{
				goto IL_01f7;
			}
		}

IL_0198:
		{
			MediationExtras_t1641207307 * L_55 = V_14;
			NullCheck(L_55);
			Dictionary_2_t3943999495 * L_56 = MediationExtras_get_Extras_m3567672655(L_55, /*hidden argument*/NULL);
			NullCheck(L_56);
			Enumerator_t969056901  L_57 = Dictionary_2_GetEnumerator_m195867968(L_56, /*hidden argument*/Dictionary_2_GetEnumerator_m195867968_MethodInfo_var);
			V_18 = L_57;
		}

IL_01a6:
		try
		{ // begin try (depth: 2)
			{
				goto IL_01c9;
			}

IL_01ab:
			{
				KeyValuePair_2_t1701344717  L_58 = Enumerator_get_Current_m893463330((&V_18), /*hidden argument*/Enumerator_get_Current_m893463330_MethodInfo_var);
				V_17 = L_58;
				IntPtr_t L_59 = V_16;
				String_t* L_60 = KeyValuePair_2_get_Key_m192190662((&V_17), /*hidden argument*/KeyValuePair_2_get_Key_m192190662_MethodInfo_var);
				String_t* L_61 = KeyValuePair_2_get_Value_m1007348211((&V_17), /*hidden argument*/KeyValuePair_2_get_Value_m1007348211_MethodInfo_var);
				Externs_GADUMutableDictionarySetValue_m84284538(NULL /*static, unused*/, L_59, L_60, L_61, /*hidden argument*/NULL);
			}

IL_01c9:
			{
				bool L_62 = Enumerator_MoveNext_m391049161((&V_18), /*hidden argument*/Enumerator_MoveNext_m391049161_MethodInfo_var);
				if (L_62)
				{
					goto IL_01ab;
				}
			}

IL_01d5:
			{
				IL2CPP_LEAVE(0x1E8, FINALLY_01da);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_01da;
		}

FINALLY_01da:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m882561911((&V_18), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
			IL2CPP_END_FINALLY(474)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(474)
		{
			IL2CPP_JUMP_TBL(0x1E8, IL_01e8)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_01e8:
		{
			IntPtr_t L_63 = V_0;
			IntPtr_t L_64 = V_16;
			MediationExtras_t1641207307 * L_65 = V_14;
			NullCheck(L_65);
			String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String GoogleMobileAds.Api.Mediation.MediationExtras::get_IOSMediationExtraBuilderClassName() */, L_65);
			Externs_GADUSetMediationExtras_m1740710460(NULL /*static, unused*/, L_63, L_64, L_66, /*hidden argument*/NULL);
		}

IL_01f7:
		{
			bool L_67 = Enumerator_MoveNext_m3623238892((&V_15), /*hidden argument*/Enumerator_MoveNext_m3623238892_MethodInfo_var);
			if (L_67)
			{
				goto IL_0177;
			}
		}

IL_0203:
		{
			IL2CPP_LEAVE(0x216, FINALLY_0208);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0208;
	}

FINALLY_0208:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2627989796((&V_15), /*hidden argument*/Enumerator_Dispose_m2627989796_MethodInfo_var);
		IL2CPP_END_FINALLY(520)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(520)
	{
		IL2CPP_JUMP_TBL(0x216, IL_0216)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0216:
	{
		IntPtr_t L_68 = V_0;
		Externs_GADUSetRequestAgent_m1142145835(NULL /*static, unused*/, L_68, _stringLiteral1200626320, /*hidden argument*/NULL);
		IntPtr_t L_69 = V_0;
		return L_69;
	}
}
// System.Void HandLegAnim::.ctor()
extern "C"  void HandLegAnim__ctor_m2582818721 (HandLegAnim_t1741127542 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HandLegAnim::Start()
extern "C"  void HandLegAnim_Start_m680137041 (HandLegAnim_t1741127542 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HandLegAnim::Update()
extern "C"  void HandLegAnim_Update_m2025511988 (HandLegAnim_t1741127542 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::.ctor()
extern Il2CppCodeGenString* _stringLiteral867000253;
extern Il2CppCodeGenString* _stringLiteral593495257;
extern const uint32_t KittyHandNFootManager__ctor_m1931548674_MetadataUsageId;
extern "C"  void KittyHandNFootManager__ctor_m1931548674 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager__ctor_m1931548674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_LeftHandCollide_53((bool)1);
		__this->set_RightHandCollide_54((bool)1);
		__this->set_LeftLegCollide_55((bool)1);
		__this->set_RightLegCollide_56((bool)1);
		__this->set_Talk1_58(_stringLiteral867000253);
		__this->set_Talk2_59(_stringLiteral593495257);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::Start()
extern Il2CppClass* U3CStartU3Ec__Iterator0_t3126961537_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_Start_m961038030_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_Start_m961038030 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_Start_m961038030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartU3Ec__Iterator0_t3126961537 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t3126961537 * L_0 = (U3CStartU3Ec__Iterator0_t3126961537 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t3126961537_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m976319080(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t3126961537 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CStartU3Ec__Iterator0_t3126961537 * L_2 = V_0;
		return L_2;
	}
}
// System.Void KittyHandNFootManager::Update()
extern "C"  void KittyHandNFootManager_Update_m1947480053 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_ManipulateRightHand1stNail_67();
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_NailCutter_13();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		Vector2U5BU5D_t686124026* L_3 = __this->get_CutterRightHandPos_20();
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localPosition_m1026930133(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::InitiateTalk()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisBubbleWaterTalkMoving_t3963847219_m3008669749_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var;
extern const uint32_t KittyHandNFootManager_InitiateTalk_m3159398165_MetadataUsageId;
extern "C"  void KittyHandNFootManager_InitiateTalk_m3159398165 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_InitiateTalk_m3159398165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		GameObject_t1756533147 * L_0 = __this->get_Talk_2();
		Transform_t3275118058 * L_1 = __this->get_Talk_Position_3();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_AddComponent_TisBubbleWaterTalkMoving_t3963847219_m3008669749(L_5, /*hidden argument*/GameObject_AddComponent_TisBubbleWaterTalkMoving_t3963847219_m3008669749_MethodInfo_var);
		ColorU5BU5D_t672350442* L_6 = __this->get_Colors_4();
		NullCheck(L_6);
		int32_t L_7 = Random_Range_m694320887(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))), /*hidden argument*/NULL);
		V_1 = L_7;
		GameObject_t1756533147 * L_8 = V_0;
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Transform_GetChild_m3838588184(L_9, 0, /*hidden argument*/NULL);
		NullCheck(L_10);
		TextMesh_t1641806576 * L_11 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_10, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		ColorU5BU5D_t672350442* L_12 = __this->get_Colors_4();
		int32_t L_13 = V_1;
		NullCheck(L_12);
		NullCheck(L_11);
		TextMesh_set_color_m4241516737(L_11, (*(Color_t2020392075 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = V_0;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = Transform_GetChild_m3838588184(L_15, 1, /*hidden argument*/NULL);
		NullCheck(L_16);
		TextMesh_t1641806576 * L_17 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_16, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		ColorU5BU5D_t672350442* L_18 = __this->get_Colors_4();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		NullCheck(L_17);
		TextMesh_set_color_m4241516737(L_17, (*(Color_t2020392075 *)((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19)))), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = V_0;
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = Transform_GetChild_m3838588184(L_21, 0, /*hidden argument*/NULL);
		NullCheck(L_22);
		TextMesh_t1641806576 * L_23 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_22, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		String_t* L_24 = __this->get_Talk1_58();
		NullCheck(L_23);
		TextMesh_set_text_m3390063817(L_23, L_24, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_25 = V_0;
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = GameObject_get_transform_m909382139(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = Transform_GetChild_m3838588184(L_26, 1, /*hidden argument*/NULL);
		NullCheck(L_27);
		TextMesh_t1641806576 * L_28 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_27, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		String_t* L_29 = __this->get_Talk2_59();
		NullCheck(L_28);
		TextMesh_set_text_m3390063817(L_28, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::ColorNails()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var;
extern const uint32_t KittyHandNFootManager_ColorNails_m1870864844_MetadataUsageId;
extern "C"  void KittyHandNFootManager_ColorNails_m1870864844 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_ColorNails_m1870864844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		V_0 = 0;
		goto IL_002a;
	}

IL_0009:
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_NailPolishObjects_27();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1756533147 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		GameObject_t1756533147 * L_4 = __this->get_NailPolishType_51();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0026;
		}
	}
	{
		goto IL_0038;
	}

IL_0026:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_7 = V_0;
		GameObjectU5BU5D_t3057952154* L_8 = __this->get_NailPolishObjects_27();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0009;
		}
	}

IL_0038:
	{
		GameObject_t1756533147 * L_9 = __this->get_CurrentHand_60();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Transform_GetChild_m3838588184(L_10, 0, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Transform_GetChild_m3838588184(L_12, 0, /*hidden argument*/NULL);
		NullCheck(L_13);
		SpriteRenderer_t1209076198 * L_14 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_13, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		SpriteU5BU5D_t3359083662* L_15 = __this->get_WhiteNails_30();
		NullCheck(L_15);
		int32_t L_16 = 0;
		Sprite_t309593783 * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_14);
		SpriteRenderer_set_sprite_m617298623(L_14, L_17, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = __this->get_CurrentHand_60();
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = GameObject_get_transform_m909382139(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_t3275118058 * L_20 = Transform_GetChild_m3838588184(L_19, 0, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = Transform_GetChild_m3838588184(L_21, 1, /*hidden argument*/NULL);
		NullCheck(L_22);
		SpriteRenderer_t1209076198 * L_23 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_22, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		SpriteU5BU5D_t3359083662* L_24 = __this->get_WhiteNails_30();
		NullCheck(L_24);
		int32_t L_25 = 1;
		Sprite_t309593783 * L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_23);
		SpriteRenderer_set_sprite_m617298623(L_23, L_26, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = __this->get_CurrentHand_60();
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = GameObject_get_transform_m909382139(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = Transform_GetChild_m3838588184(L_28, 0, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = Transform_GetChild_m3838588184(L_30, 2, /*hidden argument*/NULL);
		NullCheck(L_31);
		SpriteRenderer_t1209076198 * L_32 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_31, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		SpriteU5BU5D_t3359083662* L_33 = __this->get_WhiteNails_30();
		NullCheck(L_33);
		int32_t L_34 = 2;
		Sprite_t309593783 * L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		NullCheck(L_32);
		SpriteRenderer_set_sprite_m617298623(L_32, L_35, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = __this->get_CurrentHand_60();
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = GameObject_get_transform_m909382139(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_t3275118058 * L_38 = Transform_GetChild_m3838588184(L_37, 0, /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_t3275118058 * L_39 = Component_get_transform_m2697483695(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_t3275118058 * L_40 = Transform_GetChild_m3838588184(L_39, 0, /*hidden argument*/NULL);
		NullCheck(L_40);
		SpriteRenderer_t1209076198 * L_41 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_40, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		ColorU5BU5D_t672350442* L_42 = __this->get_NailPolishColors_28();
		int32_t L_43 = V_0;
		NullCheck(L_42);
		NullCheck(L_41);
		SpriteRenderer_set_color_m2339931967(L_41, (*(Color_t2020392075 *)((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43)))), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_44 = __this->get_CurrentHand_60();
		NullCheck(L_44);
		Transform_t3275118058 * L_45 = GameObject_get_transform_m909382139(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_t3275118058 * L_46 = Transform_GetChild_m3838588184(L_45, 0, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_t3275118058 * L_47 = Component_get_transform_m2697483695(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_t3275118058 * L_48 = Transform_GetChild_m3838588184(L_47, 1, /*hidden argument*/NULL);
		NullCheck(L_48);
		SpriteRenderer_t1209076198 * L_49 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_48, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		ColorU5BU5D_t672350442* L_50 = __this->get_NailPolishColors_28();
		int32_t L_51 = V_0;
		NullCheck(L_50);
		NullCheck(L_49);
		SpriteRenderer_set_color_m2339931967(L_49, (*(Color_t2020392075 *)((L_50)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_51)))), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_52 = __this->get_CurrentHand_60();
		NullCheck(L_52);
		Transform_t3275118058 * L_53 = GameObject_get_transform_m909382139(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		Transform_t3275118058 * L_54 = Transform_GetChild_m3838588184(L_53, 0, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_t3275118058 * L_55 = Component_get_transform_m2697483695(L_54, /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_t3275118058 * L_56 = Transform_GetChild_m3838588184(L_55, 2, /*hidden argument*/NULL);
		NullCheck(L_56);
		SpriteRenderer_t1209076198 * L_57 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_56, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		ColorU5BU5D_t672350442* L_58 = __this->get_NailPolishColors_28();
		int32_t L_59 = V_0;
		NullCheck(L_58);
		NullCheck(L_57);
		SpriteRenderer_set_color_m2339931967(L_57, (*(Color_t2020392075 *)((L_58)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_59)))), /*hidden argument*/NULL);
		Il2CppObject * L_60 = KittyHandNFootManager_PrepareForNextNailCut_m3947584807(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_60, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::MakeNailPolishIconReady()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2189039969;
extern Il2CppCodeGenString* _stringLiteral593495257;
extern Il2CppCodeGenString* _stringLiteral2923566773;
extern Il2CppCodeGenString* _stringLiteral2778558984;
extern const uint32_t KittyHandNFootManager_MakeNailPolishIconReady_m2897165473_MetadataUsageId;
extern "C"  void KittyHandNFootManager_MakeNailPolishIconReady_m2897165473 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_MakeNailPolishIconReady_m2897165473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3057952154* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObjectU5BU5D_t3057952154* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SoundManager_t654432262 * L_0 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_0);
		L_0->set_CanPlayMeow_16((bool)0);
		SoundManager_t654432262 * L_1 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = L_1->get_OpenMouth_2();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_NailPolishIcon_14();
		NullCheck(L_3);
		SpriteRenderer_t1209076198 * L_4 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_3, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m1909920690(&L_5, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		SpriteRenderer_set_color_m2339931967(L_4, L_5, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_NailPolishIcon_14();
		NullCheck(L_6);
		BoxCollider2D_t948534547 * L_7 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_6, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_7);
		Behaviour_set_enabled_m1796096907(L_7, (bool)1, /*hidden argument*/NULL);
		__this->set_Talk1_58(_stringLiteral2189039969);
		__this->set_Talk2_59(_stringLiteral593495257);
		MonoBehaviour_CancelInvoke_m2508161963(__this, _stringLiteral2923566773, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_8 = GameObject_FindGameObjectsWithTag_m2154478296(NULL /*static, unused*/, _stringLiteral2778558984, /*hidden argument*/NULL);
		V_0 = L_8;
		GameObjectU5BU5D_t3057952154* L_9 = V_0;
		V_2 = L_9;
		V_3 = 0;
		goto IL_009d;
	}

IL_008a:
	{
		GameObjectU5BU5D_t3057952154* L_10 = V_2;
		int32_t L_11 = V_3;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		GameObject_t1756533147 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_1 = L_13;
		GameObject_t1756533147 * L_14 = V_1;
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = GameObject_get_gameObject_m3662236595(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_009d:
	{
		int32_t L_17 = V_3;
		GameObjectU5BU5D_t3057952154* L_18 = V_2;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_008a;
		}
	}
	{
		MonoBehaviour_InvokeRepeating_m3468262484(__this, _stringLiteral2923566773, (0.2f), (5.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::MakeFacePowderOn()
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2578760104;
extern Il2CppCodeGenString* _stringLiteral2328219685;
extern Il2CppCodeGenString* _stringLiteral791085916;
extern const uint32_t KittyHandNFootManager_MakeFacePowderOn_m2107715037_MetadataUsageId;
extern "C"  void KittyHandNFootManager_MakeFacePowderOn_m2107715037 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_MakeFacePowderOn_m2107715037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObjectU5BU5D_t3057952154* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameObject_t1756533147 * L_0 = __this->get_FacePowderMirror_15();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Transform_GetChild_m3838588184(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_FacePowderMirror_15();
		NullCheck(L_4);
		SpriteRenderer_t1209076198 * L_5 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_4, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m1909920690(&L_6, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		SpriteRenderer_set_color_m2339931967(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_FacePowderBrush_16();
		NullCheck(L_7);
		SpriteRenderer_t1209076198 * L_8 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_7, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		SpriteRenderer_set_color_m2339931967(L_8, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = __this->get_HandIconTap_12();
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)0, /*hidden argument*/NULL);
		__this->set_Talk1_58(_stringLiteral2578760104);
		__this->set_Talk2_59(_stringLiteral2328219685);
		GameObject_t1756533147 * L_11 = __this->get_LeftHandCollider_7();
		NullCheck(L_11);
		BoxCollider2D_t948534547 * L_12 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_11, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_12);
		Behaviour_set_enabled_m1796096907(L_12, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = __this->get_RightHandCollider_8();
		NullCheck(L_13);
		BoxCollider2D_t948534547 * L_14 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_13, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_14);
		Behaviour_set_enabled_m1796096907(L_14, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = __this->get_LeftLegCollider_5();
		NullCheck(L_15);
		BoxCollider2D_t948534547 * L_16 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_15, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_16);
		Behaviour_set_enabled_m1796096907(L_16, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_17 = __this->get_RightLegCollider_6();
		NullCheck(L_17);
		BoxCollider2D_t948534547 * L_18 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_17, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_18);
		Behaviour_set_enabled_m1796096907(L_18, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = __this->get_HandIcon_11();
		NullCheck(L_19);
		Animator_t69676727 * L_20 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_19, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_20);
		Animator_SetBool_m2305662531(L_20, _stringLiteral791085916, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_21 = __this->get_HandIcon_11();
		NullCheck(L_21);
		Animator_t69676727 * L_22 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_21, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_22);
		Behaviour_set_enabled_m1796096907(L_22, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = __this->get_FacePowderBrush_16();
		NullCheck(L_23);
		BoxCollider2D_t948534547 * L_24 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_23, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_24);
		Behaviour_set_enabled_m1796096907(L_24, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_25 = __this->get_FaceGlow_31();
		V_1 = L_25;
		V_2 = 0;
		goto IL_012e;
	}

IL_011a:
	{
		GameObjectU5BU5D_t3057952154* L_26 = V_1;
		int32_t L_27 = V_2;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		GameObject_t1756533147 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_0 = L_29;
		GameObject_t1756533147 * L_30 = V_0;
		NullCheck(L_30);
		BoxCollider2D_t948534547 * L_31 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_30, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_31);
		Behaviour_set_enabled_m1796096907(L_31, (bool)1, /*hidden argument*/NULL);
		int32_t L_32 = V_2;
		V_2 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_012e:
	{
		int32_t L_33 = V_2;
		GameObjectU5BU5D_t3057952154* L_34 = V_1;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length)))))))
		{
			goto IL_011a;
		}
	}
	{
		return;
	}
}
// System.Void KittyHandNFootManager::MakePerfumeOpen()
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const uint32_t KittyHandNFootManager_MakePerfumeOpen_m347974372_MetadataUsageId;
extern "C"  void KittyHandNFootManager_MakePerfumeOpen_m347974372 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_MakePerfumeOpen_m347974372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_Perfume_17();
		NullCheck(L_0);
		BoxCollider2D_t948534547 * L_1 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_0, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_1);
		Behaviour_set_enabled_m1796096907(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_Perfume_17();
		NullCheck(L_2);
		SpriteRenderer_t1209076198 * L_3 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_2, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m1909920690(&L_4, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		SpriteRenderer_set_color_m2339931967(L_3, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_LocketStar_35();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::SprayPerfume()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t KittyHandNFootManager_SprayPerfume_m3568924841_MetadataUsageId;
extern "C"  void KittyHandNFootManager_SprayPerfume_m3568924841 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_SprayPerfume_m3568924841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_PerfumeSpray_33();
		GameObject_t1756533147 * L_1 = __this->get_Perfume_17();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_GetChild_m3838588184(L_2, 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_6 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_7 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_0, L_5, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_7;
		GameObject_t1756533147 * L_8 = V_0;
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = __this->get_Perfume_17();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Transform_GetChild_m3838588184(L_11, 0, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_localEulerAngles_m4231787854(L_13, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localEulerAngles_m2927195985(L_9, L_14, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_15 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_15);
		SoundManager_PlayPerfumeSound_m2825984396(L_15, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = V_0;
		Il2CppObject * L_17 = KittyHandNFootManager_DeleteSpray_m1861600344(__this, L_16, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::LeftHandMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2357684076;
extern Il2CppCodeGenString* _stringLiteral593495257;
extern Il2CppCodeGenString* _stringLiteral2778558984;
extern Il2CppCodeGenString* _stringLiteral3012219828;
extern const uint32_t KittyHandNFootManager_LeftHandMouseDown_m1853715275_MetadataUsageId;
extern "C"  void KittyHandNFootManager_LeftHandMouseDown_m1853715275 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_LeftHandMouseDown_m1853715275_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3057952154* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObjectU5BU5D_t3057952154* V_2 = NULL;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_StopTouchOnHand_66();
		if (L_0)
		{
			goto IL_01b3;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_LeftHandCollider_7();
		__this->set_CurrentHand_60(L_1);
		SoundManager_t654432262 * L_2 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_2);
		SoundManager_PlayButtonSound_m794282288(L_2, /*hidden argument*/NULL);
		__this->set_Talk1_58(_stringLiteral2357684076);
		__this->set_Talk2_59(_stringLiteral593495257);
		GameObjectU5BU5D_t3057952154* L_3 = GameObject_FindGameObjectsWithTag_m2154478296(NULL /*static, unused*/, _stringLiteral2778558984, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObjectU5BU5D_t3057952154* L_4 = V_0;
		V_2 = L_4;
		V_3 = 0;
		goto IL_008b;
	}

IL_004b:
	{
		GameObjectU5BU5D_t3057952154* L_5 = V_2;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_t1756533147 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_1 = L_8;
		GameObject_t1756533147 * L_9 = V_1;
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Transform_GetChild_m3838588184(L_10, 0, /*hidden argument*/NULL);
		NullCheck(L_11);
		TextMesh_t1641806576 * L_12 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_11, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		String_t* L_13 = __this->get_Talk1_58();
		NullCheck(L_12);
		TextMesh_set_text_m3390063817(L_12, L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = V_1;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = Transform_GetChild_m3838588184(L_15, 1, /*hidden argument*/NULL);
		NullCheck(L_16);
		TextMesh_t1641806576 * L_17 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_16, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		String_t* L_18 = __this->get_Talk2_59();
		NullCheck(L_17);
		TextMesh_set_text_m3390063817(L_17, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_008b:
	{
		int32_t L_20 = V_3;
		GameObjectU5BU5D_t3057952154* L_21 = V_2;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_004b;
		}
	}
	{
		GameObject_t1756533147 * L_22 = __this->get_LeftHandCollider_7();
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = GameObject_get_transform_m909382139(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = Transform_GetChild_m3838588184(L_23, 0, /*hidden argument*/NULL);
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835(L_24, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3012219828, L_25, /*hidden argument*/NULL);
		bool L_26 = __this->get_LeftHandPolishModeOnly_62();
		if (L_26)
		{
			goto IL_0141;
		}
	}
	{
		GameObject_t1756533147 * L_27 = __this->get_HandIcon_11();
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_28 = __this->get_HandIcon_11();
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = GameObject_get_transform_m909382139(L_28, /*hidden argument*/NULL);
		Vector3_t2243707580  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m2638739322(&L_30, (0.0f), (0.0f), (45.0f), /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_localEulerAngles_m2927195985(L_29, L_30, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = __this->get_NailCutter_13();
		NullCheck(L_31);
		SpriteRenderer_t1209076198 * L_32 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_31, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Color__ctor_m1909920690(&L_33, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_32);
		SpriteRenderer_set_color_m2339931967(L_32, L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_34 = __this->get_NailCutter_13();
		NullCheck(L_34);
		BoxCollider2D_t948534547 * L_35 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_34, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_35);
		Behaviour_set_enabled_m1796096907(L_35, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = __this->get_NailCutter_13();
		NullCheck(L_36);
		ToolButton_t3848681854 * L_37 = GameObject_GetComponent_TisToolButton_t3848681854_m3256490429(L_36, /*hidden argument*/GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var);
		NullCheck(L_37);
		L_37->set_Draggable_4((bool)1);
		goto IL_0147;
	}

IL_0141:
	{
		KittyHandNFootManager_MakeNailPolishIconReady_m2897165473(__this, /*hidden argument*/NULL);
	}

IL_0147:
	{
		__this->set_LeftHandPolishModeOnly_62((bool)1);
		GameObject_t1756533147 * L_38 = __this->get_HandIconTap_12();
		NullCheck(L_38);
		GameObject_SetActive_m2887581199(L_38, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_39 = __this->get_RightHandCollider_8();
		NullCheck(L_39);
		GameObject_SetActive_m2887581199(L_39, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_40 = __this->get_LeftLegCollider_5();
		NullCheck(L_40);
		GameObject_SetActive_m2887581199(L_40, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_41 = __this->get_RightLegCollider_6();
		NullCheck(L_41);
		GameObject_SetActive_m2887581199(L_41, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_42 = __this->get_NormalKitty_9();
		NullCheck(L_42);
		SpriteRenderer_t1209076198 * L_43 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_42, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Color__ctor_m1909920690(&L_44, (1.0f), (1.0f), (1.0f), (0.35f), /*hidden argument*/NULL);
		NullCheck(L_43);
		SpriteRenderer_set_color_m2339931967(L_43, L_44, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_45 = __this->get_EyeAnim_10();
		NullCheck(L_45);
		GameObject_SetActive_m2887581199(L_45, (bool)0, /*hidden argument*/NULL);
	}

IL_01b3:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::LeftHandMouseUp()
extern "C"  void KittyHandNFootManager_LeftHandMouseUp_m1465708436 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::LeftHandMouseDrag()
extern "C"  void KittyHandNFootManager_LeftHandMouseDrag_m3750075389 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::LeftHandCollisionEnter(UnityEngine.GameObject)
extern Il2CppCodeGenString* _stringLiteral906963991;
extern Il2CppCodeGenString* _stringLiteral3351924419;
extern const uint32_t KittyHandNFootManager_LeftHandCollisionEnter_m1131043820_MetadataUsageId;
extern "C"  void KittyHandNFootManager_LeftHandCollisionEnter_m1131043820 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_LeftHandCollisionEnter_m1131043820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___coll0;
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m2797152613(L_0, _stringLiteral906963991, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		bool L_2 = __this->get_LeftHandCollide_53();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		__this->set_LeftHandCollide_53((bool)0);
		GameObject_t1756533147 * L_3 = __this->get_LeftHandCollider_7();
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3351924419, L_3, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::RightHandMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2357684076;
extern Il2CppCodeGenString* _stringLiteral593495257;
extern Il2CppCodeGenString* _stringLiteral2778558984;
extern Il2CppCodeGenString* _stringLiteral3012219828;
extern const uint32_t KittyHandNFootManager_RightHandMouseDown_m3465647210_MetadataUsageId;
extern "C"  void KittyHandNFootManager_RightHandMouseDown_m3465647210 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_RightHandMouseDown_m3465647210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3057952154* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObjectU5BU5D_t3057952154* V_2 = NULL;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_StopTouchOnHand_66();
		if (L_0)
		{
			goto IL_01b3;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_RightHandCollider_8();
		__this->set_CurrentHand_60(L_1);
		SoundManager_t654432262 * L_2 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_2);
		SoundManager_PlayButtonSound_m794282288(L_2, /*hidden argument*/NULL);
		__this->set_Talk1_58(_stringLiteral2357684076);
		__this->set_Talk2_59(_stringLiteral593495257);
		GameObjectU5BU5D_t3057952154* L_3 = GameObject_FindGameObjectsWithTag_m2154478296(NULL /*static, unused*/, _stringLiteral2778558984, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObjectU5BU5D_t3057952154* L_4 = V_0;
		V_2 = L_4;
		V_3 = 0;
		goto IL_008b;
	}

IL_004b:
	{
		GameObjectU5BU5D_t3057952154* L_5 = V_2;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_t1756533147 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_1 = L_8;
		GameObject_t1756533147 * L_9 = V_1;
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Transform_GetChild_m3838588184(L_10, 0, /*hidden argument*/NULL);
		NullCheck(L_11);
		TextMesh_t1641806576 * L_12 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_11, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		String_t* L_13 = __this->get_Talk1_58();
		NullCheck(L_12);
		TextMesh_set_text_m3390063817(L_12, L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = V_1;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = Transform_GetChild_m3838588184(L_15, 1, /*hidden argument*/NULL);
		NullCheck(L_16);
		TextMesh_t1641806576 * L_17 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_16, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		String_t* L_18 = __this->get_Talk2_59();
		NullCheck(L_17);
		TextMesh_set_text_m3390063817(L_17, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_008b:
	{
		int32_t L_20 = V_3;
		GameObjectU5BU5D_t3057952154* L_21 = V_2;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_004b;
		}
	}
	{
		GameObject_t1756533147 * L_22 = __this->get_RightHandCollider_8();
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = GameObject_get_transform_m909382139(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = Transform_GetChild_m3838588184(L_23, 0, /*hidden argument*/NULL);
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835(L_24, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3012219828, L_25, /*hidden argument*/NULL);
		bool L_26 = __this->get_RightHandPolishModeOnly_63();
		if (L_26)
		{
			goto IL_0141;
		}
	}
	{
		GameObject_t1756533147 * L_27 = __this->get_HandIcon_11();
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_28 = __this->get_HandIcon_11();
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = GameObject_get_transform_m909382139(L_28, /*hidden argument*/NULL);
		Vector3_t2243707580  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m2638739322(&L_30, (0.0f), (0.0f), (-25.0f), /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_localEulerAngles_m2927195985(L_29, L_30, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = __this->get_NailCutter_13();
		NullCheck(L_31);
		SpriteRenderer_t1209076198 * L_32 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_31, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Color__ctor_m1909920690(&L_33, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_32);
		SpriteRenderer_set_color_m2339931967(L_32, L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_34 = __this->get_NailCutter_13();
		NullCheck(L_34);
		BoxCollider2D_t948534547 * L_35 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_34, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_35);
		Behaviour_set_enabled_m1796096907(L_35, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = __this->get_NailCutter_13();
		NullCheck(L_36);
		ToolButton_t3848681854 * L_37 = GameObject_GetComponent_TisToolButton_t3848681854_m3256490429(L_36, /*hidden argument*/GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var);
		NullCheck(L_37);
		L_37->set_Draggable_4((bool)1);
		goto IL_0147;
	}

IL_0141:
	{
		KittyHandNFootManager_MakeNailPolishIconReady_m2897165473(__this, /*hidden argument*/NULL);
	}

IL_0147:
	{
		__this->set_RightHandPolishModeOnly_63((bool)1);
		GameObject_t1756533147 * L_38 = __this->get_HandIconTap_12();
		NullCheck(L_38);
		GameObject_SetActive_m2887581199(L_38, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_39 = __this->get_LeftHandCollider_7();
		NullCheck(L_39);
		GameObject_SetActive_m2887581199(L_39, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_40 = __this->get_LeftLegCollider_5();
		NullCheck(L_40);
		GameObject_SetActive_m2887581199(L_40, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_41 = __this->get_RightLegCollider_6();
		NullCheck(L_41);
		GameObject_SetActive_m2887581199(L_41, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_42 = __this->get_NormalKitty_9();
		NullCheck(L_42);
		SpriteRenderer_t1209076198 * L_43 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_42, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Color__ctor_m1909920690(&L_44, (1.0f), (1.0f), (1.0f), (0.35f), /*hidden argument*/NULL);
		NullCheck(L_43);
		SpriteRenderer_set_color_m2339931967(L_43, L_44, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_45 = __this->get_EyeAnim_10();
		NullCheck(L_45);
		GameObject_SetActive_m2887581199(L_45, (bool)0, /*hidden argument*/NULL);
	}

IL_01b3:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::RightHandMouseUp()
extern "C"  void KittyHandNFootManager_RightHandMouseUp_m288136055 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::RightHandMouseDrag()
extern "C"  void KittyHandNFootManager_RightHandMouseDrag_m2276292228 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::RightHandCollisionEnter(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral906963991;
extern Il2CppCodeGenString* _stringLiteral3351924419;
extern const uint32_t KittyHandNFootManager_RightHandCollisionEnter_m569741013_MetadataUsageId;
extern "C"  void KittyHandNFootManager_RightHandCollisionEnter_m569741013 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_RightHandCollisionEnter_m569741013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___coll0;
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m2797152613(L_0, _stringLiteral906963991, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0045;
		}
	}
	{
		bool L_2 = __this->get_RightHandCollide_54();
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		GameObject_t1756533147 * L_3 = __this->get_RightHandCollider_8();
		NullCheck(L_3);
		BoxCollider2D_t948534547 * L_4 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_3, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_4);
		Behaviour_set_enabled_m1796096907(L_4, (bool)0, /*hidden argument*/NULL);
		__this->set_RightHandCollide_54((bool)0);
		GameObject_t1756533147 * L_5 = __this->get_RightHandCollider_8();
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3351924419, L_5, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::LeftLegMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2357684076;
extern Il2CppCodeGenString* _stringLiteral593495257;
extern Il2CppCodeGenString* _stringLiteral2778558984;
extern Il2CppCodeGenString* _stringLiteral3012219828;
extern const uint32_t KittyHandNFootManager_LeftLegMouseDown_m2283106250_MetadataUsageId;
extern "C"  void KittyHandNFootManager_LeftLegMouseDown_m2283106250 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_LeftLegMouseDown_m2283106250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3057952154* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObjectU5BU5D_t3057952154* V_2 = NULL;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_StopTouchOnHand_66();
		if (L_0)
		{
			goto IL_018f;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_LeftLegCollider_5();
		__this->set_CurrentHand_60(L_1);
		SoundManager_t654432262 * L_2 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_2);
		SoundManager_PlayButtonSound_m794282288(L_2, /*hidden argument*/NULL);
		__this->set_Talk1_58(_stringLiteral2357684076);
		__this->set_Talk2_59(_stringLiteral593495257);
		GameObjectU5BU5D_t3057952154* L_3 = GameObject_FindGameObjectsWithTag_m2154478296(NULL /*static, unused*/, _stringLiteral2778558984, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObjectU5BU5D_t3057952154* L_4 = V_0;
		V_2 = L_4;
		V_3 = 0;
		goto IL_008b;
	}

IL_004b:
	{
		GameObjectU5BU5D_t3057952154* L_5 = V_2;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_t1756533147 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_1 = L_8;
		GameObject_t1756533147 * L_9 = V_1;
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Transform_GetChild_m3838588184(L_10, 0, /*hidden argument*/NULL);
		NullCheck(L_11);
		TextMesh_t1641806576 * L_12 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_11, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		String_t* L_13 = __this->get_Talk1_58();
		NullCheck(L_12);
		TextMesh_set_text_m3390063817(L_12, L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = V_1;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = Transform_GetChild_m3838588184(L_15, 1, /*hidden argument*/NULL);
		NullCheck(L_16);
		TextMesh_t1641806576 * L_17 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(L_16, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		String_t* L_18 = __this->get_Talk2_59();
		NullCheck(L_17);
		TextMesh_set_text_m3390063817(L_17, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_008b:
	{
		int32_t L_20 = V_3;
		GameObjectU5BU5D_t3057952154* L_21 = V_2;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_004b;
		}
	}
	{
		GameObject_t1756533147 * L_22 = __this->get_LeftLegCollider_5();
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = GameObject_get_transform_m909382139(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = Transform_GetChild_m3838588184(L_23, 0, /*hidden argument*/NULL);
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835(L_24, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3012219828, L_25, /*hidden argument*/NULL);
		bool L_26 = __this->get_LeftLegPolishModeOnly_64();
		if (L_26)
		{
			goto IL_011d;
		}
	}
	{
		GameObject_t1756533147 * L_27 = __this->get_HandIcon_11();
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_28 = __this->get_NailCutter_13();
		NullCheck(L_28);
		SpriteRenderer_t1209076198 * L_29 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_28, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Color__ctor_m1909920690(&L_30, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_29);
		SpriteRenderer_set_color_m2339931967(L_29, L_30, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = __this->get_NailCutter_13();
		NullCheck(L_31);
		BoxCollider2D_t948534547 * L_32 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_31, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_32);
		Behaviour_set_enabled_m1796096907(L_32, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_33 = __this->get_NailCutter_13();
		NullCheck(L_33);
		ToolButton_t3848681854 * L_34 = GameObject_GetComponent_TisToolButton_t3848681854_m3256490429(L_33, /*hidden argument*/GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var);
		NullCheck(L_34);
		L_34->set_Draggable_4((bool)1);
		goto IL_0123;
	}

IL_011d:
	{
		KittyHandNFootManager_MakeNailPolishIconReady_m2897165473(__this, /*hidden argument*/NULL);
	}

IL_0123:
	{
		__this->set_LeftLegPolishModeOnly_64((bool)1);
		GameObject_t1756533147 * L_35 = __this->get_HandIconTap_12();
		NullCheck(L_35);
		GameObject_SetActive_m2887581199(L_35, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = __this->get_RightHandCollider_8();
		NullCheck(L_36);
		GameObject_SetActive_m2887581199(L_36, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_37 = __this->get_LeftHandCollider_7();
		NullCheck(L_37);
		GameObject_SetActive_m2887581199(L_37, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_38 = __this->get_RightLegCollider_6();
		NullCheck(L_38);
		GameObject_SetActive_m2887581199(L_38, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_39 = __this->get_NormalKitty_9();
		NullCheck(L_39);
		SpriteRenderer_t1209076198 * L_40 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_39, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Color__ctor_m1909920690(&L_41, (1.0f), (1.0f), (1.0f), (0.35f), /*hidden argument*/NULL);
		NullCheck(L_40);
		SpriteRenderer_set_color_m2339931967(L_40, L_41, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_42 = __this->get_EyeAnim_10();
		NullCheck(L_42);
		GameObject_SetActive_m2887581199(L_42, (bool)0, /*hidden argument*/NULL);
	}

IL_018f:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::LeftLegMouseUp()
extern "C"  void KittyHandNFootManager_LeftLegMouseUp_m91314193 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::LeftLegMouseDrag()
extern "C"  void KittyHandNFootManager_LeftLegMouseDrag_m4259368924 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::LeftLegCollisionEnter(UnityEngine.GameObject)
extern Il2CppCodeGenString* _stringLiteral906963991;
extern Il2CppCodeGenString* _stringLiteral3351924419;
extern const uint32_t KittyHandNFootManager_LeftLegCollisionEnter_m764684799_MetadataUsageId;
extern "C"  void KittyHandNFootManager_LeftLegCollisionEnter_m764684799 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_LeftLegCollisionEnter_m764684799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___coll0;
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m2797152613(L_0, _stringLiteral906963991, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		bool L_2 = __this->get_LeftLegCollide_55();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		__this->set_LeftLegCollide_55((bool)0);
		GameObject_t1756533147 * L_3 = __this->get_LeftLegCollider_5();
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3351924419, L_3, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::RightLegMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3012219828;
extern const uint32_t KittyHandNFootManager_RightLegMouseDown_m3162149081_MetadataUsageId;
extern "C"  void KittyHandNFootManager_RightLegMouseDown_m3162149081 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_RightLegMouseDown_m3162149081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_StopTouchOnHand_66();
		if (L_0)
		{
			goto IL_011c;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_RightLegCollider_6();
		__this->set_CurrentHand_60(L_1);
		SoundManager_t654432262 * L_2 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_2);
		SoundManager_PlayButtonSound_m794282288(L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_RightLegCollider_6();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_4, 0, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3012219828, L_6, /*hidden argument*/NULL);
		bool L_7 = __this->get_RightLegPolishModeOnly_65();
		if (L_7)
		{
			goto IL_00aa;
		}
	}
	{
		GameObject_t1756533147 * L_8 = __this->get_HandIcon_11();
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = __this->get_NailCutter_13();
		NullCheck(L_9);
		SpriteRenderer_t1209076198 * L_10 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m1909920690(&L_11, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		SpriteRenderer_set_color_m2339931967(L_10, L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = __this->get_NailCutter_13();
		NullCheck(L_12);
		BoxCollider2D_t948534547 * L_13 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_12, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_13);
		Behaviour_set_enabled_m1796096907(L_13, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = __this->get_NailCutter_13();
		NullCheck(L_14);
		ToolButton_t3848681854 * L_15 = GameObject_GetComponent_TisToolButton_t3848681854_m3256490429(L_14, /*hidden argument*/GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var);
		NullCheck(L_15);
		L_15->set_Draggable_4((bool)1);
		goto IL_00b0;
	}

IL_00aa:
	{
		KittyHandNFootManager_MakeNailPolishIconReady_m2897165473(__this, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		__this->set_RightLegPolishModeOnly_65((bool)1);
		GameObject_t1756533147 * L_16 = __this->get_HandIconTap_12();
		NullCheck(L_16);
		GameObject_SetActive_m2887581199(L_16, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_17 = __this->get_RightHandCollider_8();
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = __this->get_LeftLegCollider_5();
		NullCheck(L_18);
		GameObject_SetActive_m2887581199(L_18, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = __this->get_LeftHandCollider_7();
		NullCheck(L_19);
		GameObject_SetActive_m2887581199(L_19, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = __this->get_NormalKitty_9();
		NullCheck(L_20);
		SpriteRenderer_t1209076198 * L_21 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_20, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Color__ctor_m1909920690(&L_22, (1.0f), (1.0f), (1.0f), (0.35f), /*hidden argument*/NULL);
		NullCheck(L_21);
		SpriteRenderer_set_color_m2339931967(L_21, L_22, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = __this->get_EyeAnim_10();
		NullCheck(L_23);
		GameObject_SetActive_m2887581199(L_23, (bool)0, /*hidden argument*/NULL);
	}

IL_011c:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::RightLegMouseUp()
extern "C"  void KittyHandNFootManager_RightLegMouseUp_m1869735974 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::RightLegMouseDrag()
extern "C"  void KittyHandNFootManager_RightLegMouseDrag_m1429015919 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::RightLegCollisionEnter(UnityEngine.GameObject)
extern Il2CppCodeGenString* _stringLiteral906963991;
extern Il2CppCodeGenString* _stringLiteral3351924419;
extern const uint32_t KittyHandNFootManager_RightLegCollisionEnter_m538626458_MetadataUsageId;
extern "C"  void KittyHandNFootManager_RightLegCollisionEnter_m538626458 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_RightLegCollisionEnter_m538626458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___coll0;
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m2797152613(L_0, _stringLiteral906963991, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		bool L_2 = __this->get_RightLegCollide_56();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		__this->set_RightLegCollide_56((bool)0);
		GameObject_t1756533147 * L_3 = __this->get_RightLegCollider_6();
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3351924419, L_3, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::NailCutterMouseDown()
extern "C"  void KittyHandNFootManager_NailCutterMouseDown_m470457080 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_HandIcon_11();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::NailCutterMouseUp()
extern "C"  void KittyHandNFootManager_NailCutterMouseUp_m2916541575 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::NailCutterMouseDrag()
extern "C"  void KittyHandNFootManager_NailCutterMouseDrag_m815784710 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::NailCutterCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_NailCutterCollisionEnter_m3018890777 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::NailPolishIconMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern Il2CppCodeGenString* _stringLiteral2923566773;
extern const uint32_t KittyHandNFootManager_NailPolishIconMouseDown_m1671979995_MetadataUsageId;
extern "C"  void KittyHandNFootManager_NailPolishIconMouseDown_m1671979995 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_NailPolishIconMouseDown_m1671979995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_NailPolishPanel_18();
		NullCheck(L_0);
		Animator_t69676727 * L_1 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_1);
		Animator_SetBool_m2305662531(L_1, _stringLiteral3558534422, (bool)0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = KittyHandNFootManager_MakeNailPolishIconAvailable_m369200145(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_NailPolishPanel_18();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		MonoBehaviour_CancelInvoke_m2508161963(__this, _stringLiteral2923566773, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_NailCutter_13();
		NullCheck(L_4);
		SpriteRenderer_t1209076198 * L_5 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_4, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m1909920690(&L_6, (1.0f), (1.0f), (1.0f), (0.35f), /*hidden argument*/NULL);
		NullCheck(L_5);
		SpriteRenderer_set_color_m2339931967(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_NailCutter_13();
		NullCheck(L_7);
		BoxCollider2D_t948534547 * L_8 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_7, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_8);
		Behaviour_set_enabled_m1796096907(L_8, (bool)0, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_9 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_9);
		SoundManager_PlayButtonSound_m794282288(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::NailPolishIconMouseUp()
extern "C"  void KittyHandNFootManager_NailPolishIconMouseUp_m912663072 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::NailPolishIconMouseDrag()
extern "C"  void KittyHandNFootManager_NailPolishIconMouseDrag_m3568338921 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::NailPolishIconCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_NailPolishIconCollisionEnter_m2945983628 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::NailPolishMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern const uint32_t KittyHandNFootManager_NailPolishMouseDown_m1591627310_MetadataUsageId;
extern "C"  void KittyHandNFootManager_NailPolishMouseDown_m1591627310 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_NailPolishMouseDown_m1591627310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_NailPolishPanel_18();
		NullCheck(L_0);
		Animator_t69676727 * L_1 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_1);
		Animator_SetBool_m2305662531(L_1, _stringLiteral3558534422, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_NailPolishIcon_14();
		NullCheck(L_2);
		BoxCollider2D_t948534547 * L_3 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_2, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_ColorNails_m1870864844(__this, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_4 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_4);
		SoundManager_PlayButtonSound_m794282288(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::NailPolishMouseUp()
extern "C"  void KittyHandNFootManager_NailPolishMouseUp_m1381491547 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::NailPolishMouseDrag()
extern "C"  void KittyHandNFootManager_NailPolishMouseDrag_m4136548380 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::NailPolishCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_NailPolishCollisionEnter_m1361164717 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::FacePowderBrushMouseDown()
extern Il2CppCodeGenString* _stringLiteral2923566773;
extern const uint32_t KittyHandNFootManager_FacePowderBrushMouseDown_m1401345295_MetadataUsageId;
extern "C"  void KittyHandNFootManager_FacePowderBrushMouseDown_m1401345295 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_FacePowderBrushMouseDown_m1401345295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_FacePowderMirror_15();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Transform_GetChild_m3838588184(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		MonoBehaviour_CancelInvoke_m2508161963(__this, _stringLiteral2923566773, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::FacePowderBrushMouseUp()
extern "C"  void KittyHandNFootManager_FacePowderBrushMouseUp_m2883390390 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_PowderEffect_34();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m313590879(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_PowderEffect_34();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::FacePowderBrushMouseDrag()
extern "C"  void KittyHandNFootManager_FacePowderBrushMouseDrag_m3155366913 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::FacePowderBrushCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_FacePowderBrushCollisionEnter_m1731164666 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::FacePowderBrushCollisionStay(UnityEngine.GameObject)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const uint32_t KittyHandNFootManager_FacePowderBrushCollisionStay_m1600148821_MetadataUsageId;
extern "C"  void KittyHandNFootManager_FacePowderBrushCollisionStay_m1600148821 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_FacePowderBrushCollisionStay_m1600148821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1756533147 * L_0 = ___coll0;
		GameObjectU5BU5D_t3057952154* L_1 = __this->get_FaceGlow_31();
		NullCheck(L_1);
		int32_t L_2 = 0;
		GameObject_t1756533147 * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t1756533147 * L_5 = ___coll0;
		GameObjectU5BU5D_t3057952154* L_6 = __this->get_FaceGlow_31();
		NullCheck(L_6);
		int32_t L_7 = 1;
		GameObject_t1756533147 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0071;
		}
	}

IL_0026:
	{
		GameObject_t1756533147 * L_10 = ___coll0;
		NullCheck(L_10);
		SpriteRenderer_t1209076198 * L_11 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_10, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		SpriteRenderer_t1209076198 * L_12 = L_11;
		NullCheck(L_12);
		Color_t2020392075  L_13 = SpriteRenderer_get_color_m345525162(L_12, /*hidden argument*/NULL);
		float L_14 = Time_get_smoothDeltaTime_m1294084638(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m1909920690(&L_15, (0.0f), (0.0f), (0.0f), L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_16 = Color_op_Addition_m1759335993(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		NullCheck(L_12);
		SpriteRenderer_set_color_m2339931967(L_12, L_16, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_17 = __this->get_PowderEffect_34();
		NullCheck(L_17);
		bool L_18 = GameObject_get_activeSelf_m313590879(L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0071;
		}
	}
	{
		GameObject_t1756533147 * L_19 = __this->get_PowderEffect_34();
		NullCheck(L_19);
		GameObject_SetActive_m2887581199(L_19, (bool)1, /*hidden argument*/NULL);
	}

IL_0071:
	{
		GameObjectU5BU5D_t3057952154* L_20 = __this->get_FaceGlow_31();
		NullCheck(L_20);
		int32_t L_21 = 1;
		GameObject_t1756533147 * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		SpriteRenderer_t1209076198 * L_23 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_22, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		NullCheck(L_23);
		Color_t2020392075  L_24 = SpriteRenderer_get_color_m345525162(L_23, /*hidden argument*/NULL);
		V_0 = L_24;
		float L_25 = (&V_0)->get_a_3();
		if ((!(((float)L_25) >= ((float)(0.95f)))))
		{
			goto IL_009b;
		}
	}
	{
		KittyHandNFootManager_MakePerfumeOpen_m347974372(__this, /*hidden argument*/NULL);
	}

IL_009b:
	{
		return;
	}
}
// System.Void KittyHandNFootManager::PerfumeMouseDown()
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral859636689;
extern const uint32_t KittyHandNFootManager_PerfumeMouseDown_m1773689439_MetadataUsageId;
extern "C"  void KittyHandNFootManager_PerfumeMouseDown_m1773689439 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_PerfumeMouseDown_m1773689439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_FacePowderBrush_16();
		NullCheck(L_0);
		BoxCollider2D_t948534547 * L_1 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_0, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_1);
		Behaviour_set_enabled_m1796096907(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_FacePowderBrush_16();
		NullCheck(L_2);
		SpriteRenderer_t1209076198 * L_3 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_2, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m1909920690(&L_4, (1.0f), (1.0f), (1.0f), (0.35f), /*hidden argument*/NULL);
		NullCheck(L_3);
		SpriteRenderer_set_color_m2339931967(L_3, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_FacePowderMirror_15();
		NullCheck(L_5);
		SpriteRenderer_t1209076198 * L_6 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_5, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m1909920690(&L_7, (1.0f), (1.0f), (1.0f), (0.35f), /*hidden argument*/NULL);
		NullCheck(L_6);
		SpriteRenderer_set_color_m2339931967(L_6, L_7, /*hidden argument*/NULL);
		MonoBehaviour_InvokeRepeating_m3468262484(__this, _stringLiteral859636689, (0.5f), (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::PerfumeMouseUp()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral859636689;
extern const uint32_t KittyHandNFootManager_PerfumeMouseUp_m3524642962_MetadataUsageId;
extern "C"  void KittyHandNFootManager_PerfumeMouseUp_m3524642962 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_PerfumeMouseUp_m3524642962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_CancelInvoke_m2508161963(__this, _stringLiteral859636689, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_CurtainTassel_36();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_CurtainTop_45();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		Il2CppObject * L_2 = KittyHandNFootManager_MakeCurtainTasselAnimOff_m151004770(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_3 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_3);
		AudioSource_t1135106623 * L_4 = L_3->get_PerfumeSound_12();
		NullCheck(L_4);
		AudioSource_Stop_m3452679614(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::PerfumeMouseDrag()
extern "C"  void KittyHandNFootManager_PerfumeMouseDrag_m2963087325 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::PerfumeCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_PerfumeCollisionEnter_m1329363386 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::PerfumeCollisionStay(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_PerfumeCollisionStay_m2906350105 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void KittyHandNFootManager::CurtainTasselMouseDown()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1367191786;
extern const uint32_t KittyHandNFootManager_CurtainTasselMouseDown_m2926699873_MetadataUsageId;
extern "C"  void KittyHandNFootManager_CurtainTasselMouseDown_m2926699873 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_CurtainTasselMouseDown_m2926699873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_CurtainTassel_36();
		NullCheck(L_0);
		Animator_t69676727 * L_1 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_1);
		Animator_SetBool_m2305662531(L_1, _stringLiteral1367191786, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_CurtainClose_37();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_CurtainClose_37();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_CurtainOpen_38();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		Il2CppObject * L_6 = KittyHandNFootManager_LoadNextLevel_m2267124017(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::BackButtonMouseDown()
extern Il2CppClass* AdsManager_t2564570159_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_BackButtonMouseDown_m302824672_MetadataUsageId;
extern "C"  void KittyHandNFootManager_BackButtonMouseDown_m302824672 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_BackButtonMouseDown_m302824672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AdsManager_t2564570159 * L_0 = ((AdsManager_t2564570159_StaticFields*)AdsManager_t2564570159_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_0);
		AdsManager_ShowInter_m1660539497(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_StylePanel_39();
		NullCheck(L_1);
		bool L_2 = GameObject_get_activeSelf_m313590879(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_3 = Application_get_loadedLevel_m3768581785(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevelAsync_m3132594047(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_002a:
	{
		int32_t L_4 = Application_get_loadedLevel_m3768581785(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevelAsync_m3132594047(NULL /*static, unused*/, ((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0037:
	{
		SoundManager_t654432262 * L_5 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_5);
		SoundManager_PlayButtonSound_m794282288(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::FbLikeButtonMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_FbLikeButtonMouseDown_m2016386076_MetadataUsageId;
extern "C"  void KittyHandNFootManager_FbLikeButtonMouseDown_m2016386076 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_FbLikeButtonMouseDown_m2016386076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_FbLikeButton_41();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = L_1;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_localScale_m3074381503(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localScale_m2325460848(L_2, L_4, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_5 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_5);
		SoundManager_PlayButtonSound_m794282288(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::FbLikeButtonMouseUp()
extern "C"  void KittyHandNFootManager_FbLikeButtonMouseUp_m1876896813 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = KittyHandNFootManager_FbLikeButtonUp_m3249047842(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::RestartButtonMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_RestartButtonMouseDown_m744644910_MetadataUsageId;
extern "C"  void KittyHandNFootManager_RestartButtonMouseDown_m744644910 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_RestartButtonMouseDown_m744644910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_RestartButton_42();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = L_1;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_localScale_m3074381503(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localScale_m2325460848(L_2, L_4, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_5 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_5);
		SoundManager_PlayButtonSound_m794282288(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::RestartButtonMouseUp()
extern Il2CppCodeGenString* _stringLiteral2398850692;
extern const uint32_t KittyHandNFootManager_RestartButtonMouseUp_m1097162017_MetadataUsageId;
extern "C"  void KittyHandNFootManager_RestartButtonMouseUp_m1097162017 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_RestartButtonMouseUp_m1097162017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral2398850692, /*hidden argument*/NULL);
		Il2CppObject * L_0 = KittyHandNFootManager_RestartButtonUp_m4208856944(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::CloseButtonMouseDown()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern const uint32_t KittyHandNFootManager_CloseButtonMouseDown_m2177967763_MetadataUsageId;
extern "C"  void KittyHandNFootManager_CloseButtonMouseDown_m2177967763 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_CloseButtonMouseDown_m2177967763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_NailPolishPanel_18();
		NullCheck(L_0);
		Animator_t69676727 * L_1 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_1);
		Animator_SetBool_m2305662531(L_1, _stringLiteral3558534422, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_NailPolishIcon_14();
		NullCheck(L_2);
		BoxCollider2D_t948534547 * L_3 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_2, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
		Il2CppObject * L_4 = KittyHandNFootManager_PrepareForNextNailCut_m3947584807(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::DoneButtonMouseUp()
extern Il2CppClass* AdsManager_t2564570159_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1806057595;
extern const uint32_t KittyHandNFootManager_DoneButtonMouseUp_m1316066332_MetadataUsageId;
extern "C"  void KittyHandNFootManager_DoneButtonMouseUp_m1316066332 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_DoneButtonMouseUp_m1316066332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AdsManager_t2564570159 * L_0 = ((AdsManager_t2564570159_StaticFields*)AdsManager_t2564570159_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_0);
		AdsManager_ShowInter_m1660539497(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_LastPanel_48();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_DoneButton_47();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_StylePanel_39();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_4, 0, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_7 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_7);
		SoundManager_PlayLastSound_m2535147324(L_7, /*hidden argument*/NULL);
		MonoBehaviour_InvokeRepeating_m3468262484(__this, _stringLiteral1806057595, (0.2f), (1.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::ShareButtonMouseUp()
extern "C"  void KittyHandNFootManager_ShareButtonMouseUp_m599277463 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = KittyHandNFootManager_ShareScreenshot_m2207017311(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KittyHandNFootManager::SnapButtonMouseUp()
extern "C"  void KittyHandNFootManager_SnapButtonMouseUp_m2612709480 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = KittyHandNFootManager_TakeSnap_m3981926389(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::MakeBigger(UnityEngine.GameObject)
extern Il2CppClass* U3CMakeBiggerU3Ec__Iterator1_t2709881044_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_MakeBigger_m1294242164_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_MakeBigger_m1294242164 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___Obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_MakeBigger_m1294242164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CMakeBiggerU3Ec__Iterator1_t2709881044 * V_0 = NULL;
	{
		U3CMakeBiggerU3Ec__Iterator1_t2709881044 * L_0 = (U3CMakeBiggerU3Ec__Iterator1_t2709881044 *)il2cpp_codegen_object_new(U3CMakeBiggerU3Ec__Iterator1_t2709881044_il2cpp_TypeInfo_var);
		U3CMakeBiggerU3Ec__Iterator1__ctor_m1857458079(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMakeBiggerU3Ec__Iterator1_t2709881044 * L_1 = V_0;
		GameObject_t1756533147 * L_2 = ___Obj0;
		NullCheck(L_1);
		L_1->set_Obj_0(L_2);
		U3CMakeBiggerU3Ec__Iterator1_t2709881044 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		U3CMakeBiggerU3Ec__Iterator1_t2709881044 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::NailCuttingAnim(UnityEngine.GameObject)
extern Il2CppClass* U3CNailCuttingAnimU3Ec__Iterator2_t2399846568_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_NailCuttingAnim_m1025854349_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_NailCuttingAnim_m1025854349 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___Obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_NailCuttingAnim_m1025854349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * V_0 = NULL;
	{
		U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * L_0 = (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 *)il2cpp_codegen_object_new(U3CNailCuttingAnimU3Ec__Iterator2_t2399846568_il2cpp_TypeInfo_var);
		U3CNailCuttingAnimU3Ec__Iterator2__ctor_m3367604021(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * L_1 = V_0;
		GameObject_t1756533147 * L_2 = ___Obj0;
		NullCheck(L_1);
		L_1->set_Obj_0(L_2);
		U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::MakeNailPolishIconAvailable()
extern Il2CppClass* U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_MakeNailPolishIconAvailable_m369200145_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_MakeNailPolishIconAvailable_m369200145 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_MakeNailPolishIconAvailable_m369200145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * V_0 = NULL;
	{
		U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * L_0 = (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 *)il2cpp_codegen_object_new(U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693_il2cpp_TypeInfo_var);
		U3CMakeNailPolishIconAvailableU3Ec__Iterator3__ctor_m457145830(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::PrepareForNextNailCut()
extern Il2CppClass* U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_PrepareForNextNailCut_m3947584807_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_PrepareForNextNailCut_m3947584807 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_PrepareForNextNailCut_m3947584807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * V_0 = NULL;
	{
		U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * L_0 = (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 *)il2cpp_codegen_object_new(U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482_il2cpp_TypeInfo_var);
		U3CPrepareForNextNailCutU3Ec__Iterator4__ctor_m3872460669(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_4(__this);
		U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::DeleteSpray(UnityEngine.GameObject)
extern Il2CppClass* U3CDeleteSprayU3Ec__Iterator5_t3757503118_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_DeleteSpray_m1861600344_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_DeleteSpray_m1861600344 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___Spray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_DeleteSpray_m1861600344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDeleteSprayU3Ec__Iterator5_t3757503118 * V_0 = NULL;
	{
		U3CDeleteSprayU3Ec__Iterator5_t3757503118 * L_0 = (U3CDeleteSprayU3Ec__Iterator5_t3757503118 *)il2cpp_codegen_object_new(U3CDeleteSprayU3Ec__Iterator5_t3757503118_il2cpp_TypeInfo_var);
		U3CDeleteSprayU3Ec__Iterator5__ctor_m166138161(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDeleteSprayU3Ec__Iterator5_t3757503118 * L_1 = V_0;
		GameObject_t1756533147 * L_2 = ___Spray0;
		NullCheck(L_1);
		L_1->set_Spray_0(L_2);
		U3CDeleteSprayU3Ec__Iterator5_t3757503118 * L_3 = V_0;
		return L_3;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::MakeCurtainTasselAnimOff()
extern Il2CppClass* U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_MakeCurtainTasselAnimOff_m151004770_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_MakeCurtainTasselAnimOff_m151004770 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_MakeCurtainTasselAnimOff_m151004770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * V_0 = NULL;
	{
		U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * L_0 = (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 *)il2cpp_codegen_object_new(U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083_il2cpp_TypeInfo_var);
		U3CMakeCurtainTasselAnimOffU3Ec__Iterator6__ctor_m708476084(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::LoadNextLevel()
extern Il2CppClass* U3CLoadNextLevelU3Ec__Iterator7_t3146601861_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_LoadNextLevel_m2267124017_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_LoadNextLevel_m2267124017 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_LoadNextLevel_m2267124017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * V_0 = NULL;
	{
		U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * L_0 = (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 *)il2cpp_codegen_object_new(U3CLoadNextLevelU3Ec__Iterator7_t3146601861_il2cpp_TypeInfo_var);
		U3CLoadNextLevelU3Ec__Iterator7__ctor_m296652690(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::MakeStylePanelOpen()
extern Il2CppClass* U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_MakeStylePanelOpen_m2285956193_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_MakeStylePanelOpen_m2285956193 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_MakeStylePanelOpen_m2285956193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * V_0 = NULL;
	{
		U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * L_0 = (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 *)il2cpp_codegen_object_new(U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244_il2cpp_TypeInfo_var);
		U3CMakeStylePanelOpenU3Ec__Iterator8__ctor_m2328079727(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::FbLikeButtonUp()
extern Il2CppClass* U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_FbLikeButtonUp_m3249047842_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_FbLikeButtonUp_m3249047842 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_FbLikeButtonUp_m3249047842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * V_0 = NULL;
	{
		U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * L_0 = (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 *)il2cpp_codegen_object_new(U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216_il2cpp_TypeInfo_var);
		U3CFbLikeButtonUpU3Ec__Iterator9__ctor_m1680875357(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::RestartButtonUp()
extern Il2CppClass* U3CRestartButtonUpU3Ec__IteratorA_t2431284672_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_RestartButtonUp_m4208856944_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_RestartButtonUp_m4208856944 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_RestartButtonUp_m4208856944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * V_0 = NULL;
	{
		U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * L_0 = (U3CRestartButtonUpU3Ec__IteratorA_t2431284672 *)il2cpp_codegen_object_new(U3CRestartButtonUpU3Ec__IteratorA_t2431284672_il2cpp_TypeInfo_var);
		U3CRestartButtonUpU3Ec__IteratorA__ctor_m3300642729(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::TakeSnap()
extern Il2CppClass* U3CTakeSnapU3Ec__IteratorB_t2011450030_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_TakeSnap_m3981926389_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_TakeSnap_m3981926389 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_TakeSnap_m3981926389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CTakeSnapU3Ec__IteratorB_t2011450030 * V_0 = NULL;
	{
		U3CTakeSnapU3Ec__IteratorB_t2011450030 * L_0 = (U3CTakeSnapU3Ec__IteratorB_t2011450030 *)il2cpp_codegen_object_new(U3CTakeSnapU3Ec__IteratorB_t2011450030_il2cpp_TypeInfo_var);
		U3CTakeSnapU3Ec__IteratorB__ctor_m1157678565(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTakeSnapU3Ec__IteratorB_t2011450030 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CTakeSnapU3Ec__IteratorB_t2011450030 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator KittyHandNFootManager::ShareScreenshot()
extern Il2CppClass* U3CShareScreenshotU3Ec__IteratorC_t3190744391_il2cpp_TypeInfo_var;
extern const uint32_t KittyHandNFootManager_ShareScreenshot_m2207017311_MetadataUsageId;
extern "C"  Il2CppObject * KittyHandNFootManager_ShareScreenshot_m2207017311 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_ShareScreenshot_m2207017311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CShareScreenshotU3Ec__IteratorC_t3190744391 * V_0 = NULL;
	{
		U3CShareScreenshotU3Ec__IteratorC_t3190744391 * L_0 = (U3CShareScreenshotU3Ec__IteratorC_t3190744391 *)il2cpp_codegen_object_new(U3CShareScreenshotU3Ec__IteratorC_t3190744391_il2cpp_TypeInfo_var);
		U3CShareScreenshotU3Ec__IteratorC__ctor_m3064057836(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShareScreenshotU3Ec__IteratorC_t3190744391 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CShareScreenshotU3Ec__IteratorC_t3190744391 * L_2 = V_0;
		return L_2;
	}
}
// System.Void KittyHandNFootManager::GameEndAnimation()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t KittyHandNFootManager_GameEndAnimation_m2847078999_MetadataUsageId;
extern "C"  void KittyHandNFootManager_GameEndAnimation_m2847078999 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KittyHandNFootManager_GameEndAnimation_m2847078999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_GameEndAnim_50();
		int32_t L_1 = Random_Range_m694320887(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		int32_t L_2 = Random_Range_m694320887(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2638739322(&L_3, (((float)((float)L_1))), (((float)((float)L_2))), (4.5f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_4 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_5 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_5;
		return;
	}
}
// System.Void KittyHandNFootManager/<DeleteSpray>c__Iterator5::.ctor()
extern "C"  void U3CDeleteSprayU3Ec__Iterator5__ctor_m166138161 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<DeleteSpray>c__Iterator5::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t U3CDeleteSprayU3Ec__Iterator5_MoveNext_m3105239803_MetadataUsageId;
extern "C"  bool U3CDeleteSprayU3Ec__Iterator5_MoveNext_m3105239803 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDeleteSprayU3Ec__Iterator5_MoveNext_m3105239803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_005c;
	}

IL_0021:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (2.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_005e;
	}

IL_0045:
	{
		GameObject_t1756533147 * L_4 = __this->get_Spray_0();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = GameObject_get_gameObject_m3662236595(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_005c:
	{
		return (bool)0;
	}

IL_005e:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<DeleteSpray>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDeleteSprayU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2798560127 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<DeleteSpray>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDeleteSprayU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1532498007 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<DeleteSpray>c__Iterator5::Dispose()
extern "C"  void U3CDeleteSprayU3Ec__Iterator5_Dispose_m2308277552 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<DeleteSpray>c__Iterator5::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CDeleteSprayU3Ec__Iterator5_Reset_m4136052782_MetadataUsageId;
extern "C"  void U3CDeleteSprayU3Ec__Iterator5_Reset_m4136052782 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDeleteSprayU3Ec__Iterator5_Reset_m4136052782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::.ctor()
extern "C"  void U3CFbLikeButtonUpU3Ec__Iterator9__ctor_m1680875357 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral585872459;
extern const uint32_t U3CFbLikeButtonUpU3Ec__Iterator9_MoveNext_m930851739_MetadataUsageId;
extern "C"  bool U3CFbLikeButtonUpU3Ec__Iterator9_MoveNext_m930851739 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFbLikeButtonUpU3Ec__Iterator9_MoveNext_m930851739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_006a;
		}
	}
	{
		goto IL_007b;
	}

IL_0021:
	{
		KittyHandNFootManager_t1686788969 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_FbLikeButton_41();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = L_4;
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_localScale_m3074381503(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, (1.4f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localScale_m2325460848(L_5, L_7, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_8 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_8, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_8);
		bool L_9 = __this->get_U24disposing_2();
		if (L_9)
		{
			goto IL_0065;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0065:
	{
		goto IL_007d;
	}

IL_006a:
	{
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral585872459, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_007b:
	{
		return (bool)0;
	}

IL_007d:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFbLikeButtonUpU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2868013043 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFbLikeButtonUpU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3328623339 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::Dispose()
extern "C"  void U3CFbLikeButtonUpU3Ec__Iterator9_Dispose_m2709393354 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CFbLikeButtonUpU3Ec__Iterator9_Reset_m884661972_MetadataUsageId;
extern "C"  void U3CFbLikeButtonUpU3Ec__Iterator9_Reset_m884661972 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFbLikeButtonUpU3Ec__Iterator9_Reset_m884661972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<LoadNextLevel>c__Iterator7::.ctor()
extern "C"  void U3CLoadNextLevelU3Ec__Iterator7__ctor_m296652690 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<LoadNextLevel>c__Iterator7::MoveNext()
extern Il2CppClass* AdsManager_t2564570159_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadNextLevelU3Ec__Iterator7_MoveNext_m2212974190_MetadataUsageId;
extern "C"  bool U3CLoadNextLevelU3Ec__Iterator7_MoveNext_m2212974190 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadNextLevelU3Ec__Iterator7_MoveNext_m2212974190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
	}
	{
		goto IL_00a0;
	}

IL_0021:
	{
		AdsManager_t2564570159 * L_2 = ((AdsManager_t2564570159_StaticFields*)AdsManager_t2564570159_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_2);
		AdsManager_ShowInter_m1660539497(L_2, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, (4.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_3);
		bool L_4 = __this->get_U24disposing_2();
		if (L_4)
		{
			goto IL_004a;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_004a:
	{
		goto IL_00a2;
	}

IL_004f:
	{
		KittyHandNFootManager_t1686788969 * L_5 = __this->get_U24this_0();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = L_5->get_CurtainOpen_38();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)1, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = L_7->get_CurtainTassel_36();
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_9 = __this->get_U24this_0();
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = L_9->get_CurtainClose_37();
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_11 = __this->get_U24this_0();
		KittyHandNFootManager_t1686788969 * L_12 = __this->get_U24this_0();
		NullCheck(L_12);
		Il2CppObject * L_13 = KittyHandNFootManager_MakeStylePanelOpen_m2285956193(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		MonoBehaviour_StartCoroutine_m2470621050(L_11, L_13, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00a0:
	{
		return (bool)0;
	}

IL_00a2:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<LoadNextLevel>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadNextLevelU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4202989504 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<LoadNextLevel>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadNextLevelU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1668672488 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<LoadNextLevel>c__Iterator7::Dispose()
extern "C"  void U3CLoadNextLevelU3Ec__Iterator7_Dispose_m4013085871 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<LoadNextLevel>c__Iterator7::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadNextLevelU3Ec__Iterator7_Reset_m2750327949_MetadataUsageId;
extern "C"  void U3CLoadNextLevelU3Ec__Iterator7_Reset_m2750327949 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadNextLevelU3Ec__Iterator7_Reset_m2750327949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<MakeBigger>c__Iterator1::.ctor()
extern "C"  void U3CMakeBiggerU3Ec__Iterator1__ctor_m1857458079 (U3CMakeBiggerU3Ec__Iterator1_t2709881044 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<MakeBigger>c__Iterator1::MoveNext()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeBiggerU3Ec__Iterator1_MoveNext_m3780730389_MetadataUsageId;
extern "C"  bool U3CMakeBiggerU3Ec__Iterator1_MoveNext_m3780730389 (U3CMakeBiggerU3Ec__Iterator1_t2709881044 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeBiggerU3Ec__Iterator1_MoveNext_m3780730389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00f0;
	}

IL_0021:
	{
		SoundManager_t654432262 * L_2 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_2);
		L_2->set_CanPlayMeow_16((bool)0);
		SoundManager_t654432262 * L_3 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = L_3->get_OpenMouth_2();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_5 = __this->get_U24this_1();
		NullCheck(L_5);
		L_5->set_StopTouchOnHand_66((bool)1);
		goto IL_00b6;
	}

IL_004d:
	{
		GameObject_t1756533147 * L_6 = __this->get_Obj_0();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_Obj_0();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_localScale_m3074381503(L_9, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_12, (0.75f), /*hidden argument*/NULL);
		float L_14 = Time_get_smoothDeltaTime_m1294084638(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_15 = Vector2_Lerp_m1511850087(NULL /*static, unused*/, L_11, L_13, ((float)((float)L_14*(float)(2.0f))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localScale_m2325460848(L_7, L_16, /*hidden argument*/NULL);
		int32_t L_17 = 0;
		Il2CppObject * L_18 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_17);
		__this->set_U24current_2(L_18);
		bool L_19 = __this->get_U24disposing_3();
		if (L_19)
		{
			goto IL_00b1;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_00b1:
	{
		goto IL_00f2;
	}

IL_00b6:
	{
		GameObject_t1756533147 * L_20 = __this->get_Obj_0();
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_localScale_m3074381503(L_21, /*hidden argument*/NULL);
		Vector2_t2243707579  L_23 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		Vector2_t2243707579  L_24 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_25 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_24, (0.75f), /*hidden argument*/NULL);
		float L_26 = Vector2_Distance_m280750759(NULL /*static, unused*/, L_23, L_25, /*hidden argument*/NULL);
		if ((((float)L_26) >= ((float)(0.1f))))
		{
			goto IL_004d;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_00f0:
	{
		return (bool)0;
	}

IL_00f2:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<MakeBigger>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeBiggerU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m35184273 (U3CMakeBiggerU3Ec__Iterator1_t2709881044 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<MakeBigger>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeBiggerU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1046389913 (U3CMakeBiggerU3Ec__Iterator1_t2709881044 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<MakeBigger>c__Iterator1::Dispose()
extern "C"  void U3CMakeBiggerU3Ec__Iterator1_Dispose_m2928659362 (U3CMakeBiggerU3Ec__Iterator1_t2709881044 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<MakeBigger>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeBiggerU3Ec__Iterator1_Reset_m177490824_MetadataUsageId;
extern "C"  void U3CMakeBiggerU3Ec__Iterator1_Reset_m177490824 (U3CMakeBiggerU3Ec__Iterator1_t2709881044 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeBiggerU3Ec__Iterator1_Reset_m177490824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::.ctor()
extern "C"  void U3CMakeCurtainTasselAnimOffU3Ec__Iterator6__ctor_m708476084 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_MoveNext_m1788629920_MetadataUsageId;
extern "C"  bool U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_MoveNext_m1788629920 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_MoveNext_m1788629920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_004c;
	}

IL_0021:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (3.5f), /*hidden argument*/NULL);
		__this->set_U24current_0(L_2);
		bool L_3 = __this->get_U24disposing_1();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_2(1);
	}

IL_0040:
	{
		goto IL_004e;
	}

IL_0045:
	{
		__this->set_U24PC_2((-1));
	}

IL_004c:
	{
		return (bool)0;
	}

IL_004e:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1386238616 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m2490880480 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::Dispose()
extern "C"  void U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_Dispose_m3033528425 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_1((bool)1);
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_Reset_m4203996355_MetadataUsageId;
extern "C"  void U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_Reset_m4203996355 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_Reset_m4203996355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::.ctor()
extern "C"  void U3CMakeNailPolishIconAvailableU3Ec__Iterator3__ctor_m457145830 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeNailPolishIconAvailableU3Ec__Iterator3_MoveNext_m4286149166_MetadataUsageId;
extern "C"  bool U3CMakeNailPolishIconAvailableU3Ec__Iterator3_MoveNext_m4286149166 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_MoveNext_m4286149166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_004c;
	}

IL_0021:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (4.0f), /*hidden argument*/NULL);
		__this->set_U24current_0(L_2);
		bool L_3 = __this->get_U24disposing_1();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_2(1);
	}

IL_0040:
	{
		goto IL_004e;
	}

IL_0045:
	{
		__this->set_U24PC_2((-1));
	}

IL_004c:
	{
		return (bool)0;
	}

IL_004e:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeNailPolishIconAvailableU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199305384 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeNailPolishIconAvailableU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1071451200 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::Dispose()
extern "C"  void U3CMakeNailPolishIconAvailableU3Ec__Iterator3_Dispose_m2859485279 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_1((bool)1);
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeNailPolishIconAvailableU3Ec__Iterator3_Reset_m329402769_MetadataUsageId;
extern "C"  void U3CMakeNailPolishIconAvailableU3Ec__Iterator3_Reset_m329402769 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_Reset_m329402769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::.ctor()
extern "C"  void U3CMakeStylePanelOpenU3Ec__Iterator8__ctor_m2328079727 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeStylePanelOpenU3Ec__Iterator8_MoveNext_m1159711429_MetadataUsageId;
extern "C"  bool U3CMakeStylePanelOpenU3Ec__Iterator8_MoveNext_m1159711429 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeStylePanelOpenU3Ec__Iterator8_MoveNext_m1159711429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_009a;
		}
	}
	{
		goto IL_00d4;
	}

IL_0021:
	{
		KittyHandNFootManager_t1686788969 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_NailCutter_13();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_Perfume_17();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_6 = __this->get_U24this_0();
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = L_6->get_NailPolishIcon_14();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_8 = __this->get_U24this_0();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = L_8->get_FacePowderBrush_16();
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_10 = __this->get_U24this_0();
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = L_10->get_FacePowderMirror_15();
		NullCheck(L_11);
		GameObject_SetActive_m2887581199(L_11, (bool)0, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_12 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_12, (2.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_12);
		bool L_13 = __this->get_U24disposing_2();
		if (L_13)
		{
			goto IL_0095;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0095:
	{
		goto IL_00d6;
	}

IL_009a:
	{
		KittyHandNFootManager_t1686788969 * L_14 = __this->get_U24this_0();
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = L_14->get_CurtainTop_45();
		NullCheck(L_15);
		GameObject_SetActive_m2887581199(L_15, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_16 = __this->get_U24this_0();
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = L_16->get_StylePanel_39();
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)1, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_18 = __this->get_U24this_0();
		NullCheck(L_18);
		GameObject_t1756533147 * L_19 = L_18->get_StyleManager_46();
		NullCheck(L_19);
		GameObject_SetActive_m2887581199(L_19, (bool)1, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00d4:
	{
		return (bool)0;
	}

IL_00d6:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeStylePanelOpenU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3032740225 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeStylePanelOpenU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3285415289 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::Dispose()
extern "C"  void U3CMakeStylePanelOpenU3Ec__Iterator8_Dispose_m1311208818 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeStylePanelOpenU3Ec__Iterator8_Reset_m1258983352_MetadataUsageId;
extern "C"  void U3CMakeStylePanelOpenU3Ec__Iterator8_Reset_m1258983352 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeStylePanelOpenU3Ec__Iterator8_Reset_m1258983352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::.ctor()
extern "C"  void U3CNailCuttingAnimU3Ec__Iterator2__ctor_m3367604021 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::MoveNext()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2923566773;
extern const uint32_t U3CNailCuttingAnimU3Ec__Iterator2_MoveNext_m2194545375_MetadataUsageId;
extern "C"  bool U3CNailCuttingAnimU3Ec__Iterator2_MoveNext_m2194545375 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CNailCuttingAnimU3Ec__Iterator2_MoveNext_m2194545375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0085;
		}
		if (L_1 == 1)
		{
			goto IL_015c;
		}
		if (L_1 == 2)
		{
			goto IL_01c0;
		}
		if (L_1 == 3)
		{
			goto IL_0214;
		}
		if (L_1 == 4)
		{
			goto IL_0278;
		}
		if (L_1 == 5)
		{
			goto IL_02cc;
		}
		if (L_1 == 6)
		{
			goto IL_0330;
		}
		if (L_1 == 7)
		{
			goto IL_0374;
		}
		if (L_1 == 8)
		{
			goto IL_03f4;
		}
		if (L_1 == 9)
		{
			goto IL_0459;
		}
		if (L_1 == 10)
		{
			goto IL_04ba;
		}
		if (L_1 == 11)
		{
			goto IL_051f;
		}
		if (L_1 == 12)
		{
			goto IL_0574;
		}
		if (L_1 == 13)
		{
			goto IL_05d9;
		}
		if (L_1 == 14)
		{
			goto IL_066e;
		}
		if (L_1 == 15)
		{
			goto IL_06d3;
		}
		if (L_1 == 16)
		{
			goto IL_0728;
		}
		if (L_1 == 17)
		{
			goto IL_078d;
		}
		if (L_1 == 18)
		{
			goto IL_07e2;
		}
		if (L_1 == 19)
		{
			goto IL_0847;
		}
		if (L_1 == 20)
		{
			goto IL_08d7;
		}
		if (L_1 == 21)
		{
			goto IL_093c;
		}
		if (L_1 == 22)
		{
			goto IL_0991;
		}
		if (L_1 == 23)
		{
			goto IL_09f6;
		}
		if (L_1 == 24)
		{
			goto IL_0a4b;
		}
		if (L_1 == 25)
		{
			goto IL_0ab0;
		}
		if (L_1 == 26)
		{
			goto IL_0b70;
		}
	}
	{
		goto IL_0bfa;
	}

IL_0085:
	{
		GameObject_t1756533147 * L_2 = __this->get_Obj_0();
		NullCheck(L_2);
		BoxCollider2D_t948534547 * L_3 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_2, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_4 = __this->get_U24this_1();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_NailCutter_13();
		NullCheck(L_5);
		ToolButton_t3848681854 * L_6 = GameObject_GetComponent_TisToolButton_t3848681854_m3256490429(L_5, /*hidden argument*/GameObject_GetComponent_TisToolButton_t3848681854_m3256490429_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_Draggable_4((bool)0);
		KittyHandNFootManager_t1686788969 * L_7 = __this->get_U24this_1();
		NullCheck(L_7);
		MonoBehaviour_CancelInvoke_m2508161963(L_7, _stringLiteral2923566773, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_8 = __this->get_U24this_1();
		GameObject_t1756533147 * L_9 = __this->get_Obj_0();
		NullCheck(L_8);
		L_8->set_CurrentHand_60(L_9);
		GameObject_t1756533147 * L_10 = __this->get_Obj_0();
		KittyHandNFootManager_t1686788969 * L_11 = __this->get_U24this_1();
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = L_11->get_LeftHandCollider_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0335;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_14 = __this->get_U24this_1();
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = L_14->get_NailCutter_13();
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_17 = __this->get_U24this_1();
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = L_17->get_CutterLeftHandRot_23();
		NullCheck(L_16);
		Transform_set_localEulerAngles_m2927195985(L_16, L_18, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_19 = __this->get_U24this_1();
		NullCheck(L_19);
		GameObject_t1756533147 * L_20 = L_19->get_NailCutter_13();
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_22 = __this->get_U24this_1();
		NullCheck(L_22);
		Vector2U5BU5D_t686124026* L_23 = L_22->get_CutterLeftHandPos_19();
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_localPosition_m1026930133(L_21, L_24, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_25 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_25, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_25);
		bool L_26 = __this->get_U24disposing_3();
		if (L_26)
		{
			goto IL_0157;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0157:
	{
		goto IL_0bfc;
	}

IL_015c:
	{
		SoundManager_t654432262 * L_27 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_27);
		SoundManager_PlayNailCutterSound_m913856879(L_27, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_28 = __this->get_Obj_0();
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = GameObject_get_transform_m909382139(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = Transform_GetChild_m3838588184(L_29, 0, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = Component_get_transform_m2697483695(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = Transform_GetChild_m3838588184(L_31, 0, /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_t3275118058 * L_33 = Component_get_transform_m2697483695(L_32, /*hidden argument*/NULL);
		Transform_t3275118058 * L_34 = L_33;
		NullCheck(L_34);
		Vector3_t2243707580  L_35 = Transform_get_localScale_m3074381503(L_34, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_35, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_localScale_m2325460848(L_34, L_36, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_37 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_37, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_37);
		bool L_38 = __this->get_U24disposing_3();
		if (L_38)
		{
			goto IL_01bb;
		}
	}
	{
		__this->set_U24PC_4(2);
	}

IL_01bb:
	{
		goto IL_0bfc;
	}

IL_01c0:
	{
		KittyHandNFootManager_t1686788969 * L_39 = __this->get_U24this_1();
		NullCheck(L_39);
		GameObject_t1756533147 * L_40 = L_39->get_NailCutter_13();
		NullCheck(L_40);
		Transform_t3275118058 * L_41 = GameObject_get_transform_m909382139(L_40, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_42 = __this->get_U24this_1();
		NullCheck(L_42);
		Vector2U5BU5D_t686124026* L_43 = L_42->get_CutterLeftHandPos_19();
		NullCheck(L_43);
		Vector3_t2243707580  L_44 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_localPosition_m1026930133(L_41, L_44, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_45 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_45, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_45);
		bool L_46 = __this->get_U24disposing_3();
		if (L_46)
		{
			goto IL_020f;
		}
	}
	{
		__this->set_U24PC_4(3);
	}

IL_020f:
	{
		goto IL_0bfc;
	}

IL_0214:
	{
		SoundManager_t654432262 * L_47 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_47);
		SoundManager_PlayNailCutterSound_m913856879(L_47, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_48 = __this->get_Obj_0();
		NullCheck(L_48);
		Transform_t3275118058 * L_49 = GameObject_get_transform_m909382139(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_t3275118058 * L_50 = Transform_GetChild_m3838588184(L_49, 0, /*hidden argument*/NULL);
		NullCheck(L_50);
		Transform_t3275118058 * L_51 = Component_get_transform_m2697483695(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_t3275118058 * L_52 = Transform_GetChild_m3838588184(L_51, 1, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_t3275118058 * L_53 = Component_get_transform_m2697483695(L_52, /*hidden argument*/NULL);
		Transform_t3275118058 * L_54 = L_53;
		NullCheck(L_54);
		Vector3_t2243707580  L_55 = Transform_get_localScale_m3074381503(L_54, /*hidden argument*/NULL);
		Vector3_t2243707580  L_56 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_55, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_set_localScale_m2325460848(L_54, L_56, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_57 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_57, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_57);
		bool L_58 = __this->get_U24disposing_3();
		if (L_58)
		{
			goto IL_0273;
		}
	}
	{
		__this->set_U24PC_4(4);
	}

IL_0273:
	{
		goto IL_0bfc;
	}

IL_0278:
	{
		KittyHandNFootManager_t1686788969 * L_59 = __this->get_U24this_1();
		NullCheck(L_59);
		GameObject_t1756533147 * L_60 = L_59->get_NailCutter_13();
		NullCheck(L_60);
		Transform_t3275118058 * L_61 = GameObject_get_transform_m909382139(L_60, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_62 = __this->get_U24this_1();
		NullCheck(L_62);
		Vector2U5BU5D_t686124026* L_63 = L_62->get_CutterLeftHandPos_19();
		NullCheck(L_63);
		Vector3_t2243707580  L_64 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_63)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_set_localPosition_m1026930133(L_61, L_64, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_65 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_65, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_65);
		bool L_66 = __this->get_U24disposing_3();
		if (L_66)
		{
			goto IL_02c7;
		}
	}
	{
		__this->set_U24PC_4(5);
	}

IL_02c7:
	{
		goto IL_0bfc;
	}

IL_02cc:
	{
		SoundManager_t654432262 * L_67 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_67);
		SoundManager_PlayNailCutterSound_m913856879(L_67, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_68 = __this->get_Obj_0();
		NullCheck(L_68);
		Transform_t3275118058 * L_69 = GameObject_get_transform_m909382139(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		Transform_t3275118058 * L_70 = Transform_GetChild_m3838588184(L_69, 0, /*hidden argument*/NULL);
		NullCheck(L_70);
		Transform_t3275118058 * L_71 = Component_get_transform_m2697483695(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		Transform_t3275118058 * L_72 = Transform_GetChild_m3838588184(L_71, 2, /*hidden argument*/NULL);
		NullCheck(L_72);
		Transform_t3275118058 * L_73 = Component_get_transform_m2697483695(L_72, /*hidden argument*/NULL);
		Transform_t3275118058 * L_74 = L_73;
		NullCheck(L_74);
		Vector3_t2243707580  L_75 = Transform_get_localScale_m3074381503(L_74, /*hidden argument*/NULL);
		Vector3_t2243707580  L_76 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_75, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_74);
		Transform_set_localScale_m2325460848(L_74, L_76, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_77 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_77, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_77);
		bool L_78 = __this->get_U24disposing_3();
		if (L_78)
		{
			goto IL_032b;
		}
	}
	{
		__this->set_U24PC_4(6);
	}

IL_032b:
	{
		goto IL_0bfc;
	}

IL_0330:
	{
		goto IL_0847;
	}

IL_0335:
	{
		GameObject_t1756533147 * L_79 = __this->get_Obj_0();
		KittyHandNFootManager_t1686788969 * L_80 = __this->get_U24this_1();
		NullCheck(L_80);
		GameObject_t1756533147 * L_81 = L_80->get_RightHandCollider_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_82 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_79, L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_05de;
		}
	}
	{
		WaitForSeconds_t3839502067 * L_83 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_83, (0.2f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_83);
		bool L_84 = __this->get_U24disposing_3();
		if (L_84)
		{
			goto IL_036f;
		}
	}
	{
		__this->set_U24PC_4(7);
	}

IL_036f:
	{
		goto IL_0bfc;
	}

IL_0374:
	{
		KittyHandNFootManager_t1686788969 * L_85 = __this->get_U24this_1();
		NullCheck(L_85);
		GameObject_t1756533147 * L_86 = L_85->get_NailCutter_13();
		NullCheck(L_86);
		Transform_t3275118058 * L_87 = GameObject_get_transform_m909382139(L_86, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_88 = __this->get_U24this_1();
		NullCheck(L_88);
		Vector3_t2243707580  L_89 = L_88->get_CutterRightHandRot_24();
		NullCheck(L_87);
		Transform_set_localEulerAngles_m2927195985(L_87, L_89, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_90 = __this->get_U24this_1();
		NullCheck(L_90);
		L_90->set_ManipulateRightHand1stNail_67((bool)1);
		KittyHandNFootManager_t1686788969 * L_91 = __this->get_U24this_1();
		NullCheck(L_91);
		GameObject_t1756533147 * L_92 = L_91->get_NailCutter_13();
		NullCheck(L_92);
		Transform_t3275118058 * L_93 = GameObject_get_transform_m909382139(L_92, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_94 = __this->get_U24this_1();
		NullCheck(L_94);
		Vector2U5BU5D_t686124026* L_95 = L_94->get_CutterRightHandPos_20();
		NullCheck(L_95);
		Vector3_t2243707580  L_96 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_95)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		NullCheck(L_93);
		Transform_set_localPosition_m1026930133(L_93, L_96, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_97 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_97, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_97);
		bool L_98 = __this->get_U24disposing_3();
		if (L_98)
		{
			goto IL_03ef;
		}
	}
	{
		__this->set_U24PC_4(8);
	}

IL_03ef:
	{
		goto IL_0bfc;
	}

IL_03f4:
	{
		SoundManager_t654432262 * L_99 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_99);
		SoundManager_PlayNailCutterSound_m913856879(L_99, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_100 = __this->get_Obj_0();
		NullCheck(L_100);
		Transform_t3275118058 * L_101 = GameObject_get_transform_m909382139(L_100, /*hidden argument*/NULL);
		NullCheck(L_101);
		Transform_t3275118058 * L_102 = Transform_GetChild_m3838588184(L_101, 0, /*hidden argument*/NULL);
		NullCheck(L_102);
		Transform_t3275118058 * L_103 = Component_get_transform_m2697483695(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		Transform_t3275118058 * L_104 = Transform_GetChild_m3838588184(L_103, 0, /*hidden argument*/NULL);
		NullCheck(L_104);
		Transform_t3275118058 * L_105 = Component_get_transform_m2697483695(L_104, /*hidden argument*/NULL);
		Transform_t3275118058 * L_106 = L_105;
		NullCheck(L_106);
		Vector3_t2243707580  L_107 = Transform_get_localScale_m3074381503(L_106, /*hidden argument*/NULL);
		Vector3_t2243707580  L_108 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_107, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_106);
		Transform_set_localScale_m2325460848(L_106, L_108, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_109 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_109, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_109);
		bool L_110 = __this->get_U24disposing_3();
		if (L_110)
		{
			goto IL_0454;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)9));
	}

IL_0454:
	{
		goto IL_0bfc;
	}

IL_0459:
	{
		KittyHandNFootManager_t1686788969 * L_111 = __this->get_U24this_1();
		NullCheck(L_111);
		L_111->set_ManipulateRightHand1stNail_67((bool)0);
		KittyHandNFootManager_t1686788969 * L_112 = __this->get_U24this_1();
		NullCheck(L_112);
		GameObject_t1756533147 * L_113 = L_112->get_NailCutter_13();
		NullCheck(L_113);
		Transform_t3275118058 * L_114 = GameObject_get_transform_m909382139(L_113, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_115 = __this->get_U24this_1();
		NullCheck(L_115);
		Vector2U5BU5D_t686124026* L_116 = L_115->get_CutterRightHandPos_20();
		NullCheck(L_116);
		Vector3_t2243707580  L_117 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_116)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		NullCheck(L_114);
		Transform_set_localPosition_m1026930133(L_114, L_117, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_118 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_118, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_118);
		bool L_119 = __this->get_U24disposing_3();
		if (L_119)
		{
			goto IL_04b5;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)10));
	}

IL_04b5:
	{
		goto IL_0bfc;
	}

IL_04ba:
	{
		SoundManager_t654432262 * L_120 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_120);
		SoundManager_PlayNailCutterSound_m913856879(L_120, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_121 = __this->get_Obj_0();
		NullCheck(L_121);
		Transform_t3275118058 * L_122 = GameObject_get_transform_m909382139(L_121, /*hidden argument*/NULL);
		NullCheck(L_122);
		Transform_t3275118058 * L_123 = Transform_GetChild_m3838588184(L_122, 0, /*hidden argument*/NULL);
		NullCheck(L_123);
		Transform_t3275118058 * L_124 = Component_get_transform_m2697483695(L_123, /*hidden argument*/NULL);
		NullCheck(L_124);
		Transform_t3275118058 * L_125 = Transform_GetChild_m3838588184(L_124, 1, /*hidden argument*/NULL);
		NullCheck(L_125);
		Transform_t3275118058 * L_126 = Component_get_transform_m2697483695(L_125, /*hidden argument*/NULL);
		Transform_t3275118058 * L_127 = L_126;
		NullCheck(L_127);
		Vector3_t2243707580  L_128 = Transform_get_localScale_m3074381503(L_127, /*hidden argument*/NULL);
		Vector3_t2243707580  L_129 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_128, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_127);
		Transform_set_localScale_m2325460848(L_127, L_129, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_130 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_130, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_130);
		bool L_131 = __this->get_U24disposing_3();
		if (L_131)
		{
			goto IL_051a;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)11));
	}

IL_051a:
	{
		goto IL_0bfc;
	}

IL_051f:
	{
		KittyHandNFootManager_t1686788969 * L_132 = __this->get_U24this_1();
		NullCheck(L_132);
		GameObject_t1756533147 * L_133 = L_132->get_NailCutter_13();
		NullCheck(L_133);
		Transform_t3275118058 * L_134 = GameObject_get_transform_m909382139(L_133, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_135 = __this->get_U24this_1();
		NullCheck(L_135);
		Vector2U5BU5D_t686124026* L_136 = L_135->get_CutterRightHandPos_20();
		NullCheck(L_136);
		Vector3_t2243707580  L_137 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_136)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), /*hidden argument*/NULL);
		NullCheck(L_134);
		Transform_set_localPosition_m1026930133(L_134, L_137, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_138 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_138, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_138);
		bool L_139 = __this->get_U24disposing_3();
		if (L_139)
		{
			goto IL_056f;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)12));
	}

IL_056f:
	{
		goto IL_0bfc;
	}

IL_0574:
	{
		SoundManager_t654432262 * L_140 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_140);
		SoundManager_PlayNailCutterSound_m913856879(L_140, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_141 = __this->get_Obj_0();
		NullCheck(L_141);
		Transform_t3275118058 * L_142 = GameObject_get_transform_m909382139(L_141, /*hidden argument*/NULL);
		NullCheck(L_142);
		Transform_t3275118058 * L_143 = Transform_GetChild_m3838588184(L_142, 0, /*hidden argument*/NULL);
		NullCheck(L_143);
		Transform_t3275118058 * L_144 = Component_get_transform_m2697483695(L_143, /*hidden argument*/NULL);
		NullCheck(L_144);
		Transform_t3275118058 * L_145 = Transform_GetChild_m3838588184(L_144, 2, /*hidden argument*/NULL);
		NullCheck(L_145);
		Transform_t3275118058 * L_146 = Component_get_transform_m2697483695(L_145, /*hidden argument*/NULL);
		Transform_t3275118058 * L_147 = L_146;
		NullCheck(L_147);
		Vector3_t2243707580  L_148 = Transform_get_localScale_m3074381503(L_147, /*hidden argument*/NULL);
		Vector3_t2243707580  L_149 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_148, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_147);
		Transform_set_localScale_m2325460848(L_147, L_149, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_150 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_150, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_150);
		bool L_151 = __this->get_U24disposing_3();
		if (L_151)
		{
			goto IL_05d4;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)13));
	}

IL_05d4:
	{
		goto IL_0bfc;
	}

IL_05d9:
	{
		goto IL_0847;
	}

IL_05de:
	{
		GameObject_t1756533147 * L_152 = __this->get_Obj_0();
		KittyHandNFootManager_t1686788969 * L_153 = __this->get_U24this_1();
		NullCheck(L_153);
		GameObject_t1756533147 * L_154 = L_153->get_LeftLegCollider_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_155 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_152, L_154, /*hidden argument*/NULL);
		if (!L_155)
		{
			goto IL_0847;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_156 = __this->get_U24this_1();
		NullCheck(L_156);
		GameObject_t1756533147 * L_157 = L_156->get_NailCutter_13();
		NullCheck(L_157);
		Transform_t3275118058 * L_158 = GameObject_get_transform_m909382139(L_157, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_159 = __this->get_U24this_1();
		NullCheck(L_159);
		Vector3_t2243707580  L_160 = L_159->get_CutterLeftLegRot_25();
		NullCheck(L_158);
		Transform_set_localEulerAngles_m2927195985(L_158, L_160, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_161 = __this->get_U24this_1();
		NullCheck(L_161);
		GameObject_t1756533147 * L_162 = L_161->get_NailCutter_13();
		NullCheck(L_162);
		Transform_t3275118058 * L_163 = GameObject_get_transform_m909382139(L_162, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_164 = __this->get_U24this_1();
		NullCheck(L_164);
		Vector2U5BU5D_t686124026* L_165 = L_164->get_CutterLeftLegPos_21();
		NullCheck(L_165);
		Vector3_t2243707580  L_166 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_165)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		NullCheck(L_163);
		Transform_set_localPosition_m1026930133(L_163, L_166, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_167 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_167, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_167);
		bool L_168 = __this->get_U24disposing_3();
		if (L_168)
		{
			goto IL_0669;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)14));
	}

IL_0669:
	{
		goto IL_0bfc;
	}

IL_066e:
	{
		SoundManager_t654432262 * L_169 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_169);
		SoundManager_PlayNailCutterSound_m913856879(L_169, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_170 = __this->get_Obj_0();
		NullCheck(L_170);
		Transform_t3275118058 * L_171 = GameObject_get_transform_m909382139(L_170, /*hidden argument*/NULL);
		NullCheck(L_171);
		Transform_t3275118058 * L_172 = Transform_GetChild_m3838588184(L_171, 0, /*hidden argument*/NULL);
		NullCheck(L_172);
		Transform_t3275118058 * L_173 = Component_get_transform_m2697483695(L_172, /*hidden argument*/NULL);
		NullCheck(L_173);
		Transform_t3275118058 * L_174 = Transform_GetChild_m3838588184(L_173, 0, /*hidden argument*/NULL);
		NullCheck(L_174);
		Transform_t3275118058 * L_175 = Component_get_transform_m2697483695(L_174, /*hidden argument*/NULL);
		Transform_t3275118058 * L_176 = L_175;
		NullCheck(L_176);
		Vector3_t2243707580  L_177 = Transform_get_localScale_m3074381503(L_176, /*hidden argument*/NULL);
		Vector3_t2243707580  L_178 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_177, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_176);
		Transform_set_localScale_m2325460848(L_176, L_178, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_179 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_179, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_179);
		bool L_180 = __this->get_U24disposing_3();
		if (L_180)
		{
			goto IL_06ce;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)15));
	}

IL_06ce:
	{
		goto IL_0bfc;
	}

IL_06d3:
	{
		KittyHandNFootManager_t1686788969 * L_181 = __this->get_U24this_1();
		NullCheck(L_181);
		GameObject_t1756533147 * L_182 = L_181->get_NailCutter_13();
		NullCheck(L_182);
		Transform_t3275118058 * L_183 = GameObject_get_transform_m909382139(L_182, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_184 = __this->get_U24this_1();
		NullCheck(L_184);
		Vector2U5BU5D_t686124026* L_185 = L_184->get_CutterLeftLegPos_21();
		NullCheck(L_185);
		Vector3_t2243707580  L_186 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_185)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		NullCheck(L_183);
		Transform_set_localPosition_m1026930133(L_183, L_186, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_187 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_187, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_187);
		bool L_188 = __this->get_U24disposing_3();
		if (L_188)
		{
			goto IL_0723;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)16));
	}

IL_0723:
	{
		goto IL_0bfc;
	}

IL_0728:
	{
		SoundManager_t654432262 * L_189 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_189);
		SoundManager_PlayNailCutterSound_m913856879(L_189, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_190 = __this->get_Obj_0();
		NullCheck(L_190);
		Transform_t3275118058 * L_191 = GameObject_get_transform_m909382139(L_190, /*hidden argument*/NULL);
		NullCheck(L_191);
		Transform_t3275118058 * L_192 = Transform_GetChild_m3838588184(L_191, 0, /*hidden argument*/NULL);
		NullCheck(L_192);
		Transform_t3275118058 * L_193 = Component_get_transform_m2697483695(L_192, /*hidden argument*/NULL);
		NullCheck(L_193);
		Transform_t3275118058 * L_194 = Transform_GetChild_m3838588184(L_193, 1, /*hidden argument*/NULL);
		NullCheck(L_194);
		Transform_t3275118058 * L_195 = Component_get_transform_m2697483695(L_194, /*hidden argument*/NULL);
		Transform_t3275118058 * L_196 = L_195;
		NullCheck(L_196);
		Vector3_t2243707580  L_197 = Transform_get_localScale_m3074381503(L_196, /*hidden argument*/NULL);
		Vector3_t2243707580  L_198 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_197, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_196);
		Transform_set_localScale_m2325460848(L_196, L_198, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_199 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_199, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_199);
		bool L_200 = __this->get_U24disposing_3();
		if (L_200)
		{
			goto IL_0788;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)17));
	}

IL_0788:
	{
		goto IL_0bfc;
	}

IL_078d:
	{
		KittyHandNFootManager_t1686788969 * L_201 = __this->get_U24this_1();
		NullCheck(L_201);
		GameObject_t1756533147 * L_202 = L_201->get_NailCutter_13();
		NullCheck(L_202);
		Transform_t3275118058 * L_203 = GameObject_get_transform_m909382139(L_202, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_204 = __this->get_U24this_1();
		NullCheck(L_204);
		Vector2U5BU5D_t686124026* L_205 = L_204->get_CutterLeftLegPos_21();
		NullCheck(L_205);
		Vector3_t2243707580  L_206 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_205)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), /*hidden argument*/NULL);
		NullCheck(L_203);
		Transform_set_localPosition_m1026930133(L_203, L_206, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_207 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_207, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_207);
		bool L_208 = __this->get_U24disposing_3();
		if (L_208)
		{
			goto IL_07dd;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)18));
	}

IL_07dd:
	{
		goto IL_0bfc;
	}

IL_07e2:
	{
		SoundManager_t654432262 * L_209 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_209);
		SoundManager_PlayNailCutterSound_m913856879(L_209, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_210 = __this->get_Obj_0();
		NullCheck(L_210);
		Transform_t3275118058 * L_211 = GameObject_get_transform_m909382139(L_210, /*hidden argument*/NULL);
		NullCheck(L_211);
		Transform_t3275118058 * L_212 = Transform_GetChild_m3838588184(L_211, 0, /*hidden argument*/NULL);
		NullCheck(L_212);
		Transform_t3275118058 * L_213 = Component_get_transform_m2697483695(L_212, /*hidden argument*/NULL);
		NullCheck(L_213);
		Transform_t3275118058 * L_214 = Transform_GetChild_m3838588184(L_213, 2, /*hidden argument*/NULL);
		NullCheck(L_214);
		Transform_t3275118058 * L_215 = Component_get_transform_m2697483695(L_214, /*hidden argument*/NULL);
		Transform_t3275118058 * L_216 = L_215;
		NullCheck(L_216);
		Vector3_t2243707580  L_217 = Transform_get_localScale_m3074381503(L_216, /*hidden argument*/NULL);
		Vector3_t2243707580  L_218 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_217, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_216);
		Transform_set_localScale_m2325460848(L_216, L_218, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_219 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_219, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_219);
		bool L_220 = __this->get_U24disposing_3();
		if (L_220)
		{
			goto IL_0842;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)19));
	}

IL_0842:
	{
		goto IL_0bfc;
	}

IL_0847:
	{
		GameObject_t1756533147 * L_221 = __this->get_Obj_0();
		KittyHandNFootManager_t1686788969 * L_222 = __this->get_U24this_1();
		NullCheck(L_222);
		GameObject_t1756533147 * L_223 = L_222->get_RightLegCollider_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_224 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_221, L_223, /*hidden argument*/NULL);
		if (!L_224)
		{
			goto IL_0ab0;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_225 = __this->get_U24this_1();
		NullCheck(L_225);
		GameObject_t1756533147 * L_226 = L_225->get_NailCutter_13();
		NullCheck(L_226);
		Transform_t3275118058 * L_227 = GameObject_get_transform_m909382139(L_226, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_228 = __this->get_U24this_1();
		NullCheck(L_228);
		Vector3_t2243707580  L_229 = L_228->get_CutterRightLegRot_26();
		NullCheck(L_227);
		Transform_set_localEulerAngles_m2927195985(L_227, L_229, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_230 = __this->get_U24this_1();
		NullCheck(L_230);
		GameObject_t1756533147 * L_231 = L_230->get_NailCutter_13();
		NullCheck(L_231);
		Transform_t3275118058 * L_232 = GameObject_get_transform_m909382139(L_231, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_233 = __this->get_U24this_1();
		NullCheck(L_233);
		Vector2U5BU5D_t686124026* L_234 = L_233->get_CutterRightLegPos_22();
		NullCheck(L_234);
		Vector3_t2243707580  L_235 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_234)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		NullCheck(L_232);
		Transform_set_localPosition_m1026930133(L_232, L_235, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_236 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_236, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_236);
		bool L_237 = __this->get_U24disposing_3();
		if (L_237)
		{
			goto IL_08d2;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)20));
	}

IL_08d2:
	{
		goto IL_0bfc;
	}

IL_08d7:
	{
		SoundManager_t654432262 * L_238 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_238);
		SoundManager_PlayNailCutterSound_m913856879(L_238, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_239 = __this->get_Obj_0();
		NullCheck(L_239);
		Transform_t3275118058 * L_240 = GameObject_get_transform_m909382139(L_239, /*hidden argument*/NULL);
		NullCheck(L_240);
		Transform_t3275118058 * L_241 = Transform_GetChild_m3838588184(L_240, 0, /*hidden argument*/NULL);
		NullCheck(L_241);
		Transform_t3275118058 * L_242 = Component_get_transform_m2697483695(L_241, /*hidden argument*/NULL);
		NullCheck(L_242);
		Transform_t3275118058 * L_243 = Transform_GetChild_m3838588184(L_242, 0, /*hidden argument*/NULL);
		NullCheck(L_243);
		Transform_t3275118058 * L_244 = Component_get_transform_m2697483695(L_243, /*hidden argument*/NULL);
		Transform_t3275118058 * L_245 = L_244;
		NullCheck(L_245);
		Vector3_t2243707580  L_246 = Transform_get_localScale_m3074381503(L_245, /*hidden argument*/NULL);
		Vector3_t2243707580  L_247 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_246, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_245);
		Transform_set_localScale_m2325460848(L_245, L_247, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_248 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_248, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_248);
		bool L_249 = __this->get_U24disposing_3();
		if (L_249)
		{
			goto IL_0937;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)21));
	}

IL_0937:
	{
		goto IL_0bfc;
	}

IL_093c:
	{
		KittyHandNFootManager_t1686788969 * L_250 = __this->get_U24this_1();
		NullCheck(L_250);
		GameObject_t1756533147 * L_251 = L_250->get_NailCutter_13();
		NullCheck(L_251);
		Transform_t3275118058 * L_252 = GameObject_get_transform_m909382139(L_251, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_253 = __this->get_U24this_1();
		NullCheck(L_253);
		Vector2U5BU5D_t686124026* L_254 = L_253->get_CutterRightLegPos_22();
		NullCheck(L_254);
		Vector3_t2243707580  L_255 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_254)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		NullCheck(L_252);
		Transform_set_localPosition_m1026930133(L_252, L_255, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_256 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_256, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_256);
		bool L_257 = __this->get_U24disposing_3();
		if (L_257)
		{
			goto IL_098c;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)22));
	}

IL_098c:
	{
		goto IL_0bfc;
	}

IL_0991:
	{
		SoundManager_t654432262 * L_258 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_258);
		SoundManager_PlayNailCutterSound_m913856879(L_258, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_259 = __this->get_Obj_0();
		NullCheck(L_259);
		Transform_t3275118058 * L_260 = GameObject_get_transform_m909382139(L_259, /*hidden argument*/NULL);
		NullCheck(L_260);
		Transform_t3275118058 * L_261 = Transform_GetChild_m3838588184(L_260, 0, /*hidden argument*/NULL);
		NullCheck(L_261);
		Transform_t3275118058 * L_262 = Component_get_transform_m2697483695(L_261, /*hidden argument*/NULL);
		NullCheck(L_262);
		Transform_t3275118058 * L_263 = Transform_GetChild_m3838588184(L_262, 1, /*hidden argument*/NULL);
		NullCheck(L_263);
		Transform_t3275118058 * L_264 = Component_get_transform_m2697483695(L_263, /*hidden argument*/NULL);
		Transform_t3275118058 * L_265 = L_264;
		NullCheck(L_265);
		Vector3_t2243707580  L_266 = Transform_get_localScale_m3074381503(L_265, /*hidden argument*/NULL);
		Vector3_t2243707580  L_267 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_266, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_265);
		Transform_set_localScale_m2325460848(L_265, L_267, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_268 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_268, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_268);
		bool L_269 = __this->get_U24disposing_3();
		if (L_269)
		{
			goto IL_09f1;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)23));
	}

IL_09f1:
	{
		goto IL_0bfc;
	}

IL_09f6:
	{
		KittyHandNFootManager_t1686788969 * L_270 = __this->get_U24this_1();
		NullCheck(L_270);
		GameObject_t1756533147 * L_271 = L_270->get_NailCutter_13();
		NullCheck(L_271);
		Transform_t3275118058 * L_272 = GameObject_get_transform_m909382139(L_271, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_273 = __this->get_U24this_1();
		NullCheck(L_273);
		Vector2U5BU5D_t686124026* L_274 = L_273->get_CutterRightLegPos_22();
		NullCheck(L_274);
		Vector3_t2243707580  L_275 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_274)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), /*hidden argument*/NULL);
		NullCheck(L_272);
		Transform_set_localPosition_m1026930133(L_272, L_275, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_276 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_276, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_276);
		bool L_277 = __this->get_U24disposing_3();
		if (L_277)
		{
			goto IL_0a46;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)24));
	}

IL_0a46:
	{
		goto IL_0bfc;
	}

IL_0a4b:
	{
		SoundManager_t654432262 * L_278 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_278);
		SoundManager_PlayNailCutterSound_m913856879(L_278, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_279 = __this->get_Obj_0();
		NullCheck(L_279);
		Transform_t3275118058 * L_280 = GameObject_get_transform_m909382139(L_279, /*hidden argument*/NULL);
		NullCheck(L_280);
		Transform_t3275118058 * L_281 = Transform_GetChild_m3838588184(L_280, 0, /*hidden argument*/NULL);
		NullCheck(L_281);
		Transform_t3275118058 * L_282 = Component_get_transform_m2697483695(L_281, /*hidden argument*/NULL);
		NullCheck(L_282);
		Transform_t3275118058 * L_283 = Transform_GetChild_m3838588184(L_282, 2, /*hidden argument*/NULL);
		NullCheck(L_283);
		Transform_t3275118058 * L_284 = Component_get_transform_m2697483695(L_283, /*hidden argument*/NULL);
		Transform_t3275118058 * L_285 = L_284;
		NullCheck(L_285);
		Vector3_t2243707580  L_286 = Transform_get_localScale_m3074381503(L_285, /*hidden argument*/NULL);
		Vector3_t2243707580  L_287 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_286, (0.75f), /*hidden argument*/NULL);
		NullCheck(L_285);
		Transform_set_localScale_m2325460848(L_285, L_287, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_288 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_288, (0.75f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_288);
		bool L_289 = __this->get_U24disposing_3();
		if (L_289)
		{
			goto IL_0aab;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)25));
	}

IL_0aab:
	{
		goto IL_0bfc;
	}

IL_0ab0:
	{
		KittyHandNFootManager_t1686788969 * L_290 = __this->get_U24this_1();
		NullCheck(L_290);
		GameObject_t1756533147 * L_291 = L_290->get_HandIcon_11();
		NullCheck(L_291);
		GameObject_SetActive_m2887581199(L_291, (bool)0, /*hidden argument*/NULL);
		goto IL_0b70;
	}

IL_0ac6:
	{
		KittyHandNFootManager_t1686788969 * L_292 = __this->get_U24this_1();
		NullCheck(L_292);
		GameObject_t1756533147 * L_293 = L_292->get_NailCutter_13();
		NullCheck(L_293);
		Transform_t3275118058 * L_294 = GameObject_get_transform_m909382139(L_293, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_295 = __this->get_U24this_1();
		NullCheck(L_295);
		GameObject_t1756533147 * L_296 = L_295->get_NailCutter_13();
		NullCheck(L_296);
		Transform_t3275118058 * L_297 = GameObject_get_transform_m909382139(L_296, /*hidden argument*/NULL);
		NullCheck(L_297);
		Vector3_t2243707580  L_298 = Transform_get_localPosition_m2533925116(L_297, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_299 = __this->get_U24this_1();
		NullCheck(L_299);
		Vector2_t2243707579  L_300 = L_299->get_CutterInitialPos_57();
		Vector3_t2243707580  L_301 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_300, /*hidden argument*/NULL);
		float L_302 = Time_get_smoothDeltaTime_m1294084638(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_303 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_298, L_301, ((float)((float)L_302*(float)(5.0f))), /*hidden argument*/NULL);
		NullCheck(L_294);
		Transform_set_localPosition_m1026930133(L_294, L_303, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_304 = __this->get_U24this_1();
		NullCheck(L_304);
		GameObject_t1756533147 * L_305 = L_304->get_NailCutter_13();
		NullCheck(L_305);
		Transform_t3275118058 * L_306 = GameObject_get_transform_m909382139(L_305, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_307 = __this->get_U24this_1();
		NullCheck(L_307);
		GameObject_t1756533147 * L_308 = L_307->get_NailCutter_13();
		NullCheck(L_308);
		Transform_t3275118058 * L_309 = GameObject_get_transform_m909382139(L_308, /*hidden argument*/NULL);
		NullCheck(L_309);
		Vector3_t2243707580  L_310 = Transform_get_localEulerAngles_m4231787854(L_309, /*hidden argument*/NULL);
		Vector3_t2243707580  L_311 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_312 = Time_get_smoothDeltaTime_m1294084638(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_313 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_310, L_311, ((float)((float)L_312*(float)(5.0f))), /*hidden argument*/NULL);
		NullCheck(L_306);
		Transform_set_localEulerAngles_m2927195985(L_306, L_313, /*hidden argument*/NULL);
		int32_t L_314 = 0;
		Il2CppObject * L_315 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_314);
		__this->set_U24current_2(L_315);
		bool L_316 = __this->get_U24disposing_3();
		if (L_316)
		{
			goto IL_0b6b;
		}
	}
	{
		__this->set_U24PC_4(((int32_t)26));
	}

IL_0b6b:
	{
		goto IL_0bfc;
	}

IL_0b70:
	{
		KittyHandNFootManager_t1686788969 * L_317 = __this->get_U24this_1();
		NullCheck(L_317);
		GameObject_t1756533147 * L_318 = L_317->get_NailCutter_13();
		NullCheck(L_318);
		Transform_t3275118058 * L_319 = GameObject_get_transform_m909382139(L_318, /*hidden argument*/NULL);
		NullCheck(L_319);
		Vector3_t2243707580  L_320 = Transform_get_localPosition_m2533925116(L_319, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_321 = __this->get_U24this_1();
		NullCheck(L_321);
		Vector2_t2243707579  L_322 = L_321->get_CutterInitialPos_57();
		Vector3_t2243707580  L_323 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_322, /*hidden argument*/NULL);
		float L_324 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_320, L_323, /*hidden argument*/NULL);
		if ((((float)L_324) >= ((float)(0.1f))))
		{
			goto IL_0ac6;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_325 = __this->get_U24this_1();
		NullCheck(L_325);
		KittyHandNFootManager_MakeNailPolishIconReady_m2897165473(L_325, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_326 = __this->get_U24this_1();
		NullCheck(L_326);
		GameObject_t1756533147 * L_327 = L_326->get_NailCutter_13();
		NullCheck(L_327);
		SpriteRenderer_t1209076198 * L_328 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_327, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_329;
		memset(&L_329, 0, sizeof(L_329));
		Color__ctor_m1909920690(&L_329, (1.0f), (1.0f), (1.0f), (0.45f), /*hidden argument*/NULL);
		NullCheck(L_328);
		SpriteRenderer_set_color_m2339931967(L_328, L_329, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_330 = __this->get_U24this_1();
		NullCheck(L_330);
		GameObject_t1756533147 * L_331 = L_330->get_NailCutter_13();
		NullCheck(L_331);
		BoxCollider2D_t948534547 * L_332 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_331, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_332);
		Behaviour_set_enabled_m1796096907(L_332, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_0bfa:
	{
		return (bool)0;
	}

IL_0bfc:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CNailCuttingAnimU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4223533475 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNailCuttingAnimU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m4003241739 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::Dispose()
extern "C"  void U3CNailCuttingAnimU3Ec__Iterator2_Dispose_m1019253002 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CNailCuttingAnimU3Ec__Iterator2_Reset_m1681616328_MetadataUsageId;
extern "C"  void U3CNailCuttingAnimU3Ec__Iterator2_Reset_m1681616328 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CNailCuttingAnimU3Ec__Iterator2_Reset_m1681616328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::.ctor()
extern "C"  void U3CPrepareForNextNailCutU3Ec__Iterator4__ctor_m3872460669 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral867000285;
extern Il2CppCodeGenString* _stringLiteral593495257;
extern Il2CppCodeGenString* _stringLiteral2923566773;
extern const uint32_t U3CPrepareForNextNailCutU3Ec__Iterator4_MoveNext_m2643815519_MetadataUsageId;
extern "C"  bool U3CPrepareForNextNailCutU3Ec__Iterator4_MoveNext_m2643815519 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrepareForNextNailCutU3Ec__Iterator4_MoveNext_m2643815519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0031;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
		if (L_1 == 2)
		{
			goto IL_00ee;
		}
		if (L_1 == 3)
		{
			goto IL_0187;
		}
		if (L_1 == 4)
		{
			goto IL_0220;
		}
		if (L_1 == 5)
		{
			goto IL_02a6;
		}
	}
	{
		goto IL_052a;
	}

IL_0031:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (0.55f), /*hidden argument*/NULL);
		__this->set_U24current_5(L_2);
		bool L_3 = __this->get_U24disposing_6();
		if (L_3)
		{
			goto IL_0050;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0050:
	{
		goto IL_052c;
	}

IL_0055:
	{
		KittyHandNFootManager_t1686788969 * L_4 = __this->get_U24this_4();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_Star_32();
		KittyHandNFootManager_t1686788969 * L_6 = __this->get_U24this_4();
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = L_6->get_CurrentHand_60();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Transform_GetChild_m3838588184(L_8, 0, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Transform_GetChild_m3838588184(L_10, 0, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_14 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_15 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_5, L_13, L_14, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_U3CStarObjU3E__0_0(L_15);
		GameObject_t1756533147 * L_16 = __this->get_U3CStarObjU3E__0_0();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = L_17;
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m2638739322(&L_20, (0.03f), (-0.1f), (-0.2f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_position_m2469242620(L_18, L_21, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_22 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_22, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_5(L_22);
		bool L_23 = __this->get_U24disposing_6();
		if (L_23)
		{
			goto IL_00e9;
		}
	}
	{
		__this->set_U24PC_7(2);
	}

IL_00e9:
	{
		goto IL_052c;
	}

IL_00ee:
	{
		KittyHandNFootManager_t1686788969 * L_24 = __this->get_U24this_4();
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = L_24->get_Star_32();
		KittyHandNFootManager_t1686788969 * L_26 = __this->get_U24this_4();
		NullCheck(L_26);
		GameObject_t1756533147 * L_27 = L_26->get_CurrentHand_60();
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = GameObject_get_transform_m909382139(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = Transform_GetChild_m3838588184(L_28, 0, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = Transform_GetChild_m3838588184(L_30, 1, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = Component_get_transform_m2697483695(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t2243707580  L_33 = Transform_get_position_m1104419803(L_32, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_34 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_35 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_25, L_33, L_34, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_U3CStarObj1U3E__1_1(L_35);
		GameObject_t1756533147 * L_36 = __this->get_U3CStarObj1U3E__1_1();
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = GameObject_get_transform_m909382139(L_36, /*hidden argument*/NULL);
		Transform_t3275118058 * L_38 = L_37;
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_position_m1104419803(L_38, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector3__ctor_m2638739322(&L_40, (0.03f), (-0.1f), (-0.2f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_set_position_m2469242620(L_38, L_41, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_42 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_42, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_5(L_42);
		bool L_43 = __this->get_U24disposing_6();
		if (L_43)
		{
			goto IL_0182;
		}
	}
	{
		__this->set_U24PC_7(3);
	}

IL_0182:
	{
		goto IL_052c;
	}

IL_0187:
	{
		KittyHandNFootManager_t1686788969 * L_44 = __this->get_U24this_4();
		NullCheck(L_44);
		GameObject_t1756533147 * L_45 = L_44->get_Star_32();
		KittyHandNFootManager_t1686788969 * L_46 = __this->get_U24this_4();
		NullCheck(L_46);
		GameObject_t1756533147 * L_47 = L_46->get_CurrentHand_60();
		NullCheck(L_47);
		Transform_t3275118058 * L_48 = GameObject_get_transform_m909382139(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_t3275118058 * L_49 = Transform_GetChild_m3838588184(L_48, 0, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_t3275118058 * L_50 = Component_get_transform_m2697483695(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Transform_t3275118058 * L_51 = Transform_GetChild_m3838588184(L_50, 2, /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_t3275118058 * L_52 = Component_get_transform_m2697483695(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Vector3_t2243707580  L_53 = Transform_get_position_m1104419803(L_52, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_54 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_55 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_45, L_53, L_54, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_U3CStarObj2U3E__2_2(L_55);
		GameObject_t1756533147 * L_56 = __this->get_U3CStarObj2U3E__2_2();
		NullCheck(L_56);
		Transform_t3275118058 * L_57 = GameObject_get_transform_m909382139(L_56, /*hidden argument*/NULL);
		Transform_t3275118058 * L_58 = L_57;
		NullCheck(L_58);
		Vector3_t2243707580  L_59 = Transform_get_position_m1104419803(L_58, /*hidden argument*/NULL);
		Vector3_t2243707580  L_60;
		memset(&L_60, 0, sizeof(L_60));
		Vector3__ctor_m2638739322(&L_60, (0.03f), (-0.1f), (-0.2f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_61 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		NullCheck(L_58);
		Transform_set_position_m2469242620(L_58, L_61, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_62 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_62, (0.25f), /*hidden argument*/NULL);
		__this->set_U24current_5(L_62);
		bool L_63 = __this->get_U24disposing_6();
		if (L_63)
		{
			goto IL_021b;
		}
	}
	{
		__this->set_U24PC_7(4);
	}

IL_021b:
	{
		goto IL_052c;
	}

IL_0220:
	{
		KittyHandNFootManager_t1686788969 * L_64 = __this->get_U24this_4();
		NullCheck(L_64);
		GameObject_t1756533147 * L_65 = L_64->get_CurrentHand_60();
		NullCheck(L_65);
		Transform_t3275118058 * L_66 = GameObject_get_transform_m909382139(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		Transform_t3275118058 * L_67 = Transform_GetChild_m3838588184(L_66, 0, /*hidden argument*/NULL);
		NullCheck(L_67);
		GameObject_t1756533147 * L_68 = Component_get_gameObject_m3105766835(L_67, /*hidden argument*/NULL);
		__this->set_U3CTargetHandU3E__3_3(L_68);
		goto IL_02a6;
	}

IL_0246:
	{
		GameObject_t1756533147 * L_69 = __this->get_U3CTargetHandU3E__3_3();
		NullCheck(L_69);
		Transform_t3275118058 * L_70 = GameObject_get_transform_m909382139(L_69, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_71 = __this->get_U3CTargetHandU3E__3_3();
		NullCheck(L_71);
		Transform_t3275118058 * L_72 = GameObject_get_transform_m909382139(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Vector3_t2243707580  L_73 = Transform_get_localScale_m3074381503(L_72, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_74 = __this->get_U24this_4();
		NullCheck(L_74);
		Vector2_t2243707579  L_75 = L_74->get_InitialHandScale_61();
		Vector3_t2243707580  L_76 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		float L_77 = Time_get_smoothDeltaTime_m1294084638(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_78 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_73, L_76, ((float)((float)L_77*(float)(5.0f))), /*hidden argument*/NULL);
		NullCheck(L_70);
		Transform_set_localScale_m2325460848(L_70, L_78, /*hidden argument*/NULL);
		int32_t L_79 = 0;
		Il2CppObject * L_80 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_79);
		__this->set_U24current_5(L_80);
		bool L_81 = __this->get_U24disposing_6();
		if (L_81)
		{
			goto IL_02a1;
		}
	}
	{
		__this->set_U24PC_7(5);
	}

IL_02a1:
	{
		goto IL_052c;
	}

IL_02a6:
	{
		GameObject_t1756533147 * L_82 = __this->get_U3CTargetHandU3E__3_3();
		NullCheck(L_82);
		Transform_t3275118058 * L_83 = GameObject_get_transform_m909382139(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		Vector3_t2243707580  L_84 = Transform_get_localScale_m3074381503(L_83, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_85 = __this->get_U24this_4();
		NullCheck(L_85);
		Vector2_t2243707579  L_86 = L_85->get_InitialHandScale_61();
		Vector3_t2243707580  L_87 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		float L_88 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_84, L_87, /*hidden argument*/NULL);
		if ((((float)L_88) >= ((float)(0.01f))))
		{
			goto IL_0246;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_89 = __this->get_U24this_4();
		NullCheck(L_89);
		L_89->set_StopTouchOnHand_66((bool)0);
		KittyHandNFootManager_t1686788969 * L_90 = __this->get_U24this_4();
		NullCheck(L_90);
		GameObject_t1756533147 * L_91 = L_90->get_HandIconTap_12();
		NullCheck(L_91);
		GameObject_SetActive_m2887581199(L_91, (bool)1, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_92 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_92);
		L_92->set_CanPlayMeow_16((bool)1);
		KittyHandNFootManager_t1686788969 * L_93 = __this->get_U24this_4();
		NullCheck(L_93);
		GameObject_t1756533147 * L_94 = L_93->get_LeftHandCollider_7();
		NullCheck(L_94);
		GameObject_SetActive_m2887581199(L_94, (bool)1, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_95 = __this->get_U24this_4();
		NullCheck(L_95);
		GameObject_t1756533147 * L_96 = L_95->get_RightHandCollider_8();
		NullCheck(L_96);
		GameObject_SetActive_m2887581199(L_96, (bool)1, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_97 = __this->get_U24this_4();
		NullCheck(L_97);
		GameObject_t1756533147 * L_98 = L_97->get_LeftLegCollider_5();
		NullCheck(L_98);
		GameObject_SetActive_m2887581199(L_98, (bool)1, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_99 = __this->get_U24this_4();
		NullCheck(L_99);
		GameObject_t1756533147 * L_100 = L_99->get_RightLegCollider_6();
		NullCheck(L_100);
		GameObject_SetActive_m2887581199(L_100, (bool)1, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_101 = __this->get_U24this_4();
		NullCheck(L_101);
		GameObject_t1756533147 * L_102 = L_101->get_NormalKitty_9();
		NullCheck(L_102);
		SpriteRenderer_t1209076198 * L_103 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_102, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_104;
		memset(&L_104, 0, sizeof(L_104));
		Color__ctor_m1909920690(&L_104, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_103);
		SpriteRenderer_set_color_m2339931967(L_103, L_104, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_105 = __this->get_U24this_4();
		NullCheck(L_105);
		GameObject_t1756533147 * L_106 = L_105->get_EyeAnim_10();
		NullCheck(L_106);
		GameObject_SetActive_m2887581199(L_106, (bool)1, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_107 = __this->get_U24this_4();
		NullCheck(L_107);
		GameObject_t1756533147 * L_108 = L_107->get_NailPolishIcon_14();
		NullCheck(L_108);
		SpriteRenderer_t1209076198 * L_109 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_108, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Color_t2020392075  L_110;
		memset(&L_110, 0, sizeof(L_110));
		Color__ctor_m1909920690(&L_110, (1.0f), (1.0f), (1.0f), (0.35f), /*hidden argument*/NULL);
		NullCheck(L_109);
		SpriteRenderer_set_color_m2339931967(L_109, L_110, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_111 = __this->get_U24this_4();
		NullCheck(L_111);
		L_111->set_Talk1_58(_stringLiteral867000285);
		KittyHandNFootManager_t1686788969 * L_112 = __this->get_U24this_4();
		NullCheck(L_112);
		L_112->set_Talk2_59(_stringLiteral593495257);
		GameObject_t1756533147 * L_113 = __this->get_U3CTargetHandU3E__3_3();
		NullCheck(L_113);
		Transform_t3275118058 * L_114 = GameObject_get_transform_m909382139(L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		Transform_t3275118058 * L_115 = Transform_get_parent_m147407266(L_114, /*hidden argument*/NULL);
		NullCheck(L_115);
		GameObject_t1756533147 * L_116 = Component_get_gameObject_m3105766835(L_115, /*hidden argument*/NULL);
		NullCheck(L_116);
		BoxCollider2D_t948534547 * L_117 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_116, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		NullCheck(L_117);
		Behaviour_set_enabled_m1796096907(L_117, (bool)1, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_118 = __this->get_U24this_4();
		NullCheck(L_118);
		bool L_119 = L_118->get_LeftHandPolishModeOnly_62();
		if (L_119)
		{
			goto IL_042e;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_120 = __this->get_U24this_4();
		NullCheck(L_120);
		GameObject_t1756533147 * L_121 = L_120->get_HandIconTap_12();
		NullCheck(L_121);
		Transform_t3275118058 * L_122 = GameObject_get_transform_m909382139(L_121, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_123 = __this->get_U24this_4();
		NullCheck(L_123);
		Vector3U5BU5D_t1172311765* L_124 = L_123->get_HandTapPos_29();
		NullCheck(L_124);
		NullCheck(L_122);
		Transform_set_localPosition_m1026930133(L_122, (*(Vector3_t2243707580 *)((L_124)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		goto IL_04f9;
	}

IL_042e:
	{
		KittyHandNFootManager_t1686788969 * L_125 = __this->get_U24this_4();
		NullCheck(L_125);
		bool L_126 = L_125->get_RightHandPolishModeOnly_63();
		if (L_126)
		{
			goto IL_046e;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_127 = __this->get_U24this_4();
		NullCheck(L_127);
		GameObject_t1756533147 * L_128 = L_127->get_HandIconTap_12();
		NullCheck(L_128);
		Transform_t3275118058 * L_129 = GameObject_get_transform_m909382139(L_128, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_130 = __this->get_U24this_4();
		NullCheck(L_130);
		Vector3U5BU5D_t1172311765* L_131 = L_130->get_HandTapPos_29();
		NullCheck(L_131);
		NullCheck(L_129);
		Transform_set_localPosition_m1026930133(L_129, (*(Vector3_t2243707580 *)((L_131)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		goto IL_04f9;
	}

IL_046e:
	{
		KittyHandNFootManager_t1686788969 * L_132 = __this->get_U24this_4();
		NullCheck(L_132);
		bool L_133 = L_132->get_LeftLegPolishModeOnly_64();
		if (L_133)
		{
			goto IL_04ae;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_134 = __this->get_U24this_4();
		NullCheck(L_134);
		GameObject_t1756533147 * L_135 = L_134->get_HandIconTap_12();
		NullCheck(L_135);
		Transform_t3275118058 * L_136 = GameObject_get_transform_m909382139(L_135, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_137 = __this->get_U24this_4();
		NullCheck(L_137);
		Vector3U5BU5D_t1172311765* L_138 = L_137->get_HandTapPos_29();
		NullCheck(L_138);
		NullCheck(L_136);
		Transform_set_localPosition_m1026930133(L_136, (*(Vector3_t2243707580 *)((L_138)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), /*hidden argument*/NULL);
		goto IL_04f9;
	}

IL_04ae:
	{
		KittyHandNFootManager_t1686788969 * L_139 = __this->get_U24this_4();
		NullCheck(L_139);
		bool L_140 = L_139->get_RightLegPolishModeOnly_65();
		if (L_140)
		{
			goto IL_04ee;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_141 = __this->get_U24this_4();
		NullCheck(L_141);
		GameObject_t1756533147 * L_142 = L_141->get_HandIconTap_12();
		NullCheck(L_142);
		Transform_t3275118058 * L_143 = GameObject_get_transform_m909382139(L_142, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_144 = __this->get_U24this_4();
		NullCheck(L_144);
		Vector3U5BU5D_t1172311765* L_145 = L_144->get_HandTapPos_29();
		NullCheck(L_145);
		NullCheck(L_143);
		Transform_set_localPosition_m1026930133(L_143, (*(Vector3_t2243707580 *)((L_145)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))), /*hidden argument*/NULL);
		goto IL_04f9;
	}

IL_04ee:
	{
		KittyHandNFootManager_t1686788969 * L_146 = __this->get_U24this_4();
		NullCheck(L_146);
		KittyHandNFootManager_MakeFacePowderOn_m2107715037(L_146, /*hidden argument*/NULL);
	}

IL_04f9:
	{
		KittyHandNFootManager_t1686788969 * L_147 = __this->get_U24this_4();
		NullCheck(L_147);
		MonoBehaviour_CancelInvoke_m2508161963(L_147, _stringLiteral2923566773, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_148 = __this->get_U24this_4();
		NullCheck(L_148);
		MonoBehaviour_InvokeRepeating_m3468262484(L_148, _stringLiteral2923566773, (0.2f), (5.5f), /*hidden argument*/NULL);
		__this->set_U24PC_7((-1));
	}

IL_052a:
	{
		return (bool)0;
	}

IL_052c:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPrepareForNextNailCutU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m715441427 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrepareForNextNailCutU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m660017275 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::Dispose()
extern "C"  void U3CPrepareForNextNailCutU3Ec__Iterator4_Dispose_m3862261636 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrepareForNextNailCutU3Ec__Iterator4_Reset_m1984076994_MetadataUsageId;
extern "C"  void U3CPrepareForNextNailCutU3Ec__Iterator4_Reset_m1984076994 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrepareForNextNailCutU3Ec__Iterator4_Reset_m1984076994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<RestartButtonUp>c__IteratorA::.ctor()
extern "C"  void U3CRestartButtonUpU3Ec__IteratorA__ctor_m3300642729 (U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<RestartButtonUp>c__IteratorA::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CRestartButtonUpU3Ec__IteratorA_MoveNext_m3383568575_MetadataUsageId;
extern "C"  bool U3CRestartButtonUpU3Ec__IteratorA_MoveNext_m3383568575 (U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRestartButtonUpU3Ec__IteratorA_MoveNext_m3383568575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_006a;
		}
	}
	{
		goto IL_0078;
	}

IL_0021:
	{
		KittyHandNFootManager_t1686788969 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_RestartButton_42();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = L_4;
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_localScale_m3074381503(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, (1.4f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localScale_m2325460848(L_5, L_7, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_8 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_8, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_8);
		bool L_9 = __this->get_U24disposing_2();
		if (L_9)
		{
			goto IL_0065;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0065:
	{
		goto IL_007a;
	}

IL_006a:
	{
		SceneManager_LoadSceneAsync_m2131479547(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0078:
	{
		return (bool)0;
	}

IL_007a:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<RestartButtonUp>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRestartButtonUpU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m829276411 (U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<RestartButtonUp>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRestartButtonUpU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m2302795043 (U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<RestartButtonUp>c__IteratorA::Dispose()
extern "C"  void U3CRestartButtonUpU3Ec__IteratorA_Dispose_m1682347210 (U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<RestartButtonUp>c__IteratorA::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CRestartButtonUpU3Ec__IteratorA_Reset_m2040250252_MetadataUsageId;
extern "C"  void U3CRestartButtonUpU3Ec__IteratorA_Reset_m2040250252 (U3CRestartButtonUpU3Ec__IteratorA_t2431284672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRestartButtonUpU3Ec__IteratorA_Reset_m2040250252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<ShareScreenshot>c__IteratorC::.ctor()
extern "C"  void U3CShareScreenshotU3Ec__IteratorC__ctor_m3064057836 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<ShareScreenshot>c__IteratorC::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t4251328308_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_GetStatic_TisString_t_m422203629_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_Call_TisAndroidJavaObject_t4251328308_m4199176621_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m3890456357_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m273477134_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3600966477;
extern Il2CppCodeGenString* _stringLiteral3282854815;
extern Il2CppCodeGenString* _stringLiteral3042370822;
extern Il2CppCodeGenString* _stringLiteral3758962344;
extern Il2CppCodeGenString* _stringLiteral2795434125;
extern Il2CppCodeGenString* _stringLiteral1363800400;
extern Il2CppCodeGenString* _stringLiteral2609891637;
extern Il2CppCodeGenString* _stringLiteral2494949592;
extern Il2CppCodeGenString* _stringLiteral3546647819;
extern Il2CppCodeGenString* _stringLiteral1412621585;
extern Il2CppCodeGenString* _stringLiteral3720301650;
extern Il2CppCodeGenString* _stringLiteral580010394;
extern Il2CppCodeGenString* _stringLiteral3075639932;
extern Il2CppCodeGenString* _stringLiteral2584981958;
extern Il2CppCodeGenString* _stringLiteral3855509597;
extern const uint32_t U3CShareScreenshotU3Ec__IteratorC_MoveNext_m4144719932_MetadataUsageId;
extern "C"  bool U3CShareScreenshotU3Ec__IteratorC_MoveNext_m4144719932 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShareScreenshotU3Ec__IteratorC_MoveNext_m4144719932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	DateTime_t693205669  V_1;
	memset(&V_1, 0, sizeof(V_1));
	AndroidJavaClass_t2973420583 * V_2 = NULL;
	AndroidJavaObject_t4251328308 * V_3 = NULL;
	AndroidJavaClass_t2973420583 * V_4 = NULL;
	AndroidJavaObject_t4251328308 * V_5 = NULL;
	AndroidJavaClass_t2973420583 * V_6 = NULL;
	AndroidJavaObject_t4251328308 * V_7 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
		if (L_1 == 2)
		{
			goto IL_0098;
		}
		if (L_1 == 3)
		{
			goto IL_00c8;
		}
	}
	{
		goto IL_027a;
	}

IL_0029:
	{
		KittyHandNFootManager_t1686788969 * L_2 = __this->get_U24this_2();
		NullCheck(L_2);
		L_2->set_isProcessing_68((bool)1);
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_3);
		bool L_4 = __this->get_U24disposing_4();
		if (L_4)
		{
			goto IL_0054;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0054:
	{
		goto IL_027c;
	}

IL_0059:
	{
		KittyHandNFootManager_t1686788969 * L_5 = __this->get_U24this_2();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = L_5->get_SnapTransparentBG_49();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)1, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_7 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_7);
		SoundManager_PlayClickSound_m1792418272(L_7, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_8 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_8, (0.2f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_8);
		bool L_9 = __this->get_U24disposing_4();
		if (L_9)
		{
			goto IL_0093;
		}
	}
	{
		__this->set_U24PC_5(2);
	}

IL_0093:
	{
		goto IL_027c;
	}

IL_0098:
	{
		KittyHandNFootManager_t1686788969 * L_10 = __this->get_U24this_2();
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = L_10->get_SnapTransparentBG_49();
		NullCheck(L_11);
		GameObject_SetActive_m2887581199(L_11, (bool)0, /*hidden argument*/NULL);
		WaitForEndOfFrame_t1785723201 * L_12 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_12, /*hidden argument*/NULL);
		__this->set_U24current_3(L_12);
		bool L_13 = __this->get_U24disposing_4();
		if (L_13)
		{
			goto IL_00c3;
		}
	}
	{
		__this->set_U24PC_5(3);
	}

IL_00c3:
	{
		goto IL_027c;
	}

IL_00c8:
	{
		int32_t L_14 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_16 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_16, L_14, L_15, 3, (bool)1, /*hidden argument*/NULL);
		__this->set_U3CscreenTextureU3E__0_0(L_16);
		Texture2D_t3542995729 * L_17 = __this->get_U3CscreenTextureU3E__0_0();
		int32_t L_18 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_19 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Rect__ctor_m1220545469(&L_20, (0.0f), (0.0f), (((float)((float)L_18))), (((float)((float)L_19))), /*hidden argument*/NULL);
		NullCheck(L_17);
		Texture2D_ReadPixels_m1120832672(L_17, L_20, 0, 0, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_21 = __this->get_U3CscreenTextureU3E__0_0();
		NullCheck(L_21);
		Texture2D_Apply_m3543341930(L_21, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_22 = __this->get_U3CscreenTextureU3E__0_0();
		NullCheck(L_22);
		ByteU5BU5D_t3397334013* L_23 = Texture2D_EncodeToPNG_m2680110528(L_22, /*hidden argument*/NULL);
		__this->set_U3CdataToSaveU3E__1_1(L_23);
		KittyHandNFootManager_t1686788969 * L_24 = __this->get_U24this_2();
		String_t* L_25 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_26 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_26;
		String_t* L_27 = DateTime_ToString_m1473013667((&V_1), _stringLiteral3600966477, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m2596409543(NULL /*static, unused*/, L_27, _stringLiteral3282854815, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		String_t* L_29 = Path_Combine_m3185811654(NULL /*static, unused*/, L_25, L_28, /*hidden argument*/NULL);
		NullCheck(L_24);
		L_24->set_destination_69(L_29);
		KittyHandNFootManager_t1686788969 * L_30 = __this->get_U24this_2();
		NullCheck(L_30);
		String_t* L_31 = L_30->get_destination_69();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_32 = __this->get_U24this_2();
		NullCheck(L_32);
		String_t* L_33 = L_32->get_destination_69();
		ByteU5BU5D_t3397334013* L_34 = __this->get_U3CdataToSaveU3E__1_1();
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		bool L_35 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_0267;
		}
	}
	{
		AndroidJavaClass_t2973420583 * L_36 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_36, _stringLiteral3042370822, /*hidden argument*/NULL);
		V_2 = L_36;
		AndroidJavaObject_t4251328308 * L_37 = (AndroidJavaObject_t4251328308 *)il2cpp_codegen_object_new(AndroidJavaObject_t4251328308_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m1076535321(L_37, _stringLiteral3042370822, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_3 = L_37;
		AndroidJavaObject_t4251328308 * L_38 = V_3;
		ObjectU5BU5D_t3614634134* L_39 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		AndroidJavaClass_t2973420583 * L_40 = V_2;
		NullCheck(L_40);
		String_t* L_41 = AndroidJavaObject_GetStatic_TisString_t_m422203629(L_40, _stringLiteral2795434125, /*hidden argument*/AndroidJavaObject_GetStatic_TisString_t_m422203629_MethodInfo_var);
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, L_41);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_41);
		NullCheck(L_38);
		AndroidJavaObject_Call_TisAndroidJavaObject_t4251328308_m4199176621(L_38, _stringLiteral3758962344, L_39, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t4251328308_m4199176621_MethodInfo_var);
		AndroidJavaClass_t2973420583 * L_42 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_42, _stringLiteral1363800400, /*hidden argument*/NULL);
		V_4 = L_42;
		AndroidJavaClass_t2973420583 * L_43 = V_4;
		ObjectU5BU5D_t3614634134* L_44 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		KittyHandNFootManager_t1686788969 * L_45 = __this->get_U24this_2();
		NullCheck(L_45);
		String_t* L_46 = L_45->get_destination_69();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2494949592, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_47);
		NullCheck(L_43);
		AndroidJavaObject_t4251328308 * L_48 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m3890456357(L_43, _stringLiteral2609891637, L_44, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m3890456357_MethodInfo_var);
		V_5 = L_48;
		AndroidJavaObject_t4251328308 * L_49 = V_3;
		ObjectU5BU5D_t3614634134* L_50 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		AndroidJavaClass_t2973420583 * L_51 = V_2;
		NullCheck(L_51);
		String_t* L_52 = AndroidJavaObject_GetStatic_TisString_t_m422203629(L_51, _stringLiteral1412621585, /*hidden argument*/AndroidJavaObject_GetStatic_TisString_t_m422203629_MethodInfo_var);
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, L_52);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_52);
		ObjectU5BU5D_t3614634134* L_53 = L_50;
		AndroidJavaObject_t4251328308 * L_54 = V_5;
		NullCheck(L_53);
		ArrayElementTypeCheck (L_53, L_54);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_54);
		NullCheck(L_49);
		AndroidJavaObject_Call_TisAndroidJavaObject_t4251328308_m4199176621(L_49, _stringLiteral3546647819, L_53, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t4251328308_m4199176621_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_55 = V_3;
		ObjectU5BU5D_t3614634134* L_56 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, _stringLiteral580010394);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral580010394);
		NullCheck(L_55);
		AndroidJavaObject_Call_TisAndroidJavaObject_t4251328308_m4199176621(L_55, _stringLiteral3720301650, L_56, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t4251328308_m4199176621_MethodInfo_var);
		AndroidJavaClass_t2973420583 * L_57 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_57, _stringLiteral3075639932, /*hidden argument*/NULL);
		V_6 = L_57;
		AndroidJavaClass_t2973420583 * L_58 = V_6;
		NullCheck(L_58);
		AndroidJavaObject_t4251328308 * L_59 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m273477134(L_58, _stringLiteral2584981958, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m273477134_MethodInfo_var);
		V_7 = L_59;
		AndroidJavaObject_t4251328308 * L_60 = V_7;
		ObjectU5BU5D_t3614634134* L_61 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		AndroidJavaObject_t4251328308 * L_62 = V_3;
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_62);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_62);
		NullCheck(L_60);
		AndroidJavaObject_Call_m3681854287(L_60, _stringLiteral3855509597, L_61, /*hidden argument*/NULL);
	}

IL_0267:
	{
		KittyHandNFootManager_t1686788969 * L_63 = __this->get_U24this_2();
		NullCheck(L_63);
		L_63->set_isProcessing_68((bool)0);
		__this->set_U24PC_5((-1));
	}

IL_027a:
	{
		return (bool)0;
	}

IL_027c:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<ShareScreenshot>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShareScreenshotU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m649825954 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<ShareScreenshot>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShareScreenshotU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m470195962 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<ShareScreenshot>c__IteratorC::Dispose()
extern "C"  void U3CShareScreenshotU3Ec__IteratorC_Dispose_m3289387241 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<ShareScreenshot>c__IteratorC::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CShareScreenshotU3Ec__IteratorC_Reset_m1221017159_MetadataUsageId;
extern "C"  void U3CShareScreenshotU3Ec__IteratorC_Reset_m1221017159 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShareScreenshotU3Ec__IteratorC_Reset_m1221017159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m976319080 (U3CStartU3Ec__Iterator0_t3126961537 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<Start>c__Iterator0::MoveNext()
extern Il2CppClass* KittyHandNFootManager_t1686788969_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2923566773;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m2438485356_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2438485356 (U3CStartU3Ec__Iterator0_t3126961537 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m2438485356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00d7;
	}

IL_0021:
	{
		KittyHandNFootManager_t1686788969 * L_2 = __this->get_U24this_0();
		((KittyHandNFootManager_t1686788969_StaticFields*)KittyHandNFootManager_t1686788969_il2cpp_TypeInfo_var->static_fields)->set_instance_52(L_2);
		KittyHandNFootManager_t1686788969 * L_3 = __this->get_U24this_0();
		KittyHandNFootManager_t1686788969 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_NailCutter_13();
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_CutterInitialPos_57(L_8);
		KittyHandNFootManager_t1686788969 * L_9 = __this->get_U24this_0();
		KittyHandNFootManager_t1686788969 * L_10 = __this->get_U24this_0();
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = L_10->get_LeftHandCollider_7();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Transform_GetChild_m3838588184(L_12, 0, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_localScale_m3074381503(L_14, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_InitialHandScale_61(L_16);
		WaitForSeconds_t3839502067 * L_17 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_17, (2.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_17);
		bool L_18 = __this->get_U24disposing_2();
		if (L_18)
		{
			goto IL_00a0;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_00a0:
	{
		goto IL_00d9;
	}

IL_00a5:
	{
		KittyHandNFootManager_t1686788969 * L_19 = __this->get_U24this_0();
		NullCheck(L_19);
		GameObject_t1756533147 * L_20 = L_19->get_CurtainTop_45();
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)0, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_21 = __this->get_U24this_0();
		NullCheck(L_21);
		MonoBehaviour_InvokeRepeating_m3468262484(L_21, _stringLiteral2923566773, (0.2f), (5.5f), /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00d7:
	{
		return (bool)0;
	}

IL_00d9:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3056788946 (U3CStartU3Ec__Iterator0_t3126961537 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3625643882 (U3CStartU3Ec__Iterator0_t3126961537 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m707751819 (U3CStartU3Ec__Iterator0_t3126961537 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<Start>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m1540201837_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1540201837 (U3CStartU3Ec__Iterator0_t3126961537 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m1540201837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void KittyHandNFootManager/<TakeSnap>c__IteratorB::.ctor()
extern "C"  void U3CTakeSnapU3Ec__IteratorB__ctor_m1157678565 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean KittyHandNFootManager/<TakeSnap>c__IteratorB::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3600966477;
extern Il2CppCodeGenString* _stringLiteral3282854815;
extern const uint32_t U3CTakeSnapU3Ec__IteratorB_MoveNext_m1149126071_MetadataUsageId;
extern "C"  bool U3CTakeSnapU3Ec__IteratorB_MoveNext_m1149126071 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeSnapU3Ec__IteratorB_MoveNext_m1149126071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	DateTime_t693205669  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_005e;
		}
		if (L_1 == 2)
		{
			goto IL_009d;
		}
		if (L_1 == 3)
		{
			goto IL_00cd;
		}
	}
	{
		goto IL_0186;
	}

IL_0029:
	{
		KittyHandNFootManager_t1686788969 * L_2 = __this->get_U24this_2();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_LastPanel_48();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_4);
		bool L_5 = __this->get_U24disposing_4();
		if (L_5)
		{
			goto IL_0059;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0059:
	{
		goto IL_0188;
	}

IL_005e:
	{
		KittyHandNFootManager_t1686788969 * L_6 = __this->get_U24this_2();
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = L_6->get_SnapTransparentBG_49();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)1, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_8 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_8);
		SoundManager_PlayClickSound_m1792418272(L_8, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_9 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_9, (0.125f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_9);
		bool L_10 = __this->get_U24disposing_4();
		if (L_10)
		{
			goto IL_0098;
		}
	}
	{
		__this->set_U24PC_5(2);
	}

IL_0098:
	{
		goto IL_0188;
	}

IL_009d:
	{
		KittyHandNFootManager_t1686788969 * L_11 = __this->get_U24this_2();
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = L_11->get_SnapTransparentBG_49();
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)0, /*hidden argument*/NULL);
		WaitForEndOfFrame_t1785723201 * L_13 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_13, /*hidden argument*/NULL);
		__this->set_U24current_3(L_13);
		bool L_14 = __this->get_U24disposing_4();
		if (L_14)
		{
			goto IL_00c8;
		}
	}
	{
		__this->set_U24PC_5(3);
	}

IL_00c8:
	{
		goto IL_0188;
	}

IL_00cd:
	{
		int32_t L_15 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_17 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_17, L_15, L_16, 3, (bool)1, /*hidden argument*/NULL);
		__this->set_U3CscreenTextureU3E__0_0(L_17);
		Texture2D_t3542995729 * L_18 = __this->get_U3CscreenTextureU3E__0_0();
		int32_t L_19 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Rect__ctor_m1220545469(&L_21, (0.0f), (0.0f), (((float)((float)L_19))), (((float)((float)L_20))), /*hidden argument*/NULL);
		NullCheck(L_18);
		Texture2D_ReadPixels_m1120832672(L_18, L_21, 0, 0, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_22 = __this->get_U3CscreenTextureU3E__0_0();
		NullCheck(L_22);
		Texture2D_Apply_m3543341930(L_22, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_23 = __this->get_U3CscreenTextureU3E__0_0();
		NullCheck(L_23);
		ByteU5BU5D_t3397334013* L_24 = Texture2D_EncodeToPNG_m2680110528(L_23, /*hidden argument*/NULL);
		__this->set_U3CdataToSaveU3E__1_1(L_24);
		KittyHandNFootManager_t1686788969 * L_25 = __this->get_U24this_2();
		String_t* L_26 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_27 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_27;
		String_t* L_28 = DateTime_ToString_m1473013667((&V_1), _stringLiteral3600966477, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m2596409543(NULL /*static, unused*/, L_28, _stringLiteral3282854815, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		String_t* L_30 = Path_Combine_m3185811654(NULL /*static, unused*/, L_26, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		L_25->set_destination_69(L_30);
		KittyHandNFootManager_t1686788969 * L_31 = __this->get_U24this_2();
		NullCheck(L_31);
		String_t* L_32 = L_31->get_destination_69();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		KittyHandNFootManager_t1686788969 * L_33 = __this->get_U24this_2();
		NullCheck(L_33);
		String_t* L_34 = L_33->get_destination_69();
		ByteU5BU5D_t3397334013* L_35 = __this->get_U3CdataToSaveU3E__1_1();
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_0186:
	{
		return (bool)0;
	}

IL_0188:
	{
		return (bool)1;
	}
}
// System.Object KittyHandNFootManager/<TakeSnap>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTakeSnapU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1427535143 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object KittyHandNFootManager/<TakeSnap>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTakeSnapU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m4096029999 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void KittyHandNFootManager/<TakeSnap>c__IteratorB::Dispose()
extern "C"  void U3CTakeSnapU3Ec__IteratorB_Dispose_m43428456 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void KittyHandNFootManager/<TakeSnap>c__IteratorB::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTakeSnapU3Ec__IteratorB_Reset_m2599214686_MetadataUsageId;
extern "C"  void U3CTakeSnapU3Ec__IteratorB_Reset_m2599214686 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeSnapU3Ec__IteratorB_Reset_m2599214686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void LiveVideo::.ctor()
extern "C"  void LiveVideo__ctor_m2230653968 (LiveVideo_t867630853 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LiveVideo::Start()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* WebCamTexture_t1079476942_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t257310565_m1312615893_MethodInfo_var;
extern const uint32_t LiveVideo_Start_m2651110432_MetadataUsageId;
extern "C"  void LiveVideo_Start_m2651110432 (LiveVideo_t867630853 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LiveVideo_Start_m2651110432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WebCamDeviceU5BU5D_t2903637840* V_0 = NULL;
	int32_t V_1 = 0;
	WebCamTexture_t1079476942 * V_2 = NULL;
	{
		WebCamDeviceU5BU5D_t2903637840* L_0 = WebCamTexture_get_devices_m4137524804(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0022;
	}

IL_000d:
	{
		WebCamDeviceU5BU5D_t2903637840* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		String_t* L_3 = WebCamDevice_get_name_m1117076425(((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) < ((int32_t)1)))
		{
			goto IL_000d;
		}
	}
	{
		WebCamTexture_t1079476942 * L_6 = (WebCamTexture_t1079476942 *)il2cpp_codegen_object_new(WebCamTexture_t1079476942_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m1125343005(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		GameObject_t1756533147 * L_7 = __this->get_Obj_2();
		NullCheck(L_7);
		Renderer_t257310565 * L_8 = GameObject_GetComponent_TisRenderer_t257310565_m1312615893(L_7, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m1312615893_MethodInfo_var);
		NullCheck(L_8);
		Material_t193706927 * L_9 = Renderer_get_material_m2553789785(L_8, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_10 = V_2;
		NullCheck(L_9);
		Material_set_mainTexture_m3584203343(L_9, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = __this->get_Obj1_3();
		NullCheck(L_11);
		Renderer_t257310565 * L_12 = GameObject_GetComponent_TisRenderer_t257310565_m1312615893(L_11, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m1312615893_MethodInfo_var);
		NullCheck(L_12);
		Material_t193706927 * L_13 = Renderer_get_material_m2553789785(L_12, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_14 = V_2;
		NullCheck(L_13);
		Material_set_mainTexture_m3584203343(L_13, L_14, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_15 = V_2;
		NullCheck(L_15);
		WebCamTexture_Play_m1997372813(L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LiveVideo::Update()
extern "C"  void LiveVideo_Update_m2852551909 (LiveVideo_t867630853 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MaskCamera::.ctor()
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern const uint32_t MaskCamera__ctor_m1205289458_MetadataUsageId;
extern "C"  void MaskCamera__ctor_m1205289458 (MaskCamera_t4194554627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MaskCamera__ctor_m1205289458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_VX_14(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)108))));
		__this->set_VY_15(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)76))));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MaskCamera::CutHole(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void MaskCamera_CutHole_m3971818326 (MaskCamera_t4194554627 * __this, Vector2_t2243707579  ___imageSize0, Vector2_t2243707579  ___imageLocalPosition1, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		Rect__ctor_m1220545469((&V_0), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		float L_0 = (&___imageLocalPosition1)->get_x_0();
		Material_t193706927 * L_1 = __this->get_EraserMaterial_2();
		NullCheck(L_1);
		Texture_t2243626319 * L_2 = Material_get_mainTexture_m432794412(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		float L_4 = (&___imageSize0)->get_x_0();
		float L_5 = (&___imageLocalPosition1)->get_y_1();
		Material_t193706927 * L_6 = __this->get_EraserMaterial_2();
		NullCheck(L_6);
		Texture_t2243626319 * L_7 = Material_get_mainTexture_m432794412(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_7);
		float L_9 = (&___imageSize0)->get_y_1();
		Material_t193706927 * L_10 = __this->get_EraserMaterial_2();
		NullCheck(L_10);
		Texture_t2243626319 * L_11 = Material_get_mainTexture_m432794412(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_11);
		float L_13 = (&___imageSize0)->get_x_0();
		Material_t193706927 * L_14 = __this->get_EraserMaterial_2();
		NullCheck(L_14);
		Texture_t2243626319 * L_15 = Material_get_mainTexture_m432794412(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_15);
		float L_17 = (&___imageSize0)->get_y_1();
		Rect__ctor_m1220545469((&V_1), ((float)((float)((float)((float)L_0-(float)((float)((float)(0.5f)*(float)(((float)((float)L_3)))))))/(float)L_4)), ((float)((float)((float)((float)L_5-(float)((float)((float)(0.5f)*(float)(((float)((float)L_8)))))))/(float)L_9)), ((float)((float)(((float)((float)L_12)))/(float)L_13)), ((float)((float)(((float)((float)L_16)))/(float)L_17)), /*hidden argument*/NULL);
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m3764403102(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0274;
	}

IL_00b3:
	{
		Material_t193706927 * L_18 = __this->get_EraserMaterial_2();
		int32_t L_19 = V_2;
		NullCheck(L_18);
		Material_SetPass_m2448940266(L_18, L_19, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		Color_t2020392075  L_20 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Color_m3254827061(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		float L_21 = Rect_get_xMin_m1161102488((&V_0), /*hidden argument*/NULL);
		float L_22 = Rect_get_yMax_m2915146103((&V_0), /*hidden argument*/NULL);
		GL_TexCoord2_m86781742(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		float L_23 = Rect_get_xMin_m1161102488((&V_1), /*hidden argument*/NULL);
		float L_24 = Rect_get_yMax_m2915146103((&V_1), /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_23, L_24, (0.0f), /*hidden argument*/NULL);
		float L_25 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		float L_26 = Rect_get_yMax_m2915146103((&V_0), /*hidden argument*/NULL);
		GL_TexCoord2_m86781742(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		float L_27 = Rect_get_xMax_m2915145014((&V_1), /*hidden argument*/NULL);
		float L_28 = Rect_get_yMax_m2915146103((&V_1), /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_27, L_28, (0.0f), /*hidden argument*/NULL);
		float L_29 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		float L_30 = Rect_get_yMin_m1161103577((&V_0), /*hidden argument*/NULL);
		GL_TexCoord2_m86781742(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		float L_31 = Rect_get_xMax_m2915145014((&V_1), /*hidden argument*/NULL);
		float L_32 = Rect_get_yMin_m1161103577((&V_1), /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_31, L_32, (0.0f), /*hidden argument*/NULL);
		float L_33 = Rect_get_xMin_m1161102488((&V_0), /*hidden argument*/NULL);
		float L_34 = Rect_get_yMin_m1161103577((&V_0), /*hidden argument*/NULL);
		GL_TexCoord2_m86781742(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		float L_35 = Rect_get_xMin_m1161102488((&V_1), /*hidden argument*/NULL);
		float L_36 = Rect_get_yMin_m1161103577((&V_1), /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_35, L_36, (0.0f), /*hidden argument*/NULL);
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_37 = __this->get_firstFrame_6();
		if (!L_37)
		{
			goto IL_0198;
		}
	}
	{
		__this->set_firstFrame_6((bool)0);
		goto IL_0270;
	}

IL_0198:
	{
		GL_Begin_m3874173032(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		Color_t2020392075  L_38 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Color_m3254827061(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		float L_39 = Rect_get_xMin_m1161102488((&V_0), /*hidden argument*/NULL);
		float L_40 = Rect_get_yMax_m2915146103((&V_0), /*hidden argument*/NULL);
		GL_TexCoord2_m86781742(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Rect_t3681755626 * L_41 = __this->get_address_of_PreviousRect_16();
		float L_42 = Rect_get_xMin_m1161102488(L_41, /*hidden argument*/NULL);
		Rect_t3681755626 * L_43 = __this->get_address_of_PreviousRect_16();
		float L_44 = Rect_get_yMax_m2915146103(L_43, /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_42, L_44, (0.0f), /*hidden argument*/NULL);
		float L_45 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		float L_46 = Rect_get_yMax_m2915146103((&V_0), /*hidden argument*/NULL);
		GL_TexCoord2_m86781742(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		float L_47 = Rect_get_xMax_m2915145014((&V_1), /*hidden argument*/NULL);
		float L_48 = Rect_get_yMax_m2915146103((&V_1), /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_47, L_48, (0.0f), /*hidden argument*/NULL);
		float L_49 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		float L_50 = Rect_get_yMin_m1161103577((&V_0), /*hidden argument*/NULL);
		GL_TexCoord2_m86781742(NULL /*static, unused*/, L_49, L_50, /*hidden argument*/NULL);
		Rect_t3681755626 * L_51 = __this->get_address_of_PreviousRect_16();
		float L_52 = Rect_get_xMax_m2915145014(L_51, /*hidden argument*/NULL);
		Rect_t3681755626 * L_53 = __this->get_address_of_PreviousRect_16();
		float L_54 = Rect_get_yMin_m1161103577(L_53, /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_52, L_54, (0.0f), /*hidden argument*/NULL);
		float L_55 = Rect_get_xMin_m1161102488((&V_0), /*hidden argument*/NULL);
		float L_56 = Rect_get_yMin_m1161103577((&V_0), /*hidden argument*/NULL);
		GL_TexCoord2_m86781742(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		float L_57 = Rect_get_xMin_m1161102488((&V_1), /*hidden argument*/NULL);
		float L_58 = Rect_get_yMin_m1161103577((&V_1), /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_57, L_58, (0.0f), /*hidden argument*/NULL);
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_59 = V_1;
		__this->set_PreviousRect_16(L_59);
	}

IL_0270:
	{
		int32_t L_60 = V_2;
		V_2 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_0274:
	{
		int32_t L_61 = V_2;
		Material_t193706927 * L_62 = __this->get_EraserMaterial_2();
		NullCheck(L_62);
		int32_t L_63 = Material_get_passCount_m1778920671(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_61) < ((int32_t)L_63)))
		{
			goto IL_00b3;
		}
	}
	{
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MaskCamera::Start()
extern Il2CppClass* MaskCamera_t4194554627_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderTexture_t2666733923_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3061071912;
extern Il2CppCodeGenString* _stringLiteral2012146247;
extern Il2CppCodeGenString* _stringLiteral2880463552;
extern const uint32_t MaskCamera_Start_m1857878702_MetadataUsageId;
extern "C"  void MaskCamera_Start_m1857878702 (MaskCamera_t4194554627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MaskCamera_Start_m1857878702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t193706927 * V_0 = NULL;
	MaterialU5BU5D_t3123989686* V_1 = NULL;
	int32_t V_2 = 0;
	{
		__this->set_firstFrame_6((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(MaskCamera_t4194554627_il2cpp_TypeInfo_var);
		((MaskCamera_t4194554627_StaticFields*)MaskCamera_t4194554627_il2cpp_TypeInfo_var->static_fields)->set_instance_5(__this);
		int32_t L_0 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_2 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m2960228168(L_2, L_0, L_1, 0, 0, /*hidden argument*/NULL);
		__this->set_rnd_8(L_2);
		RenderTexture_t2666733923 * L_3 = __this->get_rnd_8();
		NullCheck(L_3);
		Texture_set_anisoLevel_m4242988344(L_3, 0, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_4 = __this->get_rnd_8();
		NullCheck(L_4);
		Texture_set_wrapMode_m333956747(L_4, 1, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_5 = __this->get_rnd_8();
		NullCheck(L_5);
		Texture_set_filterMode_m3838996656(L_5, 1, /*hidden argument*/NULL);
		Camera_t189460977 * L_6 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		RenderTexture_t2666733923 * L_7 = __this->get_rnd_8();
		NullCheck(L_6);
		Camera_set_targetTexture_m3925036117(L_6, L_7, /*hidden argument*/NULL);
		MaterialU5BU5D_t3123989686* L_8 = __this->get_Dust_9();
		V_1 = L_8;
		V_2 = 0;
		goto IL_0080;
	}

IL_0067:
	{
		MaterialU5BU5D_t3123989686* L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Material_t193706927 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_0 = L_12;
		Material_t193706927 * L_13 = V_0;
		RenderTexture_t2666733923 * L_14 = __this->get_rnd_8();
		NullCheck(L_13);
		Material_SetTexture_m141095205(L_13, _stringLiteral3061071912, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0080:
	{
		int32_t L_16 = V_2;
		MaterialU5BU5D_t3123989686* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0067;
		}
	}
	{
		Rect_t3681755626  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Rect__ctor_m1220545469(&L_18, (-11.0f), (-4.0f), (16.0f), (12.5f), /*hidden argument*/NULL);
		__this->set_ImageRect_10(L_18);
		ObjectU5BU5D_t3614634134* L_19 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral2012146247);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2012146247);
		ObjectU5BU5D_t3614634134* L_20 = L_19;
		int32_t L_21 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_22 = L_21;
		Il2CppObject * L_23 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_23);
		ObjectU5BU5D_t3614634134* L_24 = L_20;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral2880463552);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2880463552);
		ObjectU5BU5D_t3614634134* L_25 = L_24;
		int32_t L_26 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_27 = L_26;
		Il2CppObject * L_28 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_28);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_28);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m3881798623(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MaskCamera::Update()
extern Il2CppClass* Nullable_1_t506773894_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4060921815_MethodInfo_var;
extern const uint32_t MaskCamera_Update_m773705451_MetadataUsageId;
extern "C"  void MaskCamera_Update_m773705451 (MaskCamera_t4194554627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MaskCamera_Update_m773705451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t506773894  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (Nullable_1_t506773894_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t506773894  L_0 = V_0;
		__this->set_newHolePosition_7(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		Camera_t189460977 * L_2 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_3 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_4 = Camera_ScreenToWorldPoint_m929392728(L_2, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Rect_t3681755626 * L_6 = __this->get_address_of_ImageRect_10();
		Vector2_t2243707579  L_7 = V_1;
		bool L_8 = Rect_Contains_m1334685290(L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00a9;
		}
	}
	{
		float L_9 = (&V_1)->get_x_0();
		Rect_t3681755626 * L_10 = __this->get_address_of_ImageRect_10();
		float L_11 = Rect_get_xMin_m1161102488(L_10, /*hidden argument*/NULL);
		float L_12 = __this->get_OffsetX_17();
		Rect_t3681755626 * L_13 = __this->get_address_of_ImageRect_10();
		float L_14 = Rect_get_width_m1138015702(L_13, /*hidden argument*/NULL);
		float L_15 = (&V_1)->get_y_1();
		Rect_t3681755626 * L_16 = __this->get_address_of_ImageRect_10();
		float L_17 = Rect_get_yMin_m1161103577(L_16, /*hidden argument*/NULL);
		float L_18 = __this->get_OffsetY_18();
		Rect_t3681755626 * L_19 = __this->get_address_of_ImageRect_10();
		float L_20 = Rect_get_height_m3128694305(L_19, /*hidden argument*/NULL);
		Vector2_t2243707579  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3067419446(&L_21, ((float)((float)((float)((float)(480.0f)*(float)((float)((float)L_9-(float)((float)((float)L_11+(float)L_12))))))/(float)L_14)), ((float)((float)((float)((float)(800.0f)*(float)((float)((float)L_15-(float)((float)((float)L_17+(float)L_18))))))/(float)L_20)), /*hidden argument*/NULL);
		Nullable_1_t506773894  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Nullable_1__ctor_m4060921815(&L_22, L_21, /*hidden argument*/Nullable_1__ctor_m4060921815_MethodInfo_var);
		__this->set_newHolePosition_7(L_22);
	}

IL_00a9:
	{
		return;
	}
}
// System.Void MaskCamera::OnPostRender()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m1179702244_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3625172674_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1364046322;
extern const uint32_t MaskCamera_OnPostRender_m1760245409_MetadataUsageId;
extern "C"  void MaskCamera_OnPostRender_m1760245409 (MaskCamera_t4194554627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MaskCamera_OnPostRender_m1760245409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t506773894  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_firstFrame_6();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		__this->set_firstFrame_6((bool)0);
		Color_t2020392075  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m1909920690(&L_1, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GL_Clear_m154870981(NULL /*static, unused*/, (bool)0, (bool)1, L_1, /*hidden argument*/NULL);
	}

IL_0032:
	{
		Nullable_1_t506773894  L_2 = __this->get_newHolePosition_7();
		V_0 = L_2;
		bool L_3 = Nullable_1_get_HasValue_m1179702244((&V_0), /*hidden argument*/Nullable_1_get_HasValue_m1179702244_MethodInfo_var);
		if (!L_3)
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t1756533147 * L_4 = __this->get_CurrentCleaningTools_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_007b;
		}
	}
	{
		Vector2_t2243707579  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3067419446(&L_6, (480.0f), (800.0f), /*hidden argument*/NULL);
		Nullable_1_t506773894 * L_7 = __this->get_address_of_newHolePosition_7();
		Vector2_t2243707579  L_8 = Nullable_1_get_Value_m3625172674(L_7, /*hidden argument*/Nullable_1_get_Value_m3625172674_MethodInfo_var);
		MaskCamera_CutHole_m3971818326(__this, L_6, L_8, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_007b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1364046322, /*hidden argument*/NULL);
	}

IL_0085:
	{
		return;
	}
}
// System.Void MaskCamera::.cctor()
extern "C"  void MaskCamera__cctor_m2054578709 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m1628241538 (MonoPInvokeCallbackAttribute_t1970456718 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RatePanel::.ctor()
extern "C"  void RatePanel__ctor_m4244744291 (RatePanel_t560543380 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RatePanel::ClickedOnStar(UnityEngine.GameObject)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2328220328;
extern Il2CppCodeGenString* _stringLiteral567779955;
extern Il2CppCodeGenString* _stringLiteral4231481875;
extern Il2CppCodeGenString* _stringLiteral1737690736;
extern Il2CppCodeGenString* _stringLiteral2434250578;
extern const uint32_t RatePanel_ClickedOnStar_m3353240461_MetadataUsageId;
extern "C"  void RatePanel_ClickedOnStar_m3353240461 (RatePanel_t560543380 * __this, GameObject_t1756533147 * ___Star0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RatePanel_ClickedOnStar_m3353240461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t2243707580  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector3_t2243707580  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector3_t2243707580  V_21;
	memset(&V_21, 0, sizeof(V_21));
	Vector3_t2243707580  V_22;
	memset(&V_22, 0, sizeof(V_22));
	Vector3_t2243707580  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Vector3_t2243707580  V_24;
	memset(&V_24, 0, sizeof(V_24));
	Vector3_t2243707580  V_25;
	memset(&V_25, 0, sizeof(V_25));
	Vector3_t2243707580  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Vector3_t2243707580  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Vector3_t2243707580  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Vector3_t2243707580  V_29;
	memset(&V_29, 0, sizeof(V_29));
	Vector3_t2243707580  V_30;
	memset(&V_30, 0, sizeof(V_30));
	Vector3_t2243707580  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector3_t2243707580  V_32;
	memset(&V_32, 0, sizeof(V_32));
	Vector3_t2243707580  V_33;
	memset(&V_33, 0, sizeof(V_33));
	Vector3_t2243707580  V_34;
	memset(&V_34, 0, sizeof(V_34));
	Vector3_t2243707580  V_35;
	memset(&V_35, 0, sizeof(V_35));
	Vector3_t2243707580  V_36;
	memset(&V_36, 0, sizeof(V_36));
	Vector3_t2243707580  V_37;
	memset(&V_37, 0, sizeof(V_37));
	Vector3_t2243707580  V_38;
	memset(&V_38, 0, sizeof(V_38));
	Vector3_t2243707580  V_39;
	memset(&V_39, 0, sizeof(V_39));
	Vector3_t2243707580  V_40;
	memset(&V_40, 0, sizeof(V_40));
	Vector3_t2243707580  V_41;
	memset(&V_41, 0, sizeof(V_41));
	Vector3_t2243707580  V_42;
	memset(&V_42, 0, sizeof(V_42));
	Vector3_t2243707580  V_43;
	memset(&V_43, 0, sizeof(V_43));
	Vector3_t2243707580  V_44;
	memset(&V_44, 0, sizeof(V_44));
	Vector3_t2243707580  V_45;
	memset(&V_45, 0, sizeof(V_45));
	Vector3_t2243707580  V_46;
	memset(&V_46, 0, sizeof(V_46));
	Vector3_t2243707580  V_47;
	memset(&V_47, 0, sizeof(V_47));
	Vector3_t2243707580  V_48;
	memset(&V_48, 0, sizeof(V_48));
	Vector3_t2243707580  V_49;
	memset(&V_49, 0, sizeof(V_49));
	String_t* V_50 = NULL;
	String_t* V_51 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___Star0;
		GameObject_t1756533147 * L_1 = __this->get_Star1_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0223;
		}
	}
	{
		GameObject_t1756533147 * L_3 = __this->get_Star1_5();
		NullCheck(L_3);
		SpriteRenderer_t1209076198 * L_4 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_3, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_5 = __this->get_FillStar_11();
		NullCheck(L_4);
		SpriteRenderer_set_sprite_m617298623(L_4, L_5, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_Star1_5();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_Star1_5();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_localPosition_m2533925116(L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		float L_11 = (&V_0)->get_x_1();
		float L_12 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_13 = __this->get_Star1_5();
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = GameObject_get_transform_m909382139(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_localPosition_m2533925116(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = (&V_1)->get_z_3();
		Vector3_t2243707580  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2638739322(&L_17, L_11, L_12, L_16, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localPosition_m1026930133(L_7, L_17, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = __this->get_Star2_6();
		NullCheck(L_18);
		SpriteRenderer_t1209076198 * L_19 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_18, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_20 = __this->get_BlankStar_12();
		NullCheck(L_19);
		SpriteRenderer_set_sprite_m617298623(L_19, L_20, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_21 = __this->get_Star2_6();
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = GameObject_get_transform_m909382139(L_21, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = __this->get_Star2_6();
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t2243707580  L_25 = Transform_get_localPosition_m2533925116(L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		float L_26 = (&V_2)->get_x_1();
		float L_27 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_28 = __this->get_Star2_6();
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = GameObject_get_transform_m909382139(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_localPosition_m2533925116(L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		float L_31 = (&V_3)->get_z_3();
		Vector3_t2243707580  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Vector3__ctor_m2638739322(&L_32, L_26, L_27, L_31, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_set_localPosition_m1026930133(L_22, L_32, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_33 = __this->get_Star3_7();
		NullCheck(L_33);
		SpriteRenderer_t1209076198 * L_34 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_33, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_35 = __this->get_BlankStar_12();
		NullCheck(L_34);
		SpriteRenderer_set_sprite_m617298623(L_34, L_35, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = __this->get_Star3_7();
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = GameObject_get_transform_m909382139(L_36, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_38 = __this->get_Star3_7();
		NullCheck(L_38);
		Transform_t3275118058 * L_39 = GameObject_get_transform_m909382139(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t2243707580  L_40 = Transform_get_localPosition_m2533925116(L_39, /*hidden argument*/NULL);
		V_4 = L_40;
		float L_41 = (&V_4)->get_x_1();
		float L_42 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_43 = __this->get_Star3_7();
		NullCheck(L_43);
		Transform_t3275118058 * L_44 = GameObject_get_transform_m909382139(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t2243707580  L_45 = Transform_get_localPosition_m2533925116(L_44, /*hidden argument*/NULL);
		V_5 = L_45;
		float L_46 = (&V_5)->get_z_3();
		Vector3_t2243707580  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m2638739322(&L_47, L_41, L_42, L_46, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_set_localPosition_m1026930133(L_37, L_47, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_48 = __this->get_Star4_8();
		NullCheck(L_48);
		SpriteRenderer_t1209076198 * L_49 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_48, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_50 = __this->get_BlankStar_12();
		NullCheck(L_49);
		SpriteRenderer_set_sprite_m617298623(L_49, L_50, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_51 = __this->get_Star4_8();
		NullCheck(L_51);
		Transform_t3275118058 * L_52 = GameObject_get_transform_m909382139(L_51, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_53 = __this->get_Star4_8();
		NullCheck(L_53);
		Transform_t3275118058 * L_54 = GameObject_get_transform_m909382139(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		Vector3_t2243707580  L_55 = Transform_get_localPosition_m2533925116(L_54, /*hidden argument*/NULL);
		V_6 = L_55;
		float L_56 = (&V_6)->get_x_1();
		float L_57 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_58 = __this->get_Star4_8();
		NullCheck(L_58);
		Transform_t3275118058 * L_59 = GameObject_get_transform_m909382139(L_58, /*hidden argument*/NULL);
		NullCheck(L_59);
		Vector3_t2243707580  L_60 = Transform_get_localPosition_m2533925116(L_59, /*hidden argument*/NULL);
		V_7 = L_60;
		float L_61 = (&V_7)->get_z_3();
		Vector3_t2243707580  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Vector3__ctor_m2638739322(&L_62, L_56, L_57, L_61, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_set_localPosition_m1026930133(L_52, L_62, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_63 = __this->get_Star5_9();
		NullCheck(L_63);
		SpriteRenderer_t1209076198 * L_64 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_63, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_65 = __this->get_BlankStar_12();
		NullCheck(L_64);
		SpriteRenderer_set_sprite_m617298623(L_64, L_65, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_66 = __this->get_Star5_9();
		NullCheck(L_66);
		Transform_t3275118058 * L_67 = GameObject_get_transform_m909382139(L_66, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_68 = __this->get_Star5_9();
		NullCheck(L_68);
		Transform_t3275118058 * L_69 = GameObject_get_transform_m909382139(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		Vector3_t2243707580  L_70 = Transform_get_localPosition_m2533925116(L_69, /*hidden argument*/NULL);
		V_8 = L_70;
		float L_71 = (&V_8)->get_x_1();
		float L_72 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_73 = __this->get_Star5_9();
		NullCheck(L_73);
		Transform_t3275118058 * L_74 = GameObject_get_transform_m909382139(L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		Vector3_t2243707580  L_75 = Transform_get_localPosition_m2533925116(L_74, /*hidden argument*/NULL);
		V_9 = L_75;
		float L_76 = (&V_9)->get_z_3();
		Vector3_t2243707580  L_77;
		memset(&L_77, 0, sizeof(L_77));
		Vector3__ctor_m2638739322(&L_77, L_71, L_72, L_76, /*hidden argument*/NULL);
		NullCheck(L_67);
		Transform_set_localPosition_m1026930133(L_67, L_77, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_78 = __this->get_RateContactButton_2();
		NullCheck(L_78);
		SpriteRenderer_t1209076198 * L_79 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_78, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_80 = __this->get_Contact_4();
		NullCheck(L_79);
		SpriteRenderer_set_sprite_m617298623(L_79, L_80, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_81 = __this->get_Star1_5();
		__this->set_SelectedStar_15(L_81);
		goto IL_0b40;
	}

IL_0223:
	{
		GameObject_t1756533147 * L_82 = ___Star0;
		GameObject_t1756533147 * L_83 = __this->get_Star2_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_84 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_82, L_83, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_044a;
		}
	}
	{
		GameObject_t1756533147 * L_85 = __this->get_Star1_5();
		NullCheck(L_85);
		SpriteRenderer_t1209076198 * L_86 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_85, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_87 = __this->get_FillStar_11();
		NullCheck(L_86);
		SpriteRenderer_set_sprite_m617298623(L_86, L_87, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_88 = __this->get_Star1_5();
		NullCheck(L_88);
		Transform_t3275118058 * L_89 = GameObject_get_transform_m909382139(L_88, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_90 = __this->get_Star1_5();
		NullCheck(L_90);
		Transform_t3275118058 * L_91 = GameObject_get_transform_m909382139(L_90, /*hidden argument*/NULL);
		NullCheck(L_91);
		Vector3_t2243707580  L_92 = Transform_get_localPosition_m2533925116(L_91, /*hidden argument*/NULL);
		V_10 = L_92;
		float L_93 = (&V_10)->get_x_1();
		float L_94 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_95 = __this->get_Star1_5();
		NullCheck(L_95);
		Transform_t3275118058 * L_96 = GameObject_get_transform_m909382139(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		Vector3_t2243707580  L_97 = Transform_get_localPosition_m2533925116(L_96, /*hidden argument*/NULL);
		V_11 = L_97;
		float L_98 = (&V_11)->get_z_3();
		Vector3_t2243707580  L_99;
		memset(&L_99, 0, sizeof(L_99));
		Vector3__ctor_m2638739322(&L_99, L_93, L_94, L_98, /*hidden argument*/NULL);
		NullCheck(L_89);
		Transform_set_localPosition_m1026930133(L_89, L_99, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_100 = __this->get_Star2_6();
		NullCheck(L_100);
		SpriteRenderer_t1209076198 * L_101 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_100, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_102 = __this->get_FillStar_11();
		NullCheck(L_101);
		SpriteRenderer_set_sprite_m617298623(L_101, L_102, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_103 = __this->get_Star2_6();
		NullCheck(L_103);
		Transform_t3275118058 * L_104 = GameObject_get_transform_m909382139(L_103, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_105 = __this->get_Star2_6();
		NullCheck(L_105);
		Transform_t3275118058 * L_106 = GameObject_get_transform_m909382139(L_105, /*hidden argument*/NULL);
		NullCheck(L_106);
		Vector3_t2243707580  L_107 = Transform_get_localPosition_m2533925116(L_106, /*hidden argument*/NULL);
		V_12 = L_107;
		float L_108 = (&V_12)->get_x_1();
		float L_109 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_110 = __this->get_Star2_6();
		NullCheck(L_110);
		Transform_t3275118058 * L_111 = GameObject_get_transform_m909382139(L_110, /*hidden argument*/NULL);
		NullCheck(L_111);
		Vector3_t2243707580  L_112 = Transform_get_localPosition_m2533925116(L_111, /*hidden argument*/NULL);
		V_13 = L_112;
		float L_113 = (&V_13)->get_z_3();
		Vector3_t2243707580  L_114;
		memset(&L_114, 0, sizeof(L_114));
		Vector3__ctor_m2638739322(&L_114, L_108, L_109, L_113, /*hidden argument*/NULL);
		NullCheck(L_104);
		Transform_set_localPosition_m1026930133(L_104, L_114, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_115 = __this->get_Star3_7();
		NullCheck(L_115);
		SpriteRenderer_t1209076198 * L_116 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_115, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_117 = __this->get_BlankStar_12();
		NullCheck(L_116);
		SpriteRenderer_set_sprite_m617298623(L_116, L_117, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_118 = __this->get_Star3_7();
		NullCheck(L_118);
		Transform_t3275118058 * L_119 = GameObject_get_transform_m909382139(L_118, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_120 = __this->get_Star3_7();
		NullCheck(L_120);
		Transform_t3275118058 * L_121 = GameObject_get_transform_m909382139(L_120, /*hidden argument*/NULL);
		NullCheck(L_121);
		Vector3_t2243707580  L_122 = Transform_get_localPosition_m2533925116(L_121, /*hidden argument*/NULL);
		V_14 = L_122;
		float L_123 = (&V_14)->get_x_1();
		float L_124 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_125 = __this->get_Star3_7();
		NullCheck(L_125);
		Transform_t3275118058 * L_126 = GameObject_get_transform_m909382139(L_125, /*hidden argument*/NULL);
		NullCheck(L_126);
		Vector3_t2243707580  L_127 = Transform_get_localPosition_m2533925116(L_126, /*hidden argument*/NULL);
		V_15 = L_127;
		float L_128 = (&V_15)->get_z_3();
		Vector3_t2243707580  L_129;
		memset(&L_129, 0, sizeof(L_129));
		Vector3__ctor_m2638739322(&L_129, L_123, L_124, L_128, /*hidden argument*/NULL);
		NullCheck(L_119);
		Transform_set_localPosition_m1026930133(L_119, L_129, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_130 = __this->get_Star4_8();
		NullCheck(L_130);
		SpriteRenderer_t1209076198 * L_131 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_130, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_132 = __this->get_BlankStar_12();
		NullCheck(L_131);
		SpriteRenderer_set_sprite_m617298623(L_131, L_132, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_133 = __this->get_Star4_8();
		NullCheck(L_133);
		Transform_t3275118058 * L_134 = GameObject_get_transform_m909382139(L_133, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_135 = __this->get_Star4_8();
		NullCheck(L_135);
		Transform_t3275118058 * L_136 = GameObject_get_transform_m909382139(L_135, /*hidden argument*/NULL);
		NullCheck(L_136);
		Vector3_t2243707580  L_137 = Transform_get_localPosition_m2533925116(L_136, /*hidden argument*/NULL);
		V_16 = L_137;
		float L_138 = (&V_16)->get_x_1();
		float L_139 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_140 = __this->get_Star4_8();
		NullCheck(L_140);
		Transform_t3275118058 * L_141 = GameObject_get_transform_m909382139(L_140, /*hidden argument*/NULL);
		NullCheck(L_141);
		Vector3_t2243707580  L_142 = Transform_get_localPosition_m2533925116(L_141, /*hidden argument*/NULL);
		V_17 = L_142;
		float L_143 = (&V_17)->get_z_3();
		Vector3_t2243707580  L_144;
		memset(&L_144, 0, sizeof(L_144));
		Vector3__ctor_m2638739322(&L_144, L_138, L_139, L_143, /*hidden argument*/NULL);
		NullCheck(L_134);
		Transform_set_localPosition_m1026930133(L_134, L_144, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_145 = __this->get_Star5_9();
		NullCheck(L_145);
		SpriteRenderer_t1209076198 * L_146 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_145, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_147 = __this->get_BlankStar_12();
		NullCheck(L_146);
		SpriteRenderer_set_sprite_m617298623(L_146, L_147, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_148 = __this->get_Star5_9();
		NullCheck(L_148);
		Transform_t3275118058 * L_149 = GameObject_get_transform_m909382139(L_148, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_150 = __this->get_Star5_9();
		NullCheck(L_150);
		Transform_t3275118058 * L_151 = GameObject_get_transform_m909382139(L_150, /*hidden argument*/NULL);
		NullCheck(L_151);
		Vector3_t2243707580  L_152 = Transform_get_localPosition_m2533925116(L_151, /*hidden argument*/NULL);
		V_18 = L_152;
		float L_153 = (&V_18)->get_x_1();
		float L_154 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_155 = __this->get_Star5_9();
		NullCheck(L_155);
		Transform_t3275118058 * L_156 = GameObject_get_transform_m909382139(L_155, /*hidden argument*/NULL);
		NullCheck(L_156);
		Vector3_t2243707580  L_157 = Transform_get_localPosition_m2533925116(L_156, /*hidden argument*/NULL);
		V_19 = L_157;
		float L_158 = (&V_19)->get_z_3();
		Vector3_t2243707580  L_159;
		memset(&L_159, 0, sizeof(L_159));
		Vector3__ctor_m2638739322(&L_159, L_153, L_154, L_158, /*hidden argument*/NULL);
		NullCheck(L_149);
		Transform_set_localPosition_m1026930133(L_149, L_159, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_160 = __this->get_RateContactButton_2();
		NullCheck(L_160);
		SpriteRenderer_t1209076198 * L_161 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_160, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_162 = __this->get_Contact_4();
		NullCheck(L_161);
		SpriteRenderer_set_sprite_m617298623(L_161, L_162, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_163 = __this->get_Star2_6();
		__this->set_SelectedStar_15(L_163);
		goto IL_0b40;
	}

IL_044a:
	{
		GameObject_t1756533147 * L_164 = ___Star0;
		GameObject_t1756533147 * L_165 = __this->get_Star3_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_166 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_164, L_165, /*hidden argument*/NULL);
		if (!L_166)
		{
			goto IL_0671;
		}
	}
	{
		GameObject_t1756533147 * L_167 = __this->get_Star1_5();
		NullCheck(L_167);
		SpriteRenderer_t1209076198 * L_168 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_167, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_169 = __this->get_FillStar_11();
		NullCheck(L_168);
		SpriteRenderer_set_sprite_m617298623(L_168, L_169, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_170 = __this->get_Star1_5();
		NullCheck(L_170);
		Transform_t3275118058 * L_171 = GameObject_get_transform_m909382139(L_170, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_172 = __this->get_Star1_5();
		NullCheck(L_172);
		Transform_t3275118058 * L_173 = GameObject_get_transform_m909382139(L_172, /*hidden argument*/NULL);
		NullCheck(L_173);
		Vector3_t2243707580  L_174 = Transform_get_localPosition_m2533925116(L_173, /*hidden argument*/NULL);
		V_20 = L_174;
		float L_175 = (&V_20)->get_x_1();
		float L_176 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_177 = __this->get_Star1_5();
		NullCheck(L_177);
		Transform_t3275118058 * L_178 = GameObject_get_transform_m909382139(L_177, /*hidden argument*/NULL);
		NullCheck(L_178);
		Vector3_t2243707580  L_179 = Transform_get_localPosition_m2533925116(L_178, /*hidden argument*/NULL);
		V_21 = L_179;
		float L_180 = (&V_21)->get_z_3();
		Vector3_t2243707580  L_181;
		memset(&L_181, 0, sizeof(L_181));
		Vector3__ctor_m2638739322(&L_181, L_175, L_176, L_180, /*hidden argument*/NULL);
		NullCheck(L_171);
		Transform_set_localPosition_m1026930133(L_171, L_181, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_182 = __this->get_Star2_6();
		NullCheck(L_182);
		SpriteRenderer_t1209076198 * L_183 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_182, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_184 = __this->get_FillStar_11();
		NullCheck(L_183);
		SpriteRenderer_set_sprite_m617298623(L_183, L_184, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_185 = __this->get_Star2_6();
		NullCheck(L_185);
		Transform_t3275118058 * L_186 = GameObject_get_transform_m909382139(L_185, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_187 = __this->get_Star2_6();
		NullCheck(L_187);
		Transform_t3275118058 * L_188 = GameObject_get_transform_m909382139(L_187, /*hidden argument*/NULL);
		NullCheck(L_188);
		Vector3_t2243707580  L_189 = Transform_get_localPosition_m2533925116(L_188, /*hidden argument*/NULL);
		V_22 = L_189;
		float L_190 = (&V_22)->get_x_1();
		float L_191 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_192 = __this->get_Star2_6();
		NullCheck(L_192);
		Transform_t3275118058 * L_193 = GameObject_get_transform_m909382139(L_192, /*hidden argument*/NULL);
		NullCheck(L_193);
		Vector3_t2243707580  L_194 = Transform_get_localPosition_m2533925116(L_193, /*hidden argument*/NULL);
		V_23 = L_194;
		float L_195 = (&V_23)->get_z_3();
		Vector3_t2243707580  L_196;
		memset(&L_196, 0, sizeof(L_196));
		Vector3__ctor_m2638739322(&L_196, L_190, L_191, L_195, /*hidden argument*/NULL);
		NullCheck(L_186);
		Transform_set_localPosition_m1026930133(L_186, L_196, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_197 = __this->get_Star3_7();
		NullCheck(L_197);
		SpriteRenderer_t1209076198 * L_198 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_197, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_199 = __this->get_FillStar_11();
		NullCheck(L_198);
		SpriteRenderer_set_sprite_m617298623(L_198, L_199, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_200 = __this->get_Star3_7();
		NullCheck(L_200);
		Transform_t3275118058 * L_201 = GameObject_get_transform_m909382139(L_200, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_202 = __this->get_Star3_7();
		NullCheck(L_202);
		Transform_t3275118058 * L_203 = GameObject_get_transform_m909382139(L_202, /*hidden argument*/NULL);
		NullCheck(L_203);
		Vector3_t2243707580  L_204 = Transform_get_localPosition_m2533925116(L_203, /*hidden argument*/NULL);
		V_24 = L_204;
		float L_205 = (&V_24)->get_x_1();
		float L_206 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_207 = __this->get_Star3_7();
		NullCheck(L_207);
		Transform_t3275118058 * L_208 = GameObject_get_transform_m909382139(L_207, /*hidden argument*/NULL);
		NullCheck(L_208);
		Vector3_t2243707580  L_209 = Transform_get_localPosition_m2533925116(L_208, /*hidden argument*/NULL);
		V_25 = L_209;
		float L_210 = (&V_25)->get_z_3();
		Vector3_t2243707580  L_211;
		memset(&L_211, 0, sizeof(L_211));
		Vector3__ctor_m2638739322(&L_211, L_205, L_206, L_210, /*hidden argument*/NULL);
		NullCheck(L_201);
		Transform_set_localPosition_m1026930133(L_201, L_211, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_212 = __this->get_Star4_8();
		NullCheck(L_212);
		SpriteRenderer_t1209076198 * L_213 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_212, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_214 = __this->get_BlankStar_12();
		NullCheck(L_213);
		SpriteRenderer_set_sprite_m617298623(L_213, L_214, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_215 = __this->get_Star4_8();
		NullCheck(L_215);
		Transform_t3275118058 * L_216 = GameObject_get_transform_m909382139(L_215, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_217 = __this->get_Star4_8();
		NullCheck(L_217);
		Transform_t3275118058 * L_218 = GameObject_get_transform_m909382139(L_217, /*hidden argument*/NULL);
		NullCheck(L_218);
		Vector3_t2243707580  L_219 = Transform_get_localPosition_m2533925116(L_218, /*hidden argument*/NULL);
		V_26 = L_219;
		float L_220 = (&V_26)->get_x_1();
		float L_221 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_222 = __this->get_Star4_8();
		NullCheck(L_222);
		Transform_t3275118058 * L_223 = GameObject_get_transform_m909382139(L_222, /*hidden argument*/NULL);
		NullCheck(L_223);
		Vector3_t2243707580  L_224 = Transform_get_localPosition_m2533925116(L_223, /*hidden argument*/NULL);
		V_27 = L_224;
		float L_225 = (&V_27)->get_z_3();
		Vector3_t2243707580  L_226;
		memset(&L_226, 0, sizeof(L_226));
		Vector3__ctor_m2638739322(&L_226, L_220, L_221, L_225, /*hidden argument*/NULL);
		NullCheck(L_216);
		Transform_set_localPosition_m1026930133(L_216, L_226, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_227 = __this->get_Star5_9();
		NullCheck(L_227);
		SpriteRenderer_t1209076198 * L_228 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_227, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_229 = __this->get_BlankStar_12();
		NullCheck(L_228);
		SpriteRenderer_set_sprite_m617298623(L_228, L_229, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_230 = __this->get_Star5_9();
		NullCheck(L_230);
		Transform_t3275118058 * L_231 = GameObject_get_transform_m909382139(L_230, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_232 = __this->get_Star5_9();
		NullCheck(L_232);
		Transform_t3275118058 * L_233 = GameObject_get_transform_m909382139(L_232, /*hidden argument*/NULL);
		NullCheck(L_233);
		Vector3_t2243707580  L_234 = Transform_get_localPosition_m2533925116(L_233, /*hidden argument*/NULL);
		V_28 = L_234;
		float L_235 = (&V_28)->get_x_1();
		float L_236 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_237 = __this->get_Star5_9();
		NullCheck(L_237);
		Transform_t3275118058 * L_238 = GameObject_get_transform_m909382139(L_237, /*hidden argument*/NULL);
		NullCheck(L_238);
		Vector3_t2243707580  L_239 = Transform_get_localPosition_m2533925116(L_238, /*hidden argument*/NULL);
		V_29 = L_239;
		float L_240 = (&V_29)->get_z_3();
		Vector3_t2243707580  L_241;
		memset(&L_241, 0, sizeof(L_241));
		Vector3__ctor_m2638739322(&L_241, L_235, L_236, L_240, /*hidden argument*/NULL);
		NullCheck(L_231);
		Transform_set_localPosition_m1026930133(L_231, L_241, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_242 = __this->get_RateContactButton_2();
		NullCheck(L_242);
		SpriteRenderer_t1209076198 * L_243 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_242, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_244 = __this->get_Rate_3();
		NullCheck(L_243);
		SpriteRenderer_set_sprite_m617298623(L_243, L_244, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_245 = __this->get_Star3_7();
		__this->set_SelectedStar_15(L_245);
		goto IL_0b40;
	}

IL_0671:
	{
		GameObject_t1756533147 * L_246 = ___Star0;
		GameObject_t1756533147 * L_247 = __this->get_Star4_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_248 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_246, L_247, /*hidden argument*/NULL);
		if (!L_248)
		{
			goto IL_0898;
		}
	}
	{
		GameObject_t1756533147 * L_249 = __this->get_Star1_5();
		NullCheck(L_249);
		SpriteRenderer_t1209076198 * L_250 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_249, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_251 = __this->get_FillStar_11();
		NullCheck(L_250);
		SpriteRenderer_set_sprite_m617298623(L_250, L_251, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_252 = __this->get_Star1_5();
		NullCheck(L_252);
		Transform_t3275118058 * L_253 = GameObject_get_transform_m909382139(L_252, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_254 = __this->get_Star1_5();
		NullCheck(L_254);
		Transform_t3275118058 * L_255 = GameObject_get_transform_m909382139(L_254, /*hidden argument*/NULL);
		NullCheck(L_255);
		Vector3_t2243707580  L_256 = Transform_get_localPosition_m2533925116(L_255, /*hidden argument*/NULL);
		V_30 = L_256;
		float L_257 = (&V_30)->get_x_1();
		float L_258 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_259 = __this->get_Star1_5();
		NullCheck(L_259);
		Transform_t3275118058 * L_260 = GameObject_get_transform_m909382139(L_259, /*hidden argument*/NULL);
		NullCheck(L_260);
		Vector3_t2243707580  L_261 = Transform_get_localPosition_m2533925116(L_260, /*hidden argument*/NULL);
		V_31 = L_261;
		float L_262 = (&V_31)->get_z_3();
		Vector3_t2243707580  L_263;
		memset(&L_263, 0, sizeof(L_263));
		Vector3__ctor_m2638739322(&L_263, L_257, L_258, L_262, /*hidden argument*/NULL);
		NullCheck(L_253);
		Transform_set_localPosition_m1026930133(L_253, L_263, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_264 = __this->get_Star2_6();
		NullCheck(L_264);
		SpriteRenderer_t1209076198 * L_265 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_264, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_266 = __this->get_FillStar_11();
		NullCheck(L_265);
		SpriteRenderer_set_sprite_m617298623(L_265, L_266, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_267 = __this->get_Star2_6();
		NullCheck(L_267);
		Transform_t3275118058 * L_268 = GameObject_get_transform_m909382139(L_267, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_269 = __this->get_Star2_6();
		NullCheck(L_269);
		Transform_t3275118058 * L_270 = GameObject_get_transform_m909382139(L_269, /*hidden argument*/NULL);
		NullCheck(L_270);
		Vector3_t2243707580  L_271 = Transform_get_localPosition_m2533925116(L_270, /*hidden argument*/NULL);
		V_32 = L_271;
		float L_272 = (&V_32)->get_x_1();
		float L_273 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_274 = __this->get_Star2_6();
		NullCheck(L_274);
		Transform_t3275118058 * L_275 = GameObject_get_transform_m909382139(L_274, /*hidden argument*/NULL);
		NullCheck(L_275);
		Vector3_t2243707580  L_276 = Transform_get_localPosition_m2533925116(L_275, /*hidden argument*/NULL);
		V_33 = L_276;
		float L_277 = (&V_33)->get_z_3();
		Vector3_t2243707580  L_278;
		memset(&L_278, 0, sizeof(L_278));
		Vector3__ctor_m2638739322(&L_278, L_272, L_273, L_277, /*hidden argument*/NULL);
		NullCheck(L_268);
		Transform_set_localPosition_m1026930133(L_268, L_278, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_279 = __this->get_Star3_7();
		NullCheck(L_279);
		SpriteRenderer_t1209076198 * L_280 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_279, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_281 = __this->get_FillStar_11();
		NullCheck(L_280);
		SpriteRenderer_set_sprite_m617298623(L_280, L_281, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_282 = __this->get_Star3_7();
		NullCheck(L_282);
		Transform_t3275118058 * L_283 = GameObject_get_transform_m909382139(L_282, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_284 = __this->get_Star3_7();
		NullCheck(L_284);
		Transform_t3275118058 * L_285 = GameObject_get_transform_m909382139(L_284, /*hidden argument*/NULL);
		NullCheck(L_285);
		Vector3_t2243707580  L_286 = Transform_get_localPosition_m2533925116(L_285, /*hidden argument*/NULL);
		V_34 = L_286;
		float L_287 = (&V_34)->get_x_1();
		float L_288 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_289 = __this->get_Star3_7();
		NullCheck(L_289);
		Transform_t3275118058 * L_290 = GameObject_get_transform_m909382139(L_289, /*hidden argument*/NULL);
		NullCheck(L_290);
		Vector3_t2243707580  L_291 = Transform_get_localPosition_m2533925116(L_290, /*hidden argument*/NULL);
		V_35 = L_291;
		float L_292 = (&V_35)->get_z_3();
		Vector3_t2243707580  L_293;
		memset(&L_293, 0, sizeof(L_293));
		Vector3__ctor_m2638739322(&L_293, L_287, L_288, L_292, /*hidden argument*/NULL);
		NullCheck(L_283);
		Transform_set_localPosition_m1026930133(L_283, L_293, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_294 = __this->get_Star4_8();
		NullCheck(L_294);
		SpriteRenderer_t1209076198 * L_295 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_294, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_296 = __this->get_FillStar_11();
		NullCheck(L_295);
		SpriteRenderer_set_sprite_m617298623(L_295, L_296, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_297 = __this->get_Star4_8();
		NullCheck(L_297);
		Transform_t3275118058 * L_298 = GameObject_get_transform_m909382139(L_297, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_299 = __this->get_Star4_8();
		NullCheck(L_299);
		Transform_t3275118058 * L_300 = GameObject_get_transform_m909382139(L_299, /*hidden argument*/NULL);
		NullCheck(L_300);
		Vector3_t2243707580  L_301 = Transform_get_localPosition_m2533925116(L_300, /*hidden argument*/NULL);
		V_36 = L_301;
		float L_302 = (&V_36)->get_x_1();
		float L_303 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_304 = __this->get_Star4_8();
		NullCheck(L_304);
		Transform_t3275118058 * L_305 = GameObject_get_transform_m909382139(L_304, /*hidden argument*/NULL);
		NullCheck(L_305);
		Vector3_t2243707580  L_306 = Transform_get_localPosition_m2533925116(L_305, /*hidden argument*/NULL);
		V_37 = L_306;
		float L_307 = (&V_37)->get_z_3();
		Vector3_t2243707580  L_308;
		memset(&L_308, 0, sizeof(L_308));
		Vector3__ctor_m2638739322(&L_308, L_302, L_303, L_307, /*hidden argument*/NULL);
		NullCheck(L_298);
		Transform_set_localPosition_m1026930133(L_298, L_308, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_309 = __this->get_Star5_9();
		NullCheck(L_309);
		SpriteRenderer_t1209076198 * L_310 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_309, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_311 = __this->get_BlankStar_12();
		NullCheck(L_310);
		SpriteRenderer_set_sprite_m617298623(L_310, L_311, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_312 = __this->get_Star5_9();
		NullCheck(L_312);
		Transform_t3275118058 * L_313 = GameObject_get_transform_m909382139(L_312, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_314 = __this->get_Star5_9();
		NullCheck(L_314);
		Transform_t3275118058 * L_315 = GameObject_get_transform_m909382139(L_314, /*hidden argument*/NULL);
		NullCheck(L_315);
		Vector3_t2243707580  L_316 = Transform_get_localPosition_m2533925116(L_315, /*hidden argument*/NULL);
		V_38 = L_316;
		float L_317 = (&V_38)->get_x_1();
		float L_318 = __this->get_BlankStarY_14();
		GameObject_t1756533147 * L_319 = __this->get_Star5_9();
		NullCheck(L_319);
		Transform_t3275118058 * L_320 = GameObject_get_transform_m909382139(L_319, /*hidden argument*/NULL);
		NullCheck(L_320);
		Vector3_t2243707580  L_321 = Transform_get_localPosition_m2533925116(L_320, /*hidden argument*/NULL);
		V_39 = L_321;
		float L_322 = (&V_39)->get_z_3();
		Vector3_t2243707580  L_323;
		memset(&L_323, 0, sizeof(L_323));
		Vector3__ctor_m2638739322(&L_323, L_317, L_318, L_322, /*hidden argument*/NULL);
		NullCheck(L_313);
		Transform_set_localPosition_m1026930133(L_313, L_323, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_324 = __this->get_RateContactButton_2();
		NullCheck(L_324);
		SpriteRenderer_t1209076198 * L_325 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_324, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_326 = __this->get_Rate_3();
		NullCheck(L_325);
		SpriteRenderer_set_sprite_m617298623(L_325, L_326, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_327 = __this->get_Star4_8();
		__this->set_SelectedStar_15(L_327);
		goto IL_0b40;
	}

IL_0898:
	{
		GameObject_t1756533147 * L_328 = ___Star0;
		GameObject_t1756533147 * L_329 = __this->get_Star5_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_330 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_328, L_329, /*hidden argument*/NULL);
		if (!L_330)
		{
			goto IL_0abf;
		}
	}
	{
		GameObject_t1756533147 * L_331 = __this->get_Star1_5();
		NullCheck(L_331);
		SpriteRenderer_t1209076198 * L_332 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_331, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_333 = __this->get_FillStar_11();
		NullCheck(L_332);
		SpriteRenderer_set_sprite_m617298623(L_332, L_333, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_334 = __this->get_Star1_5();
		NullCheck(L_334);
		Transform_t3275118058 * L_335 = GameObject_get_transform_m909382139(L_334, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_336 = __this->get_Star1_5();
		NullCheck(L_336);
		Transform_t3275118058 * L_337 = GameObject_get_transform_m909382139(L_336, /*hidden argument*/NULL);
		NullCheck(L_337);
		Vector3_t2243707580  L_338 = Transform_get_localPosition_m2533925116(L_337, /*hidden argument*/NULL);
		V_40 = L_338;
		float L_339 = (&V_40)->get_x_1();
		float L_340 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_341 = __this->get_Star1_5();
		NullCheck(L_341);
		Transform_t3275118058 * L_342 = GameObject_get_transform_m909382139(L_341, /*hidden argument*/NULL);
		NullCheck(L_342);
		Vector3_t2243707580  L_343 = Transform_get_localPosition_m2533925116(L_342, /*hidden argument*/NULL);
		V_41 = L_343;
		float L_344 = (&V_41)->get_z_3();
		Vector3_t2243707580  L_345;
		memset(&L_345, 0, sizeof(L_345));
		Vector3__ctor_m2638739322(&L_345, L_339, L_340, L_344, /*hidden argument*/NULL);
		NullCheck(L_335);
		Transform_set_localPosition_m1026930133(L_335, L_345, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_346 = __this->get_Star2_6();
		NullCheck(L_346);
		SpriteRenderer_t1209076198 * L_347 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_346, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_348 = __this->get_FillStar_11();
		NullCheck(L_347);
		SpriteRenderer_set_sprite_m617298623(L_347, L_348, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_349 = __this->get_Star2_6();
		NullCheck(L_349);
		Transform_t3275118058 * L_350 = GameObject_get_transform_m909382139(L_349, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_351 = __this->get_Star2_6();
		NullCheck(L_351);
		Transform_t3275118058 * L_352 = GameObject_get_transform_m909382139(L_351, /*hidden argument*/NULL);
		NullCheck(L_352);
		Vector3_t2243707580  L_353 = Transform_get_localPosition_m2533925116(L_352, /*hidden argument*/NULL);
		V_42 = L_353;
		float L_354 = (&V_42)->get_x_1();
		float L_355 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_356 = __this->get_Star2_6();
		NullCheck(L_356);
		Transform_t3275118058 * L_357 = GameObject_get_transform_m909382139(L_356, /*hidden argument*/NULL);
		NullCheck(L_357);
		Vector3_t2243707580  L_358 = Transform_get_localPosition_m2533925116(L_357, /*hidden argument*/NULL);
		V_43 = L_358;
		float L_359 = (&V_43)->get_z_3();
		Vector3_t2243707580  L_360;
		memset(&L_360, 0, sizeof(L_360));
		Vector3__ctor_m2638739322(&L_360, L_354, L_355, L_359, /*hidden argument*/NULL);
		NullCheck(L_350);
		Transform_set_localPosition_m1026930133(L_350, L_360, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_361 = __this->get_Star3_7();
		NullCheck(L_361);
		SpriteRenderer_t1209076198 * L_362 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_361, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_363 = __this->get_FillStar_11();
		NullCheck(L_362);
		SpriteRenderer_set_sprite_m617298623(L_362, L_363, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_364 = __this->get_Star3_7();
		NullCheck(L_364);
		Transform_t3275118058 * L_365 = GameObject_get_transform_m909382139(L_364, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_366 = __this->get_Star3_7();
		NullCheck(L_366);
		Transform_t3275118058 * L_367 = GameObject_get_transform_m909382139(L_366, /*hidden argument*/NULL);
		NullCheck(L_367);
		Vector3_t2243707580  L_368 = Transform_get_localPosition_m2533925116(L_367, /*hidden argument*/NULL);
		V_44 = L_368;
		float L_369 = (&V_44)->get_x_1();
		float L_370 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_371 = __this->get_Star3_7();
		NullCheck(L_371);
		Transform_t3275118058 * L_372 = GameObject_get_transform_m909382139(L_371, /*hidden argument*/NULL);
		NullCheck(L_372);
		Vector3_t2243707580  L_373 = Transform_get_localPosition_m2533925116(L_372, /*hidden argument*/NULL);
		V_45 = L_373;
		float L_374 = (&V_45)->get_z_3();
		Vector3_t2243707580  L_375;
		memset(&L_375, 0, sizeof(L_375));
		Vector3__ctor_m2638739322(&L_375, L_369, L_370, L_374, /*hidden argument*/NULL);
		NullCheck(L_365);
		Transform_set_localPosition_m1026930133(L_365, L_375, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_376 = __this->get_Star4_8();
		NullCheck(L_376);
		SpriteRenderer_t1209076198 * L_377 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_376, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_378 = __this->get_FillStar_11();
		NullCheck(L_377);
		SpriteRenderer_set_sprite_m617298623(L_377, L_378, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_379 = __this->get_Star4_8();
		NullCheck(L_379);
		Transform_t3275118058 * L_380 = GameObject_get_transform_m909382139(L_379, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_381 = __this->get_Star4_8();
		NullCheck(L_381);
		Transform_t3275118058 * L_382 = GameObject_get_transform_m909382139(L_381, /*hidden argument*/NULL);
		NullCheck(L_382);
		Vector3_t2243707580  L_383 = Transform_get_localPosition_m2533925116(L_382, /*hidden argument*/NULL);
		V_46 = L_383;
		float L_384 = (&V_46)->get_x_1();
		float L_385 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_386 = __this->get_Star4_8();
		NullCheck(L_386);
		Transform_t3275118058 * L_387 = GameObject_get_transform_m909382139(L_386, /*hidden argument*/NULL);
		NullCheck(L_387);
		Vector3_t2243707580  L_388 = Transform_get_localPosition_m2533925116(L_387, /*hidden argument*/NULL);
		V_47 = L_388;
		float L_389 = (&V_47)->get_z_3();
		Vector3_t2243707580  L_390;
		memset(&L_390, 0, sizeof(L_390));
		Vector3__ctor_m2638739322(&L_390, L_384, L_385, L_389, /*hidden argument*/NULL);
		NullCheck(L_380);
		Transform_set_localPosition_m1026930133(L_380, L_390, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_391 = __this->get_Star5_9();
		NullCheck(L_391);
		SpriteRenderer_t1209076198 * L_392 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_391, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_393 = __this->get_FillStar_11();
		NullCheck(L_392);
		SpriteRenderer_set_sprite_m617298623(L_392, L_393, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_394 = __this->get_Star5_9();
		NullCheck(L_394);
		Transform_t3275118058 * L_395 = GameObject_get_transform_m909382139(L_394, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_396 = __this->get_Star5_9();
		NullCheck(L_396);
		Transform_t3275118058 * L_397 = GameObject_get_transform_m909382139(L_396, /*hidden argument*/NULL);
		NullCheck(L_397);
		Vector3_t2243707580  L_398 = Transform_get_localPosition_m2533925116(L_397, /*hidden argument*/NULL);
		V_48 = L_398;
		float L_399 = (&V_48)->get_x_1();
		float L_400 = __this->get_FillStarY_13();
		GameObject_t1756533147 * L_401 = __this->get_Star5_9();
		NullCheck(L_401);
		Transform_t3275118058 * L_402 = GameObject_get_transform_m909382139(L_401, /*hidden argument*/NULL);
		NullCheck(L_402);
		Vector3_t2243707580  L_403 = Transform_get_localPosition_m2533925116(L_402, /*hidden argument*/NULL);
		V_49 = L_403;
		float L_404 = (&V_49)->get_z_3();
		Vector3_t2243707580  L_405;
		memset(&L_405, 0, sizeof(L_405));
		Vector3__ctor_m2638739322(&L_405, L_399, L_400, L_404, /*hidden argument*/NULL);
		NullCheck(L_395);
		Transform_set_localPosition_m1026930133(L_395, L_405, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_406 = __this->get_RateContactButton_2();
		NullCheck(L_406);
		SpriteRenderer_t1209076198 * L_407 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_406, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Sprite_t309593783 * L_408 = __this->get_Rate_3();
		NullCheck(L_407);
		SpriteRenderer_set_sprite_m617298623(L_407, L_408, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_409 = __this->get_Star5_9();
		__this->set_SelectedStar_15(L_409);
		goto IL_0b40;
	}

IL_0abf:
	{
		GameObject_t1756533147 * L_410 = ___Star0;
		GameObject_t1756533147 * L_411 = __this->get_Close_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_412 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_410, L_411, /*hidden argument*/NULL);
		if (!L_412)
		{
			goto IL_0adb;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		goto IL_0b40;
	}

IL_0adb:
	{
		GameObject_t1756533147 * L_413 = __this->get_RateContactButton_2();
		NullCheck(L_413);
		SpriteRenderer_t1209076198 * L_414 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_413, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		NullCheck(L_414);
		Sprite_t309593783 * L_415 = SpriteRenderer_get_sprite_m3016837432(L_414, /*hidden argument*/NULL);
		Sprite_t309593783 * L_416 = __this->get_Rate_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_417 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_415, L_416, /*hidden argument*/NULL);
		if (!L_417)
		{
			goto IL_0b14;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2328220328, /*hidden argument*/NULL);
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral567779955, /*hidden argument*/NULL);
		goto IL_0b3a;
	}

IL_0b14:
	{
		V_50 = _stringLiteral4231481875;
		V_51 = _stringLiteral4231481875;
		String_t* L_418 = V_50;
		String_t* L_419 = V_51;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_420 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral1737690736, L_418, _stringLiteral2434250578, L_419, /*hidden argument*/NULL);
		Application_OpenURL_m3882634228(NULL /*static, unused*/, L_420, /*hidden argument*/NULL);
	}

IL_0b3a:
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0b40:
	{
		return;
	}
}
// System.Void SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m3417712111 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		__this->set_PlaySound_15((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManager::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1607204275;
extern const uint32_t SoundManager_Start_m640423507_MetadataUsageId;
extern "C"  void SoundManager_Start_m640423507 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_Start_m640423507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->set_instance_18(__this);
		MonoBehaviour_InvokeRepeating_m3468262484(__this, _stringLiteral1607204275, (3.5f), (10.5f), /*hidden argument*/NULL);
		bool L_1 = __this->get_PlaySound_15();
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		AudioSource_t1135106623 * L_2 = __this->get_MainMusic_5();
		NullCheck(L_2);
		AudioSource_Play_m353744792(L_2, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void SoundManager::Update()
extern "C"  void SoundManager_Update_m1087474672 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SoundManager::OnLevelWasLoaded()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1382510780;
extern const uint32_t SoundManager_OnLevelWasLoaded_m16533198_MetadataUsageId;
extern "C"  void SoundManager_OnLevelWasLoaded_m16533198 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_OnLevelWasLoaded_m16533198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3057952154* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObjectU5BU5D_t3057952154* V_2 = NULL;
	int32_t V_3 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = GameObject_FindGameObjectsWithTag_m2154478296(NULL /*static, unused*/, _stringLiteral1382510780, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObjectU5BU5D_t3057952154* L_1 = V_0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_004f;
	}

IL_0014:
	{
		GameObjectU5BU5D_t3057952154* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_t1756533147 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		GameObject_t1756533147 * L_6 = V_1;
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_9 = __this->get_count_22();
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_004b;
		}
	}
	{
		GameObject_t1756533147 * L_10 = V_1;
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m2079638459(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = V_1;
		NullCheck(L_12);
		GameObject_t1756533147 * L_13 = GameObject_get_gameObject_m3662236595(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_004b:
	{
		int32_t L_14 = V_3;
		V_3 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_15 = V_3;
		GameObjectU5BU5D_t3057952154* L_16 = V_2;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_17 = __this->get_count_22();
		__this->set_count_22(((int32_t)((int32_t)L_17+(int32_t)1)));
		return;
	}
}
// System.Void SoundManager::MeowSound()
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const uint32_t SoundManager_MeowSound_m35483486_MetadataUsageId;
extern "C"  void SoundManager_MeowSound_m35483486 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_MeowSound_m35483486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_00cb;
		}
	}
	{
		bool L_1 = __this->get_CanPlayMeow_16();
		if (!L_1)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_2 = Application_get_loadedLevel_m3768581785(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_00cb;
		}
	}
	{
		AudioSource_t1135106623 * L_3 = __this->get_MeowSource_3();
		AudioClipU5BU5D_t2203355011* L_4 = __this->get_MeowClips_17();
		AudioClipU5BU5D_t2203355011* L_5 = __this->get_MeowClips_17();
		NullCheck(L_5);
		int32_t L_6 = Random_Range_m694320887(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_7 = L_6;
		AudioClip_t1932558630 * L_8 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_3);
		AudioSource_set_clip_m738814682(L_3, L_8, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_9 = __this->get_MeowSource_3();
		NullCheck(L_9);
		AudioSource_Play_m353744792(L_9, /*hidden argument*/NULL);
		Il2CppObject * L_10 = SoundManager_CloseMouth_m3384529014(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = __this->get_OpenMouth_2();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		Vector3U5BU5D_t1172311765* L_13 = __this->get_MouthOpenPosition_19();
		int32_t L_14 = Application_get_loadedLevel_m3768581785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		NullCheck(L_12);
		Transform_set_localPosition_m1026930133(L_12, (*(Vector3_t2243707580 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)))), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = __this->get_OpenMouth_2();
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		Vector3U5BU5D_t1172311765* L_17 = __this->get_MouthOpenScale_20();
		int32_t L_18 = Application_get_loadedLevel_m3768581785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		NullCheck(L_16);
		Transform_set_localScale_m2325460848(L_16, (*(Vector3_t2243707580 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = __this->get_OpenMouth_2();
		NullCheck(L_19);
		SpriteRenderer_t1209076198 * L_20 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_19, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		Int32U5BU5D_t3030399641* L_21 = __this->get_Order_21();
		int32_t L_22 = Application_get_loadedLevel_m3768581785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_20);
		Renderer_set_sortingOrder_m809829562(L_20, L_24, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_25 = __this->get_OpenMouth_2();
		NullCheck(L_25);
		GameObject_SetActive_m2887581199(L_25, (bool)1, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		return;
	}
}
// System.Void SoundManager::PlayBubbleBlastSound()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t SoundManager_PlayBubbleBlastSound_m742743792_MetadataUsageId;
extern "C"  void SoundManager_PlayBubbleBlastSound_m742743792 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_PlayBubbleBlastSound_m742743792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_BubbleBlastSource_4();
		Vector3_t2243707580  L_2 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		Il2CppObject * L_6 = SoundManager_DestroyBubbleSound_m4103589584(__this, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void SoundManager::PlayButtonSound()
extern "C"  void SoundManager_PlayButtonSound_m794282288 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_ButtonSource_6();
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void SoundManager::PlaySoapBubbleSound()
extern "C"  void SoundManager_PlaySoapBubbleSound_m855144563 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_SoapBubbleSound_7();
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void SoundManager::PlayShowerSound()
extern "C"  void SoundManager_PlayShowerSound_m3324044858 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_ShowerSound_8();
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void SoundManager::PlayTowelSound()
extern "C"  void SoundManager_PlayTowelSound_m3757545037 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_TowelSound_9();
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void SoundManager::PlayDryerSound()
extern "C"  void SoundManager_PlayDryerSound_m766259662 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_DryerSound_10();
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void SoundManager::PlayNailCutterSound()
extern "C"  void SoundManager_PlayNailCutterSound_m913856879 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_NailCutterSound_11();
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void SoundManager::PlayPerfumeSound()
extern "C"  void SoundManager_PlayPerfumeSound_m2825984396 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_PerfumeSound_12();
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void SoundManager::PlayClickSound()
extern "C"  void SoundManager_PlayClickSound_m1792418272 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_ClickSound_13();
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void SoundManager::PlayLastSound()
extern "C"  void SoundManager_PlayLastSound_m2535147324 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_PlaySound_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_LastSound_14();
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Collections.IEnumerator SoundManager::CloseMouth()
extern Il2CppClass* U3CCloseMouthU3Ec__Iterator0_t2695835239_il2cpp_TypeInfo_var;
extern const uint32_t SoundManager_CloseMouth_m3384529014_MetadataUsageId;
extern "C"  Il2CppObject * SoundManager_CloseMouth_m3384529014 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_CloseMouth_m3384529014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCloseMouthU3Ec__Iterator0_t2695835239 * V_0 = NULL;
	{
		U3CCloseMouthU3Ec__Iterator0_t2695835239 * L_0 = (U3CCloseMouthU3Ec__Iterator0_t2695835239 *)il2cpp_codegen_object_new(U3CCloseMouthU3Ec__Iterator0_t2695835239_il2cpp_TypeInfo_var);
		U3CCloseMouthU3Ec__Iterator0__ctor_m932007510(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCloseMouthU3Ec__Iterator0_t2695835239 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CCloseMouthU3Ec__Iterator0_t2695835239 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator SoundManager::DestroyBubbleSound(UnityEngine.GameObject)
extern Il2CppClass* U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612_il2cpp_TypeInfo_var;
extern const uint32_t SoundManager_DestroyBubbleSound_m4103589584_MetadataUsageId;
extern "C"  Il2CppObject * SoundManager_DestroyBubbleSound_m4103589584 (SoundManager_t654432262 * __this, GameObject_t1756533147 * ___Bubble0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_DestroyBubbleSound_m4103589584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * V_0 = NULL;
	{
		U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * L_0 = (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 *)il2cpp_codegen_object_new(U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612_il2cpp_TypeInfo_var);
		U3CDestroyBubbleSoundU3Ec__Iterator1__ctor_m3866914725(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * L_1 = V_0;
		GameObject_t1756533147 * L_2 = ___Bubble0;
		NullCheck(L_1);
		L_1->set_Bubble_0(L_2);
		U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * L_3 = V_0;
		return L_3;
	}
}
// System.Void SoundManager/<CloseMouth>c__Iterator0::.ctor()
extern "C"  void U3CCloseMouthU3Ec__Iterator0__ctor_m932007510 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SoundManager/<CloseMouth>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CCloseMouthU3Ec__Iterator0_MoveNext_m1226697558_MetadataUsageId;
extern "C"  bool U3CCloseMouthU3Ec__Iterator0_MoveNext_m1226697558 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCloseMouthU3Ec__Iterator0_MoveNext_m1226697558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_005d;
	}

IL_0021:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (1.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_005f;
	}

IL_0045:
	{
		SoundManager_t654432262 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_OpenMouth_2();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_005d:
	{
		return (bool)0;
	}

IL_005f:
	{
		return (bool)1;
	}
}
// System.Object SoundManager/<CloseMouth>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCloseMouthU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3453636504 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object SoundManager/<CloseMouth>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCloseMouthU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m863859696 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void SoundManager/<CloseMouth>c__Iterator0::Dispose()
extern "C"  void U3CCloseMouthU3Ec__Iterator0_Dispose_m1076368153 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void SoundManager/<CloseMouth>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCloseMouthU3Ec__Iterator0_Reset_m579547531_MetadataUsageId;
extern "C"  void U3CCloseMouthU3Ec__Iterator0_Reset_m579547531 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCloseMouthU3Ec__Iterator0_Reset_m579547531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SoundManager/<DestroyBubbleSound>c__Iterator1::.ctor()
extern "C"  void U3CDestroyBubbleSoundU3Ec__Iterator1__ctor_m3866914725 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SoundManager/<DestroyBubbleSound>c__Iterator1::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t U3CDestroyBubbleSoundU3Ec__Iterator1_MoveNext_m3370383731_MetadataUsageId;
extern "C"  bool U3CDestroyBubbleSoundU3Ec__Iterator1_MoveNext_m3370383731 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDestroyBubbleSoundU3Ec__Iterator1_MoveNext_m3370383731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_005c;
	}

IL_0021:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (1.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_005e;
	}

IL_0045:
	{
		GameObject_t1756533147 * L_4 = __this->get_Bubble_0();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = GameObject_get_gameObject_m3662236595(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_005c:
	{
		return (bool)0;
	}

IL_005e:
	{
		return (bool)1;
	}
}
// System.Object SoundManager/<DestroyBubbleSound>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDestroyBubbleSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3462975135 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object SoundManager/<DestroyBubbleSound>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDestroyBubbleSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1395584071 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void SoundManager/<DestroyBubbleSound>c__Iterator1::Dispose()
extern "C"  void U3CDestroyBubbleSoundU3Ec__Iterator1_Dispose_m3909609150 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void SoundManager/<DestroyBubbleSound>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CDestroyBubbleSoundU3Ec__Iterator1_Reset_m3908795056_MetadataUsageId;
extern "C"  void U3CDestroyBubbleSoundU3Ec__Iterator1_Reset_m3908795056 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDestroyBubbleSoundU3Ec__Iterator1_Reset_m3908795056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void StyleManager::.ctor()
extern "C"  void StyleManager__ctor_m813100335 (StyleManager_t3102803280 * __this, const MethodInfo* method)
{
	{
		__this->set_HatFirstTime_4((bool)1);
		__this->set_GogglesFirstTime_7((bool)1);
		__this->set_PendantFirstTime_10((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator StyleManager::Start()
extern Il2CppClass* U3CStartU3Ec__Iterator0_t3971177230_il2cpp_TypeInfo_var;
extern const uint32_t StyleManager_Start_m157888413_MetadataUsageId;
extern "C"  Il2CppObject * StyleManager_Start_m157888413 (StyleManager_t3102803280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StyleManager_Start_m157888413_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartU3Ec__Iterator0_t3971177230 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t3971177230 * L_0 = (U3CStartU3Ec__Iterator0_t3971177230 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t3971177230_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m3834877927(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t3971177230 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CStartU3Ec__Iterator0_t3971177230 * L_2 = V_0;
		return L_2;
	}
}
// System.Void StyleManager::HatButtonMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const uint32_t StyleManager_HatButtonMouseDown_m4039786343_MetadataUsageId;
extern "C"  void StyleManager_HatButtonMouseDown_m4039786343 (StyleManager_t3102803280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StyleManager_HatButtonMouseDown_m4039786343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_HatHolder_2();
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		SpriteU5BU5D_t3359083662* L_2 = __this->get_HatObjs_3();
		int32_t L_3 = __this->get_CurrentHatIndex_15();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Sprite_t309593783 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_1);
		SpriteRenderer_set_sprite_m617298623(L_1, L_5, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_CurrentHatIndex_15();
		__this->set_CurrentHatIndex_15(((int32_t)((int32_t)L_6+(int32_t)1)));
		int32_t L_7 = __this->get_CurrentHatIndex_15();
		SpriteU5BU5D_t3359083662* L_8 = __this->get_HatObjs_3();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0045;
		}
	}
	{
		__this->set_CurrentHatIndex_15(0);
	}

IL_0045:
	{
		GameObject_t1756533147 * L_9 = __this->get_HatIcon_11();
		NullCheck(L_9);
		SpriteRenderer_t1209076198 * L_10 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		SpriteU5BU5D_t3359083662* L_11 = __this->get_HatObjs_3();
		int32_t L_12 = __this->get_CurrentHatIndex_15();
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Sprite_t309593783 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_10);
		SpriteRenderer_set_sprite_m617298623(L_10, L_14, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_15 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_15);
		SoundManager_PlayButtonSound_m794282288(L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StyleManager::GogglesButtonMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const uint32_t StyleManager_GogglesButtonMouseDown_m2318320148_MetadataUsageId;
extern "C"  void StyleManager_GogglesButtonMouseDown_m2318320148 (StyleManager_t3102803280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StyleManager_GogglesButtonMouseDown_m2318320148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_GogglesHolder_5();
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		SpriteU5BU5D_t3359083662* L_2 = __this->get_GogglesObjs_6();
		int32_t L_3 = __this->get_CurrentGogglesIndex_16();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Sprite_t309593783 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_1);
		SpriteRenderer_set_sprite_m617298623(L_1, L_5, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_CurrentGogglesIndex_16();
		__this->set_CurrentGogglesIndex_16(((int32_t)((int32_t)L_6+(int32_t)1)));
		int32_t L_7 = __this->get_CurrentGogglesIndex_16();
		SpriteU5BU5D_t3359083662* L_8 = __this->get_GogglesObjs_6();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0045;
		}
	}
	{
		__this->set_CurrentGogglesIndex_16(0);
	}

IL_0045:
	{
		GameObject_t1756533147 * L_9 = __this->get_GogglesIcon_12();
		NullCheck(L_9);
		SpriteRenderer_t1209076198 * L_10 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		SpriteU5BU5D_t3359083662* L_11 = __this->get_GogglesObjs_6();
		int32_t L_12 = __this->get_CurrentGogglesIndex_16();
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Sprite_t309593783 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_10);
		SpriteRenderer_set_sprite_m617298623(L_10, L_14, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_15 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_15);
		SoundManager_PlayButtonSound_m794282288(L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StyleManager::PendantButtonMouseDown()
extern Il2CppClass* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const uint32_t StyleManager_PendantButtonMouseDown_m4289155414_MetadataUsageId;
extern "C"  void StyleManager_PendantButtonMouseDown_m4289155414 (StyleManager_t3102803280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StyleManager_PendantButtonMouseDown_m4289155414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_PendantHolder_8();
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		SpriteU5BU5D_t3359083662* L_2 = __this->get_PendantObjs_9();
		int32_t L_3 = __this->get_CurrentPendantIndex_17();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Sprite_t309593783 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_1);
		SpriteRenderer_set_sprite_m617298623(L_1, L_5, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_CurrentPendantIndex_17();
		__this->set_CurrentPendantIndex_17(((int32_t)((int32_t)L_6+(int32_t)1)));
		int32_t L_7 = __this->get_CurrentPendantIndex_17();
		SpriteU5BU5D_t3359083662* L_8 = __this->get_PendantObjs_9();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0045;
		}
	}
	{
		__this->set_CurrentPendantIndex_17(0);
	}

IL_0045:
	{
		GameObject_t1756533147 * L_9 = __this->get_PendantIcon_13();
		NullCheck(L_9);
		SpriteRenderer_t1209076198 * L_10 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		SpriteU5BU5D_t3359083662* L_11 = __this->get_PendantObjs_9();
		int32_t L_12 = __this->get_CurrentPendantIndex_17();
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Sprite_t309593783 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_10);
		SpriteRenderer_set_sprite_m617298623(L_10, L_14, /*hidden argument*/NULL);
		SoundManager_t654432262 * L_15 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_18();
		NullCheck(L_15);
		SoundManager_PlayButtonSound_m794282288(L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StyleManager/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3834877927 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean StyleManager/<Start>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m986099081_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m986099081 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m986099081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_005d;
	}

IL_0021:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (5.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_005f;
	}

IL_0045:
	{
		StyleManager_t3102803280 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_GameOverPanel_14();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)1, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_005d:
	{
		return (bool)0;
	}

IL_005f:
	{
		return (bool)1;
	}
}
// System.Object StyleManager/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m249666013 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object StyleManager/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3436385941 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void StyleManager/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2281708188 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void StyleManager/<Start>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m2391459750_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2391459750 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m2391459750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ToolButton::.ctor()
extern "C"  void ToolButton__ctor_m2189683053 (ToolButton_t3848681854 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToolButton::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t948534547_m324820273_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPolygonCollider2D_t3220183178_m191733878_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550_MethodInfo_var;
extern const uint32_t ToolButton_Start_m298107581_MetadataUsageId;
extern "C"  void ToolButton_Start_m298107581 (ToolButton_t3848681854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolButton_Start_m298107581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_Draggable_4();
		if (!L_0)
		{
			goto IL_007f;
		}
	}
	{
		Rigidbody2D_t502193897 * L_1 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0043;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396(L_3, /*hidden argument*/GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396_MethodInfo_var);
		Rigidbody2D_t502193897 * L_4 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		NullCheck(L_4);
		Rigidbody2D_set_gravityScale_m1426625078(L_4, (0.0f), /*hidden argument*/NULL);
		Rigidbody2D_t502193897 * L_5 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		NullCheck(L_5);
		Rigidbody2D_set_fixedAngle_m1837001999(L_5, (bool)1, /*hidden argument*/NULL);
	}

IL_0043:
	{
		BoxCollider2D_t948534547 * L_6 = Component_GetComponent_TisBoxCollider2D_t948534547_m324820273(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t948534547_m324820273_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_007f;
		}
	}
	{
		PolygonCollider2D_t3220183178 * L_8 = Component_GetComponent_TisPolygonCollider2D_t3220183178_m191733878(__this, /*hidden argument*/Component_GetComponent_TisPolygonCollider2D_t3220183178_m191733878_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_007f;
		}
	}
	{
		CircleCollider2D_t13116344 * L_10 = Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442(__this, /*hidden argument*/Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007f;
		}
	}
	{
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550(L_12, /*hidden argument*/GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550_MethodInfo_var);
	}

IL_007f:
	{
		GameObject_t1756533147 * L_13 = __this->get_OnCollision_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_009c;
		}
	}
	{
		GameObject_t1756533147 * L_15 = __this->get_OnClick_3();
		__this->set_OnCollision_6(L_15);
	}

IL_009c:
	{
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_localPosition_m2533925116(L_16, /*hidden argument*/NULL);
		__this->set_InitialPos_7(L_17);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		float L_20 = (&V_0)->get_z_3();
		Camera_t189460977 * L_21 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		float L_24 = (&V_1)->get_z_3();
		__this->set_MousePositionOffsetZ_8(((float)((float)L_20-(float)L_24)));
		return;
	}
}
// System.Void ToolButton::OnMouseDown()
extern Il2CppClass* KittyHandNFootManager_t1686788969_il2cpp_TypeInfo_var;
extern Il2CppClass* ButtonType_t1054768361_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1274809451;
extern const uint32_t ToolButton_OnMouseDown_m947594191_MetadataUsageId;
extern "C"  void ToolButton_OnMouseDown_m947594191 (ToolButton_t3848681854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolButton_OnMouseDown_m947594191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_buttonType_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		KittyHandNFootManager_t1686788969 * L_1 = ((KittyHandNFootManager_t1686788969_StaticFields*)KittyHandNFootManager_t1686788969_il2cpp_TypeInfo_var->static_fields)->get_instance_52();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_NailPolishType_51(L_2);
	}

IL_001d:
	{
		GameObject_t1756533147 * L_3 = __this->get_OnClick_3();
		int32_t* L_4 = __this->get_address_of_buttonType_2();
		Il2CppObject * L_5 = Box(ButtonType_t1054768361_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, L_6, _stringLiteral1274809451, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_BroadcastMessage_m3735465996(L_3, L_7, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToolButton::OnMouseUp()
extern Il2CppClass* ButtonType_t1054768361_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral998357924;
extern const uint32_t ToolButton_OnMouseUp_m1815978676_MetadataUsageId;
extern "C"  void ToolButton_OnMouseUp_m1815978676 (ToolButton_t3848681854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolButton_OnMouseUp_m1815978676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_Draggable_4();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ToolButton_ButtonBackOnPosition_m755920680(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		GameObject_t1756533147 * L_2 = __this->get_OnClick_3();
		int32_t* L_3 = __this->get_address_of_buttonType_2();
		Il2CppObject * L_4 = Box(ButtonType_t1054768361_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, L_5, _stringLiteral998357924, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_BroadcastMessage_m3735465996(L_2, L_6, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToolButton::OnMouseDrag()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* ButtonType_t1054768361_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3694515993;
extern const uint32_t ToolButton_OnMouseDrag_m2923855017_MetadataUsageId;
extern "C"  void ToolButton_OnMouseDrag_m2923855017 (ToolButton_t3848681854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolButton_OnMouseDrag_m2923855017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_Draggable_4();
		if (!L_0)
		{
			goto IL_009f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_1 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = __this->get_MousePositionOffsetZ_8();
		(&V_0)->set_z_3(L_2);
		Camera_t189460977 * L_3 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = V_0;
		NullCheck(L_3);
		Vector3_t2243707580  L_5 = Camera_ScreenToWorldPoint_m929392728(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_localPosition_m2533925116(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = (&V_2)->get_x_1();
		float L_10 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_9, L_10, (0.8f), /*hidden argument*/NULL);
		Vector2_t2243707579 * L_12 = __this->get_address_of_DragPosOffset_5();
		float L_13 = L_12->get_x_0();
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_localPosition_m2533925116(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = (&V_3)->get_y_2();
		float L_17 = (&V_1)->get_y_2();
		float L_18 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_16, L_17, (0.8f), /*hidden argument*/NULL);
		Vector2_t2243707579 * L_19 = __this->get_address_of_DragPosOffset_5();
		float L_20 = L_19->get_y_1();
		Vector2_t2243707579  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3067419446(&L_21, ((float)((float)L_11+(float)L_13)), ((float)((float)L_18+(float)L_20)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localPosition_m1026930133(L_6, L_22, /*hidden argument*/NULL);
	}

IL_009f:
	{
		GameObject_t1756533147 * L_23 = __this->get_OnClick_3();
		int32_t* L_24 = __this->get_address_of_buttonType_2();
		Il2CppObject * L_25 = Box(ButtonType_t1054768361_il2cpp_TypeInfo_var, L_24);
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m2596409543(NULL /*static, unused*/, L_26, _stringLiteral3694515993, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_BroadcastMessage_m3735465996(L_23, L_27, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToolButton::OnCollisionEnter2D(UnityEngine.Collision2D)
extern Il2CppClass* ButtonType_t1054768361_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1110019756;
extern const uint32_t ToolButton_OnCollisionEnter2D_m2904470271_MetadataUsageId;
extern "C"  void ToolButton_OnCollisionEnter2D_m2904470271 (ToolButton_t3848681854 * __this, Collision2D_t1539500754 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolButton_OnCollisionEnter2D_m2904470271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_OnCollision_6();
		int32_t* L_1 = __this->get_address_of_buttonType_2();
		Il2CppObject * L_2 = Box(ButtonType_t1054768361_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, L_3, _stringLiteral1110019756, /*hidden argument*/NULL);
		Collision2D_t1539500754 * L_5 = ___coll0;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Collision2D_get_gameObject_m4234358314(L_5, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_BroadcastMessage_m3472370570(L_0, L_4, L_6, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToolButton::OnCollisionStay2D(UnityEngine.Collision2D)
extern Il2CppClass* ButtonType_t1054768361_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral171123953;
extern const uint32_t ToolButton_OnCollisionStay2D_m1775791980_MetadataUsageId;
extern "C"  void ToolButton_OnCollisionStay2D_m1775791980 (ToolButton_t3848681854 * __this, Collision2D_t1539500754 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolButton_OnCollisionStay2D_m1775791980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_OnCollision_6();
		int32_t* L_1 = __this->get_address_of_buttonType_2();
		Il2CppObject * L_2 = Box(ButtonType_t1054768361_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, L_3, _stringLiteral171123953, /*hidden argument*/NULL);
		Collision2D_t1539500754 * L_5 = ___coll0;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Collision2D_get_gameObject_m4234358314(L_5, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_BroadcastMessage_m3472370570(L_0, L_4, L_6, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ToolButton::ButtonBackOnPosition()
extern Il2CppClass* U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669_il2cpp_TypeInfo_var;
extern const uint32_t ToolButton_ButtonBackOnPosition_m755920680_MetadataUsageId;
extern "C"  Il2CppObject * ToolButton_ButtonBackOnPosition_m755920680 (ToolButton_t3848681854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolButton_ButtonBackOnPosition_m755920680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * V_0 = NULL;
	{
		U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * L_0 = (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 *)il2cpp_codegen_object_new(U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669_il2cpp_TypeInfo_var);
		U3CButtonBackOnPositionU3Ec__Iterator0__ctor_m1884971012(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ToolButton/<ButtonBackOnPosition>c__Iterator0::.ctor()
extern "C"  void U3CButtonBackOnPositionU3Ec__Iterator0__ctor_m1884971012 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ToolButton/<ButtonBackOnPosition>c__Iterator0::MoveNext()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t U3CButtonBackOnPositionU3Ec__Iterator0_MoveNext_m2791120256_MetadataUsageId;
extern "C"  bool U3CButtonBackOnPositionU3Ec__Iterator0_MoveNext_m2791120256 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CButtonBackOnPositionU3Ec__Iterator0_MoveNext_m2791120256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00b2;
	}

IL_0021:
	{
		goto IL_0081;
	}

IL_0026:
	{
		ToolButton_t3848681854 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		ToolButton_t3848681854 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_localPosition_m2533925116(L_5, /*hidden argument*/NULL);
		ToolButton_t3848681854 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = L_7->get_InitialPos_7();
		float L_9 = Time_get_smoothDeltaTime_m1294084638(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_6, L_8, ((float)((float)L_9*(float)(5.0f))), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localPosition_m1026930133(L_3, L_10, /*hidden argument*/NULL);
		int32_t L_11 = 0;
		Il2CppObject * L_12 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_11);
		__this->set_U24current_1(L_12);
		bool L_13 = __this->get_U24disposing_2();
		if (L_13)
		{
			goto IL_007c;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_007c:
	{
		goto IL_00b4;
	}

IL_0081:
	{
		ToolButton_t3848681854 * L_14 = __this->get_U24this_0();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_localPosition_m2533925116(L_15, /*hidden argument*/NULL);
		ToolButton_t3848681854 * L_17 = __this->get_U24this_0();
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = L_17->get_InitialPos_7();
		float L_19 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		if ((((float)L_19) >= ((float)(0.01f))))
		{
			goto IL_0026;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_00b2:
	{
		return (bool)0;
	}

IL_00b4:
	{
		return (bool)1;
	}
}
// System.Object ToolButton/<ButtonBackOnPosition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CButtonBackOnPositionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2628418430 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ToolButton/<ButtonBackOnPosition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CButtonBackOnPositionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m675756326 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void ToolButton/<ButtonBackOnPosition>c__Iterator0::Dispose()
extern "C"  void U3CButtonBackOnPositionU3Ec__Iterator0_Dispose_m2880566527 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void ToolButton/<ButtonBackOnPosition>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CButtonBackOnPositionU3Ec__Iterator0_Reset_m2591090449_MetadataUsageId;
extern "C"  void U3CButtonBackOnPositionU3Ec__Iterator0_Reset_m2591090449 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CButtonBackOnPositionU3Ec__Iterator0_Reset_m2591090449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void TouchOnStar::.ctor()
extern "C"  void TouchOnStar__ctor_m234424267 (TouchOnStar_t2108028178 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchOnStar::OnMouseDown()
extern Il2CppCodeGenString* _stringLiteral216522082;
extern const uint32_t TouchOnStar_OnMouseDown_m3839092973_MetadataUsageId;
extern "C"  void TouchOnStar_OnMouseDown_m3839092973 (TouchOnStar_t2108028178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchOnStar_OnMouseDown_m3839092973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_OnClick_2();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_BroadcastMessage_m4050727382(L_0, _stringLiteral216522082, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
