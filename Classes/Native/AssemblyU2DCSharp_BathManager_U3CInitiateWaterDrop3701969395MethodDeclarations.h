﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BathManager/<InitiateWaterDrops>c__Iterator1
struct U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BathManager/<InitiateWaterDrops>c__Iterator1::.ctor()
extern "C"  void U3CInitiateWaterDropsU3Ec__Iterator1__ctor_m3891541794 (U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BathManager/<InitiateWaterDrops>c__Iterator1::MoveNext()
extern "C"  bool U3CInitiateWaterDropsU3Ec__Iterator1_MoveNext_m2415032482 (U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BathManager/<InitiateWaterDrops>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInitiateWaterDropsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1861219554 (U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BathManager/<InitiateWaterDrops>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInitiateWaterDropsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3733019498 (U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager/<InitiateWaterDrops>c__Iterator1::Dispose()
extern "C"  void U3CInitiateWaterDropsU3Ec__Iterator1_Dispose_m2389602913 (U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager/<InitiateWaterDrops>c__Iterator1::Reset()
extern "C"  void U3CInitiateWaterDropsU3Ec__Iterator1_Reset_m1285244811 (U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
