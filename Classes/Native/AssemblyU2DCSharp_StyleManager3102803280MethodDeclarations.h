﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StyleManager
struct StyleManager_t3102803280;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void StyleManager::.ctor()
extern "C"  void StyleManager__ctor_m813100335 (StyleManager_t3102803280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StyleManager::Start()
extern "C"  Il2CppObject * StyleManager_Start_m157888413 (StyleManager_t3102803280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StyleManager::HatButtonMouseDown()
extern "C"  void StyleManager_HatButtonMouseDown_m4039786343 (StyleManager_t3102803280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StyleManager::GogglesButtonMouseDown()
extern "C"  void StyleManager_GogglesButtonMouseDown_m2318320148 (StyleManager_t3102803280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StyleManager::PendantButtonMouseDown()
extern "C"  void StyleManager_PendantButtonMouseDown_m4289155414 (StyleManager_t3102803280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
