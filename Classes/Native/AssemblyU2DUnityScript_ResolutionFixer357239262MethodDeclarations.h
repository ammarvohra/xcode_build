﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResolutionFixer
struct ResolutionFixer_t357239262;

#include "codegen/il2cpp-codegen.h"

// System.Void ResolutionFixer::.ctor()
extern "C"  void ResolutionFixer__ctor_m4009208722 (ResolutionFixer_t357239262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResolutionFixer::Start()
extern "C"  void ResolutionFixer_Start_m1398064002 (ResolutionFixer_t357239262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResolutionFixer::Main()
extern "C"  void ResolutionFixer_Main_m974232635 (ResolutionFixer_t357239262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
