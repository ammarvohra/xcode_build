﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChartboostSDK.CBJSON/Serializer
struct Serializer_t933296411;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChartboostSDK.CBJSON/Serializer::.ctor()
extern "C"  void Serializer__ctor_m746079510 (Serializer_t933296411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBJSON/Serializer::Serialize(System.Object)
extern "C"  String_t* Serializer_Serialize_m2815513607 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBJSON/Serializer::SerializeValue(System.Object)
extern "C"  void Serializer_SerializeValue_m3711930337 (Serializer_t933296411 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBJSON/Serializer::SerializeObject(System.Collections.Hashtable)
extern "C"  void Serializer_SerializeObject_m955668597 (Serializer_t933296411 * __this, Hashtable_t909839986 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBJSON/Serializer::SerializeArray(System.Collections.ArrayList)
extern "C"  void Serializer_SerializeArray_m2478755716 (Serializer_t933296411 * __this, ArrayList_t4252133567 * ___anArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBJSON/Serializer::SerializeString(System.String)
extern "C"  void Serializer_SerializeString_m1438432103 (Serializer_t933296411 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBJSON/Serializer::SerializeOther(System.Object)
extern "C"  void Serializer_SerializeOther_m4290094684 (Serializer_t933296411 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
