﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LiveVideo
struct LiveVideo_t867630853;

#include "codegen/il2cpp-codegen.h"

// System.Void LiveVideo::.ctor()
extern "C"  void LiveVideo__ctor_m2230653968 (LiveVideo_t867630853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LiveVideo::Start()
extern "C"  void LiveVideo_Start_m2651110432 (LiveVideo_t867630853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LiveVideo::Update()
extern "C"  void LiveVideo_Update_m2852551909 (LiveVideo_t867630853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
