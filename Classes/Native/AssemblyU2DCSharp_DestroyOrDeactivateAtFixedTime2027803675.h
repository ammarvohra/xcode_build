﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_DestroyOrDeactivateAtFixedTime_F1161615032.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyOrDeactivateAtFixedTime
struct  DestroyOrDeactivateAtFixedTime_t2027803675  : public MonoBehaviour_t1158329972
{
public:
	// DestroyOrDeactivateAtFixedTime/Function DestroyOrDeactivateAtFixedTime::function
	int32_t ___function_2;
	// System.Single DestroyOrDeactivateAtFixedTime::WaitingTime
	float ___WaitingTime_3;
	// UnityEngine.GameObject DestroyOrDeactivateAtFixedTime::Target
	GameObject_t1756533147 * ___Target_4;
	// System.String DestroyOrDeactivateAtFixedTime::ComponentToDeactive
	String_t* ___ComponentToDeactive_5;

public:
	inline static int32_t get_offset_of_function_2() { return static_cast<int32_t>(offsetof(DestroyOrDeactivateAtFixedTime_t2027803675, ___function_2)); }
	inline int32_t get_function_2() const { return ___function_2; }
	inline int32_t* get_address_of_function_2() { return &___function_2; }
	inline void set_function_2(int32_t value)
	{
		___function_2 = value;
	}

	inline static int32_t get_offset_of_WaitingTime_3() { return static_cast<int32_t>(offsetof(DestroyOrDeactivateAtFixedTime_t2027803675, ___WaitingTime_3)); }
	inline float get_WaitingTime_3() const { return ___WaitingTime_3; }
	inline float* get_address_of_WaitingTime_3() { return &___WaitingTime_3; }
	inline void set_WaitingTime_3(float value)
	{
		___WaitingTime_3 = value;
	}

	inline static int32_t get_offset_of_Target_4() { return static_cast<int32_t>(offsetof(DestroyOrDeactivateAtFixedTime_t2027803675, ___Target_4)); }
	inline GameObject_t1756533147 * get_Target_4() const { return ___Target_4; }
	inline GameObject_t1756533147 ** get_address_of_Target_4() { return &___Target_4; }
	inline void set_Target_4(GameObject_t1756533147 * value)
	{
		___Target_4 = value;
		Il2CppCodeGenWriteBarrier(&___Target_4, value);
	}

	inline static int32_t get_offset_of_ComponentToDeactive_5() { return static_cast<int32_t>(offsetof(DestroyOrDeactivateAtFixedTime_t2027803675, ___ComponentToDeactive_5)); }
	inline String_t* get_ComponentToDeactive_5() const { return ___ComponentToDeactive_5; }
	inline String_t** get_address_of_ComponentToDeactive_5() { return &___ComponentToDeactive_5; }
	inline void set_ComponentToDeactive_5(String_t* value)
	{
		___ComponentToDeactive_5 = value;
		Il2CppCodeGenWriteBarrier(&___ComponentToDeactive_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
