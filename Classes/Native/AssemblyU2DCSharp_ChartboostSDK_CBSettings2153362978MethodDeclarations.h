﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChartboostSDK.CBSettings
struct CBSettings_t2153362978;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChartboostSDK.CBSettings::.ctor()
extern "C"  void CBSettings__ctor_m3575649668 (CBSettings_t2153362978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChartboostSDK.CBSettings ChartboostSDK.CBSettings::get_Instance()
extern "C"  CBSettings_t2153362978 * CBSettings_get_Instance_m2993910280 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::setAppId(System.String,System.String)
extern "C"  void CBSettings_setAppId_m111225686 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, String_t* ___appSignature1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::SetAndroidPlatformIndex(System.Int32)
extern "C"  void CBSettings_SetAndroidPlatformIndex_m1543110093 (CBSettings_t2153362978 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ChartboostSDK.CBSettings::get_SelectedAndroidPlatformIndex()
extern "C"  int32_t CBSettings_get_SelectedAndroidPlatformIndex_m637474536 (CBSettings_t2153362978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ChartboostSDK.CBSettings::get_AndroidPlatformLabels()
extern "C"  StringU5BU5D_t1642385972* CBSettings_get_AndroidPlatformLabels_m2082342975 (CBSettings_t2153362978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::set_AndroidPlatformLabels(System.String[])
extern "C"  void CBSettings_set_AndroidPlatformLabels_m1686313114 (CBSettings_t2153362978 * __this, StringU5BU5D_t1642385972* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::SetIOSAppId(System.String)
extern "C"  void CBSettings_SetIOSAppId_m3661979773 (CBSettings_t2153362978 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBSettings::getIOSAppId()
extern "C"  String_t* CBSettings_getIOSAppId_m2368322546 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::SetIOSAppSecret(System.String)
extern "C"  void CBSettings_SetIOSAppSecret_m2915993690 (CBSettings_t2153362978 * __this, String_t* ___secret0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBSettings::getIOSAppSecret()
extern "C"  String_t* CBSettings_getIOSAppSecret_m2481062625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::SetAndroidAppId(System.String)
extern "C"  void CBSettings_SetAndroidAppId_m1080882367 (CBSettings_t2153362978 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBSettings::getAndroidAppId()
extern "C"  String_t* CBSettings_getAndroidAppId_m2831139948 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::SetAndroidAppSecret(System.String)
extern "C"  void CBSettings_SetAndroidAppSecret_m3496117672 (CBSettings_t2153362978 * __this, String_t* ___secret0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBSettings::getAndroidAppSecret()
extern "C"  String_t* CBSettings_getAndroidAppSecret_m47008599 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::SetAmazonAppId(System.String)
extern "C"  void CBSettings_SetAmazonAppId_m2510986458 (CBSettings_t2153362978 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBSettings::getAmazonAppId()
extern "C"  String_t* CBSettings_getAmazonAppId_m2706348033 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::SetAmazonAppSecret(System.String)
extern "C"  void CBSettings_SetAmazonAppSecret_m3426505929 (CBSettings_t2153362978 * __this, String_t* ___secret0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBSettings::getAmazonAppSecret()
extern "C"  String_t* CBSettings_getAmazonAppSecret_m3338655728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBSettings::getSelectAndroidAppId()
extern "C"  String_t* CBSettings_getSelectAndroidAppId_m3719588100 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBSettings::getSelectAndroidAppSecret()
extern "C"  String_t* CBSettings_getSelectAndroidAppSecret_m3034407199 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::enableLogging(System.Boolean)
extern "C"  void CBSettings_enableLogging_m3320217801 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBSettings::isLogging()
extern "C"  bool CBSettings_isLogging_m2819380441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::DirtyEditor()
extern "C"  void CBSettings_DirtyEditor_m3882178927 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::CredentialsWarning(System.String,System.String,System.String)
extern "C"  void CBSettings_CredentialsWarning_m4215256806 (Il2CppObject * __this /* static, unused */, String_t* ___warning0, String_t* ___platform1, String_t* ___field2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::resetSettings()
extern "C"  void CBSettings_resetSettings_m4140204110 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBSettings::.cctor()
extern "C"  void CBSettings__cctor_m3860768419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
