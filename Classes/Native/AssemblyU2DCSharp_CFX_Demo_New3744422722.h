﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIText
struct GUIText_t2411476300;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_New
struct  CFX_Demo_New_t3744422722  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GUIText CFX_Demo_New::EffectLabel
	GUIText_t2411476300 * ___EffectLabel_2;
	// UnityEngine.GUIText CFX_Demo_New::EffectIndexLabel
	GUIText_t2411476300 * ___EffectIndexLabel_3;
	// UnityEngine.Renderer CFX_Demo_New::groundRenderer
	Renderer_t257310565 * ___groundRenderer_4;
	// UnityEngine.Collider CFX_Demo_New::groundCollider
	Collider_t3497673348 * ___groundCollider_5;
	// UnityEngine.GameObject[] CFX_Demo_New::ParticleExamples
	GameObjectU5BU5D_t3057952154* ___ParticleExamples_6;
	// System.Int32 CFX_Demo_New::exampleIndex
	int32_t ___exampleIndex_7;
	// System.Boolean CFX_Demo_New::slowMo
	bool ___slowMo_8;
	// UnityEngine.Vector3 CFX_Demo_New::defaultCamPosition
	Vector3_t2243707580  ___defaultCamPosition_9;
	// UnityEngine.Quaternion CFX_Demo_New::defaultCamRotation
	Quaternion_t4030073918  ___defaultCamRotation_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CFX_Demo_New::onScreenParticles
	List_1_t1125654279 * ___onScreenParticles_11;

public:
	inline static int32_t get_offset_of_EffectLabel_2() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___EffectLabel_2)); }
	inline GUIText_t2411476300 * get_EffectLabel_2() const { return ___EffectLabel_2; }
	inline GUIText_t2411476300 ** get_address_of_EffectLabel_2() { return &___EffectLabel_2; }
	inline void set_EffectLabel_2(GUIText_t2411476300 * value)
	{
		___EffectLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___EffectLabel_2, value);
	}

	inline static int32_t get_offset_of_EffectIndexLabel_3() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___EffectIndexLabel_3)); }
	inline GUIText_t2411476300 * get_EffectIndexLabel_3() const { return ___EffectIndexLabel_3; }
	inline GUIText_t2411476300 ** get_address_of_EffectIndexLabel_3() { return &___EffectIndexLabel_3; }
	inline void set_EffectIndexLabel_3(GUIText_t2411476300 * value)
	{
		___EffectIndexLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___EffectIndexLabel_3, value);
	}

	inline static int32_t get_offset_of_groundRenderer_4() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___groundRenderer_4)); }
	inline Renderer_t257310565 * get_groundRenderer_4() const { return ___groundRenderer_4; }
	inline Renderer_t257310565 ** get_address_of_groundRenderer_4() { return &___groundRenderer_4; }
	inline void set_groundRenderer_4(Renderer_t257310565 * value)
	{
		___groundRenderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___groundRenderer_4, value);
	}

	inline static int32_t get_offset_of_groundCollider_5() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___groundCollider_5)); }
	inline Collider_t3497673348 * get_groundCollider_5() const { return ___groundCollider_5; }
	inline Collider_t3497673348 ** get_address_of_groundCollider_5() { return &___groundCollider_5; }
	inline void set_groundCollider_5(Collider_t3497673348 * value)
	{
		___groundCollider_5 = value;
		Il2CppCodeGenWriteBarrier(&___groundCollider_5, value);
	}

	inline static int32_t get_offset_of_ParticleExamples_6() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___ParticleExamples_6)); }
	inline GameObjectU5BU5D_t3057952154* get_ParticleExamples_6() const { return ___ParticleExamples_6; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ParticleExamples_6() { return &___ParticleExamples_6; }
	inline void set_ParticleExamples_6(GameObjectU5BU5D_t3057952154* value)
	{
		___ParticleExamples_6 = value;
		Il2CppCodeGenWriteBarrier(&___ParticleExamples_6, value);
	}

	inline static int32_t get_offset_of_exampleIndex_7() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___exampleIndex_7)); }
	inline int32_t get_exampleIndex_7() const { return ___exampleIndex_7; }
	inline int32_t* get_address_of_exampleIndex_7() { return &___exampleIndex_7; }
	inline void set_exampleIndex_7(int32_t value)
	{
		___exampleIndex_7 = value;
	}

	inline static int32_t get_offset_of_slowMo_8() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___slowMo_8)); }
	inline bool get_slowMo_8() const { return ___slowMo_8; }
	inline bool* get_address_of_slowMo_8() { return &___slowMo_8; }
	inline void set_slowMo_8(bool value)
	{
		___slowMo_8 = value;
	}

	inline static int32_t get_offset_of_defaultCamPosition_9() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___defaultCamPosition_9)); }
	inline Vector3_t2243707580  get_defaultCamPosition_9() const { return ___defaultCamPosition_9; }
	inline Vector3_t2243707580 * get_address_of_defaultCamPosition_9() { return &___defaultCamPosition_9; }
	inline void set_defaultCamPosition_9(Vector3_t2243707580  value)
	{
		___defaultCamPosition_9 = value;
	}

	inline static int32_t get_offset_of_defaultCamRotation_10() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___defaultCamRotation_10)); }
	inline Quaternion_t4030073918  get_defaultCamRotation_10() const { return ___defaultCamRotation_10; }
	inline Quaternion_t4030073918 * get_address_of_defaultCamRotation_10() { return &___defaultCamRotation_10; }
	inline void set_defaultCamRotation_10(Quaternion_t4030073918  value)
	{
		___defaultCamRotation_10 = value;
	}

	inline static int32_t get_offset_of_onScreenParticles_11() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___onScreenParticles_11)); }
	inline List_1_t1125654279 * get_onScreenParticles_11() const { return ___onScreenParticles_11; }
	inline List_1_t1125654279 ** get_address_of_onScreenParticles_11() { return &___onScreenParticles_11; }
	inline void set_onScreenParticles_11(List_1_t1125654279 * value)
	{
		___onScreenParticles_11 = value;
		Il2CppCodeGenWriteBarrier(&___onScreenParticles_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
