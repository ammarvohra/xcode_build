﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonManager/<PlayButtonUp>c__Iterator1
struct U3CPlayButtonUpU3Ec__Iterator1_t1133289621;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonManager/<PlayButtonUp>c__Iterator1::.ctor()
extern "C"  void U3CPlayButtonUpU3Ec__Iterator1__ctor_m2814579442 (U3CPlayButtonUpU3Ec__Iterator1_t1133289621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonManager/<PlayButtonUp>c__Iterator1::MoveNext()
extern "C"  bool U3CPlayButtonUpU3Ec__Iterator1_MoveNext_m3183973546 (U3CPlayButtonUpU3Ec__Iterator1_t1133289621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ButtonManager/<PlayButtonUp>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayButtonUpU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2801171998 (U3CPlayButtonUpU3Ec__Iterator1_t1133289621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ButtonManager/<PlayButtonUp>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayButtonUpU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2040341926 (U3CPlayButtonUpU3Ec__Iterator1_t1133289621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager/<PlayButtonUp>c__Iterator1::Dispose()
extern "C"  void U3CPlayButtonUpU3Ec__Iterator1_Dispose_m654512447 (U3CPlayButtonUpU3Ec__Iterator1_t1133289621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager/<PlayButtonUp>c__Iterator1::Reset()
extern "C"  void U3CPlayButtonUpU3Ec__Iterator1_Reset_m2634708009 (U3CPlayButtonUpU3Ec__Iterator1_t1133289621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
