﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BubbleWaterTalkMoving
struct BubbleWaterTalkMoving_t3963847219;

#include "codegen/il2cpp-codegen.h"

// System.Void BubbleWaterTalkMoving::.ctor()
extern "C"  void BubbleWaterTalkMoving__ctor_m2006371864 (BubbleWaterTalkMoving_t3963847219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BubbleWaterTalkMoving::Start()
extern "C"  void BubbleWaterTalkMoving_Start_m1340444196 (BubbleWaterTalkMoving_t3963847219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
