﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonManager
struct  ButtonManager_t868394943  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ButtonManager::PlayButton
	GameObject_t1756533147 * ___PlayButton_2;
	// UnityEngine.GameObject ButtonManager::FbLikeButton
	GameObject_t1756533147 * ___FbLikeButton_3;
	// UnityEngine.GameObject ButtonManager::MuteButton
	GameObject_t1756533147 * ___MuteButton_4;
	// UnityEngine.Sprite ButtonManager::SoundOn
	Sprite_t309593783 * ___SoundOn_5;
	// UnityEngine.Sprite ButtonManager::SoundOff
	Sprite_t309593783 * ___SoundOff_6;

public:
	inline static int32_t get_offset_of_PlayButton_2() { return static_cast<int32_t>(offsetof(ButtonManager_t868394943, ___PlayButton_2)); }
	inline GameObject_t1756533147 * get_PlayButton_2() const { return ___PlayButton_2; }
	inline GameObject_t1756533147 ** get_address_of_PlayButton_2() { return &___PlayButton_2; }
	inline void set_PlayButton_2(GameObject_t1756533147 * value)
	{
		___PlayButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___PlayButton_2, value);
	}

	inline static int32_t get_offset_of_FbLikeButton_3() { return static_cast<int32_t>(offsetof(ButtonManager_t868394943, ___FbLikeButton_3)); }
	inline GameObject_t1756533147 * get_FbLikeButton_3() const { return ___FbLikeButton_3; }
	inline GameObject_t1756533147 ** get_address_of_FbLikeButton_3() { return &___FbLikeButton_3; }
	inline void set_FbLikeButton_3(GameObject_t1756533147 * value)
	{
		___FbLikeButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___FbLikeButton_3, value);
	}

	inline static int32_t get_offset_of_MuteButton_4() { return static_cast<int32_t>(offsetof(ButtonManager_t868394943, ___MuteButton_4)); }
	inline GameObject_t1756533147 * get_MuteButton_4() const { return ___MuteButton_4; }
	inline GameObject_t1756533147 ** get_address_of_MuteButton_4() { return &___MuteButton_4; }
	inline void set_MuteButton_4(GameObject_t1756533147 * value)
	{
		___MuteButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___MuteButton_4, value);
	}

	inline static int32_t get_offset_of_SoundOn_5() { return static_cast<int32_t>(offsetof(ButtonManager_t868394943, ___SoundOn_5)); }
	inline Sprite_t309593783 * get_SoundOn_5() const { return ___SoundOn_5; }
	inline Sprite_t309593783 ** get_address_of_SoundOn_5() { return &___SoundOn_5; }
	inline void set_SoundOn_5(Sprite_t309593783 * value)
	{
		___SoundOn_5 = value;
		Il2CppCodeGenWriteBarrier(&___SoundOn_5, value);
	}

	inline static int32_t get_offset_of_SoundOff_6() { return static_cast<int32_t>(offsetof(ButtonManager_t868394943, ___SoundOff_6)); }
	inline Sprite_t309593783 * get_SoundOff_6() const { return ___SoundOff_6; }
	inline Sprite_t309593783 ** get_address_of_SoundOff_6() { return &___SoundOff_6; }
	inline void set_SoundOff_6(Sprite_t309593783 * value)
	{
		___SoundOff_6 = value;
		Il2CppCodeGenWriteBarrier(&___SoundOff_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
