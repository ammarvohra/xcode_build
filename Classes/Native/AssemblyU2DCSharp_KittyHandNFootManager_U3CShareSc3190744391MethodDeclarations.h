﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<ShareScreenshot>c__IteratorC
struct U3CShareScreenshotU3Ec__IteratorC_t3190744391;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<ShareScreenshot>c__IteratorC::.ctor()
extern "C"  void U3CShareScreenshotU3Ec__IteratorC__ctor_m3064057836 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<ShareScreenshot>c__IteratorC::MoveNext()
extern "C"  bool U3CShareScreenshotU3Ec__IteratorC_MoveNext_m4144719932 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<ShareScreenshot>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShareScreenshotU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m649825954 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<ShareScreenshot>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShareScreenshotU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m470195962 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<ShareScreenshot>c__IteratorC::Dispose()
extern "C"  void U3CShareScreenshotU3Ec__IteratorC_Dispose_m3289387241 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<ShareScreenshot>c__IteratorC::Reset()
extern "C"  void U3CShareScreenshotU3Ec__IteratorC_Reset_m1221017159 (U3CShareScreenshotU3Ec__IteratorC_t3190744391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
