﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_GTToggle
struct CFX_Demo_GTToggle_t2599739681;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_GTToggle::.ctor()
extern "C"  void CFX_Demo_GTToggle__ctor_m1625378442 (CFX_Demo_GTToggle_t2599739681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTToggle::Awake()
extern "C"  void CFX_Demo_GTToggle_Awake_m3306778225 (CFX_Demo_GTToggle_t2599739681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTToggle::Update()
extern "C"  void CFX_Demo_GTToggle_Update_m3617822317 (CFX_Demo_GTToggle_t2599739681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTToggle::OnClick()
extern "C"  void CFX_Demo_GTToggle_OnClick_m3991348591 (CFX_Demo_GTToggle_t2599739681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTToggle::UpdateTexture()
extern "C"  void CFX_Demo_GTToggle_UpdateTexture_m1942358266 (CFX_Demo_GTToggle_t2599739681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
