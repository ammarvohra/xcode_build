﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BubbleAnimationHomeScreen
struct BubbleAnimationHomeScreen_t878545717;

#include "codegen/il2cpp-codegen.h"

// System.Void BubbleAnimationHomeScreen::.ctor()
extern "C"  void BubbleAnimationHomeScreen__ctor_m3857294622 (BubbleAnimationHomeScreen_t878545717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BubbleAnimationHomeScreen::Start()
extern "C"  void BubbleAnimationHomeScreen_Start_m1024821798 (BubbleAnimationHomeScreen_t878545717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BubbleAnimationHomeScreen::CreateRandomBubbles()
extern "C"  void BubbleAnimationHomeScreen_CreateRandomBubbles_m1032966360 (BubbleAnimationHomeScreen_t878545717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
