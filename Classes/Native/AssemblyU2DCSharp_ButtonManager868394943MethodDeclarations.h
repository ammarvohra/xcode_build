﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonManager
struct ButtonManager_t868394943;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonManager::.ctor()
extern "C"  void ButtonManager__ctor_m1858121770 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ButtonManager::Start()
extern "C"  Il2CppObject * ButtonManager_Start_m2574826574 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::Update()
extern "C"  void ButtonManager_Update_m1213000415 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::PlayButtonMouseDown()
extern "C"  void ButtonManager_PlayButtonMouseDown_m1752028751 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::PlayButtonMouseUp()
extern "C"  void ButtonManager_PlayButtonMouseUp_m3810507178 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::FbLikeButtonMouseDown()
extern "C"  void ButtonManager_FbLikeButtonMouseDown_m2410543304 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::FbLikeButtonMouseUp()
extern "C"  void ButtonManager_FbLikeButtonMouseUp_m2967697951 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::MuteButtonMouseDown()
extern "C"  void ButtonManager_MuteButtonMouseDown_m1466092778 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ButtonManager::PlayButtonUp()
extern "C"  Il2CppObject * ButtonManager_PlayButtonUp_m2508579051 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ButtonManager::FbLikeButtonUp()
extern "C"  Il2CppObject * ButtonManager_FbLikeButtonUp_m220770254 (ButtonManager_t868394943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
