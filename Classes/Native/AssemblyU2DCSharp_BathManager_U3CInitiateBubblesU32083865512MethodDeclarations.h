﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BathManager/<InitiateBubbles>c__Iterator0
struct U3CInitiateBubblesU3Ec__Iterator0_t2083865512;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BathManager/<InitiateBubbles>c__Iterator0::.ctor()
extern "C"  void U3CInitiateBubblesU3Ec__Iterator0__ctor_m4067800245 (U3CInitiateBubblesU3Ec__Iterator0_t2083865512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BathManager/<InitiateBubbles>c__Iterator0::MoveNext()
extern "C"  bool U3CInitiateBubblesU3Ec__Iterator0_MoveNext_m1292349215 (U3CInitiateBubblesU3Ec__Iterator0_t2083865512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BathManager/<InitiateBubbles>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInitiateBubblesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4258742019 (U3CInitiateBubblesU3Ec__Iterator0_t2083865512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BathManager/<InitiateBubbles>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInitiateBubblesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m47666011 (U3CInitiateBubblesU3Ec__Iterator0_t2083865512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager/<InitiateBubbles>c__Iterator0::Dispose()
extern "C"  void U3CInitiateBubblesU3Ec__Iterator0_Dispose_m3084973738 (U3CInitiateBubblesU3Ec__Iterator0_t2083865512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager/<InitiateBubbles>c__Iterator0::Reset()
extern "C"  void U3CInitiateBubblesU3Ec__Iterator0_Reset_m2202438632 (U3CInitiateBubblesU3Ec__Iterator0_t2083865512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
