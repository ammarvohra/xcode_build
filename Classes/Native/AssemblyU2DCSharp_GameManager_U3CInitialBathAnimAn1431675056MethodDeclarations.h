﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager/<InitialBathAnimAndSoapAvailable>c__Iterator0
struct U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager/<InitialBathAnimAndSoapAvailable>c__Iterator0::.ctor()
extern "C"  void U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0__ctor_m3044668955 (U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager/<InitialBathAnimAndSoapAvailable>c__Iterator0::MoveNext()
extern "C"  bool U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_MoveNext_m3647801849 (U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<InitialBathAnimAndSoapAvailable>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3841113281 (U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<InitialBathAnimAndSoapAvailable>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2967230361 (U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<InitialBathAnimAndSoapAvailable>c__Iterator0::Dispose()
extern "C"  void U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_Dispose_m2446735402 (U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<InitialBathAnimAndSoapAvailable>c__Iterator0::Reset()
extern "C"  void U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_Reset_m3736415224 (U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
