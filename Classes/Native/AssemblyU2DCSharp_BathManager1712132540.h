﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BathManager
struct  BathManager_t1712132540  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject BathManager::Soap
	GameObject_t1756533147 * ___Soap_2;
	// UnityEngine.GameObject BathManager::Shower
	GameObject_t1756533147 * ___Shower_3;
	// UnityEngine.GameObject BathManager::Towel
	GameObject_t1756533147 * ___Towel_4;
	// UnityEngine.GameObject BathManager::Dryer
	GameObject_t1756533147 * ___Dryer_5;
	// UnityEngine.GameObject[] BathManager::Bubble
	GameObjectU5BU5D_t3057952154* ___Bubble_6;
	// UnityEngine.GameObject BathManager::BubbleOnKitty
	GameObject_t1756533147 * ___BubbleOnKitty_7;
	// UnityEngine.GameObject[] BathManager::WaterDrop
	GameObjectU5BU5D_t3057952154* ___WaterDrop_8;
	// UnityEngine.GameObject BathManager::BigWaterDrop
	GameObject_t1756533147 * ___BigWaterDrop_9;
	// UnityEngine.GameObject BathManager::ShowerEffects
	GameObject_t1756533147 * ___ShowerEffects_10;
	// UnityEngine.GameObject BathManager::Smoke
	GameObject_t1756533147 * ___Smoke_11;
	// UnityEngine.GameObject BathManager::Star
	GameObject_t1756533147 * ___Star_12;
	// System.Single BathManager::CountTimer
	float ___CountTimer_13;
	// UnityEngine.GameObject BathManager::Curtain
	GameObject_t1756533147 * ___Curtain_14;
	// UnityEngine.GameObject BathManager::LocketStar
	GameObject_t1756533147 * ___LocketStar_15;
	// UnityEngine.GameObject BathManager::CurtainTassel
	GameObject_t1756533147 * ___CurtainTassel_16;
	// System.Boolean BathManager::BubbleInitiateFlag
	bool ___BubbleInitiateFlag_17;
	// System.Boolean BathManager::WaterDropInitiateFlag
	bool ___WaterDropInitiateFlag_18;
	// System.Boolean BathManager::StarInitiateFlag
	bool ___StarInitiateFlag_19;
	// System.Boolean BathManager::OnShowerFirstTime
	bool ___OnShowerFirstTime_20;
	// System.Boolean BathManager::OnTowelFirstTime
	bool ___OnTowelFirstTime_21;
	// System.Boolean BathManager::OnDryerFirstTime
	bool ___OnDryerFirstTime_22;

public:
	inline static int32_t get_offset_of_Soap_2() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___Soap_2)); }
	inline GameObject_t1756533147 * get_Soap_2() const { return ___Soap_2; }
	inline GameObject_t1756533147 ** get_address_of_Soap_2() { return &___Soap_2; }
	inline void set_Soap_2(GameObject_t1756533147 * value)
	{
		___Soap_2 = value;
		Il2CppCodeGenWriteBarrier(&___Soap_2, value);
	}

	inline static int32_t get_offset_of_Shower_3() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___Shower_3)); }
	inline GameObject_t1756533147 * get_Shower_3() const { return ___Shower_3; }
	inline GameObject_t1756533147 ** get_address_of_Shower_3() { return &___Shower_3; }
	inline void set_Shower_3(GameObject_t1756533147 * value)
	{
		___Shower_3 = value;
		Il2CppCodeGenWriteBarrier(&___Shower_3, value);
	}

	inline static int32_t get_offset_of_Towel_4() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___Towel_4)); }
	inline GameObject_t1756533147 * get_Towel_4() const { return ___Towel_4; }
	inline GameObject_t1756533147 ** get_address_of_Towel_4() { return &___Towel_4; }
	inline void set_Towel_4(GameObject_t1756533147 * value)
	{
		___Towel_4 = value;
		Il2CppCodeGenWriteBarrier(&___Towel_4, value);
	}

	inline static int32_t get_offset_of_Dryer_5() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___Dryer_5)); }
	inline GameObject_t1756533147 * get_Dryer_5() const { return ___Dryer_5; }
	inline GameObject_t1756533147 ** get_address_of_Dryer_5() { return &___Dryer_5; }
	inline void set_Dryer_5(GameObject_t1756533147 * value)
	{
		___Dryer_5 = value;
		Il2CppCodeGenWriteBarrier(&___Dryer_5, value);
	}

	inline static int32_t get_offset_of_Bubble_6() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___Bubble_6)); }
	inline GameObjectU5BU5D_t3057952154* get_Bubble_6() const { return ___Bubble_6; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_Bubble_6() { return &___Bubble_6; }
	inline void set_Bubble_6(GameObjectU5BU5D_t3057952154* value)
	{
		___Bubble_6 = value;
		Il2CppCodeGenWriteBarrier(&___Bubble_6, value);
	}

	inline static int32_t get_offset_of_BubbleOnKitty_7() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___BubbleOnKitty_7)); }
	inline GameObject_t1756533147 * get_BubbleOnKitty_7() const { return ___BubbleOnKitty_7; }
	inline GameObject_t1756533147 ** get_address_of_BubbleOnKitty_7() { return &___BubbleOnKitty_7; }
	inline void set_BubbleOnKitty_7(GameObject_t1756533147 * value)
	{
		___BubbleOnKitty_7 = value;
		Il2CppCodeGenWriteBarrier(&___BubbleOnKitty_7, value);
	}

	inline static int32_t get_offset_of_WaterDrop_8() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___WaterDrop_8)); }
	inline GameObjectU5BU5D_t3057952154* get_WaterDrop_8() const { return ___WaterDrop_8; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_WaterDrop_8() { return &___WaterDrop_8; }
	inline void set_WaterDrop_8(GameObjectU5BU5D_t3057952154* value)
	{
		___WaterDrop_8 = value;
		Il2CppCodeGenWriteBarrier(&___WaterDrop_8, value);
	}

	inline static int32_t get_offset_of_BigWaterDrop_9() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___BigWaterDrop_9)); }
	inline GameObject_t1756533147 * get_BigWaterDrop_9() const { return ___BigWaterDrop_9; }
	inline GameObject_t1756533147 ** get_address_of_BigWaterDrop_9() { return &___BigWaterDrop_9; }
	inline void set_BigWaterDrop_9(GameObject_t1756533147 * value)
	{
		___BigWaterDrop_9 = value;
		Il2CppCodeGenWriteBarrier(&___BigWaterDrop_9, value);
	}

	inline static int32_t get_offset_of_ShowerEffects_10() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___ShowerEffects_10)); }
	inline GameObject_t1756533147 * get_ShowerEffects_10() const { return ___ShowerEffects_10; }
	inline GameObject_t1756533147 ** get_address_of_ShowerEffects_10() { return &___ShowerEffects_10; }
	inline void set_ShowerEffects_10(GameObject_t1756533147 * value)
	{
		___ShowerEffects_10 = value;
		Il2CppCodeGenWriteBarrier(&___ShowerEffects_10, value);
	}

	inline static int32_t get_offset_of_Smoke_11() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___Smoke_11)); }
	inline GameObject_t1756533147 * get_Smoke_11() const { return ___Smoke_11; }
	inline GameObject_t1756533147 ** get_address_of_Smoke_11() { return &___Smoke_11; }
	inline void set_Smoke_11(GameObject_t1756533147 * value)
	{
		___Smoke_11 = value;
		Il2CppCodeGenWriteBarrier(&___Smoke_11, value);
	}

	inline static int32_t get_offset_of_Star_12() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___Star_12)); }
	inline GameObject_t1756533147 * get_Star_12() const { return ___Star_12; }
	inline GameObject_t1756533147 ** get_address_of_Star_12() { return &___Star_12; }
	inline void set_Star_12(GameObject_t1756533147 * value)
	{
		___Star_12 = value;
		Il2CppCodeGenWriteBarrier(&___Star_12, value);
	}

	inline static int32_t get_offset_of_CountTimer_13() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___CountTimer_13)); }
	inline float get_CountTimer_13() const { return ___CountTimer_13; }
	inline float* get_address_of_CountTimer_13() { return &___CountTimer_13; }
	inline void set_CountTimer_13(float value)
	{
		___CountTimer_13 = value;
	}

	inline static int32_t get_offset_of_Curtain_14() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___Curtain_14)); }
	inline GameObject_t1756533147 * get_Curtain_14() const { return ___Curtain_14; }
	inline GameObject_t1756533147 ** get_address_of_Curtain_14() { return &___Curtain_14; }
	inline void set_Curtain_14(GameObject_t1756533147 * value)
	{
		___Curtain_14 = value;
		Il2CppCodeGenWriteBarrier(&___Curtain_14, value);
	}

	inline static int32_t get_offset_of_LocketStar_15() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___LocketStar_15)); }
	inline GameObject_t1756533147 * get_LocketStar_15() const { return ___LocketStar_15; }
	inline GameObject_t1756533147 ** get_address_of_LocketStar_15() { return &___LocketStar_15; }
	inline void set_LocketStar_15(GameObject_t1756533147 * value)
	{
		___LocketStar_15 = value;
		Il2CppCodeGenWriteBarrier(&___LocketStar_15, value);
	}

	inline static int32_t get_offset_of_CurtainTassel_16() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___CurtainTassel_16)); }
	inline GameObject_t1756533147 * get_CurtainTassel_16() const { return ___CurtainTassel_16; }
	inline GameObject_t1756533147 ** get_address_of_CurtainTassel_16() { return &___CurtainTassel_16; }
	inline void set_CurtainTassel_16(GameObject_t1756533147 * value)
	{
		___CurtainTassel_16 = value;
		Il2CppCodeGenWriteBarrier(&___CurtainTassel_16, value);
	}

	inline static int32_t get_offset_of_BubbleInitiateFlag_17() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___BubbleInitiateFlag_17)); }
	inline bool get_BubbleInitiateFlag_17() const { return ___BubbleInitiateFlag_17; }
	inline bool* get_address_of_BubbleInitiateFlag_17() { return &___BubbleInitiateFlag_17; }
	inline void set_BubbleInitiateFlag_17(bool value)
	{
		___BubbleInitiateFlag_17 = value;
	}

	inline static int32_t get_offset_of_WaterDropInitiateFlag_18() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___WaterDropInitiateFlag_18)); }
	inline bool get_WaterDropInitiateFlag_18() const { return ___WaterDropInitiateFlag_18; }
	inline bool* get_address_of_WaterDropInitiateFlag_18() { return &___WaterDropInitiateFlag_18; }
	inline void set_WaterDropInitiateFlag_18(bool value)
	{
		___WaterDropInitiateFlag_18 = value;
	}

	inline static int32_t get_offset_of_StarInitiateFlag_19() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___StarInitiateFlag_19)); }
	inline bool get_StarInitiateFlag_19() const { return ___StarInitiateFlag_19; }
	inline bool* get_address_of_StarInitiateFlag_19() { return &___StarInitiateFlag_19; }
	inline void set_StarInitiateFlag_19(bool value)
	{
		___StarInitiateFlag_19 = value;
	}

	inline static int32_t get_offset_of_OnShowerFirstTime_20() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___OnShowerFirstTime_20)); }
	inline bool get_OnShowerFirstTime_20() const { return ___OnShowerFirstTime_20; }
	inline bool* get_address_of_OnShowerFirstTime_20() { return &___OnShowerFirstTime_20; }
	inline void set_OnShowerFirstTime_20(bool value)
	{
		___OnShowerFirstTime_20 = value;
	}

	inline static int32_t get_offset_of_OnTowelFirstTime_21() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___OnTowelFirstTime_21)); }
	inline bool get_OnTowelFirstTime_21() const { return ___OnTowelFirstTime_21; }
	inline bool* get_address_of_OnTowelFirstTime_21() { return &___OnTowelFirstTime_21; }
	inline void set_OnTowelFirstTime_21(bool value)
	{
		___OnTowelFirstTime_21 = value;
	}

	inline static int32_t get_offset_of_OnDryerFirstTime_22() { return static_cast<int32_t>(offsetof(BathManager_t1712132540, ___OnDryerFirstTime_22)); }
	inline bool get_OnDryerFirstTime_22() const { return ___OnDryerFirstTime_22; }
	inline bool* get_address_of_OnDryerFirstTime_22() { return &___OnDryerFirstTime_22; }
	inline void set_OnDryerFirstTime_22(bool value)
	{
		___OnDryerFirstTime_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
