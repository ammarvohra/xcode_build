﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BubbleAnimationHomeScreen
struct  BubbleAnimationHomeScreen_t878545717  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] BubbleAnimationHomeScreen::Bubbles
	GameObjectU5BU5D_t3057952154* ___Bubbles_2;

public:
	inline static int32_t get_offset_of_Bubbles_2() { return static_cast<int32_t>(offsetof(BubbleAnimationHomeScreen_t878545717, ___Bubbles_2)); }
	inline GameObjectU5BU5D_t3057952154* get_Bubbles_2() const { return ___Bubbles_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_Bubbles_2() { return &___Bubbles_2; }
	inline void set_Bubbles_2(GameObjectU5BU5D_t3057952154* value)
	{
		___Bubbles_2 = value;
		Il2CppCodeGenWriteBarrier(&___Bubbles_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
