﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>
struct ValueCollection_t371629903;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1668570060;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t3847001055;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3355102824.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3515137709_gshared (ValueCollection_t371629903 * __this, Dictionary_2_t1668570060 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3515137709(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t371629903 *, Dictionary_2_t1668570060 *, const MethodInfo*))ValueCollection__ctor_m3515137709_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3922752079_gshared (ValueCollection_t371629903 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3922752079(__this, ___item0, method) ((  void (*) (ValueCollection_t371629903 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3922752079_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2934356600_gshared (ValueCollection_t371629903 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2934356600(__this, method) ((  void (*) (ValueCollection_t371629903 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2934356600_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2885363365_gshared (ValueCollection_t371629903 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2885363365(__this, ___item0, method) ((  bool (*) (ValueCollection_t371629903 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2885363365_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2972460002_gshared (ValueCollection_t371629903 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2972460002(__this, ___item0, method) ((  bool (*) (ValueCollection_t371629903 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2972460002_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1108020688_gshared (ValueCollection_t371629903 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1108020688(__this, method) ((  Il2CppObject* (*) (ValueCollection_t371629903 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1108020688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m4129844936_gshared (ValueCollection_t371629903 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m4129844936(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t371629903 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m4129844936_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1607842279_gshared (ValueCollection_t371629903 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1607842279(__this, method) ((  Il2CppObject * (*) (ValueCollection_t371629903 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1607842279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m443612254_gshared (ValueCollection_t371629903 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m443612254(__this, method) ((  bool (*) (ValueCollection_t371629903 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m443612254_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4287515044_gshared (ValueCollection_t371629903 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4287515044(__this, method) ((  bool (*) (ValueCollection_t371629903 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4287515044_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3331633640_gshared (ValueCollection_t371629903 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3331633640(__this, method) ((  Il2CppObject * (*) (ValueCollection_t371629903 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3331633640_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2913520516_gshared (ValueCollection_t371629903 * __this, SingleU5BU5D_t577127397* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2913520516(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t371629903 *, SingleU5BU5D_t577127397*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2913520516_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::GetEnumerator()
extern "C"  Enumerator_t3355102824  ValueCollection_GetEnumerator_m1902112745_gshared (ValueCollection_t371629903 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1902112745(__this, method) ((  Enumerator_t3355102824  (*) (ValueCollection_t371629903 *, const MethodInfo*))ValueCollection_GetEnumerator_m1902112745_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m4265976900_gshared (ValueCollection_t371629903 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m4265976900(__this, method) ((  int32_t (*) (ValueCollection_t371629903 *, const MethodInfo*))ValueCollection_get_Count_m4265976900_gshared)(__this, method)
