﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundManager/<CloseMouth>c__Iterator0
struct U3CCloseMouthU3Ec__Iterator0_t2695835239;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SoundManager/<CloseMouth>c__Iterator0::.ctor()
extern "C"  void U3CCloseMouthU3Ec__Iterator0__ctor_m932007510 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundManager/<CloseMouth>c__Iterator0::MoveNext()
extern "C"  bool U3CCloseMouthU3Ec__Iterator0_MoveNext_m1226697558 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SoundManager/<CloseMouth>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCloseMouthU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3453636504 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SoundManager/<CloseMouth>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCloseMouthU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m863859696 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager/<CloseMouth>c__Iterator0::Dispose()
extern "C"  void U3CCloseMouthU3Ec__Iterator0_Dispose_m1076368153 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager/<CloseMouth>c__Iterator0::Reset()
extern "C"  void U3CCloseMouthU3Ec__Iterator0_Reset_m579547531 (U3CCloseMouthU3Ec__Iterator0_t2695835239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
