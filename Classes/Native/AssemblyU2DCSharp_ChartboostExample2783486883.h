﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// ChartboostSDK.CBInPlay
struct CBInPlay_t2057847888;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBStatusBarBehavio2216036290.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChartboostExample
struct  ChartboostExample_t2783486883  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ChartboostExample::inPlayIcon
	GameObject_t1756533147 * ___inPlayIcon_2;
	// UnityEngine.GameObject ChartboostExample::inPlayText
	GameObject_t1756533147 * ___inPlayText_3;
	// UnityEngine.Texture2D ChartboostExample::logo
	Texture2D_t3542995729 * ___logo_4;
	// ChartboostSDK.CBInPlay ChartboostExample::inPlayAd
	CBInPlay_t2057847888 * ___inPlayAd_5;
	// UnityEngine.Vector2 ChartboostExample::scrollPosition
	Vector2_t2243707579  ___scrollPosition_6;
	// System.Collections.Generic.List`1<System.String> ChartboostExample::delegateHistory
	List_1_t1398341365 * ___delegateHistory_7;
	// System.Boolean ChartboostExample::hasInterstitial
	bool ___hasInterstitial_8;
	// System.Boolean ChartboostExample::hasMoreApps
	bool ___hasMoreApps_9;
	// System.Boolean ChartboostExample::hasRewardedVideo
	bool ___hasRewardedVideo_10;
	// System.Boolean ChartboostExample::hasInPlay
	bool ___hasInPlay_11;
	// System.Int32 ChartboostExample::frameCount
	int32_t ___frameCount_12;
	// System.Boolean ChartboostExample::ageGate
	bool ___ageGate_13;
	// System.Boolean ChartboostExample::autocache
	bool ___autocache_14;
	// System.Boolean ChartboostExample::activeAgeGate
	bool ___activeAgeGate_15;
	// System.Boolean ChartboostExample::showInterstitial
	bool ___showInterstitial_16;
	// System.Boolean ChartboostExample::showMoreApps
	bool ___showMoreApps_17;
	// System.Boolean ChartboostExample::showRewardedVideo
	bool ___showRewardedVideo_18;
	// System.Int32 ChartboostExample::BANNER_HEIGHT
	int32_t ___BANNER_HEIGHT_19;
	// System.Int32 ChartboostExample::REQUIRED_HEIGHT
	int32_t ___REQUIRED_HEIGHT_20;
	// System.Int32 ChartboostExample::ELEMENT_WIDTH
	int32_t ___ELEMENT_WIDTH_21;
	// UnityEngine.Rect ChartboostExample::scrollRect
	Rect_t3681755626  ___scrollRect_22;
	// UnityEngine.Rect ChartboostExample::scrollArea
	Rect_t3681755626  ___scrollArea_23;
	// UnityEngine.Vector3 ChartboostExample::guiScale
	Vector3_t2243707580  ___guiScale_24;
	// System.Single ChartboostExample::scale
	float ___scale_25;
	// ChartboostSDK.CBStatusBarBehavior ChartboostExample::statusBar
	int32_t ___statusBar_26;
	// UnityEngine.Vector2 ChartboostExample::beginFinger
	Vector2_t2243707579  ___beginFinger_27;
	// System.Single ChartboostExample::deltaFingerY
	float ___deltaFingerY_28;
	// UnityEngine.Vector2 ChartboostExample::beginPanel
	Vector2_t2243707579  ___beginPanel_29;
	// UnityEngine.Vector2 ChartboostExample::latestPanel
	Vector2_t2243707579  ___latestPanel_30;

public:
	inline static int32_t get_offset_of_inPlayIcon_2() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___inPlayIcon_2)); }
	inline GameObject_t1756533147 * get_inPlayIcon_2() const { return ___inPlayIcon_2; }
	inline GameObject_t1756533147 ** get_address_of_inPlayIcon_2() { return &___inPlayIcon_2; }
	inline void set_inPlayIcon_2(GameObject_t1756533147 * value)
	{
		___inPlayIcon_2 = value;
		Il2CppCodeGenWriteBarrier(&___inPlayIcon_2, value);
	}

	inline static int32_t get_offset_of_inPlayText_3() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___inPlayText_3)); }
	inline GameObject_t1756533147 * get_inPlayText_3() const { return ___inPlayText_3; }
	inline GameObject_t1756533147 ** get_address_of_inPlayText_3() { return &___inPlayText_3; }
	inline void set_inPlayText_3(GameObject_t1756533147 * value)
	{
		___inPlayText_3 = value;
		Il2CppCodeGenWriteBarrier(&___inPlayText_3, value);
	}

	inline static int32_t get_offset_of_logo_4() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___logo_4)); }
	inline Texture2D_t3542995729 * get_logo_4() const { return ___logo_4; }
	inline Texture2D_t3542995729 ** get_address_of_logo_4() { return &___logo_4; }
	inline void set_logo_4(Texture2D_t3542995729 * value)
	{
		___logo_4 = value;
		Il2CppCodeGenWriteBarrier(&___logo_4, value);
	}

	inline static int32_t get_offset_of_inPlayAd_5() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___inPlayAd_5)); }
	inline CBInPlay_t2057847888 * get_inPlayAd_5() const { return ___inPlayAd_5; }
	inline CBInPlay_t2057847888 ** get_address_of_inPlayAd_5() { return &___inPlayAd_5; }
	inline void set_inPlayAd_5(CBInPlay_t2057847888 * value)
	{
		___inPlayAd_5 = value;
		Il2CppCodeGenWriteBarrier(&___inPlayAd_5, value);
	}

	inline static int32_t get_offset_of_scrollPosition_6() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___scrollPosition_6)); }
	inline Vector2_t2243707579  get_scrollPosition_6() const { return ___scrollPosition_6; }
	inline Vector2_t2243707579 * get_address_of_scrollPosition_6() { return &___scrollPosition_6; }
	inline void set_scrollPosition_6(Vector2_t2243707579  value)
	{
		___scrollPosition_6 = value;
	}

	inline static int32_t get_offset_of_delegateHistory_7() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___delegateHistory_7)); }
	inline List_1_t1398341365 * get_delegateHistory_7() const { return ___delegateHistory_7; }
	inline List_1_t1398341365 ** get_address_of_delegateHistory_7() { return &___delegateHistory_7; }
	inline void set_delegateHistory_7(List_1_t1398341365 * value)
	{
		___delegateHistory_7 = value;
		Il2CppCodeGenWriteBarrier(&___delegateHistory_7, value);
	}

	inline static int32_t get_offset_of_hasInterstitial_8() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___hasInterstitial_8)); }
	inline bool get_hasInterstitial_8() const { return ___hasInterstitial_8; }
	inline bool* get_address_of_hasInterstitial_8() { return &___hasInterstitial_8; }
	inline void set_hasInterstitial_8(bool value)
	{
		___hasInterstitial_8 = value;
	}

	inline static int32_t get_offset_of_hasMoreApps_9() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___hasMoreApps_9)); }
	inline bool get_hasMoreApps_9() const { return ___hasMoreApps_9; }
	inline bool* get_address_of_hasMoreApps_9() { return &___hasMoreApps_9; }
	inline void set_hasMoreApps_9(bool value)
	{
		___hasMoreApps_9 = value;
	}

	inline static int32_t get_offset_of_hasRewardedVideo_10() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___hasRewardedVideo_10)); }
	inline bool get_hasRewardedVideo_10() const { return ___hasRewardedVideo_10; }
	inline bool* get_address_of_hasRewardedVideo_10() { return &___hasRewardedVideo_10; }
	inline void set_hasRewardedVideo_10(bool value)
	{
		___hasRewardedVideo_10 = value;
	}

	inline static int32_t get_offset_of_hasInPlay_11() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___hasInPlay_11)); }
	inline bool get_hasInPlay_11() const { return ___hasInPlay_11; }
	inline bool* get_address_of_hasInPlay_11() { return &___hasInPlay_11; }
	inline void set_hasInPlay_11(bool value)
	{
		___hasInPlay_11 = value;
	}

	inline static int32_t get_offset_of_frameCount_12() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___frameCount_12)); }
	inline int32_t get_frameCount_12() const { return ___frameCount_12; }
	inline int32_t* get_address_of_frameCount_12() { return &___frameCount_12; }
	inline void set_frameCount_12(int32_t value)
	{
		___frameCount_12 = value;
	}

	inline static int32_t get_offset_of_ageGate_13() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___ageGate_13)); }
	inline bool get_ageGate_13() const { return ___ageGate_13; }
	inline bool* get_address_of_ageGate_13() { return &___ageGate_13; }
	inline void set_ageGate_13(bool value)
	{
		___ageGate_13 = value;
	}

	inline static int32_t get_offset_of_autocache_14() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___autocache_14)); }
	inline bool get_autocache_14() const { return ___autocache_14; }
	inline bool* get_address_of_autocache_14() { return &___autocache_14; }
	inline void set_autocache_14(bool value)
	{
		___autocache_14 = value;
	}

	inline static int32_t get_offset_of_activeAgeGate_15() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___activeAgeGate_15)); }
	inline bool get_activeAgeGate_15() const { return ___activeAgeGate_15; }
	inline bool* get_address_of_activeAgeGate_15() { return &___activeAgeGate_15; }
	inline void set_activeAgeGate_15(bool value)
	{
		___activeAgeGate_15 = value;
	}

	inline static int32_t get_offset_of_showInterstitial_16() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___showInterstitial_16)); }
	inline bool get_showInterstitial_16() const { return ___showInterstitial_16; }
	inline bool* get_address_of_showInterstitial_16() { return &___showInterstitial_16; }
	inline void set_showInterstitial_16(bool value)
	{
		___showInterstitial_16 = value;
	}

	inline static int32_t get_offset_of_showMoreApps_17() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___showMoreApps_17)); }
	inline bool get_showMoreApps_17() const { return ___showMoreApps_17; }
	inline bool* get_address_of_showMoreApps_17() { return &___showMoreApps_17; }
	inline void set_showMoreApps_17(bool value)
	{
		___showMoreApps_17 = value;
	}

	inline static int32_t get_offset_of_showRewardedVideo_18() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___showRewardedVideo_18)); }
	inline bool get_showRewardedVideo_18() const { return ___showRewardedVideo_18; }
	inline bool* get_address_of_showRewardedVideo_18() { return &___showRewardedVideo_18; }
	inline void set_showRewardedVideo_18(bool value)
	{
		___showRewardedVideo_18 = value;
	}

	inline static int32_t get_offset_of_BANNER_HEIGHT_19() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___BANNER_HEIGHT_19)); }
	inline int32_t get_BANNER_HEIGHT_19() const { return ___BANNER_HEIGHT_19; }
	inline int32_t* get_address_of_BANNER_HEIGHT_19() { return &___BANNER_HEIGHT_19; }
	inline void set_BANNER_HEIGHT_19(int32_t value)
	{
		___BANNER_HEIGHT_19 = value;
	}

	inline static int32_t get_offset_of_REQUIRED_HEIGHT_20() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___REQUIRED_HEIGHT_20)); }
	inline int32_t get_REQUIRED_HEIGHT_20() const { return ___REQUIRED_HEIGHT_20; }
	inline int32_t* get_address_of_REQUIRED_HEIGHT_20() { return &___REQUIRED_HEIGHT_20; }
	inline void set_REQUIRED_HEIGHT_20(int32_t value)
	{
		___REQUIRED_HEIGHT_20 = value;
	}

	inline static int32_t get_offset_of_ELEMENT_WIDTH_21() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___ELEMENT_WIDTH_21)); }
	inline int32_t get_ELEMENT_WIDTH_21() const { return ___ELEMENT_WIDTH_21; }
	inline int32_t* get_address_of_ELEMENT_WIDTH_21() { return &___ELEMENT_WIDTH_21; }
	inline void set_ELEMENT_WIDTH_21(int32_t value)
	{
		___ELEMENT_WIDTH_21 = value;
	}

	inline static int32_t get_offset_of_scrollRect_22() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___scrollRect_22)); }
	inline Rect_t3681755626  get_scrollRect_22() const { return ___scrollRect_22; }
	inline Rect_t3681755626 * get_address_of_scrollRect_22() { return &___scrollRect_22; }
	inline void set_scrollRect_22(Rect_t3681755626  value)
	{
		___scrollRect_22 = value;
	}

	inline static int32_t get_offset_of_scrollArea_23() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___scrollArea_23)); }
	inline Rect_t3681755626  get_scrollArea_23() const { return ___scrollArea_23; }
	inline Rect_t3681755626 * get_address_of_scrollArea_23() { return &___scrollArea_23; }
	inline void set_scrollArea_23(Rect_t3681755626  value)
	{
		___scrollArea_23 = value;
	}

	inline static int32_t get_offset_of_guiScale_24() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___guiScale_24)); }
	inline Vector3_t2243707580  get_guiScale_24() const { return ___guiScale_24; }
	inline Vector3_t2243707580 * get_address_of_guiScale_24() { return &___guiScale_24; }
	inline void set_guiScale_24(Vector3_t2243707580  value)
	{
		___guiScale_24 = value;
	}

	inline static int32_t get_offset_of_scale_25() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___scale_25)); }
	inline float get_scale_25() const { return ___scale_25; }
	inline float* get_address_of_scale_25() { return &___scale_25; }
	inline void set_scale_25(float value)
	{
		___scale_25 = value;
	}

	inline static int32_t get_offset_of_statusBar_26() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___statusBar_26)); }
	inline int32_t get_statusBar_26() const { return ___statusBar_26; }
	inline int32_t* get_address_of_statusBar_26() { return &___statusBar_26; }
	inline void set_statusBar_26(int32_t value)
	{
		___statusBar_26 = value;
	}

	inline static int32_t get_offset_of_beginFinger_27() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___beginFinger_27)); }
	inline Vector2_t2243707579  get_beginFinger_27() const { return ___beginFinger_27; }
	inline Vector2_t2243707579 * get_address_of_beginFinger_27() { return &___beginFinger_27; }
	inline void set_beginFinger_27(Vector2_t2243707579  value)
	{
		___beginFinger_27 = value;
	}

	inline static int32_t get_offset_of_deltaFingerY_28() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___deltaFingerY_28)); }
	inline float get_deltaFingerY_28() const { return ___deltaFingerY_28; }
	inline float* get_address_of_deltaFingerY_28() { return &___deltaFingerY_28; }
	inline void set_deltaFingerY_28(float value)
	{
		___deltaFingerY_28 = value;
	}

	inline static int32_t get_offset_of_beginPanel_29() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___beginPanel_29)); }
	inline Vector2_t2243707579  get_beginPanel_29() const { return ___beginPanel_29; }
	inline Vector2_t2243707579 * get_address_of_beginPanel_29() { return &___beginPanel_29; }
	inline void set_beginPanel_29(Vector2_t2243707579  value)
	{
		___beginPanel_29 = value;
	}

	inline static int32_t get_offset_of_latestPanel_30() { return static_cast<int32_t>(offsetof(ChartboostExample_t2783486883, ___latestPanel_30)); }
	inline Vector2_t2243707579  get_latestPanel_30() const { return ___latestPanel_30; }
	inline Vector2_t2243707579 * get_address_of_latestPanel_30() { return &___latestPanel_30; }
	inline void set_latestPanel_30(Vector2_t2243707579  value)
	{
		___latestPanel_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
