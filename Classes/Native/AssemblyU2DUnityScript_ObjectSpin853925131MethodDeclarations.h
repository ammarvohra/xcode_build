﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ObjectSpin
struct ObjectSpin_t853925131;

#include "codegen/il2cpp-codegen.h"

// System.Void ObjectSpin::.ctor()
extern "C"  void ObjectSpin__ctor_m43479655 (ObjectSpin_t853925131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectSpin::Start()
extern "C"  void ObjectSpin_Start_m1391810155 (ObjectSpin_t853925131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectSpin::Update()
extern "C"  void ObjectSpin_Update_m2105600082 (ObjectSpin_t853925131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectSpin::Main()
extern "C"  void ObjectSpin_Main_m3772133374 (ObjectSpin_t853925131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
