﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChartboostSDK.Chartboost
struct Chartboost_t2610413123;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Func`2<ChartboostSDK.CBLocation,System.Boolean>
struct Func_2_t362347193;
// System.Action`1<ChartboostSDK.CBLocation>
struct Action_1_t1875398900;
// System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>
struct Action_2_t388935095;
// System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBClickError>
struct Action_2_t3934376746;
// System.Action`2<ChartboostSDK.CBLocation,System.Int32>
struct Action_2_t2650164891;
// System.Action
struct Action_t3226471752;
// ChartboostSDK.CBLocation
struct CBLocation_t2073599518;
// ChartboostSDK.CBInPlay
struct CBInPlay_t2057847888;
// System.String
struct String_t;
// ChartboostSDK.CBMediation
struct CBMediation_t669766003;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBLocation2073599518.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBLevelType2978753963.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBStatusBarBehavio2216036290.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBMediation669766003.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBImpressionError4105614948.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBClickError3356089303.h"

// System.Void ChartboostSDK.Chartboost::.ctor()
extern "C"  void Chartboost__ctor_m1088438101 (Chartboost_t2610413123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didInitialize(System.Action`1<System.Boolean>)
extern "C"  void Chartboost_add_didInitialize_m2807740179 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didInitialize(System.Action`1<System.Boolean>)
extern "C"  void Chartboost_remove_didInitialize_m896555052 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_shouldDisplayInterstitial(System.Func`2<ChartboostSDK.CBLocation,System.Boolean>)
extern "C"  void Chartboost_add_shouldDisplayInterstitial_m2989113487 (Il2CppObject * __this /* static, unused */, Func_2_t362347193 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_shouldDisplayInterstitial(System.Func`2<ChartboostSDK.CBLocation,System.Boolean>)
extern "C"  void Chartboost_remove_shouldDisplayInterstitial_m82911570 (Il2CppObject * __this /* static, unused */, Func_2_t362347193 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didDisplayInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didDisplayInterstitial_m577222927 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didDisplayInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didDisplayInterstitial_m446483994 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didCacheInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didCacheInterstitial_m163086029 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didCacheInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didCacheInterstitial_m1430091070 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didClickInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didClickInterstitial_m1973259287 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didClickInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didClickInterstitial_m1404739396 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didCloseInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didCloseInterstitial_m430830063 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didCloseInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didCloseInterstitial_m3535917388 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didDismissInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didDismissInterstitial_m4253749259 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didDismissInterstitial(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didDismissInterstitial_m1804760128 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didFailToLoadInterstitial(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>)
extern "C"  void Chartboost_add_didFailToLoadInterstitial_m2303939348 (Il2CppObject * __this /* static, unused */, Action_2_t388935095 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didFailToLoadInterstitial(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>)
extern "C"  void Chartboost_remove_didFailToLoadInterstitial_m3100899235 (Il2CppObject * __this /* static, unused */, Action_2_t388935095 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didFailToRecordClick(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBClickError>)
extern "C"  void Chartboost_add_didFailToRecordClick_m1677915246 (Il2CppObject * __this /* static, unused */, Action_2_t3934376746 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didFailToRecordClick(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBClickError>)
extern "C"  void Chartboost_remove_didFailToRecordClick_m348622905 (Il2CppObject * __this /* static, unused */, Action_2_t3934376746 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_shouldDisplayMoreApps(System.Func`2<ChartboostSDK.CBLocation,System.Boolean>)
extern "C"  void Chartboost_add_shouldDisplayMoreApps_m924671066 (Il2CppObject * __this /* static, unused */, Func_2_t362347193 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_shouldDisplayMoreApps(System.Func`2<ChartboostSDK.CBLocation,System.Boolean>)
extern "C"  void Chartboost_remove_shouldDisplayMoreApps_m3685546521 (Il2CppObject * __this /* static, unused */, Func_2_t362347193 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didDisplayMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didDisplayMoreApps_m2669682112 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didDisplayMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didDisplayMoreApps_m3959876733 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didCacheMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didCacheMoreApps_m762063756 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didCacheMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didCacheMoreApps_m798987051 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didClickMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didClickMoreApps_m1961878974 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didClickMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didClickMoreApps_m1627303529 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didCloseMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didCloseMoreApps_m3679982662 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didCloseMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didCloseMoreApps_m3593154689 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didDismissMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didDismissMoreApps_m2084314938 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didDismissMoreApps(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didDismissMoreApps_m1810036605 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didFailToLoadMoreApps(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>)
extern "C"  void Chartboost_add_didFailToLoadMoreApps_m3987583993 (Il2CppObject * __this /* static, unused */, Action_2_t388935095 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didFailToLoadMoreApps(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>)
extern "C"  void Chartboost_remove_didFailToLoadMoreApps_m2764148012 (Il2CppObject * __this /* static, unused */, Action_2_t388935095 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_shouldDisplayRewardedVideo(System.Func`2<ChartboostSDK.CBLocation,System.Boolean>)
extern "C"  void Chartboost_add_shouldDisplayRewardedVideo_m1090668960 (Il2CppObject * __this /* static, unused */, Func_2_t362347193 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_shouldDisplayRewardedVideo(System.Func`2<ChartboostSDK.CBLocation,System.Boolean>)
extern "C"  void Chartboost_remove_shouldDisplayRewardedVideo_m2351543101 (Il2CppObject * __this /* static, unused */, Func_2_t362347193 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didDisplayRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didDisplayRewardedVideo_m2169608442 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didDisplayRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didDisplayRewardedVideo_m3768530137 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didCacheRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didCacheRewardedVideo_m2489988594 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didCacheRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didCacheRewardedVideo_m2402514671 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didClickRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didClickRewardedVideo_m2429515748 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didClickRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didClickRewardedVideo_m1852877805 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didCloseRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didCloseRewardedVideo_m340386412 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didCloseRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didCloseRewardedVideo_m3525030149 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didDismissRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didDismissRewardedVideo_m2170979936 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didDismissRewardedVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didDismissRewardedVideo_m2982553601 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didCompleteRewardedVideo(System.Action`2<ChartboostSDK.CBLocation,System.Int32>)
extern "C"  void Chartboost_add_didCompleteRewardedVideo_m2864476615 (Il2CppObject * __this /* static, unused */, Action_2_t2650164891 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didCompleteRewardedVideo(System.Action`2<ChartboostSDK.CBLocation,System.Int32>)
extern "C"  void Chartboost_remove_didCompleteRewardedVideo_m2467200898 (Il2CppObject * __this /* static, unused */, Action_2_t2650164891 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didFailToLoadRewardedVideo(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>)
extern "C"  void Chartboost_add_didFailToLoadRewardedVideo_m1177240929 (Il2CppObject * __this /* static, unused */, Action_2_t388935095 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didFailToLoadRewardedVideo(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>)
extern "C"  void Chartboost_remove_didFailToLoadRewardedVideo_m2146164398 (Il2CppObject * __this /* static, unused */, Action_2_t388935095 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didCacheInPlay(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_didCacheInPlay_m2998799284 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didCacheInPlay(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_didCacheInPlay_m3596228495 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didFailToLoadInPlay(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>)
extern "C"  void Chartboost_add_didFailToLoadInPlay_m3021637177 (Il2CppObject * __this /* static, unused */, Action_2_t388935095 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didFailToLoadInPlay(System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>)
extern "C"  void Chartboost_remove_didFailToLoadInPlay_m3672129440 (Il2CppObject * __this /* static, unused */, Action_2_t388935095 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_willDisplayVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_add_willDisplayVideo_m229375895 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_willDisplayVideo(System.Action`1<ChartboostSDK.CBLocation>)
extern "C"  void Chartboost_remove_willDisplayVideo_m955006668 (Il2CppObject * __this /* static, unused */, Action_1_t1875398900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didPauseClickForConfirmation(System.Action)
extern "C"  void Chartboost_add_didPauseClickForConfirmation_m2666726651 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didPauseClickForConfirmation(System.Action)
extern "C"  void Chartboost_remove_didPauseClickForConfirmation_m912833154 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::add_didCompleteAppStoreSheetFlow(System.Action)
extern "C"  void Chartboost_add_didCompleteAppStoreSheetFlow_m3376394237 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::remove_didCompleteAppStoreSheetFlow(System.Action)
extern "C"  void Chartboost_remove_didCompleteAppStoreSheetFlow_m1409027188 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.Chartboost::isInitialized()
extern "C"  bool Chartboost_isInitialized_m1317228963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.Chartboost::isAnyViewVisible()
extern "C"  bool Chartboost_isAnyViewVisible_m3576545596 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::cacheInterstitial(ChartboostSDK.CBLocation)
extern "C"  void Chartboost_cacheInterstitial_m4112646228 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.Chartboost::hasInterstitial(ChartboostSDK.CBLocation)
extern "C"  bool Chartboost_hasInterstitial_m2591947268 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::showInterstitial(ChartboostSDK.CBLocation)
extern "C"  void Chartboost_showInterstitial_m2154289375 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::cacheMoreApps(ChartboostSDK.CBLocation)
extern "C"  void Chartboost_cacheMoreApps_m3324392537 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.Chartboost::hasMoreApps(ChartboostSDK.CBLocation)
extern "C"  bool Chartboost_hasMoreApps_m616965083 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::showMoreApps(ChartboostSDK.CBLocation)
extern "C"  void Chartboost_showMoreApps_m648286696 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::cacheRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  void Chartboost_cacheRewardedVideo_m1193405665 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.Chartboost::hasRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  bool Chartboost_hasRewardedVideo_m1461608831 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::showRewardedVideo(ChartboostSDK.CBLocation)
extern "C"  void Chartboost_showRewardedVideo_m3216299970 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::cacheInPlay(ChartboostSDK.CBLocation)
extern "C"  void Chartboost_cacheInPlay_m2983603353 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.Chartboost::hasInPlay(ChartboostSDK.CBLocation)
extern "C"  bool Chartboost_hasInPlay_m3749927727 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChartboostSDK.CBInPlay ChartboostSDK.Chartboost::getInPlay(ChartboostSDK.CBLocation)
extern "C"  CBInPlay_t2057847888 * Chartboost_getInPlay_m2665605921 (Il2CppObject * __this /* static, unused */, CBLocation_t2073599518 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didPassAgeGate(System.Boolean)
extern "C"  void Chartboost_didPassAgeGate_m2687512008 (Il2CppObject * __this /* static, unused */, bool ___pass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::setShouldPauseClickForConfirmation(System.Boolean)
extern "C"  void Chartboost_setShouldPauseClickForConfirmation_m4019422205 (Il2CppObject * __this /* static, unused */, bool ___shouldPause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.Chartboost::getCustomId()
extern "C"  String_t* Chartboost_getCustomId_m1622598840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::setCustomId(System.String)
extern "C"  void Chartboost_setCustomId_m2944296419 (Il2CppObject * __this /* static, unused */, String_t* ___customId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.Chartboost::getAutoCacheAds()
extern "C"  bool Chartboost_getAutoCacheAds_m2653804464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::setAutoCacheAds(System.Boolean)
extern "C"  void Chartboost_setAutoCacheAds_m118351201 (Il2CppObject * __this /* static, unused */, bool ___autoCacheAds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::setShouldRequestInterstitialsInFirstSession(System.Boolean)
extern "C"  void Chartboost_setShouldRequestInterstitialsInFirstSession_m4211647558 (Il2CppObject * __this /* static, unused */, bool ___shouldRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::setShouldDisplayLoadingViewForMoreApps(System.Boolean)
extern "C"  void Chartboost_setShouldDisplayLoadingViewForMoreApps_m3495076578 (Il2CppObject * __this /* static, unused */, bool ___shouldDisplay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::setShouldPrefetchVideoContent(System.Boolean)
extern "C"  void Chartboost_setShouldPrefetchVideoContent_m2595815044 (Il2CppObject * __this /* static, unused */, bool ___shouldPrefetch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::trackLevelInfo(System.String,ChartboostSDK.CBLevelType,System.Int32,System.Int32,System.String)
extern "C"  void Chartboost_trackLevelInfo_m857084784 (Il2CppObject * __this /* static, unused */, String_t* ___eventLabel0, int32_t ___type1, int32_t ___mainLevel2, int32_t ___subLevel3, String_t* ___description4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::trackLevelInfo(System.String,ChartboostSDK.CBLevelType,System.Int32,System.String)
extern "C"  void Chartboost_trackLevelInfo_m3482395891 (Il2CppObject * __this /* static, unused */, String_t* ___eventLabel0, int32_t ___type1, int32_t ___mainLevel2, String_t* ___description3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::trackInAppAppleStorePurchaseEvent(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void Chartboost_trackInAppAppleStorePurchaseEvent_m1493519668 (Il2CppObject * __this /* static, unused */, String_t* ___receipt0, String_t* ___productTitle1, String_t* ___productDescription2, String_t* ___productPrice3, String_t* ___productCurrency4, String_t* ___productIdentifier5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::setStatusBarBehavior(ChartboostSDK.CBStatusBarBehavior)
extern "C"  void Chartboost_setStatusBarBehavior_m1896054705 (Il2CppObject * __this /* static, unused */, int32_t ___statusBarBehavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::setMediation(ChartboostSDK.CBMediation,System.String)
extern "C"  void Chartboost_setMediation_m1594805819 (Il2CppObject * __this /* static, unused */, CBMediation_t669766003 * ___mediator0, String_t* ___version1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChartboostSDK.Chartboost ChartboostSDK.Chartboost::Create()
extern "C"  Chartboost_t2610413123 * Chartboost_Create_m4215025116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChartboostSDK.Chartboost ChartboostSDK.Chartboost::CreateWithAppId(System.String,System.String)
extern "C"  Chartboost_t2610413123 * Chartboost_CreateWithAppId_m3453951770 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, String_t* ___appSignature1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::Awake()
extern "C"  void Chartboost_Awake_m3497641938 (Chartboost_t2610413123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::OnDestroy()
extern "C"  void Chartboost_OnDestroy_m2120181936 (Chartboost_t2610413123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::Update()
extern "C"  void Chartboost_Update_m2412775860 (Chartboost_t2610413123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::OnApplicationPause(System.Boolean)
extern "C"  void Chartboost_OnApplicationPause_m2706686315 (Chartboost_t2610413123 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::OnDisable()
extern "C"  void Chartboost_OnDisable_m2335324362 (Chartboost_t2610413123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChartboostSDK.CBImpressionError ChartboostSDK.Chartboost::impressionErrorFromInt(System.Object)
extern "C"  int32_t Chartboost_impressionErrorFromInt_m2314277563 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___errorObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChartboostSDK.CBClickError ChartboostSDK.Chartboost::clickErrorFromInt(System.Object)
extern "C"  int32_t Chartboost_clickErrorFromInt_m4085664587 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___errorObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didInitializeEvent(System.String)
extern "C"  void Chartboost_didInitializeEvent_m2208141218 (Chartboost_t2610413123 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didFailToLoadInterstitialEvent(System.String)
extern "C"  void Chartboost_didFailToLoadInterstitialEvent_m3363289771 (Chartboost_t2610413123 * __this, String_t* ___dataString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didDismissInterstitialEvent(System.String)
extern "C"  void Chartboost_didDismissInterstitialEvent_m2416911208 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didClickInterstitialEvent(System.String)
extern "C"  void Chartboost_didClickInterstitialEvent_m2871118916 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didCloseInterstitialEvent(System.String)
extern "C"  void Chartboost_didCloseInterstitialEvent_m815018572 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didCacheInterstitialEvent(System.String)
extern "C"  void Chartboost_didCacheInterstitialEvent_m2479735154 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::shouldDisplayInterstitialEvent(System.String)
extern "C"  void Chartboost_shouldDisplayInterstitialEvent_m494620944 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didDisplayInterstitialEvent(System.String)
extern "C"  void Chartboost_didDisplayInterstitialEvent_m154463058 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didFailToRecordClickEvent(System.String)
extern "C"  void Chartboost_didFailToRecordClickEvent_m1255575246 (Chartboost_t2610413123 * __this, String_t* ___dataString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didFailToLoadRewardedVideoEvent(System.String)
extern "C"  void Chartboost_didFailToLoadRewardedVideoEvent_m3517980656 (Chartboost_t2610413123 * __this, String_t* ___dataString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didDismissRewardedVideoEvent(System.String)
extern "C"  void Chartboost_didDismissRewardedVideoEvent_m3435366159 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didClickRewardedVideoEvent(System.String)
extern "C"  void Chartboost_didClickRewardedVideoEvent_m2133752867 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didCloseRewardedVideoEvent(System.String)
extern "C"  void Chartboost_didCloseRewardedVideoEvent_m2827181595 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didCacheRewardedVideoEvent(System.String)
extern "C"  void Chartboost_didCacheRewardedVideoEvent_m13578285 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::shouldDisplayRewardedVideoEvent(System.String)
extern "C"  void Chartboost_shouldDisplayRewardedVideoEvent_m905360723 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didCompleteRewardedVideoEvent(System.String)
extern "C"  void Chartboost_didCompleteRewardedVideoEvent_m1340685850 (Chartboost_t2610413123 * __this, String_t* ___dataString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didDisplayRewardedVideoEvent(System.String)
extern "C"  void Chartboost_didDisplayRewardedVideoEvent_m1700473707 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didCacheInPlayEvent(System.String)
extern "C"  void Chartboost_didCacheInPlayEvent_m3312133681 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didFailToLoadInPlayEvent(System.String)
extern "C"  void Chartboost_didFailToLoadInPlayEvent_m3039195772 (Chartboost_t2610413123 * __this, String_t* ___dataString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didPauseClickForConfirmationEvent()
extern "C"  void Chartboost_didPauseClickForConfirmationEvent_m3023005292 (Chartboost_t2610413123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::willDisplayVideoEvent(System.String)
extern "C"  void Chartboost_willDisplayVideoEvent_m4231679020 (Chartboost_t2610413123 * __this, String_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::didCompleteAppStoreSheetFlowEvent(System.String)
extern "C"  void Chartboost_didCompleteAppStoreSheetFlowEvent_m326199160 (Chartboost_t2610413123 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::doUnityPause(System.Boolean,System.Boolean)
extern "C"  void Chartboost_doUnityPause_m384958199 (Il2CppObject * __this /* static, unused */, bool ___pause0, bool ___setShouldPause1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::doShowAgeGate(System.Boolean)
extern "C"  void Chartboost_doShowAgeGate_m675057784 (Il2CppObject * __this /* static, unused */, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::disableUI(System.Boolean)
extern "C"  void Chartboost_disableUI_m92025526 (Il2CppObject * __this /* static, unused */, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.Chartboost::isImpressionVisible()
extern "C"  bool Chartboost_isImpressionVisible_m2326854598 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.Chartboost::.cctor()
extern "C"  void Chartboost__cctor_m2296155236 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
