﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// MaskCamera
struct MaskCamera_t4194554627;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_Nullable_1_gen506773894.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaskCamera
struct  MaskCamera_t4194554627  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material MaskCamera::EraserMaterial
	Material_t193706927 * ___EraserMaterial_2;
	// UnityEngine.Material MaskCamera::EraserIOS
	Material_t193706927 * ___EraserIOS_3;
	// UnityEngine.GameObject MaskCamera::CurrentCleaningTools
	GameObject_t1756533147 * ___CurrentCleaningTools_4;
	// System.Boolean MaskCamera::firstFrame
	bool ___firstFrame_6;
	// System.Nullable`1<UnityEngine.Vector2> MaskCamera::newHolePosition
	Nullable_1_t506773894  ___newHolePosition_7;
	// UnityEngine.RenderTexture MaskCamera::rnd
	RenderTexture_t2666733923 * ___rnd_8;
	// UnityEngine.Material[] MaskCamera::Dust
	MaterialU5BU5D_t3123989686* ___Dust_9;
	// UnityEngine.Rect MaskCamera::ImageRect
	Rect_t3681755626  ___ImageRect_10;
	// System.Single MaskCamera::TotalVX
	float ___TotalVX_12;
	// System.Single MaskCamera::TotalVY
	float ___TotalVY_13;
	// System.Single[] MaskCamera::VX
	SingleU5BU5D_t577127397* ___VX_14;
	// System.Single[] MaskCamera::VY
	SingleU5BU5D_t577127397* ___VY_15;
	// UnityEngine.Rect MaskCamera::PreviousRect
	Rect_t3681755626  ___PreviousRect_16;
	// System.Single MaskCamera::OffsetX
	float ___OffsetX_17;
	// System.Single MaskCamera::OffsetY
	float ___OffsetY_18;

public:
	inline static int32_t get_offset_of_EraserMaterial_2() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___EraserMaterial_2)); }
	inline Material_t193706927 * get_EraserMaterial_2() const { return ___EraserMaterial_2; }
	inline Material_t193706927 ** get_address_of_EraserMaterial_2() { return &___EraserMaterial_2; }
	inline void set_EraserMaterial_2(Material_t193706927 * value)
	{
		___EraserMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___EraserMaterial_2, value);
	}

	inline static int32_t get_offset_of_EraserIOS_3() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___EraserIOS_3)); }
	inline Material_t193706927 * get_EraserIOS_3() const { return ___EraserIOS_3; }
	inline Material_t193706927 ** get_address_of_EraserIOS_3() { return &___EraserIOS_3; }
	inline void set_EraserIOS_3(Material_t193706927 * value)
	{
		___EraserIOS_3 = value;
		Il2CppCodeGenWriteBarrier(&___EraserIOS_3, value);
	}

	inline static int32_t get_offset_of_CurrentCleaningTools_4() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___CurrentCleaningTools_4)); }
	inline GameObject_t1756533147 * get_CurrentCleaningTools_4() const { return ___CurrentCleaningTools_4; }
	inline GameObject_t1756533147 ** get_address_of_CurrentCleaningTools_4() { return &___CurrentCleaningTools_4; }
	inline void set_CurrentCleaningTools_4(GameObject_t1756533147 * value)
	{
		___CurrentCleaningTools_4 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentCleaningTools_4, value);
	}

	inline static int32_t get_offset_of_firstFrame_6() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___firstFrame_6)); }
	inline bool get_firstFrame_6() const { return ___firstFrame_6; }
	inline bool* get_address_of_firstFrame_6() { return &___firstFrame_6; }
	inline void set_firstFrame_6(bool value)
	{
		___firstFrame_6 = value;
	}

	inline static int32_t get_offset_of_newHolePosition_7() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___newHolePosition_7)); }
	inline Nullable_1_t506773894  get_newHolePosition_7() const { return ___newHolePosition_7; }
	inline Nullable_1_t506773894 * get_address_of_newHolePosition_7() { return &___newHolePosition_7; }
	inline void set_newHolePosition_7(Nullable_1_t506773894  value)
	{
		___newHolePosition_7 = value;
	}

	inline static int32_t get_offset_of_rnd_8() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___rnd_8)); }
	inline RenderTexture_t2666733923 * get_rnd_8() const { return ___rnd_8; }
	inline RenderTexture_t2666733923 ** get_address_of_rnd_8() { return &___rnd_8; }
	inline void set_rnd_8(RenderTexture_t2666733923 * value)
	{
		___rnd_8 = value;
		Il2CppCodeGenWriteBarrier(&___rnd_8, value);
	}

	inline static int32_t get_offset_of_Dust_9() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___Dust_9)); }
	inline MaterialU5BU5D_t3123989686* get_Dust_9() const { return ___Dust_9; }
	inline MaterialU5BU5D_t3123989686** get_address_of_Dust_9() { return &___Dust_9; }
	inline void set_Dust_9(MaterialU5BU5D_t3123989686* value)
	{
		___Dust_9 = value;
		Il2CppCodeGenWriteBarrier(&___Dust_9, value);
	}

	inline static int32_t get_offset_of_ImageRect_10() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___ImageRect_10)); }
	inline Rect_t3681755626  get_ImageRect_10() const { return ___ImageRect_10; }
	inline Rect_t3681755626 * get_address_of_ImageRect_10() { return &___ImageRect_10; }
	inline void set_ImageRect_10(Rect_t3681755626  value)
	{
		___ImageRect_10 = value;
	}

	inline static int32_t get_offset_of_TotalVX_12() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___TotalVX_12)); }
	inline float get_TotalVX_12() const { return ___TotalVX_12; }
	inline float* get_address_of_TotalVX_12() { return &___TotalVX_12; }
	inline void set_TotalVX_12(float value)
	{
		___TotalVX_12 = value;
	}

	inline static int32_t get_offset_of_TotalVY_13() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___TotalVY_13)); }
	inline float get_TotalVY_13() const { return ___TotalVY_13; }
	inline float* get_address_of_TotalVY_13() { return &___TotalVY_13; }
	inline void set_TotalVY_13(float value)
	{
		___TotalVY_13 = value;
	}

	inline static int32_t get_offset_of_VX_14() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___VX_14)); }
	inline SingleU5BU5D_t577127397* get_VX_14() const { return ___VX_14; }
	inline SingleU5BU5D_t577127397** get_address_of_VX_14() { return &___VX_14; }
	inline void set_VX_14(SingleU5BU5D_t577127397* value)
	{
		___VX_14 = value;
		Il2CppCodeGenWriteBarrier(&___VX_14, value);
	}

	inline static int32_t get_offset_of_VY_15() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___VY_15)); }
	inline SingleU5BU5D_t577127397* get_VY_15() const { return ___VY_15; }
	inline SingleU5BU5D_t577127397** get_address_of_VY_15() { return &___VY_15; }
	inline void set_VY_15(SingleU5BU5D_t577127397* value)
	{
		___VY_15 = value;
		Il2CppCodeGenWriteBarrier(&___VY_15, value);
	}

	inline static int32_t get_offset_of_PreviousRect_16() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___PreviousRect_16)); }
	inline Rect_t3681755626  get_PreviousRect_16() const { return ___PreviousRect_16; }
	inline Rect_t3681755626 * get_address_of_PreviousRect_16() { return &___PreviousRect_16; }
	inline void set_PreviousRect_16(Rect_t3681755626  value)
	{
		___PreviousRect_16 = value;
	}

	inline static int32_t get_offset_of_OffsetX_17() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___OffsetX_17)); }
	inline float get_OffsetX_17() const { return ___OffsetX_17; }
	inline float* get_address_of_OffsetX_17() { return &___OffsetX_17; }
	inline void set_OffsetX_17(float value)
	{
		___OffsetX_17 = value;
	}

	inline static int32_t get_offset_of_OffsetY_18() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627, ___OffsetY_18)); }
	inline float get_OffsetY_18() const { return ___OffsetY_18; }
	inline float* get_address_of_OffsetY_18() { return &___OffsetY_18; }
	inline void set_OffsetY_18(float value)
	{
		___OffsetY_18 = value;
	}
};

struct MaskCamera_t4194554627_StaticFields
{
public:
	// MaskCamera MaskCamera::instance
	MaskCamera_t4194554627 * ___instance_5;
	// System.Boolean MaskCamera::flag
	bool ___flag_11;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627_StaticFields, ___instance_5)); }
	inline MaskCamera_t4194554627 * get_instance_5() const { return ___instance_5; }
	inline MaskCamera_t4194554627 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(MaskCamera_t4194554627 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}

	inline static int32_t get_offset_of_flag_11() { return static_cast<int32_t>(offsetof(MaskCamera_t4194554627_StaticFields, ___flag_11)); }
	inline bool get_flag_11() const { return ___flag_11; }
	inline bool* get_address_of_flag_11() { return &___flag_11; }
	inline void set_flag_11(bool value)
	{
		___flag_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
