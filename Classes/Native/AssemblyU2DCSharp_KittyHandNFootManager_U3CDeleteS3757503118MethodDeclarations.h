﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<DeleteSpray>c__Iterator5
struct U3CDeleteSprayU3Ec__Iterator5_t3757503118;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<DeleteSpray>c__Iterator5::.ctor()
extern "C"  void U3CDeleteSprayU3Ec__Iterator5__ctor_m166138161 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<DeleteSpray>c__Iterator5::MoveNext()
extern "C"  bool U3CDeleteSprayU3Ec__Iterator5_MoveNext_m3105239803 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<DeleteSpray>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDeleteSprayU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2798560127 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<DeleteSpray>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDeleteSprayU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1532498007 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<DeleteSpray>c__Iterator5::Dispose()
extern "C"  void U3CDeleteSprayU3Ec__Iterator5_Dispose_m2308277552 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<DeleteSpray>c__Iterator5::Reset()
extern "C"  void U3CDeleteSprayU3Ec__Iterator5_Reset_m4136052782 (U3CDeleteSprayU3Ec__Iterator5_t3757503118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
