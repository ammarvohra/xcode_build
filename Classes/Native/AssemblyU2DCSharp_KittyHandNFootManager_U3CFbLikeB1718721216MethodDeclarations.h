﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9
struct U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::.ctor()
extern "C"  void U3CFbLikeButtonUpU3Ec__Iterator9__ctor_m1680875357 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::MoveNext()
extern "C"  bool U3CFbLikeButtonUpU3Ec__Iterator9_MoveNext_m930851739 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFbLikeButtonUpU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2868013043 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFbLikeButtonUpU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3328623339 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::Dispose()
extern "C"  void U3CFbLikeButtonUpU3Ec__Iterator9_Dispose_m2709393354 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<FbLikeButtonUp>c__Iterator9::Reset()
extern "C"  void U3CFbLikeButtonUpU3Ec__Iterator9_Reset_m884661972 (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
