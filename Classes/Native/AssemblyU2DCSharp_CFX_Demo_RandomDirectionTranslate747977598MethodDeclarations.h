﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_RandomDirectionTranslate
struct CFX_Demo_RandomDirectionTranslate_t747977598;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_RandomDirectionTranslate::.ctor()
extern "C"  void CFX_Demo_RandomDirectionTranslate__ctor_m1987876213 (CFX_Demo_RandomDirectionTranslate_t747977598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RandomDirectionTranslate::Start()
extern "C"  void CFX_Demo_RandomDirectionTranslate_Start_m2266977989 (CFX_Demo_RandomDirectionTranslate_t747977598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RandomDirectionTranslate::Update()
extern "C"  void CFX_Demo_RandomDirectionTranslate_Update_m1855616736 (CFX_Demo_RandomDirectionTranslate_t747977598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
