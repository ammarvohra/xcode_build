﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BubbleTap/<WaitForDestroy>c__Iterator0
struct U3CWaitForDestroyU3Ec__Iterator0_t4043257003;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BubbleTap/<WaitForDestroy>c__Iterator0::.ctor()
extern "C"  void U3CWaitForDestroyU3Ec__Iterator0__ctor_m4080516562 (U3CWaitForDestroyU3Ec__Iterator0_t4043257003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BubbleTap/<WaitForDestroy>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForDestroyU3Ec__Iterator0_MoveNext_m1428492818 (U3CWaitForDestroyU3Ec__Iterator0_t4043257003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BubbleTap/<WaitForDestroy>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForDestroyU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1949610234 (U3CWaitForDestroyU3Ec__Iterator0_t4043257003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BubbleTap/<WaitForDestroy>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForDestroyU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3470748210 (U3CWaitForDestroyU3Ec__Iterator0_t4043257003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BubbleTap/<WaitForDestroy>c__Iterator0::Dispose()
extern "C"  void U3CWaitForDestroyU3Ec__Iterator0_Dispose_m1171973217 (U3CWaitForDestroyU3Ec__Iterator0_t4043257003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BubbleTap/<WaitForDestroy>c__Iterator0::Reset()
extern "C"  void U3CWaitForDestroyU3Ec__Iterator0_Reset_m2115441371 (U3CWaitForDestroyU3Ec__Iterator0_t4043257003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
