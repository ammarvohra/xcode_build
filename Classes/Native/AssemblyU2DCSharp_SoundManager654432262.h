﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2203355011;
// SoundManager
struct SoundManager_t654432262;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t654432262  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SoundManager::OpenMouth
	GameObject_t1756533147 * ___OpenMouth_2;
	// UnityEngine.AudioSource SoundManager::MeowSource
	AudioSource_t1135106623 * ___MeowSource_3;
	// UnityEngine.GameObject SoundManager::BubbleBlastSource
	GameObject_t1756533147 * ___BubbleBlastSource_4;
	// UnityEngine.AudioSource SoundManager::MainMusic
	AudioSource_t1135106623 * ___MainMusic_5;
	// UnityEngine.AudioSource SoundManager::ButtonSource
	AudioSource_t1135106623 * ___ButtonSource_6;
	// UnityEngine.AudioSource SoundManager::SoapBubbleSound
	AudioSource_t1135106623 * ___SoapBubbleSound_7;
	// UnityEngine.AudioSource SoundManager::ShowerSound
	AudioSource_t1135106623 * ___ShowerSound_8;
	// UnityEngine.AudioSource SoundManager::TowelSound
	AudioSource_t1135106623 * ___TowelSound_9;
	// UnityEngine.AudioSource SoundManager::DryerSound
	AudioSource_t1135106623 * ___DryerSound_10;
	// UnityEngine.AudioSource SoundManager::NailCutterSound
	AudioSource_t1135106623 * ___NailCutterSound_11;
	// UnityEngine.AudioSource SoundManager::PerfumeSound
	AudioSource_t1135106623 * ___PerfumeSound_12;
	// UnityEngine.AudioSource SoundManager::ClickSound
	AudioSource_t1135106623 * ___ClickSound_13;
	// UnityEngine.AudioSource SoundManager::LastSound
	AudioSource_t1135106623 * ___LastSound_14;
	// System.Boolean SoundManager::PlaySound
	bool ___PlaySound_15;
	// System.Boolean SoundManager::CanPlayMeow
	bool ___CanPlayMeow_16;
	// UnityEngine.AudioClip[] SoundManager::MeowClips
	AudioClipU5BU5D_t2203355011* ___MeowClips_17;
	// UnityEngine.Vector3[] SoundManager::MouthOpenPosition
	Vector3U5BU5D_t1172311765* ___MouthOpenPosition_19;
	// UnityEngine.Vector3[] SoundManager::MouthOpenScale
	Vector3U5BU5D_t1172311765* ___MouthOpenScale_20;
	// System.Int32[] SoundManager::Order
	Int32U5BU5D_t3030399641* ___Order_21;
	// System.Int32 SoundManager::count
	int32_t ___count_22;

public:
	inline static int32_t get_offset_of_OpenMouth_2() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___OpenMouth_2)); }
	inline GameObject_t1756533147 * get_OpenMouth_2() const { return ___OpenMouth_2; }
	inline GameObject_t1756533147 ** get_address_of_OpenMouth_2() { return &___OpenMouth_2; }
	inline void set_OpenMouth_2(GameObject_t1756533147 * value)
	{
		___OpenMouth_2 = value;
		Il2CppCodeGenWriteBarrier(&___OpenMouth_2, value);
	}

	inline static int32_t get_offset_of_MeowSource_3() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___MeowSource_3)); }
	inline AudioSource_t1135106623 * get_MeowSource_3() const { return ___MeowSource_3; }
	inline AudioSource_t1135106623 ** get_address_of_MeowSource_3() { return &___MeowSource_3; }
	inline void set_MeowSource_3(AudioSource_t1135106623 * value)
	{
		___MeowSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___MeowSource_3, value);
	}

	inline static int32_t get_offset_of_BubbleBlastSource_4() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___BubbleBlastSource_4)); }
	inline GameObject_t1756533147 * get_BubbleBlastSource_4() const { return ___BubbleBlastSource_4; }
	inline GameObject_t1756533147 ** get_address_of_BubbleBlastSource_4() { return &___BubbleBlastSource_4; }
	inline void set_BubbleBlastSource_4(GameObject_t1756533147 * value)
	{
		___BubbleBlastSource_4 = value;
		Il2CppCodeGenWriteBarrier(&___BubbleBlastSource_4, value);
	}

	inline static int32_t get_offset_of_MainMusic_5() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___MainMusic_5)); }
	inline AudioSource_t1135106623 * get_MainMusic_5() const { return ___MainMusic_5; }
	inline AudioSource_t1135106623 ** get_address_of_MainMusic_5() { return &___MainMusic_5; }
	inline void set_MainMusic_5(AudioSource_t1135106623 * value)
	{
		___MainMusic_5 = value;
		Il2CppCodeGenWriteBarrier(&___MainMusic_5, value);
	}

	inline static int32_t get_offset_of_ButtonSource_6() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___ButtonSource_6)); }
	inline AudioSource_t1135106623 * get_ButtonSource_6() const { return ___ButtonSource_6; }
	inline AudioSource_t1135106623 ** get_address_of_ButtonSource_6() { return &___ButtonSource_6; }
	inline void set_ButtonSource_6(AudioSource_t1135106623 * value)
	{
		___ButtonSource_6 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonSource_6, value);
	}

	inline static int32_t get_offset_of_SoapBubbleSound_7() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___SoapBubbleSound_7)); }
	inline AudioSource_t1135106623 * get_SoapBubbleSound_7() const { return ___SoapBubbleSound_7; }
	inline AudioSource_t1135106623 ** get_address_of_SoapBubbleSound_7() { return &___SoapBubbleSound_7; }
	inline void set_SoapBubbleSound_7(AudioSource_t1135106623 * value)
	{
		___SoapBubbleSound_7 = value;
		Il2CppCodeGenWriteBarrier(&___SoapBubbleSound_7, value);
	}

	inline static int32_t get_offset_of_ShowerSound_8() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___ShowerSound_8)); }
	inline AudioSource_t1135106623 * get_ShowerSound_8() const { return ___ShowerSound_8; }
	inline AudioSource_t1135106623 ** get_address_of_ShowerSound_8() { return &___ShowerSound_8; }
	inline void set_ShowerSound_8(AudioSource_t1135106623 * value)
	{
		___ShowerSound_8 = value;
		Il2CppCodeGenWriteBarrier(&___ShowerSound_8, value);
	}

	inline static int32_t get_offset_of_TowelSound_9() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___TowelSound_9)); }
	inline AudioSource_t1135106623 * get_TowelSound_9() const { return ___TowelSound_9; }
	inline AudioSource_t1135106623 ** get_address_of_TowelSound_9() { return &___TowelSound_9; }
	inline void set_TowelSound_9(AudioSource_t1135106623 * value)
	{
		___TowelSound_9 = value;
		Il2CppCodeGenWriteBarrier(&___TowelSound_9, value);
	}

	inline static int32_t get_offset_of_DryerSound_10() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___DryerSound_10)); }
	inline AudioSource_t1135106623 * get_DryerSound_10() const { return ___DryerSound_10; }
	inline AudioSource_t1135106623 ** get_address_of_DryerSound_10() { return &___DryerSound_10; }
	inline void set_DryerSound_10(AudioSource_t1135106623 * value)
	{
		___DryerSound_10 = value;
		Il2CppCodeGenWriteBarrier(&___DryerSound_10, value);
	}

	inline static int32_t get_offset_of_NailCutterSound_11() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___NailCutterSound_11)); }
	inline AudioSource_t1135106623 * get_NailCutterSound_11() const { return ___NailCutterSound_11; }
	inline AudioSource_t1135106623 ** get_address_of_NailCutterSound_11() { return &___NailCutterSound_11; }
	inline void set_NailCutterSound_11(AudioSource_t1135106623 * value)
	{
		___NailCutterSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___NailCutterSound_11, value);
	}

	inline static int32_t get_offset_of_PerfumeSound_12() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___PerfumeSound_12)); }
	inline AudioSource_t1135106623 * get_PerfumeSound_12() const { return ___PerfumeSound_12; }
	inline AudioSource_t1135106623 ** get_address_of_PerfumeSound_12() { return &___PerfumeSound_12; }
	inline void set_PerfumeSound_12(AudioSource_t1135106623 * value)
	{
		___PerfumeSound_12 = value;
		Il2CppCodeGenWriteBarrier(&___PerfumeSound_12, value);
	}

	inline static int32_t get_offset_of_ClickSound_13() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___ClickSound_13)); }
	inline AudioSource_t1135106623 * get_ClickSound_13() const { return ___ClickSound_13; }
	inline AudioSource_t1135106623 ** get_address_of_ClickSound_13() { return &___ClickSound_13; }
	inline void set_ClickSound_13(AudioSource_t1135106623 * value)
	{
		___ClickSound_13 = value;
		Il2CppCodeGenWriteBarrier(&___ClickSound_13, value);
	}

	inline static int32_t get_offset_of_LastSound_14() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___LastSound_14)); }
	inline AudioSource_t1135106623 * get_LastSound_14() const { return ___LastSound_14; }
	inline AudioSource_t1135106623 ** get_address_of_LastSound_14() { return &___LastSound_14; }
	inline void set_LastSound_14(AudioSource_t1135106623 * value)
	{
		___LastSound_14 = value;
		Il2CppCodeGenWriteBarrier(&___LastSound_14, value);
	}

	inline static int32_t get_offset_of_PlaySound_15() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___PlaySound_15)); }
	inline bool get_PlaySound_15() const { return ___PlaySound_15; }
	inline bool* get_address_of_PlaySound_15() { return &___PlaySound_15; }
	inline void set_PlaySound_15(bool value)
	{
		___PlaySound_15 = value;
	}

	inline static int32_t get_offset_of_CanPlayMeow_16() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___CanPlayMeow_16)); }
	inline bool get_CanPlayMeow_16() const { return ___CanPlayMeow_16; }
	inline bool* get_address_of_CanPlayMeow_16() { return &___CanPlayMeow_16; }
	inline void set_CanPlayMeow_16(bool value)
	{
		___CanPlayMeow_16 = value;
	}

	inline static int32_t get_offset_of_MeowClips_17() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___MeowClips_17)); }
	inline AudioClipU5BU5D_t2203355011* get_MeowClips_17() const { return ___MeowClips_17; }
	inline AudioClipU5BU5D_t2203355011** get_address_of_MeowClips_17() { return &___MeowClips_17; }
	inline void set_MeowClips_17(AudioClipU5BU5D_t2203355011* value)
	{
		___MeowClips_17 = value;
		Il2CppCodeGenWriteBarrier(&___MeowClips_17, value);
	}

	inline static int32_t get_offset_of_MouthOpenPosition_19() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___MouthOpenPosition_19)); }
	inline Vector3U5BU5D_t1172311765* get_MouthOpenPosition_19() const { return ___MouthOpenPosition_19; }
	inline Vector3U5BU5D_t1172311765** get_address_of_MouthOpenPosition_19() { return &___MouthOpenPosition_19; }
	inline void set_MouthOpenPosition_19(Vector3U5BU5D_t1172311765* value)
	{
		___MouthOpenPosition_19 = value;
		Il2CppCodeGenWriteBarrier(&___MouthOpenPosition_19, value);
	}

	inline static int32_t get_offset_of_MouthOpenScale_20() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___MouthOpenScale_20)); }
	inline Vector3U5BU5D_t1172311765* get_MouthOpenScale_20() const { return ___MouthOpenScale_20; }
	inline Vector3U5BU5D_t1172311765** get_address_of_MouthOpenScale_20() { return &___MouthOpenScale_20; }
	inline void set_MouthOpenScale_20(Vector3U5BU5D_t1172311765* value)
	{
		___MouthOpenScale_20 = value;
		Il2CppCodeGenWriteBarrier(&___MouthOpenScale_20, value);
	}

	inline static int32_t get_offset_of_Order_21() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___Order_21)); }
	inline Int32U5BU5D_t3030399641* get_Order_21() const { return ___Order_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_Order_21() { return &___Order_21; }
	inline void set_Order_21(Int32U5BU5D_t3030399641* value)
	{
		___Order_21 = value;
		Il2CppCodeGenWriteBarrier(&___Order_21, value);
	}

	inline static int32_t get_offset_of_count_22() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___count_22)); }
	inline int32_t get_count_22() const { return ___count_22; }
	inline int32_t* get_address_of_count_22() { return &___count_22; }
	inline void set_count_22(int32_t value)
	{
		___count_22 = value;
	}
};

struct SoundManager_t654432262_StaticFields
{
public:
	// SoundManager SoundManager::instance
	SoundManager_t654432262 * ___instance_18;

public:
	inline static int32_t get_offset_of_instance_18() { return static_cast<int32_t>(offsetof(SoundManager_t654432262_StaticFields, ___instance_18)); }
	inline SoundManager_t654432262 * get_instance_18() const { return ___instance_18; }
	inline SoundManager_t654432262 ** get_address_of_instance_18() { return &___instance_18; }
	inline void set_instance_18(SoundManager_t654432262 * value)
	{
		___instance_18 = value;
		Il2CppCodeGenWriteBarrier(&___instance_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
