﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// GameManager
struct GameManager_t2252321495;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject GameManager::BathCleaningToolsParent
	GameObject_t1756533147 * ___BathCleaningToolsParent_2;
	// System.Single GameManager::BathCleaningAnimTimer
	float ___BathCleaningAnimTimer_3;
	// UnityEngine.GameObject GameManager::Soap
	GameObject_t1756533147 * ___Soap_4;
	// UnityEngine.GameObject GameManager::Shower
	GameObject_t1756533147 * ___Shower_5;
	// UnityEngine.GameObject GameManager::Towel
	GameObject_t1756533147 * ___Towel_6;
	// UnityEngine.GameObject GameManager::Dryer
	GameObject_t1756533147 * ___Dryer_7;
	// UnityEngine.GameObject GameManager::KittyClean
	GameObject_t1756533147 * ___KittyClean_8;
	// UnityEngine.GameObject GameManager::KittyMud
	GameObject_t1756533147 * ___KittyMud_9;
	// UnityEngine.GameObject GameManager::KittyBubble
	GameObject_t1756533147 * ___KittyBubble_10;
	// UnityEngine.GameObject GameManager::KittyWater
	GameObject_t1756533147 * ___KittyWater_11;
	// UnityEngine.GameObject GameManager::MaskKittyWater
	GameObject_t1756533147 * ___MaskKittyWater_12;
	// UnityEngine.GameObject GameManager::MaskKittyClean
	GameObject_t1756533147 * ___MaskKittyClean_13;
	// UnityEngine.GameObject GameManager::MaskKittyMud
	GameObject_t1756533147 * ___MaskKittyMud_14;
	// UnityEngine.GameObject GameManager::MaskKittyBubble
	GameObject_t1756533147 * ___MaskKittyBubble_15;
	// UnityEngine.GameObject GameManager::CurtainTassel
	GameObject_t1756533147 * ___CurtainTassel_16;
	// UnityEngine.GameObject GameManager::CurtainTop
	GameObject_t1756533147 * ___CurtainTop_17;

public:
	inline static int32_t get_offset_of_BathCleaningToolsParent_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___BathCleaningToolsParent_2)); }
	inline GameObject_t1756533147 * get_BathCleaningToolsParent_2() const { return ___BathCleaningToolsParent_2; }
	inline GameObject_t1756533147 ** get_address_of_BathCleaningToolsParent_2() { return &___BathCleaningToolsParent_2; }
	inline void set_BathCleaningToolsParent_2(GameObject_t1756533147 * value)
	{
		___BathCleaningToolsParent_2 = value;
		Il2CppCodeGenWriteBarrier(&___BathCleaningToolsParent_2, value);
	}

	inline static int32_t get_offset_of_BathCleaningAnimTimer_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___BathCleaningAnimTimer_3)); }
	inline float get_BathCleaningAnimTimer_3() const { return ___BathCleaningAnimTimer_3; }
	inline float* get_address_of_BathCleaningAnimTimer_3() { return &___BathCleaningAnimTimer_3; }
	inline void set_BathCleaningAnimTimer_3(float value)
	{
		___BathCleaningAnimTimer_3 = value;
	}

	inline static int32_t get_offset_of_Soap_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___Soap_4)); }
	inline GameObject_t1756533147 * get_Soap_4() const { return ___Soap_4; }
	inline GameObject_t1756533147 ** get_address_of_Soap_4() { return &___Soap_4; }
	inline void set_Soap_4(GameObject_t1756533147 * value)
	{
		___Soap_4 = value;
		Il2CppCodeGenWriteBarrier(&___Soap_4, value);
	}

	inline static int32_t get_offset_of_Shower_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___Shower_5)); }
	inline GameObject_t1756533147 * get_Shower_5() const { return ___Shower_5; }
	inline GameObject_t1756533147 ** get_address_of_Shower_5() { return &___Shower_5; }
	inline void set_Shower_5(GameObject_t1756533147 * value)
	{
		___Shower_5 = value;
		Il2CppCodeGenWriteBarrier(&___Shower_5, value);
	}

	inline static int32_t get_offset_of_Towel_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___Towel_6)); }
	inline GameObject_t1756533147 * get_Towel_6() const { return ___Towel_6; }
	inline GameObject_t1756533147 ** get_address_of_Towel_6() { return &___Towel_6; }
	inline void set_Towel_6(GameObject_t1756533147 * value)
	{
		___Towel_6 = value;
		Il2CppCodeGenWriteBarrier(&___Towel_6, value);
	}

	inline static int32_t get_offset_of_Dryer_7() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___Dryer_7)); }
	inline GameObject_t1756533147 * get_Dryer_7() const { return ___Dryer_7; }
	inline GameObject_t1756533147 ** get_address_of_Dryer_7() { return &___Dryer_7; }
	inline void set_Dryer_7(GameObject_t1756533147 * value)
	{
		___Dryer_7 = value;
		Il2CppCodeGenWriteBarrier(&___Dryer_7, value);
	}

	inline static int32_t get_offset_of_KittyClean_8() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___KittyClean_8)); }
	inline GameObject_t1756533147 * get_KittyClean_8() const { return ___KittyClean_8; }
	inline GameObject_t1756533147 ** get_address_of_KittyClean_8() { return &___KittyClean_8; }
	inline void set_KittyClean_8(GameObject_t1756533147 * value)
	{
		___KittyClean_8 = value;
		Il2CppCodeGenWriteBarrier(&___KittyClean_8, value);
	}

	inline static int32_t get_offset_of_KittyMud_9() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___KittyMud_9)); }
	inline GameObject_t1756533147 * get_KittyMud_9() const { return ___KittyMud_9; }
	inline GameObject_t1756533147 ** get_address_of_KittyMud_9() { return &___KittyMud_9; }
	inline void set_KittyMud_9(GameObject_t1756533147 * value)
	{
		___KittyMud_9 = value;
		Il2CppCodeGenWriteBarrier(&___KittyMud_9, value);
	}

	inline static int32_t get_offset_of_KittyBubble_10() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___KittyBubble_10)); }
	inline GameObject_t1756533147 * get_KittyBubble_10() const { return ___KittyBubble_10; }
	inline GameObject_t1756533147 ** get_address_of_KittyBubble_10() { return &___KittyBubble_10; }
	inline void set_KittyBubble_10(GameObject_t1756533147 * value)
	{
		___KittyBubble_10 = value;
		Il2CppCodeGenWriteBarrier(&___KittyBubble_10, value);
	}

	inline static int32_t get_offset_of_KittyWater_11() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___KittyWater_11)); }
	inline GameObject_t1756533147 * get_KittyWater_11() const { return ___KittyWater_11; }
	inline GameObject_t1756533147 ** get_address_of_KittyWater_11() { return &___KittyWater_11; }
	inline void set_KittyWater_11(GameObject_t1756533147 * value)
	{
		___KittyWater_11 = value;
		Il2CppCodeGenWriteBarrier(&___KittyWater_11, value);
	}

	inline static int32_t get_offset_of_MaskKittyWater_12() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___MaskKittyWater_12)); }
	inline GameObject_t1756533147 * get_MaskKittyWater_12() const { return ___MaskKittyWater_12; }
	inline GameObject_t1756533147 ** get_address_of_MaskKittyWater_12() { return &___MaskKittyWater_12; }
	inline void set_MaskKittyWater_12(GameObject_t1756533147 * value)
	{
		___MaskKittyWater_12 = value;
		Il2CppCodeGenWriteBarrier(&___MaskKittyWater_12, value);
	}

	inline static int32_t get_offset_of_MaskKittyClean_13() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___MaskKittyClean_13)); }
	inline GameObject_t1756533147 * get_MaskKittyClean_13() const { return ___MaskKittyClean_13; }
	inline GameObject_t1756533147 ** get_address_of_MaskKittyClean_13() { return &___MaskKittyClean_13; }
	inline void set_MaskKittyClean_13(GameObject_t1756533147 * value)
	{
		___MaskKittyClean_13 = value;
		Il2CppCodeGenWriteBarrier(&___MaskKittyClean_13, value);
	}

	inline static int32_t get_offset_of_MaskKittyMud_14() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___MaskKittyMud_14)); }
	inline GameObject_t1756533147 * get_MaskKittyMud_14() const { return ___MaskKittyMud_14; }
	inline GameObject_t1756533147 ** get_address_of_MaskKittyMud_14() { return &___MaskKittyMud_14; }
	inline void set_MaskKittyMud_14(GameObject_t1756533147 * value)
	{
		___MaskKittyMud_14 = value;
		Il2CppCodeGenWriteBarrier(&___MaskKittyMud_14, value);
	}

	inline static int32_t get_offset_of_MaskKittyBubble_15() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___MaskKittyBubble_15)); }
	inline GameObject_t1756533147 * get_MaskKittyBubble_15() const { return ___MaskKittyBubble_15; }
	inline GameObject_t1756533147 ** get_address_of_MaskKittyBubble_15() { return &___MaskKittyBubble_15; }
	inline void set_MaskKittyBubble_15(GameObject_t1756533147 * value)
	{
		___MaskKittyBubble_15 = value;
		Il2CppCodeGenWriteBarrier(&___MaskKittyBubble_15, value);
	}

	inline static int32_t get_offset_of_CurtainTassel_16() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___CurtainTassel_16)); }
	inline GameObject_t1756533147 * get_CurtainTassel_16() const { return ___CurtainTassel_16; }
	inline GameObject_t1756533147 ** get_address_of_CurtainTassel_16() { return &___CurtainTassel_16; }
	inline void set_CurtainTassel_16(GameObject_t1756533147 * value)
	{
		___CurtainTassel_16 = value;
		Il2CppCodeGenWriteBarrier(&___CurtainTassel_16, value);
	}

	inline static int32_t get_offset_of_CurtainTop_17() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___CurtainTop_17)); }
	inline GameObject_t1756533147 * get_CurtainTop_17() const { return ___CurtainTop_17; }
	inline GameObject_t1756533147 ** get_address_of_CurtainTop_17() { return &___CurtainTop_17; }
	inline void set_CurtainTop_17(GameObject_t1756533147 * value)
	{
		___CurtainTop_17 = value;
		Il2CppCodeGenWriteBarrier(&___CurtainTop_17, value);
	}
};

struct GameManager_t2252321495_StaticFields
{
public:
	// GameManager GameManager::instance
	GameManager_t2252321495 * ___instance_18;

public:
	inline static int32_t get_offset_of_instance_18() { return static_cast<int32_t>(offsetof(GameManager_t2252321495_StaticFields, ___instance_18)); }
	inline GameManager_t2252321495 * get_instance_18() const { return ___instance_18; }
	inline GameManager_t2252321495 ** get_address_of_instance_18() { return &___instance_18; }
	inline void set_instance_18(GameManager_t2252321495 * value)
	{
		___instance_18 = value;
		Il2CppCodeGenWriteBarrier(&___instance_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
