﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_GTButton
struct CFX_Demo_GTButton_t113862661;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_GTButton::.ctor()
extern "C"  void CFX_Demo_GTButton__ctor_m1628334610 (CFX_Demo_GTButton_t113862661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTButton::Awake()
extern "C"  void CFX_Demo_GTButton_Awake_m3115696401 (CFX_Demo_GTButton_t113862661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTButton::Update()
extern "C"  void CFX_Demo_GTButton_Update_m3744209165 (CFX_Demo_GTButton_t113862661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTButton::OnClick()
extern "C"  void CFX_Demo_GTButton_OnClick_m1410720995 (CFX_Demo_GTButton_t113862661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
