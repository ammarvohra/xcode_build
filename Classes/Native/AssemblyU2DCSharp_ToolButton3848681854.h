﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_ToolButton_ButtonType1054768361.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToolButton
struct  ToolButton_t3848681854  : public MonoBehaviour_t1158329972
{
public:
	// ToolButton/ButtonType ToolButton::buttonType
	int32_t ___buttonType_2;
	// UnityEngine.GameObject ToolButton::OnClick
	GameObject_t1756533147 * ___OnClick_3;
	// System.Boolean ToolButton::Draggable
	bool ___Draggable_4;
	// UnityEngine.Vector2 ToolButton::DragPosOffset
	Vector2_t2243707579  ___DragPosOffset_5;
	// UnityEngine.GameObject ToolButton::OnCollision
	GameObject_t1756533147 * ___OnCollision_6;
	// UnityEngine.Vector3 ToolButton::InitialPos
	Vector3_t2243707580  ___InitialPos_7;
	// System.Single ToolButton::MousePositionOffsetZ
	float ___MousePositionOffsetZ_8;

public:
	inline static int32_t get_offset_of_buttonType_2() { return static_cast<int32_t>(offsetof(ToolButton_t3848681854, ___buttonType_2)); }
	inline int32_t get_buttonType_2() const { return ___buttonType_2; }
	inline int32_t* get_address_of_buttonType_2() { return &___buttonType_2; }
	inline void set_buttonType_2(int32_t value)
	{
		___buttonType_2 = value;
	}

	inline static int32_t get_offset_of_OnClick_3() { return static_cast<int32_t>(offsetof(ToolButton_t3848681854, ___OnClick_3)); }
	inline GameObject_t1756533147 * get_OnClick_3() const { return ___OnClick_3; }
	inline GameObject_t1756533147 ** get_address_of_OnClick_3() { return &___OnClick_3; }
	inline void set_OnClick_3(GameObject_t1756533147 * value)
	{
		___OnClick_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnClick_3, value);
	}

	inline static int32_t get_offset_of_Draggable_4() { return static_cast<int32_t>(offsetof(ToolButton_t3848681854, ___Draggable_4)); }
	inline bool get_Draggable_4() const { return ___Draggable_4; }
	inline bool* get_address_of_Draggable_4() { return &___Draggable_4; }
	inline void set_Draggable_4(bool value)
	{
		___Draggable_4 = value;
	}

	inline static int32_t get_offset_of_DragPosOffset_5() { return static_cast<int32_t>(offsetof(ToolButton_t3848681854, ___DragPosOffset_5)); }
	inline Vector2_t2243707579  get_DragPosOffset_5() const { return ___DragPosOffset_5; }
	inline Vector2_t2243707579 * get_address_of_DragPosOffset_5() { return &___DragPosOffset_5; }
	inline void set_DragPosOffset_5(Vector2_t2243707579  value)
	{
		___DragPosOffset_5 = value;
	}

	inline static int32_t get_offset_of_OnCollision_6() { return static_cast<int32_t>(offsetof(ToolButton_t3848681854, ___OnCollision_6)); }
	inline GameObject_t1756533147 * get_OnCollision_6() const { return ___OnCollision_6; }
	inline GameObject_t1756533147 ** get_address_of_OnCollision_6() { return &___OnCollision_6; }
	inline void set_OnCollision_6(GameObject_t1756533147 * value)
	{
		___OnCollision_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnCollision_6, value);
	}

	inline static int32_t get_offset_of_InitialPos_7() { return static_cast<int32_t>(offsetof(ToolButton_t3848681854, ___InitialPos_7)); }
	inline Vector3_t2243707580  get_InitialPos_7() const { return ___InitialPos_7; }
	inline Vector3_t2243707580 * get_address_of_InitialPos_7() { return &___InitialPos_7; }
	inline void set_InitialPos_7(Vector3_t2243707580  value)
	{
		___InitialPos_7 = value;
	}

	inline static int32_t get_offset_of_MousePositionOffsetZ_8() { return static_cast<int32_t>(offsetof(ToolButton_t3848681854, ___MousePositionOffsetZ_8)); }
	inline float get_MousePositionOffsetZ_8() const { return ___MousePositionOffsetZ_8; }
	inline float* get_address_of_MousePositionOffsetZ_8() { return &___MousePositionOffsetZ_8; }
	inline void set_MousePositionOffsetZ_8(float value)
	{
		___MousePositionOffsetZ_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
