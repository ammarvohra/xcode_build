﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StyleManager/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3971177230;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StyleManager/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3834877927 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StyleManager/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m986099081 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StyleManager/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m249666013 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StyleManager/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3436385941 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StyleManager/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2281708188 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StyleManager/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2391459750 (U3CStartU3Ec__Iterator0_t3971177230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
