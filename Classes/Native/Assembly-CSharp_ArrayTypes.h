﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// GoogleMobileAds.Api.Mediation.MediationExtras
struct MediationExtras_t1641207307;
// CFX_AutoDestructShuriken
struct CFX_AutoDestructShuriken_t1500114366;
// CFX_LightIntensityFade
struct CFX_LightIntensityFade_t4221734619;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_NativeAdType1094124130.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Mediation_Me1641207307.h"
#include "AssemblyU2DCSharp_CFX_AutoDestructShuriken1500114366.h"
#include "AssemblyU2DCSharp_CFX_LightIntensityFade4221734619.h"

#pragma once
// GoogleMobileAds.Api.NativeAdType[]
struct NativeAdTypeU5BU5D_t2772766167  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// GoogleMobileAds.Api.Mediation.MediationExtras[]
struct MediationExtrasU5BU5D_t1435373930  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MediationExtras_t1641207307 * m_Items[1];

public:
	inline MediationExtras_t1641207307 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MediationExtras_t1641207307 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MediationExtras_t1641207307 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MediationExtras_t1641207307 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MediationExtras_t1641207307 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MediationExtras_t1641207307 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CFX_AutoDestructShuriken[]
struct CFX_AutoDestructShurikenU5BU5D_t2040307083  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CFX_AutoDestructShuriken_t1500114366 * m_Items[1];

public:
	inline CFX_AutoDestructShuriken_t1500114366 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CFX_AutoDestructShuriken_t1500114366 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CFX_AutoDestructShuriken_t1500114366 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CFX_AutoDestructShuriken_t1500114366 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CFX_AutoDestructShuriken_t1500114366 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CFX_AutoDestructShuriken_t1500114366 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CFX_LightIntensityFade[]
struct CFX_LightIntensityFadeU5BU5D_t2555693914  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CFX_LightIntensityFade_t4221734619 * m_Items[1];

public:
	inline CFX_LightIntensityFade_t4221734619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CFX_LightIntensityFade_t4221734619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CFX_LightIntensityFade_t4221734619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CFX_LightIntensityFade_t4221734619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CFX_LightIntensityFade_t4221734619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CFX_LightIntensityFade_t4221734619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
