﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_RandomDir
struct CFX_Demo_RandomDir_t1076153330;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_RandomDir::.ctor()
extern "C"  void CFX_Demo_RandomDir__ctor_m1640169709 (CFX_Demo_RandomDir_t1076153330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RandomDir::Start()
extern "C"  void CFX_Demo_RandomDir_Start_m2717481693 (CFX_Demo_RandomDir_t1076153330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
