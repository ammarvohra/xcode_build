﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_AutoRotate
struct CFX_AutoRotate_t1831446564;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_AutoRotate::.ctor()
extern "C"  void CFX_AutoRotate__ctor_m3313194767 (CFX_AutoRotate_t1831446564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoRotate::Update()
extern "C"  void CFX_AutoRotate_Update_m1106375846 (CFX_AutoRotate_t1831446564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
