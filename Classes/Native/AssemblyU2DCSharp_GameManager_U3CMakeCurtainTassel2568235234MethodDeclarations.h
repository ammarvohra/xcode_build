﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager/<MakeCurtainTasselAnimOff>c__Iterator1
struct U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager/<MakeCurtainTasselAnimOff>c__Iterator1::.ctor()
extern "C"  void U3CMakeCurtainTasselAnimOffU3Ec__Iterator1__ctor_m2273567347 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager/<MakeCurtainTasselAnimOff>c__Iterator1::MoveNext()
extern "C"  bool U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_MoveNext_m991891085 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<MakeCurtainTasselAnimOff>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3196899121 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<MakeCurtainTasselAnimOff>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2519905977 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<MakeCurtainTasselAnimOff>c__Iterator1::Dispose()
extern "C"  void U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_Dispose_m3766012336 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<MakeCurtainTasselAnimOff>c__Iterator1::Reset()
extern "C"  void U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_Reset_m1439168954 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
