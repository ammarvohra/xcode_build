﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Light
struct Light_t494725636;

#include "codegen/il2cpp-codegen.h"

// System.Single UnityEngine.Light::get_intensity()
extern "C"  float Light_get_intensity_m392824783 (Light_t494725636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C"  void Light_set_intensity_m1790590300 (Light_t494725636 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
