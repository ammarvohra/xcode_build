﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DestroyOrDeactivateAtFixedTime
struct DestroyOrDeactivateAtFixedTime_t2027803675;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void DestroyOrDeactivateAtFixedTime::.ctor()
extern "C"  void DestroyOrDeactivateAtFixedTime__ctor_m3737081894 (DestroyOrDeactivateAtFixedTime_t2027803675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyOrDeactivateAtFixedTime::OnEnable()
extern "C"  void DestroyOrDeactivateAtFixedTime_OnEnable_m730114202 (DestroyOrDeactivateAtFixedTime_t2027803675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DestroyOrDeactivateAtFixedTime::DestroyOrDeactivate()
extern "C"  Il2CppObject * DestroyOrDeactivateAtFixedTime_DestroyOrDeactivate_m494602103 (DestroyOrDeactivateAtFixedTime_t2027803675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
