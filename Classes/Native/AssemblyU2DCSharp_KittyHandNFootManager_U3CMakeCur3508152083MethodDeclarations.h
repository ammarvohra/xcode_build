﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6
struct U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::.ctor()
extern "C"  void U3CMakeCurtainTasselAnimOffU3Ec__Iterator6__ctor_m708476084 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::MoveNext()
extern "C"  bool U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_MoveNext_m1788629920 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1386238616 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m2490880480 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::Dispose()
extern "C"  void U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_Dispose_m3033528425 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<MakeCurtainTasselAnimOff>c__Iterator6::Reset()
extern "C"  void U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_Reset_m4203996355 (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
