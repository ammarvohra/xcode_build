﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToolButton
struct ToolButton_t3848681854;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"

// System.Void ToolButton::.ctor()
extern "C"  void ToolButton__ctor_m2189683053 (ToolButton_t3848681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToolButton::Start()
extern "C"  void ToolButton_Start_m298107581 (ToolButton_t3848681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToolButton::OnMouseDown()
extern "C"  void ToolButton_OnMouseDown_m947594191 (ToolButton_t3848681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToolButton::OnMouseUp()
extern "C"  void ToolButton_OnMouseUp_m1815978676 (ToolButton_t3848681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToolButton::OnMouseDrag()
extern "C"  void ToolButton_OnMouseDrag_m2923855017 (ToolButton_t3848681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToolButton::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void ToolButton_OnCollisionEnter2D_m2904470271 (ToolButton_t3848681854 * __this, Collision2D_t1539500754 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToolButton::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void ToolButton_OnCollisionStay2D_m1775791980 (ToolButton_t3848681854 * __this, Collision2D_t1539500754 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ToolButton::ButtonBackOnPosition()
extern "C"  Il2CppObject * ToolButton_ButtonBackOnPosition_m755920680 (ToolButton_t3848681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
