﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BathManager
struct BathManager_t1712132540;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void BathManager::.ctor()
extern "C"  void BathManager__ctor_m405701347 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::Start()
extern "C"  void BathManager_Start_m1358124567 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::Update()
extern "C"  void BathManager_Update_m3276145598 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::FirstTimeOnShower()
extern "C"  void BathManager_FirstTimeOnShower_m2379771909 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::FirstTimeOnTowel()
extern "C"  void BathManager_FirstTimeOnTowel_m3245076506 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::FirstTimeOnDryer()
extern "C"  void BathManager_FirstTimeOnDryer_m2883027865 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::SoapMouseDown()
extern "C"  void BathManager_SoapMouseDown_m256016445 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::SoapMouseUp()
extern "C"  void BathManager_SoapMouseUp_m3197282804 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::SoapMouseDrag()
extern "C"  void BathManager_SoapMouseDrag_m601343283 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::SoapCollisionEnter(UnityEngine.GameObject)
extern "C"  void BathManager_SoapCollisionEnter_m2381891276 (BathManager_t1712132540 * __this, GameObject_t1756533147 * ___Coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::ShowerMouseDown()
extern "C"  void BathManager_ShowerMouseDown_m432168686 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::ShowerMouseUp()
extern "C"  void BathManager_ShowerMouseUp_m3440034111 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::ShowerMouseDrag()
extern "C"  void BathManager_ShowerMouseDrag_m1072274760 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::TowelMouseDown()
extern "C"  void BathManager_TowelMouseDown_m167195285 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::TowelMouseUp()
extern "C"  void BathManager_TowelMouseUp_m2169313212 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::TowelMouseDrag()
extern "C"  void BathManager_TowelMouseDrag_m2143466535 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::DryerMouseDown()
extern "C"  void BathManager_DryerMouseDown_m3129100458 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::DryerMouseUp()
extern "C"  void BathManager_DryerMouseUp_m73446799 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::DryerMouseDrag()
extern "C"  void BathManager_DryerMouseDrag_m810420388 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::CurtainTasselMouseDown()
extern "C"  void BathManager_CurtainTasselMouseDown_m3751271230 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager::BackButtonMouseDown()
extern "C"  void BathManager_BackButtonMouseDown_m3847500657 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BathManager::InitiateBubbles()
extern "C"  Il2CppObject * BathManager_InitiateBubbles_m1145650107 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BathManager::InitiateWaterDrops()
extern "C"  Il2CppObject * BathManager_InitiateWaterDrops_m4243393981 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BathManager::InitiateStar()
extern "C"  Il2CppObject * BathManager_InitiateStar_m2940284096 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BathManager::LoadNextLevel()
extern "C"  Il2CppObject * BathManager_LoadNextLevel_m2369436626 (BathManager_t1712132540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
