﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonManager/<FbLikeButtonUp>c__Iterator2
struct U3CFbLikeButtonUpU3Ec__Iterator2_t384855249;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonManager/<FbLikeButtonUp>c__Iterator2::.ctor()
extern "C"  void U3CFbLikeButtonUpU3Ec__Iterator2__ctor_m1042108856 (U3CFbLikeButtonUpU3Ec__Iterator2_t384855249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonManager/<FbLikeButtonUp>c__Iterator2::MoveNext()
extern "C"  bool U3CFbLikeButtonUpU3Ec__Iterator2_MoveNext_m719452128 (U3CFbLikeButtonUpU3Ec__Iterator2_t384855249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ButtonManager/<FbLikeButtonUp>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFbLikeButtonUpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1770780436 (U3CFbLikeButtonUpU3Ec__Iterator2_t384855249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ButtonManager/<FbLikeButtonUp>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFbLikeButtonUpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m380634444 (U3CFbLikeButtonUpU3Ec__Iterator2_t384855249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager/<FbLikeButtonUp>c__Iterator2::Dispose()
extern "C"  void U3CFbLikeButtonUpU3Ec__Iterator2_Dispose_m3889458939 (U3CFbLikeButtonUpU3Ec__Iterator2_t384855249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager/<FbLikeButtonUp>c__Iterator2::Reset()
extern "C"  void U3CFbLikeButtonUpU3Ec__Iterator2_Reset_m4262284769 (U3CFbLikeButtonUpU3Ec__Iterator2_t384855249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
