﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MaskCamera
struct MaskCamera_t4194554627;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void MaskCamera::.ctor()
extern "C"  void MaskCamera__ctor_m1205289458 (MaskCamera_t4194554627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaskCamera::CutHole(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void MaskCamera_CutHole_m3971818326 (MaskCamera_t4194554627 * __this, Vector2_t2243707579  ___imageSize0, Vector2_t2243707579  ___imageLocalPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaskCamera::Start()
extern "C"  void MaskCamera_Start_m1857878702 (MaskCamera_t4194554627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaskCamera::Update()
extern "C"  void MaskCamera_Update_m773705451 (MaskCamera_t4194554627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaskCamera::OnPostRender()
extern "C"  void MaskCamera_OnPostRender_m1760245409 (MaskCamera_t4194554627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaskCamera::.cctor()
extern "C"  void MaskCamera__cctor_m2054578709 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
