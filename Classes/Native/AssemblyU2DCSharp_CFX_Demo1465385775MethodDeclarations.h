﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo
struct CFX_Demo_t1465385775;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo::.ctor()
extern "C"  void CFX_Demo__ctor_m1379620782 (CFX_Demo_t1465385775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::OnMouseDown()
extern "C"  void CFX_Demo_OnMouseDown_m3357879778 (CFX_Demo_t1465385775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX_Demo::spawnParticle()
extern "C"  GameObject_t1756533147 * CFX_Demo_spawnParticle_m2270354716 (CFX_Demo_t1465385775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::OnGUI()
extern "C"  void CFX_Demo_OnGUI_m3714444190 (CFX_Demo_t1465385775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX_Demo::RandomSpawnsCoroutine()
extern "C"  Il2CppObject * CFX_Demo_RandomSpawnsCoroutine_m3592711837 (CFX_Demo_t1465385775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::Update()
extern "C"  void CFX_Demo_Update_m512698871 (CFX_Demo_t1465385775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::prevParticle()
extern "C"  void CFX_Demo_prevParticle_m3438691415 (CFX_Demo_t1465385775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::nextParticle()
extern "C"  void CFX_Demo_nextParticle_m3063863573 (CFX_Demo_t1465385775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
