﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChartboostSDK.CBMediation
struct CBMediation_t669766003;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChartboostSDK.CBMediation::.ctor(System.String)
extern "C"  void CBMediation__ctor_m997159027 (CBMediation_t669766003 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBMediation::ToString()
extern "C"  String_t* CBMediation_ToString_m3403320774 (CBMediation_t669766003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBMediation::.cctor()
extern "C"  void CBMediation__cctor_m3639357812 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
