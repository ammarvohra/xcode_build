﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_ShurikenThreadFix
struct CFX_ShurikenThreadFix_t3286853382;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_ShurikenThreadFix::.ctor()
extern "C"  void CFX_ShurikenThreadFix__ctor_m3709704813 (CFX_ShurikenThreadFix_t3286853382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_ShurikenThreadFix::OnEnable()
extern "C"  void CFX_ShurikenThreadFix_OnEnable_m2449814541 (CFX_ShurikenThreadFix_t3286853382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX_ShurikenThreadFix::WaitFrame()
extern "C"  Il2CppObject * CFX_ShurikenThreadFix_WaitFrame_m2779789185 (CFX_ShurikenThreadFix_t3286853382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
