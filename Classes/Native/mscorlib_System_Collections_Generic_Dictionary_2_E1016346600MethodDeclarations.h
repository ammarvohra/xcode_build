﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2988594762MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3746505122(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1016346600 *, Dictionary_2_t3991289194 *, const MethodInfo*))Enumerator__ctor_m1050411420_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4288679005(__this, method) ((  Il2CppObject * (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1686586217_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1373791253(__this, method) ((  void (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4227218433_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m81273188(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m542906350_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m685004587(__this, method) ((  Il2CppObject * (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3398460119_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3418968203(__this, method) ((  Il2CppObject * (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m700552431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::MoveNext()
#define Enumerator_MoveNext_m4283113066(__this, method) ((  bool (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_MoveNext_m1671601793_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_Current()
#define Enumerator_get_Current_m527319379(__this, method) ((  KeyValuePair_2_t1748634416  (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_get_Current_m2163799293_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2706670610(__this, method) ((  String_t* (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_get_CurrentKey_m2539943900_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2487534290(__this, method) ((  float (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_get_CurrentValue_m3141163996_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::Reset()
#define Enumerator_Reset_m3556844764(__this, method) ((  void (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_Reset_m308315030_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::VerifyState()
#define Enumerator_VerifyState_m2930862907(__this, method) ((  void (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_VerifyState_m3000094783_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m427668901(__this, method) ((  void (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_VerifyCurrent_m220863089_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::Dispose()
#define Enumerator_Dispose_m169701842(__this, method) ((  void (*) (Enumerator_t1016346600 *, const MethodInfo*))Enumerator_Dispose_m884018764_gshared)(__this, method)
