﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StyleManager
struct  StyleManager_t3102803280  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject StyleManager::HatHolder
	GameObject_t1756533147 * ___HatHolder_2;
	// UnityEngine.Sprite[] StyleManager::HatObjs
	SpriteU5BU5D_t3359083662* ___HatObjs_3;
	// System.Boolean StyleManager::HatFirstTime
	bool ___HatFirstTime_4;
	// UnityEngine.GameObject StyleManager::GogglesHolder
	GameObject_t1756533147 * ___GogglesHolder_5;
	// UnityEngine.Sprite[] StyleManager::GogglesObjs
	SpriteU5BU5D_t3359083662* ___GogglesObjs_6;
	// System.Boolean StyleManager::GogglesFirstTime
	bool ___GogglesFirstTime_7;
	// UnityEngine.GameObject StyleManager::PendantHolder
	GameObject_t1756533147 * ___PendantHolder_8;
	// UnityEngine.Sprite[] StyleManager::PendantObjs
	SpriteU5BU5D_t3359083662* ___PendantObjs_9;
	// System.Boolean StyleManager::PendantFirstTime
	bool ___PendantFirstTime_10;
	// UnityEngine.GameObject StyleManager::HatIcon
	GameObject_t1756533147 * ___HatIcon_11;
	// UnityEngine.GameObject StyleManager::GogglesIcon
	GameObject_t1756533147 * ___GogglesIcon_12;
	// UnityEngine.GameObject StyleManager::PendantIcon
	GameObject_t1756533147 * ___PendantIcon_13;
	// UnityEngine.GameObject StyleManager::GameOverPanel
	GameObject_t1756533147 * ___GameOverPanel_14;
	// System.Int32 StyleManager::CurrentHatIndex
	int32_t ___CurrentHatIndex_15;
	// System.Int32 StyleManager::CurrentGogglesIndex
	int32_t ___CurrentGogglesIndex_16;
	// System.Int32 StyleManager::CurrentPendantIndex
	int32_t ___CurrentPendantIndex_17;

public:
	inline static int32_t get_offset_of_HatHolder_2() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___HatHolder_2)); }
	inline GameObject_t1756533147 * get_HatHolder_2() const { return ___HatHolder_2; }
	inline GameObject_t1756533147 ** get_address_of_HatHolder_2() { return &___HatHolder_2; }
	inline void set_HatHolder_2(GameObject_t1756533147 * value)
	{
		___HatHolder_2 = value;
		Il2CppCodeGenWriteBarrier(&___HatHolder_2, value);
	}

	inline static int32_t get_offset_of_HatObjs_3() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___HatObjs_3)); }
	inline SpriteU5BU5D_t3359083662* get_HatObjs_3() const { return ___HatObjs_3; }
	inline SpriteU5BU5D_t3359083662** get_address_of_HatObjs_3() { return &___HatObjs_3; }
	inline void set_HatObjs_3(SpriteU5BU5D_t3359083662* value)
	{
		___HatObjs_3 = value;
		Il2CppCodeGenWriteBarrier(&___HatObjs_3, value);
	}

	inline static int32_t get_offset_of_HatFirstTime_4() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___HatFirstTime_4)); }
	inline bool get_HatFirstTime_4() const { return ___HatFirstTime_4; }
	inline bool* get_address_of_HatFirstTime_4() { return &___HatFirstTime_4; }
	inline void set_HatFirstTime_4(bool value)
	{
		___HatFirstTime_4 = value;
	}

	inline static int32_t get_offset_of_GogglesHolder_5() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___GogglesHolder_5)); }
	inline GameObject_t1756533147 * get_GogglesHolder_5() const { return ___GogglesHolder_5; }
	inline GameObject_t1756533147 ** get_address_of_GogglesHolder_5() { return &___GogglesHolder_5; }
	inline void set_GogglesHolder_5(GameObject_t1756533147 * value)
	{
		___GogglesHolder_5 = value;
		Il2CppCodeGenWriteBarrier(&___GogglesHolder_5, value);
	}

	inline static int32_t get_offset_of_GogglesObjs_6() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___GogglesObjs_6)); }
	inline SpriteU5BU5D_t3359083662* get_GogglesObjs_6() const { return ___GogglesObjs_6; }
	inline SpriteU5BU5D_t3359083662** get_address_of_GogglesObjs_6() { return &___GogglesObjs_6; }
	inline void set_GogglesObjs_6(SpriteU5BU5D_t3359083662* value)
	{
		___GogglesObjs_6 = value;
		Il2CppCodeGenWriteBarrier(&___GogglesObjs_6, value);
	}

	inline static int32_t get_offset_of_GogglesFirstTime_7() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___GogglesFirstTime_7)); }
	inline bool get_GogglesFirstTime_7() const { return ___GogglesFirstTime_7; }
	inline bool* get_address_of_GogglesFirstTime_7() { return &___GogglesFirstTime_7; }
	inline void set_GogglesFirstTime_7(bool value)
	{
		___GogglesFirstTime_7 = value;
	}

	inline static int32_t get_offset_of_PendantHolder_8() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___PendantHolder_8)); }
	inline GameObject_t1756533147 * get_PendantHolder_8() const { return ___PendantHolder_8; }
	inline GameObject_t1756533147 ** get_address_of_PendantHolder_8() { return &___PendantHolder_8; }
	inline void set_PendantHolder_8(GameObject_t1756533147 * value)
	{
		___PendantHolder_8 = value;
		Il2CppCodeGenWriteBarrier(&___PendantHolder_8, value);
	}

	inline static int32_t get_offset_of_PendantObjs_9() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___PendantObjs_9)); }
	inline SpriteU5BU5D_t3359083662* get_PendantObjs_9() const { return ___PendantObjs_9; }
	inline SpriteU5BU5D_t3359083662** get_address_of_PendantObjs_9() { return &___PendantObjs_9; }
	inline void set_PendantObjs_9(SpriteU5BU5D_t3359083662* value)
	{
		___PendantObjs_9 = value;
		Il2CppCodeGenWriteBarrier(&___PendantObjs_9, value);
	}

	inline static int32_t get_offset_of_PendantFirstTime_10() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___PendantFirstTime_10)); }
	inline bool get_PendantFirstTime_10() const { return ___PendantFirstTime_10; }
	inline bool* get_address_of_PendantFirstTime_10() { return &___PendantFirstTime_10; }
	inline void set_PendantFirstTime_10(bool value)
	{
		___PendantFirstTime_10 = value;
	}

	inline static int32_t get_offset_of_HatIcon_11() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___HatIcon_11)); }
	inline GameObject_t1756533147 * get_HatIcon_11() const { return ___HatIcon_11; }
	inline GameObject_t1756533147 ** get_address_of_HatIcon_11() { return &___HatIcon_11; }
	inline void set_HatIcon_11(GameObject_t1756533147 * value)
	{
		___HatIcon_11 = value;
		Il2CppCodeGenWriteBarrier(&___HatIcon_11, value);
	}

	inline static int32_t get_offset_of_GogglesIcon_12() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___GogglesIcon_12)); }
	inline GameObject_t1756533147 * get_GogglesIcon_12() const { return ___GogglesIcon_12; }
	inline GameObject_t1756533147 ** get_address_of_GogglesIcon_12() { return &___GogglesIcon_12; }
	inline void set_GogglesIcon_12(GameObject_t1756533147 * value)
	{
		___GogglesIcon_12 = value;
		Il2CppCodeGenWriteBarrier(&___GogglesIcon_12, value);
	}

	inline static int32_t get_offset_of_PendantIcon_13() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___PendantIcon_13)); }
	inline GameObject_t1756533147 * get_PendantIcon_13() const { return ___PendantIcon_13; }
	inline GameObject_t1756533147 ** get_address_of_PendantIcon_13() { return &___PendantIcon_13; }
	inline void set_PendantIcon_13(GameObject_t1756533147 * value)
	{
		___PendantIcon_13 = value;
		Il2CppCodeGenWriteBarrier(&___PendantIcon_13, value);
	}

	inline static int32_t get_offset_of_GameOverPanel_14() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___GameOverPanel_14)); }
	inline GameObject_t1756533147 * get_GameOverPanel_14() const { return ___GameOverPanel_14; }
	inline GameObject_t1756533147 ** get_address_of_GameOverPanel_14() { return &___GameOverPanel_14; }
	inline void set_GameOverPanel_14(GameObject_t1756533147 * value)
	{
		___GameOverPanel_14 = value;
		Il2CppCodeGenWriteBarrier(&___GameOverPanel_14, value);
	}

	inline static int32_t get_offset_of_CurrentHatIndex_15() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___CurrentHatIndex_15)); }
	inline int32_t get_CurrentHatIndex_15() const { return ___CurrentHatIndex_15; }
	inline int32_t* get_address_of_CurrentHatIndex_15() { return &___CurrentHatIndex_15; }
	inline void set_CurrentHatIndex_15(int32_t value)
	{
		___CurrentHatIndex_15 = value;
	}

	inline static int32_t get_offset_of_CurrentGogglesIndex_16() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___CurrentGogglesIndex_16)); }
	inline int32_t get_CurrentGogglesIndex_16() const { return ___CurrentGogglesIndex_16; }
	inline int32_t* get_address_of_CurrentGogglesIndex_16() { return &___CurrentGogglesIndex_16; }
	inline void set_CurrentGogglesIndex_16(int32_t value)
	{
		___CurrentGogglesIndex_16 = value;
	}

	inline static int32_t get_offset_of_CurrentPendantIndex_17() { return static_cast<int32_t>(offsetof(StyleManager_t3102803280, ___CurrentPendantIndex_17)); }
	inline int32_t get_CurrentPendantIndex_17() const { return ___CurrentPendantIndex_17; }
	inline int32_t* get_address_of_CurrentPendantIndex_17() { return &___CurrentPendantIndex_17; }
	inline void set_CurrentPendantIndex_17(int32_t value)
	{
		___CurrentPendantIndex_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
