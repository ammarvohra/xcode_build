﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3
struct U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::.ctor()
extern "C"  void U3CMakeNailPolishIconAvailableU3Ec__Iterator3__ctor_m457145830 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::MoveNext()
extern "C"  bool U3CMakeNailPolishIconAvailableU3Ec__Iterator3_MoveNext_m4286149166 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeNailPolishIconAvailableU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199305384 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeNailPolishIconAvailableU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1071451200 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::Dispose()
extern "C"  void U3CMakeNailPolishIconAvailableU3Ec__Iterator3_Dispose_m2859485279 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<MakeNailPolishIconAvailable>c__Iterator3::Reset()
extern "C"  void U3CMakeNailPolishIconAvailableU3Ec__Iterator3_Reset_m329402769 (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
