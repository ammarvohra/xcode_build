﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ObjectSpin
struct ObjectSpin_t853925131;
// PrefabLooping
struct PrefabLooping_t2427949168;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;
// ResolutionFixer
struct ResolutionFixer_t357239262;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ObjectSpin853925131.h"
#include "AssemblyU2DUnityScript_ObjectSpin853925131MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DUnityScript_PrefabLooping2427949168.h"
#include "AssemblyU2DUnityScript_PrefabLooping2427949168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DUnityScript_ResolutionFixer357239262.h"
#include "AssemblyU2DUnityScript_ResolutionFixer357239262MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"

// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ObjectSpin::.ctor()
extern "C"  void ObjectSpin__ctor_m43479655 (ObjectSpin_t853925131 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObjectSpin::Start()
extern "C"  void ObjectSpin_Start_m1391810155 (ObjectSpin_t853925131 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localRotation_m2055111962(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObjectSpin::Update()
extern "C"  void ObjectSpin_Update_m2105600082 (ObjectSpin_t853925131 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_DegreesX_2();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_DegreesY_3();
		float L_4 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_DegreesZ_4();
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m4255273365(L_0, ((float)((float)L_1*(float)L_2)), ((float)((float)L_3*(float)L_4)), ((float)((float)L_5*(float)L_6)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObjectSpin::Main()
extern "C"  void ObjectSpin_Main_m3772133374 (ObjectSpin_t853925131 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PrefabLooping::.ctor()
extern "C"  void PrefabLooping__ctor_m3104908688 (PrefabLooping_t2427949168 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PrefabLooping::Start()
extern Il2CppCodeGenString* _stringLiteral1433818480;
extern const uint32_t PrefabLooping_Start_m3505107736_MetadataUsageId;
extern "C"  void PrefabLooping_Start_m3505107736 (PrefabLooping_t2427949168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabLooping_Start_m3505107736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_intervalMin_3();
		float L_1 = __this->get_intervalMax_4();
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_InvokeRepeating_m3468262484(__this, _stringLiteral1433818480, (((float)((float)0))), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PrefabLooping::Burst()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t PrefabLooping_Burst_m3615015702_MetadataUsageId;
extern "C"  void PrefabLooping_Burst_m3615015702 (PrefabLooping_t2427949168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabLooping_Burst_m3615015702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_get_active_m1672294863(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		goto IL_00af;
	}

IL_0015:
	{
		V_0 = (GameObject_t1756533147 *)NULL;
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = (&V_1)->get_x_1();
		float L_5 = __this->get_positionFactor_5();
		float L_6 = __this->get_positionFactor_5();
		float L_7 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_5)), L_6, /*hidden argument*/NULL);
		(&V_1)->set_x_1(((float)((float)L_4+(float)L_7)));
		float L_8 = (&V_1)->get_y_2();
		float L_9 = __this->get_positionFactor_5();
		float L_10 = __this->get_positionFactor_5();
		float L_11 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_9)), L_10, /*hidden argument*/NULL);
		(&V_1)->set_y_2(((float)((float)L_8+(float)L_11)));
		float L_12 = (&V_1)->get_z_3();
		float L_13 = __this->get_positionFactor_5();
		float L_14 = __this->get_positionFactor_5();
		float L_15 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_13)), L_14, /*hidden argument*/NULL);
		(&V_1)->set_z_3(((float)((float)L_12+(float)L_15)));
		GameObject_t1756533147 * L_16 = __this->get_FXLoop_2();
		Vector3_t2243707580  L_17 = V_1;
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t4030073918  L_19 = Transform_get_rotation_m1033555130(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_20 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_16, L_17, L_19, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_20;
		GameObject_t1756533147 * L_21 = V_0;
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = GameObject_get_gameObject_m3662236595(L_21, /*hidden argument*/NULL);
		float L_23 = __this->get_killtime_6();
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
	}

IL_00af:
	{
		return;
	}
}
// System.Void PrefabLooping::Main()
extern "C"  void PrefabLooping_Main_m2062010799 (PrefabLooping_t2427949168 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ResolutionFixer::.ctor()
extern "C"  void ResolutionFixer__ctor_m4009208722 (ResolutionFixer_t357239262 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ResolutionFixer::Start()
extern "C"  void ResolutionFixer_Start_m1398064002 (ResolutionFixer_t357239262 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	float V_12 = 0.0f;
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	float V_14 = 0.0f;
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float V_17 = 0.0f;
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t2243707580  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector3_t2243707580  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector3_t2243707580  V_21;
	memset(&V_21, 0, sizeof(V_21));
	float V_22 = 0.0f;
	Vector3_t2243707580  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Vector3_t2243707580  V_24;
	memset(&V_24, 0, sizeof(V_24));
	float V_25 = 0.0f;
	Vector3_t2243707580  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Vector3_t2243707580  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Vector3_t2243707580  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Vector3_t2243707580  V_29;
	memset(&V_29, 0, sizeof(V_29));
	float V_30 = 0.0f;
	Vector3_t2243707580  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector3_t2243707580  V_32;
	memset(&V_32, 0, sizeof(V_32));
	float V_33 = 0.0f;
	Vector3_t2243707580  V_34;
	memset(&V_34, 0, sizeof(V_34));
	Vector3_t2243707580  V_35;
	memset(&V_35, 0, sizeof(V_35));
	Vector3_t2243707580  V_36;
	memset(&V_36, 0, sizeof(V_36));
	Vector3_t2243707580  V_37;
	memset(&V_37, 0, sizeof(V_37));
	float V_38 = 0.0f;
	Vector3_t2243707580  V_39;
	memset(&V_39, 0, sizeof(V_39));
	Vector3_t2243707580  V_40;
	memset(&V_40, 0, sizeof(V_40));
	float V_41 = 0.0f;
	Vector3_t2243707580  V_42;
	memset(&V_42, 0, sizeof(V_42));
	Vector3_t2243707580  V_43;
	memset(&V_43, 0, sizeof(V_43));
	Vector3_t2243707580  V_44;
	memset(&V_44, 0, sizeof(V_44));
	Vector3_t2243707580  V_45;
	memset(&V_45, 0, sizeof(V_45));
	float V_46 = 0.0f;
	Vector3_t2243707580  V_47;
	memset(&V_47, 0, sizeof(V_47));
	{
		int32_t L_0 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_HeightStore_9(L_0);
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_WidthStore_10(L_1);
		int32_t L_2 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_WorkingYorRatio_3();
		__this->set_ratiox_11((((float)((float)((int32_t)((int32_t)L_2*(int32_t)L_3))))));
		int32_t L_4 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_WorkingXorRatio_2();
		__this->set_ratioy_12((((float)((float)((int32_t)((int32_t)L_4*(int32_t)L_5))))));
		bool L_6 = __this->get_ChangeX_4();
		if (!L_6)
		{
			goto IL_0136;
		}
	}
	{
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_localScale_m3074381503(L_7, /*hidden argument*/NULL);
		V_16 = L_8;
		float L_9 = (&V_16)->get_x_1();
		float L_10 = __this->get_ratiox_11();
		float L_11 = __this->get_ratioy_12();
		float L_12 = ((float)((float)((float)((float)L_9*(float)L_10))/(float)L_11));
		V_0 = L_12;
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_localScale_m3074381503(L_13, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = L_14;
		V_1 = L_15;
		float L_16 = V_0;
		float L_17 = L_16;
		V_17 = L_17;
		(&V_1)->set_x_1(L_17);
		float L_18 = V_17;
		Transform_t3275118058 * L_19 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = V_1;
		Vector3_t2243707580  L_21 = L_20;
		V_18 = L_21;
		NullCheck(L_19);
		Transform_set_localScale_m2325460848(L_19, L_21, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = V_18;
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_position_m1104419803(L_23, /*hidden argument*/NULL);
		V_19 = L_24;
		float L_25 = (&V_19)->get_x_1();
		Transform_t3275118058 * L_26 = __this->get_MainCamera_8();
		NullCheck(L_26);
		Vector3_t2243707580  L_27 = Transform_get_position_m1104419803(L_26, /*hidden argument*/NULL);
		V_20 = L_27;
		float L_28 = (&V_20)->get_x_1();
		__this->set_xdistance_13(((float)((float)L_25-(float)L_28)));
		float L_29 = __this->get_xdistance_13();
		float L_30 = __this->get_ratiox_11();
		float L_31 = __this->get_ratioy_12();
		__this->set_newxdistance_15(((float)((float)L_29*(float)((float)((float)L_30/(float)L_31)))));
		Transform_t3275118058 * L_32 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t2243707580  L_33 = Transform_get_position_m1104419803(L_32, /*hidden argument*/NULL);
		V_21 = L_33;
		float L_34 = (&V_21)->get_x_1();
		float L_35 = __this->get_newxdistance_15();
		float L_36 = __this->get_xdistance_13();
		float L_37 = ((float)((float)L_34+(float)((float)((float)L_35-(float)L_36))));
		V_2 = L_37;
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_position_m1104419803(L_38, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = L_39;
		V_3 = L_40;
		float L_41 = V_2;
		float L_42 = L_41;
		V_22 = L_42;
		(&V_3)->set_x_1(L_42);
		float L_43 = V_22;
		Transform_t3275118058 * L_44 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_45 = V_3;
		Vector3_t2243707580  L_46 = L_45;
		V_23 = L_46;
		NullCheck(L_44);
		Transform_set_position_m2469242620(L_44, L_46, /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = V_23;
	}

IL_0136:
	{
		bool L_48 = __this->get_ChangeY_5();
		if (!L_48)
		{
			goto IL_0238;
		}
	}
	{
		Transform_t3275118058 * L_49 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		Vector3_t2243707580  L_50 = Transform_get_localScale_m3074381503(L_49, /*hidden argument*/NULL);
		V_24 = L_50;
		float L_51 = (&V_24)->get_y_2();
		float L_52 = __this->get_ratioy_12();
		float L_53 = __this->get_ratiox_11();
		float L_54 = ((float)((float)((float)((float)L_51*(float)L_52))/(float)L_53));
		V_4 = L_54;
		Transform_t3275118058 * L_55 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		Vector3_t2243707580  L_56 = Transform_get_localScale_m3074381503(L_55, /*hidden argument*/NULL);
		Vector3_t2243707580  L_57 = L_56;
		V_5 = L_57;
		float L_58 = V_4;
		float L_59 = L_58;
		V_25 = L_59;
		(&V_5)->set_y_2(L_59);
		float L_60 = V_25;
		Transform_t3275118058 * L_61 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_62 = V_5;
		Vector3_t2243707580  L_63 = L_62;
		V_26 = L_63;
		NullCheck(L_61);
		Transform_set_localScale_m2325460848(L_61, L_63, /*hidden argument*/NULL);
		Vector3_t2243707580  L_64 = V_26;
		Transform_t3275118058 * L_65 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_65);
		Vector3_t2243707580  L_66 = Transform_get_position_m1104419803(L_65, /*hidden argument*/NULL);
		V_27 = L_66;
		float L_67 = (&V_27)->get_y_2();
		Transform_t3275118058 * L_68 = __this->get_MainCamera_8();
		NullCheck(L_68);
		Vector3_t2243707580  L_69 = Transform_get_position_m1104419803(L_68, /*hidden argument*/NULL);
		V_28 = L_69;
		float L_70 = (&V_28)->get_y_2();
		__this->set_xdistance_13(((float)((float)L_67-(float)L_70)));
		float L_71 = __this->get_xdistance_13();
		float L_72 = __this->get_ratioy_12();
		float L_73 = __this->get_ratiox_11();
		__this->set_newxdistance_15(((float)((float)L_71*(float)((float)((float)L_72/(float)L_73)))));
		Transform_t3275118058 * L_74 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_74);
		Vector3_t2243707580  L_75 = Transform_get_position_m1104419803(L_74, /*hidden argument*/NULL);
		V_29 = L_75;
		float L_76 = (&V_29)->get_y_2();
		float L_77 = __this->get_newxdistance_15();
		float L_78 = __this->get_xdistance_13();
		float L_79 = ((float)((float)L_76+(float)((float)((float)L_77-(float)L_78))));
		V_6 = L_79;
		Transform_t3275118058 * L_80 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_80);
		Vector3_t2243707580  L_81 = Transform_get_position_m1104419803(L_80, /*hidden argument*/NULL);
		Vector3_t2243707580  L_82 = L_81;
		V_7 = L_82;
		float L_83 = V_6;
		float L_84 = L_83;
		V_30 = L_84;
		(&V_7)->set_y_2(L_84);
		float L_85 = V_30;
		Transform_t3275118058 * L_86 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_87 = V_7;
		Vector3_t2243707580  L_88 = L_87;
		V_31 = L_88;
		NullCheck(L_86);
		Transform_set_position_m2469242620(L_86, L_88, /*hidden argument*/NULL);
		Vector3_t2243707580  L_89 = V_31;
	}

IL_0238:
	{
		bool L_90 = __this->get_ChangeXRotated_6();
		if (!L_90)
		{
			goto IL_033a;
		}
	}
	{
		Transform_t3275118058 * L_91 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_91);
		Vector3_t2243707580  L_92 = Transform_get_localScale_m3074381503(L_91, /*hidden argument*/NULL);
		V_32 = L_92;
		float L_93 = (&V_32)->get_y_2();
		float L_94 = __this->get_ratiox_11();
		float L_95 = __this->get_ratioy_12();
		float L_96 = ((float)((float)((float)((float)L_93*(float)L_94))/(float)L_95));
		V_8 = L_96;
		Transform_t3275118058 * L_97 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_97);
		Vector3_t2243707580  L_98 = Transform_get_localScale_m3074381503(L_97, /*hidden argument*/NULL);
		Vector3_t2243707580  L_99 = L_98;
		V_9 = L_99;
		float L_100 = V_8;
		float L_101 = L_100;
		V_33 = L_101;
		(&V_9)->set_y_2(L_101);
		float L_102 = V_33;
		Transform_t3275118058 * L_103 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_104 = V_9;
		Vector3_t2243707580  L_105 = L_104;
		V_34 = L_105;
		NullCheck(L_103);
		Transform_set_localScale_m2325460848(L_103, L_105, /*hidden argument*/NULL);
		Vector3_t2243707580  L_106 = V_34;
		Transform_t3275118058 * L_107 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_107);
		Vector3_t2243707580  L_108 = Transform_get_position_m1104419803(L_107, /*hidden argument*/NULL);
		V_35 = L_108;
		float L_109 = (&V_35)->get_x_1();
		Transform_t3275118058 * L_110 = __this->get_MainCamera_8();
		NullCheck(L_110);
		Vector3_t2243707580  L_111 = Transform_get_position_m1104419803(L_110, /*hidden argument*/NULL);
		V_36 = L_111;
		float L_112 = (&V_36)->get_x_1();
		__this->set_xdistance_13(((float)((float)L_109-(float)L_112)));
		float L_113 = __this->get_xdistance_13();
		float L_114 = __this->get_ratiox_11();
		float L_115 = __this->get_ratioy_12();
		__this->set_newxdistance_15(((float)((float)L_113*(float)((float)((float)L_114/(float)L_115)))));
		Transform_t3275118058 * L_116 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_116);
		Vector3_t2243707580  L_117 = Transform_get_position_m1104419803(L_116, /*hidden argument*/NULL);
		V_37 = L_117;
		float L_118 = (&V_37)->get_x_1();
		float L_119 = __this->get_newxdistance_15();
		float L_120 = __this->get_xdistance_13();
		float L_121 = ((float)((float)L_118+(float)((float)((float)L_119-(float)L_120))));
		V_10 = L_121;
		Transform_t3275118058 * L_122 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_122);
		Vector3_t2243707580  L_123 = Transform_get_position_m1104419803(L_122, /*hidden argument*/NULL);
		Vector3_t2243707580  L_124 = L_123;
		V_11 = L_124;
		float L_125 = V_10;
		float L_126 = L_125;
		V_38 = L_126;
		(&V_11)->set_x_1(L_126);
		float L_127 = V_38;
		Transform_t3275118058 * L_128 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_129 = V_11;
		Vector3_t2243707580  L_130 = L_129;
		V_39 = L_130;
		NullCheck(L_128);
		Transform_set_position_m2469242620(L_128, L_130, /*hidden argument*/NULL);
		Vector3_t2243707580  L_131 = V_39;
	}

IL_033a:
	{
		bool L_132 = __this->get_ChangeYRotated_7();
		if (!L_132)
		{
			goto IL_043c;
		}
	}
	{
		Transform_t3275118058 * L_133 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_133);
		Vector3_t2243707580  L_134 = Transform_get_localScale_m3074381503(L_133, /*hidden argument*/NULL);
		V_40 = L_134;
		float L_135 = (&V_40)->get_x_1();
		float L_136 = __this->get_ratioy_12();
		float L_137 = __this->get_ratiox_11();
		float L_138 = ((float)((float)((float)((float)L_135*(float)L_136))/(float)L_137));
		V_12 = L_138;
		Transform_t3275118058 * L_139 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_139);
		Vector3_t2243707580  L_140 = Transform_get_localScale_m3074381503(L_139, /*hidden argument*/NULL);
		Vector3_t2243707580  L_141 = L_140;
		V_13 = L_141;
		float L_142 = V_12;
		float L_143 = L_142;
		V_41 = L_143;
		(&V_13)->set_x_1(L_143);
		float L_144 = V_41;
		Transform_t3275118058 * L_145 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_146 = V_13;
		Vector3_t2243707580  L_147 = L_146;
		V_42 = L_147;
		NullCheck(L_145);
		Transform_set_localScale_m2325460848(L_145, L_147, /*hidden argument*/NULL);
		Vector3_t2243707580  L_148 = V_42;
		Transform_t3275118058 * L_149 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_149);
		Vector3_t2243707580  L_150 = Transform_get_position_m1104419803(L_149, /*hidden argument*/NULL);
		V_43 = L_150;
		float L_151 = (&V_43)->get_y_2();
		Transform_t3275118058 * L_152 = __this->get_MainCamera_8();
		NullCheck(L_152);
		Vector3_t2243707580  L_153 = Transform_get_position_m1104419803(L_152, /*hidden argument*/NULL);
		V_44 = L_153;
		float L_154 = (&V_44)->get_y_2();
		__this->set_xdistance_13(((float)((float)L_151-(float)L_154)));
		float L_155 = __this->get_xdistance_13();
		float L_156 = __this->get_ratioy_12();
		float L_157 = __this->get_ratiox_11();
		__this->set_newxdistance_15(((float)((float)L_155*(float)((float)((float)L_156/(float)L_157)))));
		Transform_t3275118058 * L_158 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_158);
		Vector3_t2243707580  L_159 = Transform_get_position_m1104419803(L_158, /*hidden argument*/NULL);
		V_45 = L_159;
		float L_160 = (&V_45)->get_y_2();
		float L_161 = __this->get_newxdistance_15();
		float L_162 = __this->get_xdistance_13();
		float L_163 = ((float)((float)L_160+(float)((float)((float)L_161-(float)L_162))));
		V_14 = L_163;
		Transform_t3275118058 * L_164 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_164);
		Vector3_t2243707580  L_165 = Transform_get_position_m1104419803(L_164, /*hidden argument*/NULL);
		Vector3_t2243707580  L_166 = L_165;
		V_15 = L_166;
		float L_167 = V_14;
		float L_168 = L_167;
		V_46 = L_168;
		(&V_15)->set_y_2(L_168);
		float L_169 = V_46;
		Transform_t3275118058 * L_170 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_171 = V_15;
		Vector3_t2243707580  L_172 = L_171;
		V_47 = L_172;
		NullCheck(L_170);
		Transform_set_position_m2469242620(L_170, L_172, /*hidden argument*/NULL);
		Vector3_t2243707580  L_173 = V_47;
	}

IL_043c:
	{
		return;
	}
}
// System.Void ResolutionFixer::Main()
extern "C"  void ResolutionFixer_Main_m974232635 (ResolutionFixer_t357239262 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
