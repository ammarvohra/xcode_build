﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrefabLooping
struct  PrefabLooping_t2427949168  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PrefabLooping::FXLoop
	GameObject_t1756533147 * ___FXLoop_2;
	// System.Single PrefabLooping::intervalMin
	float ___intervalMin_3;
	// System.Single PrefabLooping::intervalMax
	float ___intervalMax_4;
	// System.Single PrefabLooping::positionFactor
	float ___positionFactor_5;
	// System.Single PrefabLooping::killtime
	float ___killtime_6;

public:
	inline static int32_t get_offset_of_FXLoop_2() { return static_cast<int32_t>(offsetof(PrefabLooping_t2427949168, ___FXLoop_2)); }
	inline GameObject_t1756533147 * get_FXLoop_2() const { return ___FXLoop_2; }
	inline GameObject_t1756533147 ** get_address_of_FXLoop_2() { return &___FXLoop_2; }
	inline void set_FXLoop_2(GameObject_t1756533147 * value)
	{
		___FXLoop_2 = value;
		Il2CppCodeGenWriteBarrier(&___FXLoop_2, value);
	}

	inline static int32_t get_offset_of_intervalMin_3() { return static_cast<int32_t>(offsetof(PrefabLooping_t2427949168, ___intervalMin_3)); }
	inline float get_intervalMin_3() const { return ___intervalMin_3; }
	inline float* get_address_of_intervalMin_3() { return &___intervalMin_3; }
	inline void set_intervalMin_3(float value)
	{
		___intervalMin_3 = value;
	}

	inline static int32_t get_offset_of_intervalMax_4() { return static_cast<int32_t>(offsetof(PrefabLooping_t2427949168, ___intervalMax_4)); }
	inline float get_intervalMax_4() const { return ___intervalMax_4; }
	inline float* get_address_of_intervalMax_4() { return &___intervalMax_4; }
	inline void set_intervalMax_4(float value)
	{
		___intervalMax_4 = value;
	}

	inline static int32_t get_offset_of_positionFactor_5() { return static_cast<int32_t>(offsetof(PrefabLooping_t2427949168, ___positionFactor_5)); }
	inline float get_positionFactor_5() const { return ___positionFactor_5; }
	inline float* get_address_of_positionFactor_5() { return &___positionFactor_5; }
	inline void set_positionFactor_5(float value)
	{
		___positionFactor_5 = value;
	}

	inline static int32_t get_offset_of_killtime_6() { return static_cast<int32_t>(offsetof(PrefabLooping_t2427949168, ___killtime_6)); }
	inline float get_killtime_6() const { return ___killtime_6; }
	inline float* get_address_of_killtime_6() { return &___killtime_6; }
	inline void set_killtime_6(float value)
	{
		___killtime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
