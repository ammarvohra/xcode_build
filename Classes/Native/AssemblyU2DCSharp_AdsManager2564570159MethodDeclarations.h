﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdsManager
struct AdsManager_t2564570159;

#include "codegen/il2cpp-codegen.h"

// System.Void AdsManager::.ctor()
extern "C"  void AdsManager__ctor_m4144563992 (AdsManager_t2564570159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::Start()
extern "C"  void AdsManager_Start_m1299725372 (AdsManager_t2564570159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::RequestAds()
extern "C"  void AdsManager_RequestAds_m2040783761 (AdsManager_t2564570159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::RequestBanner()
extern "C"  void AdsManager_RequestBanner_m1456507415 (AdsManager_t2564570159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::RequestInterstitial()
extern "C"  void AdsManager_RequestInterstitial_m2248702057 (AdsManager_t2564570159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::ShowInter()
extern "C"  void AdsManager_ShowInter_m1660539497 (AdsManager_t2564570159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdsManager::_isInterstitialLoaded()
extern "C"  bool AdsManager__isInterstitialLoaded_m511819348 (AdsManager_t2564570159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::ShowChartboostAdsNet()
extern "C"  void AdsManager_ShowChartboostAdsNet_m3982552325 (AdsManager_t2564570159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
