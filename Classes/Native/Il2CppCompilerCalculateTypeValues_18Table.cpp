﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CFX_AutoRotate1831446564.h"
#include "AssemblyU2DCSharp_CFX_AutodestructWhenNoChildren3972162727.h"
#include "AssemblyU2DCSharp_CFX_InspectorHelp3280468206.h"
#include "AssemblyU2DCSharp_CFX_LightIntensityFade4221734619.h"
#include "AssemblyU2DCSharp_CFX_ShurikenThreadFix3286853382.h"
#include "AssemblyU2DCSharp_CFX_ShurikenThreadFix_U3CWaitFram410180078.h"
#include "AssemblyU2DCSharp_CFX_SpawnSystem3600628354.h"
#include "AssemblyU2DCSharp_AdsManager2564570159.h"
#include "AssemblyU2DCSharp_BathManager1712132540.h"
#include "AssemblyU2DCSharp_BathManager_U3CInitiateBubblesU32083865512.h"
#include "AssemblyU2DCSharp_BathManager_U3CInitiateWaterDrop3701969395.h"
#include "AssemblyU2DCSharp_BathManager_U3CInitiateStarU3Ec__998597087.h"
#include "AssemblyU2DCSharp_BathManager_U3CLoadNextLevelU3Ec2707564528.h"
#include "AssemblyU2DCSharp_BubbleAnimationHomeScreen878545717.h"
#include "AssemblyU2DCSharp_BubbleTap1235356101.h"
#include "AssemblyU2DCSharp_BubbleTap_U3CWaitForDestroyU3Ec_4043257003.h"
#include "AssemblyU2DCSharp_BubbleWaterTalkMoving3963847219.h"
#include "AssemblyU2DCSharp_ButtonManager868394943.h"
#include "AssemblyU2DCSharp_ButtonManager_U3CStartU3Ec__Iter4248045775.h"
#include "AssemblyU2DCSharp_ButtonManager_U3CPlayButtonUpU3E1133289621.h"
#include "AssemblyU2DCSharp_ButtonManager_U3CFbLikeButtonUpU3384855249.h"
#include "AssemblyU2DCSharp_DestroyOrDeactivateAtFixedTime2027803675.h"
#include "AssemblyU2DCSharp_DestroyOrDeactivateAtFixedTime_F1161615032.h"
#include "AssemblyU2DCSharp_DestroyOrDeactivateAtFixedTime_U3291299954.h"
#include "AssemblyU2DCSharp_GameManager2252321495.h"
#include "AssemblyU2DCSharp_GameManager_U3CInitialBathAnimAn1431675056.h"
#include "AssemblyU2DCSharp_GameManager_U3CMakeCurtainTassel2568235234.h"
#include "AssemblyU2DCSharp_HandLegAnim1741127542.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager1686788969.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CStartU33126961537.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeBig2709881044.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CNailCut2399846568.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeNai1756903693.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CPrepareF599836482.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CDeleteS3757503118.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeCur3508152083.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CLoadNex3146601861.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CMakeSty3184490244.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CFbLikeB1718721216.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CRestart2431284672.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CTakeSna2011450030.h"
#include "AssemblyU2DCSharp_KittyHandNFootManager_U3CShareSc3190744391.h"
#include "AssemblyU2DCSharp_LiveVideo867630853.h"
#include "AssemblyU2DCSharp_RatePanel560543380.h"
#include "AssemblyU2DCSharp_SoundManager654432262.h"
#include "AssemblyU2DCSharp_SoundManager_U3CCloseMouthU3Ec__2695835239.h"
#include "AssemblyU2DCSharp_SoundManager_U3CDestroyBubbleSou1692068612.h"
#include "AssemblyU2DCSharp_StyleManager3102803280.h"
#include "AssemblyU2DCSharp_StyleManager_U3CStartU3Ec__Itera3971177230.h"
#include "AssemblyU2DCSharp_ToolButton3848681854.h"
#include "AssemblyU2DCSharp_ToolButton_ButtonType1054768361.h"
#include "AssemblyU2DCSharp_ToolButton_U3CButtonBackOnPositi3433254669.h"
#include "AssemblyU2DCSharp_TouchOnStar2108028178.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_ObjectSpin853925131.h"
#include "AssemblyU2DUnityScript_PrefabLooping2427949168.h"
#include "AssemblyU2DUnityScript_ResolutionFixer357239262.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (CFX_AutoRotate_t1831446564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[2] = 
{
	CFX_AutoRotate_t1831446564::get_offset_of_rotation_2(),
	CFX_AutoRotate_t1831446564::get_offset_of_space_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (CFX_AutodestructWhenNoChildren_t3972162727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (CFX_InspectorHelp_t3280468206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[4] = 
{
	CFX_InspectorHelp_t3280468206::get_offset_of_Locked_2(),
	CFX_InspectorHelp_t3280468206::get_offset_of_Title_3(),
	CFX_InspectorHelp_t3280468206::get_offset_of_HelpText_4(),
	CFX_InspectorHelp_t3280468206::get_offset_of_MsgType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (CFX_LightIntensityFade_t4221734619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[7] = 
{
	CFX_LightIntensityFade_t4221734619::get_offset_of_duration_2(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_delay_3(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_finalIntensity_4(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_baseIntensity_5(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_autodestruct_6(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_p_lifetime_7(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_p_delay_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (CFX_ShurikenThreadFix_t3286853382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[1] = 
{
	CFX_ShurikenThreadFix_t3286853382::get_offset_of_systems_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (U3CWaitFrameU3Ec__Iterator0_t410180078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[6] = 
{
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24locvar0_0(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24locvar1_1(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24this_2(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24current_3(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24disposing_4(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (CFX_SpawnSystem_t3600628354), -1, sizeof(CFX_SpawnSystem_t3600628354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1806[7] = 
{
	CFX_SpawnSystem_t3600628354_StaticFields::get_offset_of_instance_2(),
	CFX_SpawnSystem_t3600628354::get_offset_of_objectsToPreload_3(),
	CFX_SpawnSystem_t3600628354::get_offset_of_objectsToPreloadTimes_4(),
	CFX_SpawnSystem_t3600628354::get_offset_of_hideObjectsInHierarchy_5(),
	CFX_SpawnSystem_t3600628354::get_offset_of_allObjectsLoaded_6(),
	CFX_SpawnSystem_t3600628354::get_offset_of_instantiatedObjects_7(),
	CFX_SpawnSystem_t3600628354::get_offset_of_poolCursors_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (AdsManager_t2564570159), -1, sizeof(AdsManager_t2564570159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1807[10] = 
{
	AdsManager_t2564570159_StaticFields::get_offset_of_instance_2(),
	AdsManager_t2564570159::get_offset_of_interstitial_3(),
	AdsManager_t2564570159::get_offset_of_rewardBasedVideo_4(),
	AdsManager_t2564570159::get_offset_of_AndroidSmartBannerID_5(),
	AdsManager_t2564570159::get_offset_of_AndroidInterstitialID_6(),
	AdsManager_t2564570159::get_offset_of_iosSmartBannerID_7(),
	AdsManager_t2564570159::get_offset_of_iosInterstitialID_8(),
	AdsManager_t2564570159::get_offset_of_GameOverCount_9(),
	AdsManager_t2564570159::get_offset_of_ShowAdsOnXCount_10(),
	AdsManager_t2564570159::get_offset_of_count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (BathManager_t1712132540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[21] = 
{
	BathManager_t1712132540::get_offset_of_Soap_2(),
	BathManager_t1712132540::get_offset_of_Shower_3(),
	BathManager_t1712132540::get_offset_of_Towel_4(),
	BathManager_t1712132540::get_offset_of_Dryer_5(),
	BathManager_t1712132540::get_offset_of_Bubble_6(),
	BathManager_t1712132540::get_offset_of_BubbleOnKitty_7(),
	BathManager_t1712132540::get_offset_of_WaterDrop_8(),
	BathManager_t1712132540::get_offset_of_BigWaterDrop_9(),
	BathManager_t1712132540::get_offset_of_ShowerEffects_10(),
	BathManager_t1712132540::get_offset_of_Smoke_11(),
	BathManager_t1712132540::get_offset_of_Star_12(),
	BathManager_t1712132540::get_offset_of_CountTimer_13(),
	BathManager_t1712132540::get_offset_of_Curtain_14(),
	BathManager_t1712132540::get_offset_of_LocketStar_15(),
	BathManager_t1712132540::get_offset_of_CurtainTassel_16(),
	BathManager_t1712132540::get_offset_of_BubbleInitiateFlag_17(),
	BathManager_t1712132540::get_offset_of_WaterDropInitiateFlag_18(),
	BathManager_t1712132540::get_offset_of_StarInitiateFlag_19(),
	BathManager_t1712132540::get_offset_of_OnShowerFirstTime_20(),
	BathManager_t1712132540::get_offset_of_OnTowelFirstTime_21(),
	BathManager_t1712132540::get_offset_of_OnDryerFirstTime_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (U3CInitiateBubblesU3Ec__Iterator0_t2083865512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[5] = 
{
	U3CInitiateBubblesU3Ec__Iterator0_t2083865512::get_offset_of_U3CBubbleRoamingU3E__0_0(),
	U3CInitiateBubblesU3Ec__Iterator0_t2083865512::get_offset_of_U24this_1(),
	U3CInitiateBubblesU3Ec__Iterator0_t2083865512::get_offset_of_U24current_2(),
	U3CInitiateBubblesU3Ec__Iterator0_t2083865512::get_offset_of_U24disposing_3(),
	U3CInitiateBubblesU3Ec__Iterator0_t2083865512::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[5] = 
{
	U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395::get_offset_of_U3CWaterDropObjU3E__0_0(),
	U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395::get_offset_of_U24this_1(),
	U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395::get_offset_of_U24current_2(),
	U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395::get_offset_of_U24disposing_3(),
	U3CInitiateWaterDropsU3Ec__Iterator1_t3701969395::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (U3CInitiateStarU3Ec__Iterator2_t998597087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[5] = 
{
	U3CInitiateStarU3Ec__Iterator2_t998597087::get_offset_of_U3CStarObjU3E__0_0(),
	U3CInitiateStarU3Ec__Iterator2_t998597087::get_offset_of_U24this_1(),
	U3CInitiateStarU3Ec__Iterator2_t998597087::get_offset_of_U24current_2(),
	U3CInitiateStarU3Ec__Iterator2_t998597087::get_offset_of_U24disposing_3(),
	U3CInitiateStarU3Ec__Iterator2_t998597087::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (U3CLoadNextLevelU3Ec__Iterator3_t2707564528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[3] = 
{
	U3CLoadNextLevelU3Ec__Iterator3_t2707564528::get_offset_of_U24current_0(),
	U3CLoadNextLevelU3Ec__Iterator3_t2707564528::get_offset_of_U24disposing_1(),
	U3CLoadNextLevelU3Ec__Iterator3_t2707564528::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (BubbleAnimationHomeScreen_t878545717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[1] = 
{
	BubbleAnimationHomeScreen_t878545717::get_offset_of_Bubbles_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (BubbleTap_t1235356101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[1] = 
{
	BubbleTap_t1235356101::get_offset_of_Star_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (U3CWaitForDestroyU3Ec__Iterator0_t4043257003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	U3CWaitForDestroyU3Ec__Iterator0_t4043257003::get_offset_of_U24this_0(),
	U3CWaitForDestroyU3Ec__Iterator0_t4043257003::get_offset_of_U24current_1(),
	U3CWaitForDestroyU3Ec__Iterator0_t4043257003::get_offset_of_U24disposing_2(),
	U3CWaitForDestroyU3Ec__Iterator0_t4043257003::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (BubbleWaterTalkMoving_t3963847219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (ButtonManager_t868394943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[5] = 
{
	ButtonManager_t868394943::get_offset_of_PlayButton_2(),
	ButtonManager_t868394943::get_offset_of_FbLikeButton_3(),
	ButtonManager_t868394943::get_offset_of_MuteButton_4(),
	ButtonManager_t868394943::get_offset_of_SoundOn_5(),
	ButtonManager_t868394943::get_offset_of_SoundOff_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (U3CStartU3Ec__Iterator0_t4248045775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	U3CStartU3Ec__Iterator0_t4248045775::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t4248045775::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t4248045775::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t4248045775::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (U3CPlayButtonUpU3Ec__Iterator1_t1133289621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[4] = 
{
	U3CPlayButtonUpU3Ec__Iterator1_t1133289621::get_offset_of_U24this_0(),
	U3CPlayButtonUpU3Ec__Iterator1_t1133289621::get_offset_of_U24current_1(),
	U3CPlayButtonUpU3Ec__Iterator1_t1133289621::get_offset_of_U24disposing_2(),
	U3CPlayButtonUpU3Ec__Iterator1_t1133289621::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (U3CFbLikeButtonUpU3Ec__Iterator2_t384855249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[4] = 
{
	U3CFbLikeButtonUpU3Ec__Iterator2_t384855249::get_offset_of_U24this_0(),
	U3CFbLikeButtonUpU3Ec__Iterator2_t384855249::get_offset_of_U24current_1(),
	U3CFbLikeButtonUpU3Ec__Iterator2_t384855249::get_offset_of_U24disposing_2(),
	U3CFbLikeButtonUpU3Ec__Iterator2_t384855249::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (DestroyOrDeactivateAtFixedTime_t2027803675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[4] = 
{
	DestroyOrDeactivateAtFixedTime_t2027803675::get_offset_of_function_2(),
	DestroyOrDeactivateAtFixedTime_t2027803675::get_offset_of_WaitingTime_3(),
	DestroyOrDeactivateAtFixedTime_t2027803675::get_offset_of_Target_4(),
	DestroyOrDeactivateAtFixedTime_t2027803675::get_offset_of_ComponentToDeactive_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (Function_t1161615032)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1822[4] = 
{
	Function_t1161615032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[5] = 
{
	U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954::get_offset_of_U24locvar0_0(),
	U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954::get_offset_of_U24this_1(),
	U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954::get_offset_of_U24current_2(),
	U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954::get_offset_of_U24disposing_3(),
	U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (GameManager_t2252321495), -1, sizeof(GameManager_t2252321495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1824[17] = 
{
	GameManager_t2252321495::get_offset_of_BathCleaningToolsParent_2(),
	GameManager_t2252321495::get_offset_of_BathCleaningAnimTimer_3(),
	GameManager_t2252321495::get_offset_of_Soap_4(),
	GameManager_t2252321495::get_offset_of_Shower_5(),
	GameManager_t2252321495::get_offset_of_Towel_6(),
	GameManager_t2252321495::get_offset_of_Dryer_7(),
	GameManager_t2252321495::get_offset_of_KittyClean_8(),
	GameManager_t2252321495::get_offset_of_KittyMud_9(),
	GameManager_t2252321495::get_offset_of_KittyBubble_10(),
	GameManager_t2252321495::get_offset_of_KittyWater_11(),
	GameManager_t2252321495::get_offset_of_MaskKittyWater_12(),
	GameManager_t2252321495::get_offset_of_MaskKittyClean_13(),
	GameManager_t2252321495::get_offset_of_MaskKittyMud_14(),
	GameManager_t2252321495::get_offset_of_MaskKittyBubble_15(),
	GameManager_t2252321495::get_offset_of_CurtainTassel_16(),
	GameManager_t2252321495::get_offset_of_CurtainTop_17(),
	GameManager_t2252321495_StaticFields::get_offset_of_instance_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[4] = 
{
	U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056::get_offset_of_U24this_0(),
	U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056::get_offset_of_U24current_1(),
	U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056::get_offset_of_U24disposing_2(),
	U3CInitialBathAnimAndSoapAvailableU3Ec__Iterator0_t1431675056::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[3] = 
{
	U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234::get_offset_of_U24current_0(),
	U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234::get_offset_of_U24disposing_1(),
	U3CMakeCurtainTasselAnimOffU3Ec__Iterator1_t2568235234::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (HandLegAnim_t1741127542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[1] = 
{
	HandLegAnim_t1741127542::get_offset_of_InitialScale_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (KittyHandNFootManager_t1686788969), -1, sizeof(KittyHandNFootManager_t1686788969_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1828[68] = 
{
	KittyHandNFootManager_t1686788969::get_offset_of_Talk_2(),
	KittyHandNFootManager_t1686788969::get_offset_of_Talk_Position_3(),
	KittyHandNFootManager_t1686788969::get_offset_of_Colors_4(),
	KittyHandNFootManager_t1686788969::get_offset_of_LeftLegCollider_5(),
	KittyHandNFootManager_t1686788969::get_offset_of_RightLegCollider_6(),
	KittyHandNFootManager_t1686788969::get_offset_of_LeftHandCollider_7(),
	KittyHandNFootManager_t1686788969::get_offset_of_RightHandCollider_8(),
	KittyHandNFootManager_t1686788969::get_offset_of_NormalKitty_9(),
	KittyHandNFootManager_t1686788969::get_offset_of_EyeAnim_10(),
	KittyHandNFootManager_t1686788969::get_offset_of_HandIcon_11(),
	KittyHandNFootManager_t1686788969::get_offset_of_HandIconTap_12(),
	KittyHandNFootManager_t1686788969::get_offset_of_NailCutter_13(),
	KittyHandNFootManager_t1686788969::get_offset_of_NailPolishIcon_14(),
	KittyHandNFootManager_t1686788969::get_offset_of_FacePowderMirror_15(),
	KittyHandNFootManager_t1686788969::get_offset_of_FacePowderBrush_16(),
	KittyHandNFootManager_t1686788969::get_offset_of_Perfume_17(),
	KittyHandNFootManager_t1686788969::get_offset_of_NailPolishPanel_18(),
	KittyHandNFootManager_t1686788969::get_offset_of_CutterLeftHandPos_19(),
	KittyHandNFootManager_t1686788969::get_offset_of_CutterRightHandPos_20(),
	KittyHandNFootManager_t1686788969::get_offset_of_CutterLeftLegPos_21(),
	KittyHandNFootManager_t1686788969::get_offset_of_CutterRightLegPos_22(),
	KittyHandNFootManager_t1686788969::get_offset_of_CutterLeftHandRot_23(),
	KittyHandNFootManager_t1686788969::get_offset_of_CutterRightHandRot_24(),
	KittyHandNFootManager_t1686788969::get_offset_of_CutterLeftLegRot_25(),
	KittyHandNFootManager_t1686788969::get_offset_of_CutterRightLegRot_26(),
	KittyHandNFootManager_t1686788969::get_offset_of_NailPolishObjects_27(),
	KittyHandNFootManager_t1686788969::get_offset_of_NailPolishColors_28(),
	KittyHandNFootManager_t1686788969::get_offset_of_HandTapPos_29(),
	KittyHandNFootManager_t1686788969::get_offset_of_WhiteNails_30(),
	KittyHandNFootManager_t1686788969::get_offset_of_FaceGlow_31(),
	KittyHandNFootManager_t1686788969::get_offset_of_Star_32(),
	KittyHandNFootManager_t1686788969::get_offset_of_PerfumeSpray_33(),
	KittyHandNFootManager_t1686788969::get_offset_of_PowderEffect_34(),
	KittyHandNFootManager_t1686788969::get_offset_of_LocketStar_35(),
	KittyHandNFootManager_t1686788969::get_offset_of_CurtainTassel_36(),
	KittyHandNFootManager_t1686788969::get_offset_of_CurtainClose_37(),
	KittyHandNFootManager_t1686788969::get_offset_of_CurtainOpen_38(),
	KittyHandNFootManager_t1686788969::get_offset_of_StylePanel_39(),
	KittyHandNFootManager_t1686788969::get_offset_of_GameOverPanel_40(),
	KittyHandNFootManager_t1686788969::get_offset_of_FbLikeButton_41(),
	KittyHandNFootManager_t1686788969::get_offset_of_RestartButton_42(),
	KittyHandNFootManager_t1686788969::get_offset_of_RateButton_43(),
	KittyHandNFootManager_t1686788969::get_offset_of_RatePanel_44(),
	KittyHandNFootManager_t1686788969::get_offset_of_CurtainTop_45(),
	KittyHandNFootManager_t1686788969::get_offset_of_StyleManager_46(),
	KittyHandNFootManager_t1686788969::get_offset_of_DoneButton_47(),
	KittyHandNFootManager_t1686788969::get_offset_of_LastPanel_48(),
	KittyHandNFootManager_t1686788969::get_offset_of_SnapTransparentBG_49(),
	KittyHandNFootManager_t1686788969::get_offset_of_GameEndAnim_50(),
	KittyHandNFootManager_t1686788969::get_offset_of_NailPolishType_51(),
	KittyHandNFootManager_t1686788969_StaticFields::get_offset_of_instance_52(),
	KittyHandNFootManager_t1686788969::get_offset_of_LeftHandCollide_53(),
	KittyHandNFootManager_t1686788969::get_offset_of_RightHandCollide_54(),
	KittyHandNFootManager_t1686788969::get_offset_of_LeftLegCollide_55(),
	KittyHandNFootManager_t1686788969::get_offset_of_RightLegCollide_56(),
	KittyHandNFootManager_t1686788969::get_offset_of_CutterInitialPos_57(),
	KittyHandNFootManager_t1686788969::get_offset_of_Talk1_58(),
	KittyHandNFootManager_t1686788969::get_offset_of_Talk2_59(),
	KittyHandNFootManager_t1686788969::get_offset_of_CurrentHand_60(),
	KittyHandNFootManager_t1686788969::get_offset_of_InitialHandScale_61(),
	KittyHandNFootManager_t1686788969::get_offset_of_LeftHandPolishModeOnly_62(),
	KittyHandNFootManager_t1686788969::get_offset_of_RightHandPolishModeOnly_63(),
	KittyHandNFootManager_t1686788969::get_offset_of_LeftLegPolishModeOnly_64(),
	KittyHandNFootManager_t1686788969::get_offset_of_RightLegPolishModeOnly_65(),
	KittyHandNFootManager_t1686788969::get_offset_of_StopTouchOnHand_66(),
	KittyHandNFootManager_t1686788969::get_offset_of_ManipulateRightHand1stNail_67(),
	KittyHandNFootManager_t1686788969::get_offset_of_isProcessing_68(),
	KittyHandNFootManager_t1686788969::get_offset_of_destination_69(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (U3CStartU3Ec__Iterator0_t3126961537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[4] = 
{
	U3CStartU3Ec__Iterator0_t3126961537::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3126961537::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3126961537::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3126961537::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (U3CMakeBiggerU3Ec__Iterator1_t2709881044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[5] = 
{
	U3CMakeBiggerU3Ec__Iterator1_t2709881044::get_offset_of_Obj_0(),
	U3CMakeBiggerU3Ec__Iterator1_t2709881044::get_offset_of_U24this_1(),
	U3CMakeBiggerU3Ec__Iterator1_t2709881044::get_offset_of_U24current_2(),
	U3CMakeBiggerU3Ec__Iterator1_t2709881044::get_offset_of_U24disposing_3(),
	U3CMakeBiggerU3Ec__Iterator1_t2709881044::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[5] = 
{
	U3CNailCuttingAnimU3Ec__Iterator2_t2399846568::get_offset_of_Obj_0(),
	U3CNailCuttingAnimU3Ec__Iterator2_t2399846568::get_offset_of_U24this_1(),
	U3CNailCuttingAnimU3Ec__Iterator2_t2399846568::get_offset_of_U24current_2(),
	U3CNailCuttingAnimU3Ec__Iterator2_t2399846568::get_offset_of_U24disposing_3(),
	U3CNailCuttingAnimU3Ec__Iterator2_t2399846568::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[3] = 
{
	U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693::get_offset_of_U24current_0(),
	U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693::get_offset_of_U24disposing_1(),
	U3CMakeNailPolishIconAvailableU3Ec__Iterator3_t1756903693::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[8] = 
{
	U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482::get_offset_of_U3CStarObjU3E__0_0(),
	U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482::get_offset_of_U3CStarObj1U3E__1_1(),
	U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482::get_offset_of_U3CStarObj2U3E__2_2(),
	U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482::get_offset_of_U3CTargetHandU3E__3_3(),
	U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482::get_offset_of_U24this_4(),
	U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482::get_offset_of_U24current_5(),
	U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482::get_offset_of_U24disposing_6(),
	U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (U3CDeleteSprayU3Ec__Iterator5_t3757503118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[4] = 
{
	U3CDeleteSprayU3Ec__Iterator5_t3757503118::get_offset_of_Spray_0(),
	U3CDeleteSprayU3Ec__Iterator5_t3757503118::get_offset_of_U24current_1(),
	U3CDeleteSprayU3Ec__Iterator5_t3757503118::get_offset_of_U24disposing_2(),
	U3CDeleteSprayU3Ec__Iterator5_t3757503118::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[3] = 
{
	U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083::get_offset_of_U24current_0(),
	U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083::get_offset_of_U24disposing_1(),
	U3CMakeCurtainTasselAnimOffU3Ec__Iterator6_t3508152083::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (U3CLoadNextLevelU3Ec__Iterator7_t3146601861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1836[4] = 
{
	U3CLoadNextLevelU3Ec__Iterator7_t3146601861::get_offset_of_U24this_0(),
	U3CLoadNextLevelU3Ec__Iterator7_t3146601861::get_offset_of_U24current_1(),
	U3CLoadNextLevelU3Ec__Iterator7_t3146601861::get_offset_of_U24disposing_2(),
	U3CLoadNextLevelU3Ec__Iterator7_t3146601861::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[4] = 
{
	U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244::get_offset_of_U24this_0(),
	U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244::get_offset_of_U24current_1(),
	U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244::get_offset_of_U24disposing_2(),
	U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[4] = 
{
	U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216::get_offset_of_U24this_0(),
	U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216::get_offset_of_U24current_1(),
	U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216::get_offset_of_U24disposing_2(),
	U3CFbLikeButtonUpU3Ec__Iterator9_t1718721216::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (U3CRestartButtonUpU3Ec__IteratorA_t2431284672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[4] = 
{
	U3CRestartButtonUpU3Ec__IteratorA_t2431284672::get_offset_of_U24this_0(),
	U3CRestartButtonUpU3Ec__IteratorA_t2431284672::get_offset_of_U24current_1(),
	U3CRestartButtonUpU3Ec__IteratorA_t2431284672::get_offset_of_U24disposing_2(),
	U3CRestartButtonUpU3Ec__IteratorA_t2431284672::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (U3CTakeSnapU3Ec__IteratorB_t2011450030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[6] = 
{
	U3CTakeSnapU3Ec__IteratorB_t2011450030::get_offset_of_U3CscreenTextureU3E__0_0(),
	U3CTakeSnapU3Ec__IteratorB_t2011450030::get_offset_of_U3CdataToSaveU3E__1_1(),
	U3CTakeSnapU3Ec__IteratorB_t2011450030::get_offset_of_U24this_2(),
	U3CTakeSnapU3Ec__IteratorB_t2011450030::get_offset_of_U24current_3(),
	U3CTakeSnapU3Ec__IteratorB_t2011450030::get_offset_of_U24disposing_4(),
	U3CTakeSnapU3Ec__IteratorB_t2011450030::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (U3CShareScreenshotU3Ec__IteratorC_t3190744391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[6] = 
{
	U3CShareScreenshotU3Ec__IteratorC_t3190744391::get_offset_of_U3CscreenTextureU3E__0_0(),
	U3CShareScreenshotU3Ec__IteratorC_t3190744391::get_offset_of_U3CdataToSaveU3E__1_1(),
	U3CShareScreenshotU3Ec__IteratorC_t3190744391::get_offset_of_U24this_2(),
	U3CShareScreenshotU3Ec__IteratorC_t3190744391::get_offset_of_U24current_3(),
	U3CShareScreenshotU3Ec__IteratorC_t3190744391::get_offset_of_U24disposing_4(),
	U3CShareScreenshotU3Ec__IteratorC_t3190744391::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (LiveVideo_t867630853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[2] = 
{
	LiveVideo_t867630853::get_offset_of_Obj_2(),
	LiveVideo_t867630853::get_offset_of_Obj1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (RatePanel_t560543380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[14] = 
{
	RatePanel_t560543380::get_offset_of_RateContactButton_2(),
	RatePanel_t560543380::get_offset_of_Rate_3(),
	RatePanel_t560543380::get_offset_of_Contact_4(),
	RatePanel_t560543380::get_offset_of_Star1_5(),
	RatePanel_t560543380::get_offset_of_Star2_6(),
	RatePanel_t560543380::get_offset_of_Star3_7(),
	RatePanel_t560543380::get_offset_of_Star4_8(),
	RatePanel_t560543380::get_offset_of_Star5_9(),
	RatePanel_t560543380::get_offset_of_Close_10(),
	RatePanel_t560543380::get_offset_of_FillStar_11(),
	RatePanel_t560543380::get_offset_of_BlankStar_12(),
	RatePanel_t560543380::get_offset_of_FillStarY_13(),
	RatePanel_t560543380::get_offset_of_BlankStarY_14(),
	RatePanel_t560543380::get_offset_of_SelectedStar_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (SoundManager_t654432262), -1, sizeof(SoundManager_t654432262_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1844[21] = 
{
	SoundManager_t654432262::get_offset_of_OpenMouth_2(),
	SoundManager_t654432262::get_offset_of_MeowSource_3(),
	SoundManager_t654432262::get_offset_of_BubbleBlastSource_4(),
	SoundManager_t654432262::get_offset_of_MainMusic_5(),
	SoundManager_t654432262::get_offset_of_ButtonSource_6(),
	SoundManager_t654432262::get_offset_of_SoapBubbleSound_7(),
	SoundManager_t654432262::get_offset_of_ShowerSound_8(),
	SoundManager_t654432262::get_offset_of_TowelSound_9(),
	SoundManager_t654432262::get_offset_of_DryerSound_10(),
	SoundManager_t654432262::get_offset_of_NailCutterSound_11(),
	SoundManager_t654432262::get_offset_of_PerfumeSound_12(),
	SoundManager_t654432262::get_offset_of_ClickSound_13(),
	SoundManager_t654432262::get_offset_of_LastSound_14(),
	SoundManager_t654432262::get_offset_of_PlaySound_15(),
	SoundManager_t654432262::get_offset_of_CanPlayMeow_16(),
	SoundManager_t654432262::get_offset_of_MeowClips_17(),
	SoundManager_t654432262_StaticFields::get_offset_of_instance_18(),
	SoundManager_t654432262::get_offset_of_MouthOpenPosition_19(),
	SoundManager_t654432262::get_offset_of_MouthOpenScale_20(),
	SoundManager_t654432262::get_offset_of_Order_21(),
	SoundManager_t654432262::get_offset_of_count_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (U3CCloseMouthU3Ec__Iterator0_t2695835239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[4] = 
{
	U3CCloseMouthU3Ec__Iterator0_t2695835239::get_offset_of_U24this_0(),
	U3CCloseMouthU3Ec__Iterator0_t2695835239::get_offset_of_U24current_1(),
	U3CCloseMouthU3Ec__Iterator0_t2695835239::get_offset_of_U24disposing_2(),
	U3CCloseMouthU3Ec__Iterator0_t2695835239::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[4] = 
{
	U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612::get_offset_of_Bubble_0(),
	U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612::get_offset_of_U24current_1(),
	U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612::get_offset_of_U24disposing_2(),
	U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (StyleManager_t3102803280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[16] = 
{
	StyleManager_t3102803280::get_offset_of_HatHolder_2(),
	StyleManager_t3102803280::get_offset_of_HatObjs_3(),
	StyleManager_t3102803280::get_offset_of_HatFirstTime_4(),
	StyleManager_t3102803280::get_offset_of_GogglesHolder_5(),
	StyleManager_t3102803280::get_offset_of_GogglesObjs_6(),
	StyleManager_t3102803280::get_offset_of_GogglesFirstTime_7(),
	StyleManager_t3102803280::get_offset_of_PendantHolder_8(),
	StyleManager_t3102803280::get_offset_of_PendantObjs_9(),
	StyleManager_t3102803280::get_offset_of_PendantFirstTime_10(),
	StyleManager_t3102803280::get_offset_of_HatIcon_11(),
	StyleManager_t3102803280::get_offset_of_GogglesIcon_12(),
	StyleManager_t3102803280::get_offset_of_PendantIcon_13(),
	StyleManager_t3102803280::get_offset_of_GameOverPanel_14(),
	StyleManager_t3102803280::get_offset_of_CurrentHatIndex_15(),
	StyleManager_t3102803280::get_offset_of_CurrentGogglesIndex_16(),
	StyleManager_t3102803280::get_offset_of_CurrentPendantIndex_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (U3CStartU3Ec__Iterator0_t3971177230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[4] = 
{
	U3CStartU3Ec__Iterator0_t3971177230::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3971177230::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3971177230::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3971177230::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (ToolButton_t3848681854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[7] = 
{
	ToolButton_t3848681854::get_offset_of_buttonType_2(),
	ToolButton_t3848681854::get_offset_of_OnClick_3(),
	ToolButton_t3848681854::get_offset_of_Draggable_4(),
	ToolButton_t3848681854::get_offset_of_DragPosOffset_5(),
	ToolButton_t3848681854::get_offset_of_OnCollision_6(),
	ToolButton_t3848681854::get_offset_of_InitialPos_7(),
	ToolButton_t3848681854::get_offset_of_MousePositionOffsetZ_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (ButtonType_t1054768361)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1850[29] = 
{
	ButtonType_t1054768361::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[4] = 
{
	U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669::get_offset_of_U24this_0(),
	U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669::get_offset_of_U24current_1(),
	U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669::get_offset_of_U24disposing_2(),
	U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (TouchOnStar_t2108028178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[1] = 
{
	TouchOnStar_t2108028178::get_offset_of_OnClick_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (ObjectSpin_t853925131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[3] = 
{
	ObjectSpin_t853925131::get_offset_of_DegreesX_2(),
	ObjectSpin_t853925131::get_offset_of_DegreesY_3(),
	ObjectSpin_t853925131::get_offset_of_DegreesZ_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (PrefabLooping_t2427949168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[5] = 
{
	PrefabLooping_t2427949168::get_offset_of_FXLoop_2(),
	PrefabLooping_t2427949168::get_offset_of_intervalMin_3(),
	PrefabLooping_t2427949168::get_offset_of_intervalMax_4(),
	PrefabLooping_t2427949168::get_offset_of_positionFactor_5(),
	PrefabLooping_t2427949168::get_offset_of_killtime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (ResolutionFixer_t357239262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[14] = 
{
	ResolutionFixer_t357239262::get_offset_of_WorkingXorRatio_2(),
	ResolutionFixer_t357239262::get_offset_of_WorkingYorRatio_3(),
	ResolutionFixer_t357239262::get_offset_of_ChangeX_4(),
	ResolutionFixer_t357239262::get_offset_of_ChangeY_5(),
	ResolutionFixer_t357239262::get_offset_of_ChangeXRotated_6(),
	ResolutionFixer_t357239262::get_offset_of_ChangeYRotated_7(),
	ResolutionFixer_t357239262::get_offset_of_MainCamera_8(),
	ResolutionFixer_t357239262::get_offset_of_HeightStore_9(),
	ResolutionFixer_t357239262::get_offset_of_WidthStore_10(),
	ResolutionFixer_t357239262::get_offset_of_ratiox_11(),
	ResolutionFixer_t357239262::get_offset_of_ratioy_12(),
	ResolutionFixer_t357239262::get_offset_of_xdistance_13(),
	ResolutionFixer_t357239262::get_offset_of_ydistance_14(),
	ResolutionFixer_t357239262::get_offset_of_newxdistance_15(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
