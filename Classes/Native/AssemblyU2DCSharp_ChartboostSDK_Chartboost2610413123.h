﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Func`2<ChartboostSDK.CBLocation,System.Boolean>
struct Func_2_t362347193;
// System.Action`1<ChartboostSDK.CBLocation>
struct Action_1_t1875398900;
// System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError>
struct Action_2_t388935095;
// System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBClickError>
struct Action_2_t3934376746;
// System.Action`2<ChartboostSDK.CBLocation,System.Int32>
struct Action_2_t2650164891;
// System.Action
struct Action_t3226471752;
// ChartboostSDK.Chartboost
struct Chartboost_t2610413123;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChartboostSDK.Chartboost
struct  Chartboost_t2610413123  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Chartboost_t2610413123_StaticFields
{
public:
	// System.Action`1<System.Boolean> ChartboostSDK.Chartboost::didInitialize
	Action_1_t3627374100 * ___didInitialize_2;
	// System.Func`2<ChartboostSDK.CBLocation,System.Boolean> ChartboostSDK.Chartboost::shouldDisplayInterstitial
	Func_2_t362347193 * ___shouldDisplayInterstitial_3;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didDisplayInterstitial
	Action_1_t1875398900 * ___didDisplayInterstitial_4;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didCacheInterstitial
	Action_1_t1875398900 * ___didCacheInterstitial_5;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didClickInterstitial
	Action_1_t1875398900 * ___didClickInterstitial_6;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didCloseInterstitial
	Action_1_t1875398900 * ___didCloseInterstitial_7;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didDismissInterstitial
	Action_1_t1875398900 * ___didDismissInterstitial_8;
	// System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError> ChartboostSDK.Chartboost::didFailToLoadInterstitial
	Action_2_t388935095 * ___didFailToLoadInterstitial_9;
	// System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBClickError> ChartboostSDK.Chartboost::didFailToRecordClick
	Action_2_t3934376746 * ___didFailToRecordClick_10;
	// System.Func`2<ChartboostSDK.CBLocation,System.Boolean> ChartboostSDK.Chartboost::shouldDisplayMoreApps
	Func_2_t362347193 * ___shouldDisplayMoreApps_11;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didDisplayMoreApps
	Action_1_t1875398900 * ___didDisplayMoreApps_12;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didCacheMoreApps
	Action_1_t1875398900 * ___didCacheMoreApps_13;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didClickMoreApps
	Action_1_t1875398900 * ___didClickMoreApps_14;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didCloseMoreApps
	Action_1_t1875398900 * ___didCloseMoreApps_15;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didDismissMoreApps
	Action_1_t1875398900 * ___didDismissMoreApps_16;
	// System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError> ChartboostSDK.Chartboost::didFailToLoadMoreApps
	Action_2_t388935095 * ___didFailToLoadMoreApps_17;
	// System.Func`2<ChartboostSDK.CBLocation,System.Boolean> ChartboostSDK.Chartboost::shouldDisplayRewardedVideo
	Func_2_t362347193 * ___shouldDisplayRewardedVideo_18;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didDisplayRewardedVideo
	Action_1_t1875398900 * ___didDisplayRewardedVideo_19;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didCacheRewardedVideo
	Action_1_t1875398900 * ___didCacheRewardedVideo_20;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didClickRewardedVideo
	Action_1_t1875398900 * ___didClickRewardedVideo_21;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didCloseRewardedVideo
	Action_1_t1875398900 * ___didCloseRewardedVideo_22;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didDismissRewardedVideo
	Action_1_t1875398900 * ___didDismissRewardedVideo_23;
	// System.Action`2<ChartboostSDK.CBLocation,System.Int32> ChartboostSDK.Chartboost::didCompleteRewardedVideo
	Action_2_t2650164891 * ___didCompleteRewardedVideo_24;
	// System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError> ChartboostSDK.Chartboost::didFailToLoadRewardedVideo
	Action_2_t388935095 * ___didFailToLoadRewardedVideo_25;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::didCacheInPlay
	Action_1_t1875398900 * ___didCacheInPlay_26;
	// System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBImpressionError> ChartboostSDK.Chartboost::didFailToLoadInPlay
	Action_2_t388935095 * ___didFailToLoadInPlay_27;
	// System.Action`1<ChartboostSDK.CBLocation> ChartboostSDK.Chartboost::willDisplayVideo
	Action_1_t1875398900 * ___willDisplayVideo_28;
	// System.Action ChartboostSDK.Chartboost::didPauseClickForConfirmation
	Action_t3226471752 * ___didPauseClickForConfirmation_29;
	// System.Action ChartboostSDK.Chartboost::didCompleteAppStoreSheetFlow
	Action_t3226471752 * ___didCompleteAppStoreSheetFlow_30;
	// System.Boolean ChartboostSDK.Chartboost::showingAgeGate
	bool ___showingAgeGate_31;
	// ChartboostSDK.Chartboost ChartboostSDK.Chartboost::instance
	Chartboost_t2610413123 * ___instance_32;
	// System.Boolean ChartboostSDK.Chartboost::isPaused
	bool ___isPaused_33;
	// System.Boolean ChartboostSDK.Chartboost::shouldPause
	bool ___shouldPause_34;
	// System.Single ChartboostSDK.Chartboost::lastTimeScale
	float ___lastTimeScale_35;
	// UnityEngine.EventSystems.EventSystem ChartboostSDK.Chartboost::kEventSystem
	EventSystem_t3466835263 * ___kEventSystem_36;

public:
	inline static int32_t get_offset_of_didInitialize_2() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didInitialize_2)); }
	inline Action_1_t3627374100 * get_didInitialize_2() const { return ___didInitialize_2; }
	inline Action_1_t3627374100 ** get_address_of_didInitialize_2() { return &___didInitialize_2; }
	inline void set_didInitialize_2(Action_1_t3627374100 * value)
	{
		___didInitialize_2 = value;
		Il2CppCodeGenWriteBarrier(&___didInitialize_2, value);
	}

	inline static int32_t get_offset_of_shouldDisplayInterstitial_3() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___shouldDisplayInterstitial_3)); }
	inline Func_2_t362347193 * get_shouldDisplayInterstitial_3() const { return ___shouldDisplayInterstitial_3; }
	inline Func_2_t362347193 ** get_address_of_shouldDisplayInterstitial_3() { return &___shouldDisplayInterstitial_3; }
	inline void set_shouldDisplayInterstitial_3(Func_2_t362347193 * value)
	{
		___shouldDisplayInterstitial_3 = value;
		Il2CppCodeGenWriteBarrier(&___shouldDisplayInterstitial_3, value);
	}

	inline static int32_t get_offset_of_didDisplayInterstitial_4() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didDisplayInterstitial_4)); }
	inline Action_1_t1875398900 * get_didDisplayInterstitial_4() const { return ___didDisplayInterstitial_4; }
	inline Action_1_t1875398900 ** get_address_of_didDisplayInterstitial_4() { return &___didDisplayInterstitial_4; }
	inline void set_didDisplayInterstitial_4(Action_1_t1875398900 * value)
	{
		___didDisplayInterstitial_4 = value;
		Il2CppCodeGenWriteBarrier(&___didDisplayInterstitial_4, value);
	}

	inline static int32_t get_offset_of_didCacheInterstitial_5() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didCacheInterstitial_5)); }
	inline Action_1_t1875398900 * get_didCacheInterstitial_5() const { return ___didCacheInterstitial_5; }
	inline Action_1_t1875398900 ** get_address_of_didCacheInterstitial_5() { return &___didCacheInterstitial_5; }
	inline void set_didCacheInterstitial_5(Action_1_t1875398900 * value)
	{
		___didCacheInterstitial_5 = value;
		Il2CppCodeGenWriteBarrier(&___didCacheInterstitial_5, value);
	}

	inline static int32_t get_offset_of_didClickInterstitial_6() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didClickInterstitial_6)); }
	inline Action_1_t1875398900 * get_didClickInterstitial_6() const { return ___didClickInterstitial_6; }
	inline Action_1_t1875398900 ** get_address_of_didClickInterstitial_6() { return &___didClickInterstitial_6; }
	inline void set_didClickInterstitial_6(Action_1_t1875398900 * value)
	{
		___didClickInterstitial_6 = value;
		Il2CppCodeGenWriteBarrier(&___didClickInterstitial_6, value);
	}

	inline static int32_t get_offset_of_didCloseInterstitial_7() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didCloseInterstitial_7)); }
	inline Action_1_t1875398900 * get_didCloseInterstitial_7() const { return ___didCloseInterstitial_7; }
	inline Action_1_t1875398900 ** get_address_of_didCloseInterstitial_7() { return &___didCloseInterstitial_7; }
	inline void set_didCloseInterstitial_7(Action_1_t1875398900 * value)
	{
		___didCloseInterstitial_7 = value;
		Il2CppCodeGenWriteBarrier(&___didCloseInterstitial_7, value);
	}

	inline static int32_t get_offset_of_didDismissInterstitial_8() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didDismissInterstitial_8)); }
	inline Action_1_t1875398900 * get_didDismissInterstitial_8() const { return ___didDismissInterstitial_8; }
	inline Action_1_t1875398900 ** get_address_of_didDismissInterstitial_8() { return &___didDismissInterstitial_8; }
	inline void set_didDismissInterstitial_8(Action_1_t1875398900 * value)
	{
		___didDismissInterstitial_8 = value;
		Il2CppCodeGenWriteBarrier(&___didDismissInterstitial_8, value);
	}

	inline static int32_t get_offset_of_didFailToLoadInterstitial_9() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didFailToLoadInterstitial_9)); }
	inline Action_2_t388935095 * get_didFailToLoadInterstitial_9() const { return ___didFailToLoadInterstitial_9; }
	inline Action_2_t388935095 ** get_address_of_didFailToLoadInterstitial_9() { return &___didFailToLoadInterstitial_9; }
	inline void set_didFailToLoadInterstitial_9(Action_2_t388935095 * value)
	{
		___didFailToLoadInterstitial_9 = value;
		Il2CppCodeGenWriteBarrier(&___didFailToLoadInterstitial_9, value);
	}

	inline static int32_t get_offset_of_didFailToRecordClick_10() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didFailToRecordClick_10)); }
	inline Action_2_t3934376746 * get_didFailToRecordClick_10() const { return ___didFailToRecordClick_10; }
	inline Action_2_t3934376746 ** get_address_of_didFailToRecordClick_10() { return &___didFailToRecordClick_10; }
	inline void set_didFailToRecordClick_10(Action_2_t3934376746 * value)
	{
		___didFailToRecordClick_10 = value;
		Il2CppCodeGenWriteBarrier(&___didFailToRecordClick_10, value);
	}

	inline static int32_t get_offset_of_shouldDisplayMoreApps_11() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___shouldDisplayMoreApps_11)); }
	inline Func_2_t362347193 * get_shouldDisplayMoreApps_11() const { return ___shouldDisplayMoreApps_11; }
	inline Func_2_t362347193 ** get_address_of_shouldDisplayMoreApps_11() { return &___shouldDisplayMoreApps_11; }
	inline void set_shouldDisplayMoreApps_11(Func_2_t362347193 * value)
	{
		___shouldDisplayMoreApps_11 = value;
		Il2CppCodeGenWriteBarrier(&___shouldDisplayMoreApps_11, value);
	}

	inline static int32_t get_offset_of_didDisplayMoreApps_12() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didDisplayMoreApps_12)); }
	inline Action_1_t1875398900 * get_didDisplayMoreApps_12() const { return ___didDisplayMoreApps_12; }
	inline Action_1_t1875398900 ** get_address_of_didDisplayMoreApps_12() { return &___didDisplayMoreApps_12; }
	inline void set_didDisplayMoreApps_12(Action_1_t1875398900 * value)
	{
		___didDisplayMoreApps_12 = value;
		Il2CppCodeGenWriteBarrier(&___didDisplayMoreApps_12, value);
	}

	inline static int32_t get_offset_of_didCacheMoreApps_13() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didCacheMoreApps_13)); }
	inline Action_1_t1875398900 * get_didCacheMoreApps_13() const { return ___didCacheMoreApps_13; }
	inline Action_1_t1875398900 ** get_address_of_didCacheMoreApps_13() { return &___didCacheMoreApps_13; }
	inline void set_didCacheMoreApps_13(Action_1_t1875398900 * value)
	{
		___didCacheMoreApps_13 = value;
		Il2CppCodeGenWriteBarrier(&___didCacheMoreApps_13, value);
	}

	inline static int32_t get_offset_of_didClickMoreApps_14() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didClickMoreApps_14)); }
	inline Action_1_t1875398900 * get_didClickMoreApps_14() const { return ___didClickMoreApps_14; }
	inline Action_1_t1875398900 ** get_address_of_didClickMoreApps_14() { return &___didClickMoreApps_14; }
	inline void set_didClickMoreApps_14(Action_1_t1875398900 * value)
	{
		___didClickMoreApps_14 = value;
		Il2CppCodeGenWriteBarrier(&___didClickMoreApps_14, value);
	}

	inline static int32_t get_offset_of_didCloseMoreApps_15() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didCloseMoreApps_15)); }
	inline Action_1_t1875398900 * get_didCloseMoreApps_15() const { return ___didCloseMoreApps_15; }
	inline Action_1_t1875398900 ** get_address_of_didCloseMoreApps_15() { return &___didCloseMoreApps_15; }
	inline void set_didCloseMoreApps_15(Action_1_t1875398900 * value)
	{
		___didCloseMoreApps_15 = value;
		Il2CppCodeGenWriteBarrier(&___didCloseMoreApps_15, value);
	}

	inline static int32_t get_offset_of_didDismissMoreApps_16() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didDismissMoreApps_16)); }
	inline Action_1_t1875398900 * get_didDismissMoreApps_16() const { return ___didDismissMoreApps_16; }
	inline Action_1_t1875398900 ** get_address_of_didDismissMoreApps_16() { return &___didDismissMoreApps_16; }
	inline void set_didDismissMoreApps_16(Action_1_t1875398900 * value)
	{
		___didDismissMoreApps_16 = value;
		Il2CppCodeGenWriteBarrier(&___didDismissMoreApps_16, value);
	}

	inline static int32_t get_offset_of_didFailToLoadMoreApps_17() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didFailToLoadMoreApps_17)); }
	inline Action_2_t388935095 * get_didFailToLoadMoreApps_17() const { return ___didFailToLoadMoreApps_17; }
	inline Action_2_t388935095 ** get_address_of_didFailToLoadMoreApps_17() { return &___didFailToLoadMoreApps_17; }
	inline void set_didFailToLoadMoreApps_17(Action_2_t388935095 * value)
	{
		___didFailToLoadMoreApps_17 = value;
		Il2CppCodeGenWriteBarrier(&___didFailToLoadMoreApps_17, value);
	}

	inline static int32_t get_offset_of_shouldDisplayRewardedVideo_18() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___shouldDisplayRewardedVideo_18)); }
	inline Func_2_t362347193 * get_shouldDisplayRewardedVideo_18() const { return ___shouldDisplayRewardedVideo_18; }
	inline Func_2_t362347193 ** get_address_of_shouldDisplayRewardedVideo_18() { return &___shouldDisplayRewardedVideo_18; }
	inline void set_shouldDisplayRewardedVideo_18(Func_2_t362347193 * value)
	{
		___shouldDisplayRewardedVideo_18 = value;
		Il2CppCodeGenWriteBarrier(&___shouldDisplayRewardedVideo_18, value);
	}

	inline static int32_t get_offset_of_didDisplayRewardedVideo_19() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didDisplayRewardedVideo_19)); }
	inline Action_1_t1875398900 * get_didDisplayRewardedVideo_19() const { return ___didDisplayRewardedVideo_19; }
	inline Action_1_t1875398900 ** get_address_of_didDisplayRewardedVideo_19() { return &___didDisplayRewardedVideo_19; }
	inline void set_didDisplayRewardedVideo_19(Action_1_t1875398900 * value)
	{
		___didDisplayRewardedVideo_19 = value;
		Il2CppCodeGenWriteBarrier(&___didDisplayRewardedVideo_19, value);
	}

	inline static int32_t get_offset_of_didCacheRewardedVideo_20() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didCacheRewardedVideo_20)); }
	inline Action_1_t1875398900 * get_didCacheRewardedVideo_20() const { return ___didCacheRewardedVideo_20; }
	inline Action_1_t1875398900 ** get_address_of_didCacheRewardedVideo_20() { return &___didCacheRewardedVideo_20; }
	inline void set_didCacheRewardedVideo_20(Action_1_t1875398900 * value)
	{
		___didCacheRewardedVideo_20 = value;
		Il2CppCodeGenWriteBarrier(&___didCacheRewardedVideo_20, value);
	}

	inline static int32_t get_offset_of_didClickRewardedVideo_21() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didClickRewardedVideo_21)); }
	inline Action_1_t1875398900 * get_didClickRewardedVideo_21() const { return ___didClickRewardedVideo_21; }
	inline Action_1_t1875398900 ** get_address_of_didClickRewardedVideo_21() { return &___didClickRewardedVideo_21; }
	inline void set_didClickRewardedVideo_21(Action_1_t1875398900 * value)
	{
		___didClickRewardedVideo_21 = value;
		Il2CppCodeGenWriteBarrier(&___didClickRewardedVideo_21, value);
	}

	inline static int32_t get_offset_of_didCloseRewardedVideo_22() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didCloseRewardedVideo_22)); }
	inline Action_1_t1875398900 * get_didCloseRewardedVideo_22() const { return ___didCloseRewardedVideo_22; }
	inline Action_1_t1875398900 ** get_address_of_didCloseRewardedVideo_22() { return &___didCloseRewardedVideo_22; }
	inline void set_didCloseRewardedVideo_22(Action_1_t1875398900 * value)
	{
		___didCloseRewardedVideo_22 = value;
		Il2CppCodeGenWriteBarrier(&___didCloseRewardedVideo_22, value);
	}

	inline static int32_t get_offset_of_didDismissRewardedVideo_23() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didDismissRewardedVideo_23)); }
	inline Action_1_t1875398900 * get_didDismissRewardedVideo_23() const { return ___didDismissRewardedVideo_23; }
	inline Action_1_t1875398900 ** get_address_of_didDismissRewardedVideo_23() { return &___didDismissRewardedVideo_23; }
	inline void set_didDismissRewardedVideo_23(Action_1_t1875398900 * value)
	{
		___didDismissRewardedVideo_23 = value;
		Il2CppCodeGenWriteBarrier(&___didDismissRewardedVideo_23, value);
	}

	inline static int32_t get_offset_of_didCompleteRewardedVideo_24() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didCompleteRewardedVideo_24)); }
	inline Action_2_t2650164891 * get_didCompleteRewardedVideo_24() const { return ___didCompleteRewardedVideo_24; }
	inline Action_2_t2650164891 ** get_address_of_didCompleteRewardedVideo_24() { return &___didCompleteRewardedVideo_24; }
	inline void set_didCompleteRewardedVideo_24(Action_2_t2650164891 * value)
	{
		___didCompleteRewardedVideo_24 = value;
		Il2CppCodeGenWriteBarrier(&___didCompleteRewardedVideo_24, value);
	}

	inline static int32_t get_offset_of_didFailToLoadRewardedVideo_25() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didFailToLoadRewardedVideo_25)); }
	inline Action_2_t388935095 * get_didFailToLoadRewardedVideo_25() const { return ___didFailToLoadRewardedVideo_25; }
	inline Action_2_t388935095 ** get_address_of_didFailToLoadRewardedVideo_25() { return &___didFailToLoadRewardedVideo_25; }
	inline void set_didFailToLoadRewardedVideo_25(Action_2_t388935095 * value)
	{
		___didFailToLoadRewardedVideo_25 = value;
		Il2CppCodeGenWriteBarrier(&___didFailToLoadRewardedVideo_25, value);
	}

	inline static int32_t get_offset_of_didCacheInPlay_26() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didCacheInPlay_26)); }
	inline Action_1_t1875398900 * get_didCacheInPlay_26() const { return ___didCacheInPlay_26; }
	inline Action_1_t1875398900 ** get_address_of_didCacheInPlay_26() { return &___didCacheInPlay_26; }
	inline void set_didCacheInPlay_26(Action_1_t1875398900 * value)
	{
		___didCacheInPlay_26 = value;
		Il2CppCodeGenWriteBarrier(&___didCacheInPlay_26, value);
	}

	inline static int32_t get_offset_of_didFailToLoadInPlay_27() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didFailToLoadInPlay_27)); }
	inline Action_2_t388935095 * get_didFailToLoadInPlay_27() const { return ___didFailToLoadInPlay_27; }
	inline Action_2_t388935095 ** get_address_of_didFailToLoadInPlay_27() { return &___didFailToLoadInPlay_27; }
	inline void set_didFailToLoadInPlay_27(Action_2_t388935095 * value)
	{
		___didFailToLoadInPlay_27 = value;
		Il2CppCodeGenWriteBarrier(&___didFailToLoadInPlay_27, value);
	}

	inline static int32_t get_offset_of_willDisplayVideo_28() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___willDisplayVideo_28)); }
	inline Action_1_t1875398900 * get_willDisplayVideo_28() const { return ___willDisplayVideo_28; }
	inline Action_1_t1875398900 ** get_address_of_willDisplayVideo_28() { return &___willDisplayVideo_28; }
	inline void set_willDisplayVideo_28(Action_1_t1875398900 * value)
	{
		___willDisplayVideo_28 = value;
		Il2CppCodeGenWriteBarrier(&___willDisplayVideo_28, value);
	}

	inline static int32_t get_offset_of_didPauseClickForConfirmation_29() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didPauseClickForConfirmation_29)); }
	inline Action_t3226471752 * get_didPauseClickForConfirmation_29() const { return ___didPauseClickForConfirmation_29; }
	inline Action_t3226471752 ** get_address_of_didPauseClickForConfirmation_29() { return &___didPauseClickForConfirmation_29; }
	inline void set_didPauseClickForConfirmation_29(Action_t3226471752 * value)
	{
		___didPauseClickForConfirmation_29 = value;
		Il2CppCodeGenWriteBarrier(&___didPauseClickForConfirmation_29, value);
	}

	inline static int32_t get_offset_of_didCompleteAppStoreSheetFlow_30() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___didCompleteAppStoreSheetFlow_30)); }
	inline Action_t3226471752 * get_didCompleteAppStoreSheetFlow_30() const { return ___didCompleteAppStoreSheetFlow_30; }
	inline Action_t3226471752 ** get_address_of_didCompleteAppStoreSheetFlow_30() { return &___didCompleteAppStoreSheetFlow_30; }
	inline void set_didCompleteAppStoreSheetFlow_30(Action_t3226471752 * value)
	{
		___didCompleteAppStoreSheetFlow_30 = value;
		Il2CppCodeGenWriteBarrier(&___didCompleteAppStoreSheetFlow_30, value);
	}

	inline static int32_t get_offset_of_showingAgeGate_31() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___showingAgeGate_31)); }
	inline bool get_showingAgeGate_31() const { return ___showingAgeGate_31; }
	inline bool* get_address_of_showingAgeGate_31() { return &___showingAgeGate_31; }
	inline void set_showingAgeGate_31(bool value)
	{
		___showingAgeGate_31 = value;
	}

	inline static int32_t get_offset_of_instance_32() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___instance_32)); }
	inline Chartboost_t2610413123 * get_instance_32() const { return ___instance_32; }
	inline Chartboost_t2610413123 ** get_address_of_instance_32() { return &___instance_32; }
	inline void set_instance_32(Chartboost_t2610413123 * value)
	{
		___instance_32 = value;
		Il2CppCodeGenWriteBarrier(&___instance_32, value);
	}

	inline static int32_t get_offset_of_isPaused_33() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___isPaused_33)); }
	inline bool get_isPaused_33() const { return ___isPaused_33; }
	inline bool* get_address_of_isPaused_33() { return &___isPaused_33; }
	inline void set_isPaused_33(bool value)
	{
		___isPaused_33 = value;
	}

	inline static int32_t get_offset_of_shouldPause_34() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___shouldPause_34)); }
	inline bool get_shouldPause_34() const { return ___shouldPause_34; }
	inline bool* get_address_of_shouldPause_34() { return &___shouldPause_34; }
	inline void set_shouldPause_34(bool value)
	{
		___shouldPause_34 = value;
	}

	inline static int32_t get_offset_of_lastTimeScale_35() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___lastTimeScale_35)); }
	inline float get_lastTimeScale_35() const { return ___lastTimeScale_35; }
	inline float* get_address_of_lastTimeScale_35() { return &___lastTimeScale_35; }
	inline void set_lastTimeScale_35(float value)
	{
		___lastTimeScale_35 = value;
	}

	inline static int32_t get_offset_of_kEventSystem_36() { return static_cast<int32_t>(offsetof(Chartboost_t2610413123_StaticFields, ___kEventSystem_36)); }
	inline EventSystem_t3466835263 * get_kEventSystem_36() const { return ___kEventSystem_36; }
	inline EventSystem_t3466835263 ** get_address_of_kEventSystem_36() { return &___kEventSystem_36; }
	inline void set_kEventSystem_36(EventSystem_t3466835263 * value)
	{
		___kEventSystem_36 = value;
		Il2CppCodeGenWriteBarrier(&___kEventSystem_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
