﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_InspectorHelp
struct CFX_InspectorHelp_t3280468206;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_InspectorHelp::.ctor()
extern "C"  void CFX_InspectorHelp__ctor_m3431706363 (CFX_InspectorHelp_t3280468206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_InspectorHelp::Unlock()
extern "C"  void CFX_InspectorHelp_Unlock_m3825725155 (CFX_InspectorHelp_t3280468206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
