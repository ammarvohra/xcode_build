﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToolButton/<ButtonBackOnPosition>c__Iterator0
struct U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ToolButton/<ButtonBackOnPosition>c__Iterator0::.ctor()
extern "C"  void U3CButtonBackOnPositionU3Ec__Iterator0__ctor_m1884971012 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ToolButton/<ButtonBackOnPosition>c__Iterator0::MoveNext()
extern "C"  bool U3CButtonBackOnPositionU3Ec__Iterator0_MoveNext_m2791120256 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ToolButton/<ButtonBackOnPosition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CButtonBackOnPositionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2628418430 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ToolButton/<ButtonBackOnPosition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CButtonBackOnPositionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m675756326 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToolButton/<ButtonBackOnPosition>c__Iterator0::Dispose()
extern "C"  void U3CButtonBackOnPositionU3Ec__Iterator0_Dispose_m2880566527 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToolButton/<ButtonBackOnPosition>c__Iterator0::Reset()
extern "C"  void U3CButtonBackOnPositionU3Ec__Iterator0_Reset_m2591090449 (U3CButtonBackOnPositionU3Ec__Iterator0_t3433254669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
