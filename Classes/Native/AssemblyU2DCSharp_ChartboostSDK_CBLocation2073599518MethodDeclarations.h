﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChartboostSDK.CBLocation
struct CBLocation_t2073599518;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChartboostSDK.CBLocation::.ctor(System.String)
extern "C"  void CBLocation__ctor_m2327780356 (CBLocation_t2073599518 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBLocation::ToString()
extern "C"  String_t* CBLocation_ToString_m2133033231 (CBLocation_t2073599518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChartboostSDK.CBLocation ChartboostSDK.CBLocation::locationFromName(System.String)
extern "C"  CBLocation_t2073599518 * CBLocation_locationFromName_m185118612 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBLocation::.cctor()
extern "C"  void CBLocation__cctor_m2818677095 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
