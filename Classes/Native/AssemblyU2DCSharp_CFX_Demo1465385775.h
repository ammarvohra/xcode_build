﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t3991289194;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo
struct  CFX_Demo_t1465385775  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CFX_Demo::orderedSpawns
	bool ___orderedSpawns_2;
	// System.Single CFX_Demo::step
	float ___step_3;
	// System.Single CFX_Demo::range
	float ___range_4;
	// System.Single CFX_Demo::order
	float ___order_5;
	// UnityEngine.Material CFX_Demo::groundMat
	Material_t193706927 * ___groundMat_6;
	// UnityEngine.Material CFX_Demo::waterMat
	Material_t193706927 * ___waterMat_7;
	// UnityEngine.GameObject[] CFX_Demo::ParticleExamples
	GameObjectU5BU5D_t3057952154* ___ParticleExamples_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> CFX_Demo::ParticlesYOffsetD
	Dictionary_2_t3991289194 * ___ParticlesYOffsetD_9;
	// System.Int32 CFX_Demo::exampleIndex
	int32_t ___exampleIndex_10;
	// System.String CFX_Demo::randomSpawnsDelay
	String_t* ___randomSpawnsDelay_11;
	// System.Boolean CFX_Demo::randomSpawns
	bool ___randomSpawns_12;
	// System.Boolean CFX_Demo::slowMo
	bool ___slowMo_13;

public:
	inline static int32_t get_offset_of_orderedSpawns_2() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___orderedSpawns_2)); }
	inline bool get_orderedSpawns_2() const { return ___orderedSpawns_2; }
	inline bool* get_address_of_orderedSpawns_2() { return &___orderedSpawns_2; }
	inline void set_orderedSpawns_2(bool value)
	{
		___orderedSpawns_2 = value;
	}

	inline static int32_t get_offset_of_step_3() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___step_3)); }
	inline float get_step_3() const { return ___step_3; }
	inline float* get_address_of_step_3() { return &___step_3; }
	inline void set_step_3(float value)
	{
		___step_3 = value;
	}

	inline static int32_t get_offset_of_range_4() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___range_4)); }
	inline float get_range_4() const { return ___range_4; }
	inline float* get_address_of_range_4() { return &___range_4; }
	inline void set_range_4(float value)
	{
		___range_4 = value;
	}

	inline static int32_t get_offset_of_order_5() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___order_5)); }
	inline float get_order_5() const { return ___order_5; }
	inline float* get_address_of_order_5() { return &___order_5; }
	inline void set_order_5(float value)
	{
		___order_5 = value;
	}

	inline static int32_t get_offset_of_groundMat_6() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___groundMat_6)); }
	inline Material_t193706927 * get_groundMat_6() const { return ___groundMat_6; }
	inline Material_t193706927 ** get_address_of_groundMat_6() { return &___groundMat_6; }
	inline void set_groundMat_6(Material_t193706927 * value)
	{
		___groundMat_6 = value;
		Il2CppCodeGenWriteBarrier(&___groundMat_6, value);
	}

	inline static int32_t get_offset_of_waterMat_7() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___waterMat_7)); }
	inline Material_t193706927 * get_waterMat_7() const { return ___waterMat_7; }
	inline Material_t193706927 ** get_address_of_waterMat_7() { return &___waterMat_7; }
	inline void set_waterMat_7(Material_t193706927 * value)
	{
		___waterMat_7 = value;
		Il2CppCodeGenWriteBarrier(&___waterMat_7, value);
	}

	inline static int32_t get_offset_of_ParticleExamples_8() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___ParticleExamples_8)); }
	inline GameObjectU5BU5D_t3057952154* get_ParticleExamples_8() const { return ___ParticleExamples_8; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ParticleExamples_8() { return &___ParticleExamples_8; }
	inline void set_ParticleExamples_8(GameObjectU5BU5D_t3057952154* value)
	{
		___ParticleExamples_8 = value;
		Il2CppCodeGenWriteBarrier(&___ParticleExamples_8, value);
	}

	inline static int32_t get_offset_of_ParticlesYOffsetD_9() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___ParticlesYOffsetD_9)); }
	inline Dictionary_2_t3991289194 * get_ParticlesYOffsetD_9() const { return ___ParticlesYOffsetD_9; }
	inline Dictionary_2_t3991289194 ** get_address_of_ParticlesYOffsetD_9() { return &___ParticlesYOffsetD_9; }
	inline void set_ParticlesYOffsetD_9(Dictionary_2_t3991289194 * value)
	{
		___ParticlesYOffsetD_9 = value;
		Il2CppCodeGenWriteBarrier(&___ParticlesYOffsetD_9, value);
	}

	inline static int32_t get_offset_of_exampleIndex_10() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___exampleIndex_10)); }
	inline int32_t get_exampleIndex_10() const { return ___exampleIndex_10; }
	inline int32_t* get_address_of_exampleIndex_10() { return &___exampleIndex_10; }
	inline void set_exampleIndex_10(int32_t value)
	{
		___exampleIndex_10 = value;
	}

	inline static int32_t get_offset_of_randomSpawnsDelay_11() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___randomSpawnsDelay_11)); }
	inline String_t* get_randomSpawnsDelay_11() const { return ___randomSpawnsDelay_11; }
	inline String_t** get_address_of_randomSpawnsDelay_11() { return &___randomSpawnsDelay_11; }
	inline void set_randomSpawnsDelay_11(String_t* value)
	{
		___randomSpawnsDelay_11 = value;
		Il2CppCodeGenWriteBarrier(&___randomSpawnsDelay_11, value);
	}

	inline static int32_t get_offset_of_randomSpawns_12() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___randomSpawns_12)); }
	inline bool get_randomSpawns_12() const { return ___randomSpawns_12; }
	inline bool* get_address_of_randomSpawns_12() { return &___randomSpawns_12; }
	inline void set_randomSpawns_12(bool value)
	{
		___randomSpawns_12 = value;
	}

	inline static int32_t get_offset_of_slowMo_13() { return static_cast<int32_t>(offsetof(CFX_Demo_t1465385775, ___slowMo_13)); }
	inline bool get_slowMo_13() const { return ___slowMo_13; }
	inline bool* get_address_of_slowMo_13() { return &___slowMo_13; }
	inline void set_slowMo_13(bool value)
	{
		___slowMo_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
