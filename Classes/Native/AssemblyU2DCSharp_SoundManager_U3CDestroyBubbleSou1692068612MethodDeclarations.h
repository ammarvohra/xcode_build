﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundManager/<DestroyBubbleSound>c__Iterator1
struct U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SoundManager/<DestroyBubbleSound>c__Iterator1::.ctor()
extern "C"  void U3CDestroyBubbleSoundU3Ec__Iterator1__ctor_m3866914725 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundManager/<DestroyBubbleSound>c__Iterator1::MoveNext()
extern "C"  bool U3CDestroyBubbleSoundU3Ec__Iterator1_MoveNext_m3370383731 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SoundManager/<DestroyBubbleSound>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDestroyBubbleSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3462975135 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SoundManager/<DestroyBubbleSound>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDestroyBubbleSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1395584071 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager/<DestroyBubbleSound>c__Iterator1::Dispose()
extern "C"  void U3CDestroyBubbleSoundU3Ec__Iterator1_Dispose_m3909609150 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager/<DestroyBubbleSound>c__Iterator1::Reset()
extern "C"  void U3CDestroyBubbleSoundU3Ec__Iterator1_Reset_m3908795056 (U3CDestroyBubbleSoundU3Ec__Iterator1_t1692068612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
