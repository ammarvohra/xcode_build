﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen1954480006MethodDeclarations.h"

// System.Void System.Action`2<ChartboostSDK.CBLocation,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m519703102(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t2650164891 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m631378381_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<ChartboostSDK.CBLocation,System.Int32>::Invoke(T1,T2)
#define Action_2_Invoke_m2971938949(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2650164891 *, CBLocation_t2073599518 *, int32_t, const MethodInfo*))Action_2_Invoke_m1256339344_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<ChartboostSDK.CBLocation,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2583222062(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t2650164891 *, CBLocation_t2073599518 *, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m2449831909_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<ChartboostSDK.CBLocation,System.Int32>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1659914790(__this, ___result0, method) ((  void (*) (Action_2_t2650164891 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m1881200271_gshared)(__this, ___result0, method)
