﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen3238691861MethodDeclarations.h"

// System.Void System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBClickError>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1380119865(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3934376746 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m4087602012_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBClickError>::Invoke(T1,T2)
#define Action_2_Invoke_m1023741656(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3934376746 *, CBLocation_t2073599518 *, int32_t, const MethodInfo*))Action_2_Invoke_m3783736003_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBClickError>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1685803369(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3934376746 *, CBLocation_t2073599518 *, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3212627622_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<ChartboostSDK.CBLocation,ChartboostSDK.CBClickError>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m2006024367(__this, ___result0, method) ((  void (*) (Action_2_t3934376746 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m4109283410_gshared)(__this, ___result0, method)
