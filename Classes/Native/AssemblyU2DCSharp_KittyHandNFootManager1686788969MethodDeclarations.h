﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager
struct KittyHandNFootManager_t1686788969;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void KittyHandNFootManager::.ctor()
extern "C"  void KittyHandNFootManager__ctor_m1931548674 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::Start()
extern "C"  Il2CppObject * KittyHandNFootManager_Start_m961038030 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::Update()
extern "C"  void KittyHandNFootManager_Update_m1947480053 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::InitiateTalk()
extern "C"  void KittyHandNFootManager_InitiateTalk_m3159398165 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::ColorNails()
extern "C"  void KittyHandNFootManager_ColorNails_m1870864844 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::MakeNailPolishIconReady()
extern "C"  void KittyHandNFootManager_MakeNailPolishIconReady_m2897165473 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::MakeFacePowderOn()
extern "C"  void KittyHandNFootManager_MakeFacePowderOn_m2107715037 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::MakePerfumeOpen()
extern "C"  void KittyHandNFootManager_MakePerfumeOpen_m347974372 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::SprayPerfume()
extern "C"  void KittyHandNFootManager_SprayPerfume_m3568924841 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::LeftHandMouseDown()
extern "C"  void KittyHandNFootManager_LeftHandMouseDown_m1853715275 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::LeftHandMouseUp()
extern "C"  void KittyHandNFootManager_LeftHandMouseUp_m1465708436 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::LeftHandMouseDrag()
extern "C"  void KittyHandNFootManager_LeftHandMouseDrag_m3750075389 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::LeftHandCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_LeftHandCollisionEnter_m1131043820 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RightHandMouseDown()
extern "C"  void KittyHandNFootManager_RightHandMouseDown_m3465647210 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RightHandMouseUp()
extern "C"  void KittyHandNFootManager_RightHandMouseUp_m288136055 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RightHandMouseDrag()
extern "C"  void KittyHandNFootManager_RightHandMouseDrag_m2276292228 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RightHandCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_RightHandCollisionEnter_m569741013 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::LeftLegMouseDown()
extern "C"  void KittyHandNFootManager_LeftLegMouseDown_m2283106250 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::LeftLegMouseUp()
extern "C"  void KittyHandNFootManager_LeftLegMouseUp_m91314193 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::LeftLegMouseDrag()
extern "C"  void KittyHandNFootManager_LeftLegMouseDrag_m4259368924 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::LeftLegCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_LeftLegCollisionEnter_m764684799 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RightLegMouseDown()
extern "C"  void KittyHandNFootManager_RightLegMouseDown_m3162149081 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RightLegMouseUp()
extern "C"  void KittyHandNFootManager_RightLegMouseUp_m1869735974 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RightLegMouseDrag()
extern "C"  void KittyHandNFootManager_RightLegMouseDrag_m1429015919 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RightLegCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_RightLegCollisionEnter_m538626458 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailCutterMouseDown()
extern "C"  void KittyHandNFootManager_NailCutterMouseDown_m470457080 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailCutterMouseUp()
extern "C"  void KittyHandNFootManager_NailCutterMouseUp_m2916541575 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailCutterMouseDrag()
extern "C"  void KittyHandNFootManager_NailCutterMouseDrag_m815784710 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailCutterCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_NailCutterCollisionEnter_m3018890777 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailPolishIconMouseDown()
extern "C"  void KittyHandNFootManager_NailPolishIconMouseDown_m1671979995 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailPolishIconMouseUp()
extern "C"  void KittyHandNFootManager_NailPolishIconMouseUp_m912663072 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailPolishIconMouseDrag()
extern "C"  void KittyHandNFootManager_NailPolishIconMouseDrag_m3568338921 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailPolishIconCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_NailPolishIconCollisionEnter_m2945983628 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailPolishMouseDown()
extern "C"  void KittyHandNFootManager_NailPolishMouseDown_m1591627310 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailPolishMouseUp()
extern "C"  void KittyHandNFootManager_NailPolishMouseUp_m1381491547 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailPolishMouseDrag()
extern "C"  void KittyHandNFootManager_NailPolishMouseDrag_m4136548380 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::NailPolishCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_NailPolishCollisionEnter_m1361164717 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::FacePowderBrushMouseDown()
extern "C"  void KittyHandNFootManager_FacePowderBrushMouseDown_m1401345295 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::FacePowderBrushMouseUp()
extern "C"  void KittyHandNFootManager_FacePowderBrushMouseUp_m2883390390 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::FacePowderBrushMouseDrag()
extern "C"  void KittyHandNFootManager_FacePowderBrushMouseDrag_m3155366913 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::FacePowderBrushCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_FacePowderBrushCollisionEnter_m1731164666 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::FacePowderBrushCollisionStay(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_FacePowderBrushCollisionStay_m1600148821 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::PerfumeMouseDown()
extern "C"  void KittyHandNFootManager_PerfumeMouseDown_m1773689439 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::PerfumeMouseUp()
extern "C"  void KittyHandNFootManager_PerfumeMouseUp_m3524642962 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::PerfumeMouseDrag()
extern "C"  void KittyHandNFootManager_PerfumeMouseDrag_m2963087325 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::PerfumeCollisionEnter(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_PerfumeCollisionEnter_m1329363386 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::PerfumeCollisionStay(UnityEngine.GameObject)
extern "C"  void KittyHandNFootManager_PerfumeCollisionStay_m2906350105 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::CurtainTasselMouseDown()
extern "C"  void KittyHandNFootManager_CurtainTasselMouseDown_m2926699873 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::BackButtonMouseDown()
extern "C"  void KittyHandNFootManager_BackButtonMouseDown_m302824672 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::FbLikeButtonMouseDown()
extern "C"  void KittyHandNFootManager_FbLikeButtonMouseDown_m2016386076 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::FbLikeButtonMouseUp()
extern "C"  void KittyHandNFootManager_FbLikeButtonMouseUp_m1876896813 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RestartButtonMouseDown()
extern "C"  void KittyHandNFootManager_RestartButtonMouseDown_m744644910 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::RestartButtonMouseUp()
extern "C"  void KittyHandNFootManager_RestartButtonMouseUp_m1097162017 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::CloseButtonMouseDown()
extern "C"  void KittyHandNFootManager_CloseButtonMouseDown_m2177967763 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::DoneButtonMouseUp()
extern "C"  void KittyHandNFootManager_DoneButtonMouseUp_m1316066332 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::ShareButtonMouseUp()
extern "C"  void KittyHandNFootManager_ShareButtonMouseUp_m599277463 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::SnapButtonMouseUp()
extern "C"  void KittyHandNFootManager_SnapButtonMouseUp_m2612709480 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::MakeBigger(UnityEngine.GameObject)
extern "C"  Il2CppObject * KittyHandNFootManager_MakeBigger_m1294242164 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___Obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::NailCuttingAnim(UnityEngine.GameObject)
extern "C"  Il2CppObject * KittyHandNFootManager_NailCuttingAnim_m1025854349 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___Obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::MakeNailPolishIconAvailable()
extern "C"  Il2CppObject * KittyHandNFootManager_MakeNailPolishIconAvailable_m369200145 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::PrepareForNextNailCut()
extern "C"  Il2CppObject * KittyHandNFootManager_PrepareForNextNailCut_m3947584807 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::DeleteSpray(UnityEngine.GameObject)
extern "C"  Il2CppObject * KittyHandNFootManager_DeleteSpray_m1861600344 (KittyHandNFootManager_t1686788969 * __this, GameObject_t1756533147 * ___Spray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::MakeCurtainTasselAnimOff()
extern "C"  Il2CppObject * KittyHandNFootManager_MakeCurtainTasselAnimOff_m151004770 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::LoadNextLevel()
extern "C"  Il2CppObject * KittyHandNFootManager_LoadNextLevel_m2267124017 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::MakeStylePanelOpen()
extern "C"  Il2CppObject * KittyHandNFootManager_MakeStylePanelOpen_m2285956193 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::FbLikeButtonUp()
extern "C"  Il2CppObject * KittyHandNFootManager_FbLikeButtonUp_m3249047842 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::RestartButtonUp()
extern "C"  Il2CppObject * KittyHandNFootManager_RestartButtonUp_m4208856944 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::TakeSnap()
extern "C"  Il2CppObject * KittyHandNFootManager_TakeSnap_m3981926389 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KittyHandNFootManager::ShareScreenshot()
extern "C"  Il2CppObject * KittyHandNFootManager_ShareScreenshot_m2207017311 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager::GameEndAnimation()
extern "C"  void KittyHandNFootManager_GameEndAnimation_m2847078999 (KittyHandNFootManager_t1686788969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
