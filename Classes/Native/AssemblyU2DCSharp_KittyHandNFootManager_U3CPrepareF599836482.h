﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// KittyHandNFootManager
struct KittyHandNFootManager_t1686788969;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4
struct  U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482  : public Il2CppObject
{
public:
	// UnityEngine.GameObject KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::<StarObj>__0
	GameObject_t1756533147 * ___U3CStarObjU3E__0_0;
	// UnityEngine.GameObject KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::<StarObj1>__1
	GameObject_t1756533147 * ___U3CStarObj1U3E__1_1;
	// UnityEngine.GameObject KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::<StarObj2>__2
	GameObject_t1756533147 * ___U3CStarObj2U3E__2_2;
	// UnityEngine.GameObject KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::<TargetHand>__3
	GameObject_t1756533147 * ___U3CTargetHandU3E__3_3;
	// KittyHandNFootManager KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::$this
	KittyHandNFootManager_t1686788969 * ___U24this_4;
	// System.Object KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::$disposing
	bool ___U24disposing_6;
	// System.Int32 KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CStarObjU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482, ___U3CStarObjU3E__0_0)); }
	inline GameObject_t1756533147 * get_U3CStarObjU3E__0_0() const { return ___U3CStarObjU3E__0_0; }
	inline GameObject_t1756533147 ** get_address_of_U3CStarObjU3E__0_0() { return &___U3CStarObjU3E__0_0; }
	inline void set_U3CStarObjU3E__0_0(GameObject_t1756533147 * value)
	{
		___U3CStarObjU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStarObjU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CStarObj1U3E__1_1() { return static_cast<int32_t>(offsetof(U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482, ___U3CStarObj1U3E__1_1)); }
	inline GameObject_t1756533147 * get_U3CStarObj1U3E__1_1() const { return ___U3CStarObj1U3E__1_1; }
	inline GameObject_t1756533147 ** get_address_of_U3CStarObj1U3E__1_1() { return &___U3CStarObj1U3E__1_1; }
	inline void set_U3CStarObj1U3E__1_1(GameObject_t1756533147 * value)
	{
		___U3CStarObj1U3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStarObj1U3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CStarObj2U3E__2_2() { return static_cast<int32_t>(offsetof(U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482, ___U3CStarObj2U3E__2_2)); }
	inline GameObject_t1756533147 * get_U3CStarObj2U3E__2_2() const { return ___U3CStarObj2U3E__2_2; }
	inline GameObject_t1756533147 ** get_address_of_U3CStarObj2U3E__2_2() { return &___U3CStarObj2U3E__2_2; }
	inline void set_U3CStarObj2U3E__2_2(GameObject_t1756533147 * value)
	{
		___U3CStarObj2U3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStarObj2U3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CTargetHandU3E__3_3() { return static_cast<int32_t>(offsetof(U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482, ___U3CTargetHandU3E__3_3)); }
	inline GameObject_t1756533147 * get_U3CTargetHandU3E__3_3() const { return ___U3CTargetHandU3E__3_3; }
	inline GameObject_t1756533147 ** get_address_of_U3CTargetHandU3E__3_3() { return &___U3CTargetHandU3E__3_3; }
	inline void set_U3CTargetHandU3E__3_3(GameObject_t1756533147 * value)
	{
		___U3CTargetHandU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTargetHandU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482, ___U24this_4)); }
	inline KittyHandNFootManager_t1686788969 * get_U24this_4() const { return ___U24this_4; }
	inline KittyHandNFootManager_t1686788969 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(KittyHandNFootManager_t1686788969 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
