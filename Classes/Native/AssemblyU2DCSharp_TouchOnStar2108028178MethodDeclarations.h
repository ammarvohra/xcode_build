﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchOnStar
struct TouchOnStar_t2108028178;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchOnStar::.ctor()
extern "C"  void TouchOnStar__ctor_m234424267 (TouchOnStar_t2108028178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchOnStar::OnMouseDown()
extern "C"  void TouchOnStar_OnMouseDown_m3839092973 (TouchOnStar_t2108028178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
