﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DestroyOrDeactivateAtFixedTime/<DestroyOrDeactivate>c__Iterator0
struct U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DestroyOrDeactivateAtFixedTime/<DestroyOrDeactivate>c__Iterator0::.ctor()
extern "C"  void U3CDestroyOrDeactivateU3Ec__Iterator0__ctor_m4263041569 (U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DestroyOrDeactivateAtFixedTime/<DestroyOrDeactivate>c__Iterator0::MoveNext()
extern "C"  bool U3CDestroyOrDeactivateU3Ec__Iterator0_MoveNext_m2123171851 (U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DestroyOrDeactivateAtFixedTime/<DestroyOrDeactivate>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDestroyOrDeactivateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m739866411 (U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DestroyOrDeactivateAtFixedTime/<DestroyOrDeactivate>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDestroyOrDeactivateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3165037811 (U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyOrDeactivateAtFixedTime/<DestroyOrDeactivate>c__Iterator0::Dispose()
extern "C"  void U3CDestroyOrDeactivateU3Ec__Iterator0_Dispose_m674398668 (U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyOrDeactivateAtFixedTime/<DestroyOrDeactivate>c__Iterator0::Reset()
extern "C"  void U3CDestroyOrDeactivateU3Ec__Iterator0_Reset_m3190711298 (U3CDestroyOrDeactivateU3Ec__Iterator0_t3291299954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
