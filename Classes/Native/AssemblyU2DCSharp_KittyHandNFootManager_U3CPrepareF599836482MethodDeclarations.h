﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4
struct U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::.ctor()
extern "C"  void U3CPrepareForNextNailCutU3Ec__Iterator4__ctor_m3872460669 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::MoveNext()
extern "C"  bool U3CPrepareForNextNailCutU3Ec__Iterator4_MoveNext_m2643815519 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPrepareForNextNailCutU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m715441427 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrepareForNextNailCutU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m660017275 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::Dispose()
extern "C"  void U3CPrepareForNextNailCutU3Ec__Iterator4_Dispose_m3862261636 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<PrepareForNextNailCut>c__Iterator4::Reset()
extern "C"  void U3CPrepareForNextNailCutU3Ec__Iterator4_Reset_m1984076994 (U3CPrepareForNextNailCutU3Ec__Iterator4_t599836482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
