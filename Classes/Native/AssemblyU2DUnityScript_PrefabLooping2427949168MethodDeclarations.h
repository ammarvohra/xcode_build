﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PrefabLooping
struct PrefabLooping_t2427949168;

#include "codegen/il2cpp-codegen.h"

// System.Void PrefabLooping::.ctor()
extern "C"  void PrefabLooping__ctor_m3104908688 (PrefabLooping_t2427949168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabLooping::Start()
extern "C"  void PrefabLooping_Start_m3505107736 (PrefabLooping_t2427949168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabLooping::Burst()
extern "C"  void PrefabLooping_Burst_m3615015702 (PrefabLooping_t2427949168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabLooping::Main()
extern "C"  void PrefabLooping_Main_m2062010799 (PrefabLooping_t2427949168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
