﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo/<RandomSpawnsCoroutine>c__Iterator0
struct U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo/<RandomSpawnsCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CRandomSpawnsCoroutineU3Ec__Iterator0__ctor_m972310719 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_Demo/<RandomSpawnsCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3CRandomSpawnsCoroutineU3Ec__Iterator0_MoveNext_m2380379753 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_Demo/<RandomSpawnsCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRandomSpawnsCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1570755341 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_Demo/<RandomSpawnsCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRandomSpawnsCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m854655829 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo/<RandomSpawnsCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CRandomSpawnsCoroutineU3Ec__Iterator0_Dispose_m3433255342 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo/<RandomSpawnsCoroutine>c__Iterator0::Reset()
extern "C"  void U3CRandomSpawnsCoroutineU3Ec__Iterator0_Reset_m792716024 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
