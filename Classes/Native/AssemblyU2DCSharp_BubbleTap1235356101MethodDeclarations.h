﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BubbleTap
struct BubbleTap_t1235356101;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void BubbleTap::.ctor()
extern "C"  void BubbleTap__ctor_m567702318 (BubbleTap_t1235356101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BubbleTap::OnMouseDown()
extern "C"  void BubbleTap_OnMouseDown_m3031186794 (BubbleTap_t1235356101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BubbleTap::WaitForDestroy()
extern "C"  Il2CppObject * BubbleTap_WaitForDestroy_m465767390 (BubbleTap_t1235356101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
