﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<LoadNextLevel>c__Iterator7
struct U3CLoadNextLevelU3Ec__Iterator7_t3146601861;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<LoadNextLevel>c__Iterator7::.ctor()
extern "C"  void U3CLoadNextLevelU3Ec__Iterator7__ctor_m296652690 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<LoadNextLevel>c__Iterator7::MoveNext()
extern "C"  bool U3CLoadNextLevelU3Ec__Iterator7_MoveNext_m2212974190 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<LoadNextLevel>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadNextLevelU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4202989504 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<LoadNextLevel>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadNextLevelU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1668672488 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<LoadNextLevel>c__Iterator7::Dispose()
extern "C"  void U3CLoadNextLevelU3Ec__Iterator7_Dispose_m4013085871 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<LoadNextLevel>c__Iterator7::Reset()
extern "C"  void U3CLoadNextLevelU3Ec__Iterator7_Reset_m2750327949 (U3CLoadNextLevelU3Ec__Iterator7_t3146601861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
