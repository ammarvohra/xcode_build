﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChartboostSDK.CBInPlay
struct CBInPlay_t2057847888;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ChartboostSDK.CBInPlay::.ctor(System.IntPtr)
extern "C"  void CBInPlay__ctor_m2140861950 (CBInPlay_t2057847888 * __this, IntPtr_t ___uniqueId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBInPlay::_chartBoostInPlayClick(System.IntPtr)
extern "C"  void CBInPlay__chartBoostInPlayClick_m2322771395 (Il2CppObject * __this /* static, unused */, IntPtr_t ___uniqueID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBInPlay::_chartBoostInPlayShow(System.IntPtr)
extern "C"  void CBInPlay__chartBoostInPlayShow_m624853880 (Il2CppObject * __this /* static, unused */, IntPtr_t ___uniqueID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ChartboostSDK.CBInPlay::_chartBoostInPlayGetAppIcon(System.IntPtr)
extern "C"  IntPtr_t CBInPlay__chartBoostInPlayGetAppIcon_m1528365100 (Il2CppObject * __this /* static, unused */, IntPtr_t ___uniqueID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ChartboostSDK.CBInPlay::_chartBoostInPlayGetAppIconSize(System.IntPtr)
extern "C"  int32_t CBInPlay__chartBoostInPlayGetAppIconSize_m4203650580 (Il2CppObject * __this /* static, unused */, IntPtr_t ___uniqueID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBInPlay::_chartBoostInPlayGetAppName(System.IntPtr)
extern "C"  String_t* CBInPlay__chartBoostInPlayGetAppName_m2984402690 (Il2CppObject * __this /* static, unused */, IntPtr_t ___uniqueID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBInPlay::_chartBoostFreeInPlayObject(System.IntPtr)
extern "C"  void CBInPlay__chartBoostFreeInPlayObject_m2846062276 (Il2CppObject * __this /* static, unused */, IntPtr_t ___uniqueID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBInPlay::setAppName()
extern "C"  void CBInPlay_setAppName_m144792862 (CBInPlay_t2057847888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBInPlay::setAppIcon()
extern "C"  void CBInPlay_setAppIcon_m2294695304 (CBInPlay_t2057847888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBInPlay::show()
extern "C"  void CBInPlay_show_m3814710305 (CBInPlay_t2057847888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBInPlay::click()
extern "C"  void CBInPlay_click_m2400789248 (CBInPlay_t2057847888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBInPlay::Finalize()
extern "C"  void CBInPlay_Finalize_m1113396920 (CBInPlay_t2057847888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
