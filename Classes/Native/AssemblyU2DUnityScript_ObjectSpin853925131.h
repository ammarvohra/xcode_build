﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectSpin
struct  ObjectSpin_t853925131  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ObjectSpin::DegreesX
	float ___DegreesX_2;
	// System.Single ObjectSpin::DegreesY
	float ___DegreesY_3;
	// System.Single ObjectSpin::DegreesZ
	float ___DegreesZ_4;

public:
	inline static int32_t get_offset_of_DegreesX_2() { return static_cast<int32_t>(offsetof(ObjectSpin_t853925131, ___DegreesX_2)); }
	inline float get_DegreesX_2() const { return ___DegreesX_2; }
	inline float* get_address_of_DegreesX_2() { return &___DegreesX_2; }
	inline void set_DegreesX_2(float value)
	{
		___DegreesX_2 = value;
	}

	inline static int32_t get_offset_of_DegreesY_3() { return static_cast<int32_t>(offsetof(ObjectSpin_t853925131, ___DegreesY_3)); }
	inline float get_DegreesY_3() const { return ___DegreesY_3; }
	inline float* get_address_of_DegreesY_3() { return &___DegreesY_3; }
	inline void set_DegreesY_3(float value)
	{
		___DegreesY_3 = value;
	}

	inline static int32_t get_offset_of_DegreesZ_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t853925131, ___DegreesZ_4)); }
	inline float get_DegreesZ_4() const { return ___DegreesZ_4; }
	inline float* get_address_of_DegreesZ_4() { return &___DegreesZ_4; }
	inline void set_DegreesZ_4(float value)
	{
		___DegreesZ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
