﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// KittyHandNFootManager
struct KittyHandNFootManager_t1686788969;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KittyHandNFootManager/<TakeSnap>c__IteratorB
struct  U3CTakeSnapU3Ec__IteratorB_t2011450030  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D KittyHandNFootManager/<TakeSnap>c__IteratorB::<screenTexture>__0
	Texture2D_t3542995729 * ___U3CscreenTextureU3E__0_0;
	// System.Byte[] KittyHandNFootManager/<TakeSnap>c__IteratorB::<dataToSave>__1
	ByteU5BU5D_t3397334013* ___U3CdataToSaveU3E__1_1;
	// KittyHandNFootManager KittyHandNFootManager/<TakeSnap>c__IteratorB::$this
	KittyHandNFootManager_t1686788969 * ___U24this_2;
	// System.Object KittyHandNFootManager/<TakeSnap>c__IteratorB::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean KittyHandNFootManager/<TakeSnap>c__IteratorB::$disposing
	bool ___U24disposing_4;
	// System.Int32 KittyHandNFootManager/<TakeSnap>c__IteratorB::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CscreenTextureU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTakeSnapU3Ec__IteratorB_t2011450030, ___U3CscreenTextureU3E__0_0)); }
	inline Texture2D_t3542995729 * get_U3CscreenTextureU3E__0_0() const { return ___U3CscreenTextureU3E__0_0; }
	inline Texture2D_t3542995729 ** get_address_of_U3CscreenTextureU3E__0_0() { return &___U3CscreenTextureU3E__0_0; }
	inline void set_U3CscreenTextureU3E__0_0(Texture2D_t3542995729 * value)
	{
		___U3CscreenTextureU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CscreenTextureU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CdataToSaveU3E__1_1() { return static_cast<int32_t>(offsetof(U3CTakeSnapU3Ec__IteratorB_t2011450030, ___U3CdataToSaveU3E__1_1)); }
	inline ByteU5BU5D_t3397334013* get_U3CdataToSaveU3E__1_1() const { return ___U3CdataToSaveU3E__1_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CdataToSaveU3E__1_1() { return &___U3CdataToSaveU3E__1_1; }
	inline void set_U3CdataToSaveU3E__1_1(ByteU5BU5D_t3397334013* value)
	{
		___U3CdataToSaveU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdataToSaveU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CTakeSnapU3Ec__IteratorB_t2011450030, ___U24this_2)); }
	inline KittyHandNFootManager_t1686788969 * get_U24this_2() const { return ___U24this_2; }
	inline KittyHandNFootManager_t1686788969 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(KittyHandNFootManager_t1686788969 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CTakeSnapU3Ec__IteratorB_t2011450030, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CTakeSnapU3Ec__IteratorB_t2011450030, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CTakeSnapU3Ec__IteratorB_t2011450030, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
