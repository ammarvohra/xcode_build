﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RatePanel
struct RatePanel_t560543380;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void RatePanel::.ctor()
extern "C"  void RatePanel__ctor_m4244744291 (RatePanel_t560543380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RatePanel::ClickedOnStar(UnityEngine.GameObject)
extern "C"  void RatePanel_ClickedOnStar_m3353240461 (RatePanel_t560543380 * __this, GameObject_t1756533147 * ___Star0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
