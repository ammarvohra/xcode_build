﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1668570060;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3355102824.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3826601930_gshared (Enumerator_t3355102824 * __this, Dictionary_2_t1668570060 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3826601930(__this, ___host0, method) ((  void (*) (Enumerator_t3355102824 *, Dictionary_2_t1668570060 *, const MethodInfo*))Enumerator__ctor_m3826601930_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3849677355_gshared (Enumerator_t3355102824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3849677355(__this, method) ((  Il2CppObject * (*) (Enumerator_t3355102824 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3849677355_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m847712767_gshared (Enumerator_t3355102824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m847712767(__this, method) ((  void (*) (Enumerator_t3355102824 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m847712767_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m415346334_gshared (Enumerator_t3355102824 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m415346334(__this, method) ((  void (*) (Enumerator_t3355102824 *, const MethodInfo*))Enumerator_Dispose_m415346334_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3950372443_gshared (Enumerator_t3355102824 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3950372443(__this, method) ((  bool (*) (Enumerator_t3355102824 *, const MethodInfo*))Enumerator_MoveNext_m3950372443_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::get_Current()
extern "C"  float Enumerator_get_Current_m2091105989_gshared (Enumerator_t3355102824 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2091105989(__this, method) ((  float (*) (Enumerator_t3355102824 *, const MethodInfo*))Enumerator_get_Current_m2091105989_gshared)(__this, method)
