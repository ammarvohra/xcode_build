﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<NailCuttingAnim>c__Iterator2
struct U3CNailCuttingAnimU3Ec__Iterator2_t2399846568;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::.ctor()
extern "C"  void U3CNailCuttingAnimU3Ec__Iterator2__ctor_m3367604021 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::MoveNext()
extern "C"  bool U3CNailCuttingAnimU3Ec__Iterator2_MoveNext_m2194545375 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CNailCuttingAnimU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4223533475 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNailCuttingAnimU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m4003241739 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::Dispose()
extern "C"  void U3CNailCuttingAnimU3Ec__Iterator2_Dispose_m1019253002 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::Reset()
extern "C"  void U3CNailCuttingAnimU3Ec__Iterator2_Reset_m1681616328 (U3CNailCuttingAnimU3Ec__Iterator2_t2399846568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
