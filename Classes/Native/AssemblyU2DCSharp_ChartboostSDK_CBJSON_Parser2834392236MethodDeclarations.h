﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChartboostSDK.CBJSON/Parser
struct Parser_t2834392236;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBJSON_Parser_TOKE2588697294.h"

// System.Void ChartboostSDK.CBJSON/Parser::.ctor(System.String)
extern "C"  void Parser__ctor_m2780279127 (Parser_t2834392236 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChartboostSDK.CBJSON/Parser::IsWordBreak(System.Char)
extern "C"  bool Parser_IsWordBreak_m1199184369 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChartboostSDK.CBJSON/Parser::Parse(System.String)
extern "C"  Il2CppObject * Parser_Parse_m784926631 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBJSON/Parser::Dispose()
extern "C"  void Parser_Dispose_m4247867438 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable ChartboostSDK.CBJSON/Parser::ParseObject()
extern "C"  Hashtable_t909839986 * Parser_ParseObject_m186862152 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList ChartboostSDK.CBJSON/Parser::ParseArray()
extern "C"  ArrayList_t4252133567 * Parser_ParseArray_m4205983445 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChartboostSDK.CBJSON/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m3736879694 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChartboostSDK.CBJSON/Parser::ParseByToken(ChartboostSDK.CBJSON/Parser/TOKEN)
extern "C"  Il2CppObject * Parser_ParseByToken_m4092523043 (Parser_t2834392236 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBJSON/Parser::ParseString()
extern "C"  String_t* Parser_ParseString_m2539330848 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChartboostSDK.CBJSON/Parser::ParseNumber()
extern "C"  Il2CppObject * Parser_ParseNumber_m3033399334 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChartboostSDK.CBJSON/Parser::EatWhitespace()
extern "C"  void Parser_EatWhitespace_m3425227560 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ChartboostSDK.CBJSON/Parser::get_PeekChar()
extern "C"  Il2CppChar Parser_get_PeekChar_m2482325523 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ChartboostSDK.CBJSON/Parser::get_NextChar()
extern "C"  Il2CppChar Parser_get_NextChar_m1429798795 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChartboostSDK.CBJSON/Parser::get_NextWord()
extern "C"  String_t* Parser_get_NextWord_m2506282280 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChartboostSDK.CBJSON/Parser/TOKEN ChartboostSDK.CBJSON/Parser::get_NextToken()
extern "C"  int32_t Parser_get_NextToken_m3898697017 (Parser_t2834392236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
