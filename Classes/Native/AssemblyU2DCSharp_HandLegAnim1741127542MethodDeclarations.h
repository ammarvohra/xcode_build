﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HandLegAnim
struct HandLegAnim_t1741127542;

#include "codegen/il2cpp-codegen.h"

// System.Void HandLegAnim::.ctor()
extern "C"  void HandLegAnim__ctor_m2582818721 (HandLegAnim_t1741127542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandLegAnim::Start()
extern "C"  void HandLegAnim_Start_m680137041 (HandLegAnim_t1741127542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandLegAnim::Update()
extern "C"  void HandLegAnim_Update_m2025511988 (HandLegAnim_t1741127542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
