﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8
struct U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::.ctor()
extern "C"  void U3CMakeStylePanelOpenU3Ec__Iterator8__ctor_m2328079727 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::MoveNext()
extern "C"  bool U3CMakeStylePanelOpenU3Ec__Iterator8_MoveNext_m1159711429 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeStylePanelOpenU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3032740225 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeStylePanelOpenU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3285415289 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::Dispose()
extern "C"  void U3CMakeStylePanelOpenU3Ec__Iterator8_Dispose_m1311208818 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<MakeStylePanelOpen>c__Iterator8::Reset()
extern "C"  void U3CMakeStylePanelOpenU3Ec__Iterator8_Reset_m1258983352 (U3CMakeStylePanelOpenU3Ec__Iterator8_t3184490244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
