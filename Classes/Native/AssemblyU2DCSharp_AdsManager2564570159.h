﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AdsManager
struct AdsManager_t2564570159;
// GoogleMobileAds.Api.InterstitialAd
struct InterstitialAd_t3805611425;
// GoogleMobileAds.Api.RewardBasedVideoAd
struct RewardBasedVideoAd_t2581948736;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdsManager
struct  AdsManager_t2564570159  : public MonoBehaviour_t1158329972
{
public:
	// GoogleMobileAds.Api.InterstitialAd AdsManager::interstitial
	InterstitialAd_t3805611425 * ___interstitial_3;
	// GoogleMobileAds.Api.RewardBasedVideoAd AdsManager::rewardBasedVideo
	RewardBasedVideoAd_t2581948736 * ___rewardBasedVideo_4;
	// System.String AdsManager::AndroidSmartBannerID
	String_t* ___AndroidSmartBannerID_5;
	// System.String AdsManager::AndroidInterstitialID
	String_t* ___AndroidInterstitialID_6;
	// System.String AdsManager::iosSmartBannerID
	String_t* ___iosSmartBannerID_7;
	// System.String AdsManager::iosInterstitialID
	String_t* ___iosInterstitialID_8;
	// System.Int32 AdsManager::GameOverCount
	int32_t ___GameOverCount_9;
	// System.Int32 AdsManager::ShowAdsOnXCount
	int32_t ___ShowAdsOnXCount_10;
	// System.Int32 AdsManager::count
	int32_t ___count_11;

public:
	inline static int32_t get_offset_of_interstitial_3() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159, ___interstitial_3)); }
	inline InterstitialAd_t3805611425 * get_interstitial_3() const { return ___interstitial_3; }
	inline InterstitialAd_t3805611425 ** get_address_of_interstitial_3() { return &___interstitial_3; }
	inline void set_interstitial_3(InterstitialAd_t3805611425 * value)
	{
		___interstitial_3 = value;
		Il2CppCodeGenWriteBarrier(&___interstitial_3, value);
	}

	inline static int32_t get_offset_of_rewardBasedVideo_4() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159, ___rewardBasedVideo_4)); }
	inline RewardBasedVideoAd_t2581948736 * get_rewardBasedVideo_4() const { return ___rewardBasedVideo_4; }
	inline RewardBasedVideoAd_t2581948736 ** get_address_of_rewardBasedVideo_4() { return &___rewardBasedVideo_4; }
	inline void set_rewardBasedVideo_4(RewardBasedVideoAd_t2581948736 * value)
	{
		___rewardBasedVideo_4 = value;
		Il2CppCodeGenWriteBarrier(&___rewardBasedVideo_4, value);
	}

	inline static int32_t get_offset_of_AndroidSmartBannerID_5() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159, ___AndroidSmartBannerID_5)); }
	inline String_t* get_AndroidSmartBannerID_5() const { return ___AndroidSmartBannerID_5; }
	inline String_t** get_address_of_AndroidSmartBannerID_5() { return &___AndroidSmartBannerID_5; }
	inline void set_AndroidSmartBannerID_5(String_t* value)
	{
		___AndroidSmartBannerID_5 = value;
		Il2CppCodeGenWriteBarrier(&___AndroidSmartBannerID_5, value);
	}

	inline static int32_t get_offset_of_AndroidInterstitialID_6() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159, ___AndroidInterstitialID_6)); }
	inline String_t* get_AndroidInterstitialID_6() const { return ___AndroidInterstitialID_6; }
	inline String_t** get_address_of_AndroidInterstitialID_6() { return &___AndroidInterstitialID_6; }
	inline void set_AndroidInterstitialID_6(String_t* value)
	{
		___AndroidInterstitialID_6 = value;
		Il2CppCodeGenWriteBarrier(&___AndroidInterstitialID_6, value);
	}

	inline static int32_t get_offset_of_iosSmartBannerID_7() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159, ___iosSmartBannerID_7)); }
	inline String_t* get_iosSmartBannerID_7() const { return ___iosSmartBannerID_7; }
	inline String_t** get_address_of_iosSmartBannerID_7() { return &___iosSmartBannerID_7; }
	inline void set_iosSmartBannerID_7(String_t* value)
	{
		___iosSmartBannerID_7 = value;
		Il2CppCodeGenWriteBarrier(&___iosSmartBannerID_7, value);
	}

	inline static int32_t get_offset_of_iosInterstitialID_8() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159, ___iosInterstitialID_8)); }
	inline String_t* get_iosInterstitialID_8() const { return ___iosInterstitialID_8; }
	inline String_t** get_address_of_iosInterstitialID_8() { return &___iosInterstitialID_8; }
	inline void set_iosInterstitialID_8(String_t* value)
	{
		___iosInterstitialID_8 = value;
		Il2CppCodeGenWriteBarrier(&___iosInterstitialID_8, value);
	}

	inline static int32_t get_offset_of_GameOverCount_9() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159, ___GameOverCount_9)); }
	inline int32_t get_GameOverCount_9() const { return ___GameOverCount_9; }
	inline int32_t* get_address_of_GameOverCount_9() { return &___GameOverCount_9; }
	inline void set_GameOverCount_9(int32_t value)
	{
		___GameOverCount_9 = value;
	}

	inline static int32_t get_offset_of_ShowAdsOnXCount_10() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159, ___ShowAdsOnXCount_10)); }
	inline int32_t get_ShowAdsOnXCount_10() const { return ___ShowAdsOnXCount_10; }
	inline int32_t* get_address_of_ShowAdsOnXCount_10() { return &___ShowAdsOnXCount_10; }
	inline void set_ShowAdsOnXCount_10(int32_t value)
	{
		___ShowAdsOnXCount_10 = value;
	}

	inline static int32_t get_offset_of_count_11() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159, ___count_11)); }
	inline int32_t get_count_11() const { return ___count_11; }
	inline int32_t* get_address_of_count_11() { return &___count_11; }
	inline void set_count_11(int32_t value)
	{
		___count_11 = value;
	}
};

struct AdsManager_t2564570159_StaticFields
{
public:
	// AdsManager AdsManager::instance
	AdsManager_t2564570159 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(AdsManager_t2564570159_StaticFields, ___instance_2)); }
	inline AdsManager_t2564570159 * get_instance_2() const { return ___instance_2; }
	inline AdsManager_t2564570159 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AdsManager_t2564570159 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
