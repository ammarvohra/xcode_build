﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LiveVideo
struct  LiveVideo_t867630853  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject LiveVideo::Obj
	GameObject_t1756533147 * ___Obj_2;
	// UnityEngine.GameObject LiveVideo::Obj1
	GameObject_t1756533147 * ___Obj1_3;

public:
	inline static int32_t get_offset_of_Obj_2() { return static_cast<int32_t>(offsetof(LiveVideo_t867630853, ___Obj_2)); }
	inline GameObject_t1756533147 * get_Obj_2() const { return ___Obj_2; }
	inline GameObject_t1756533147 ** get_address_of_Obj_2() { return &___Obj_2; }
	inline void set_Obj_2(GameObject_t1756533147 * value)
	{
		___Obj_2 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_2, value);
	}

	inline static int32_t get_offset_of_Obj1_3() { return static_cast<int32_t>(offsetof(LiveVideo_t867630853, ___Obj1_3)); }
	inline GameObject_t1756533147 * get_Obj1_3() const { return ___Obj1_3; }
	inline GameObject_t1756533147 ** get_address_of_Obj1_3() { return &___Obj1_3; }
	inline void set_Obj1_3(GameObject_t1756533147 * value)
	{
		___Obj1_3 = value;
		Il2CppCodeGenWriteBarrier(&___Obj1_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
