﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundManager
struct SoundManager_t654432262;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m3417712111 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::Start()
extern "C"  void SoundManager_Start_m640423507 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::Update()
extern "C"  void SoundManager_Update_m1087474672 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::OnLevelWasLoaded()
extern "C"  void SoundManager_OnLevelWasLoaded_m16533198 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::MeowSound()
extern "C"  void SoundManager_MeowSound_m35483486 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayBubbleBlastSound()
extern "C"  void SoundManager_PlayBubbleBlastSound_m742743792 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayButtonSound()
extern "C"  void SoundManager_PlayButtonSound_m794282288 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlaySoapBubbleSound()
extern "C"  void SoundManager_PlaySoapBubbleSound_m855144563 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayShowerSound()
extern "C"  void SoundManager_PlayShowerSound_m3324044858 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayTowelSound()
extern "C"  void SoundManager_PlayTowelSound_m3757545037 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayDryerSound()
extern "C"  void SoundManager_PlayDryerSound_m766259662 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayNailCutterSound()
extern "C"  void SoundManager_PlayNailCutterSound_m913856879 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayPerfumeSound()
extern "C"  void SoundManager_PlayPerfumeSound_m2825984396 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayClickSound()
extern "C"  void SoundManager_PlayClickSound_m1792418272 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayLastSound()
extern "C"  void SoundManager_PlayLastSound_m2535147324 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SoundManager::CloseMouth()
extern "C"  Il2CppObject * SoundManager_CloseMouth_m3384529014 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SoundManager::DestroyBubbleSound(UnityEngine.GameObject)
extern "C"  Il2CppObject * SoundManager_DestroyBubbleSound_m4103589584 (SoundManager_t654432262 * __this, GameObject_t1756533147 * ___Bubble0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
