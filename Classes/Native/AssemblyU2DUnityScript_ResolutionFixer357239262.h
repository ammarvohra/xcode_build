﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResolutionFixer
struct  ResolutionFixer_t357239262  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ResolutionFixer::WorkingXorRatio
	int32_t ___WorkingXorRatio_2;
	// System.Int32 ResolutionFixer::WorkingYorRatio
	int32_t ___WorkingYorRatio_3;
	// System.Boolean ResolutionFixer::ChangeX
	bool ___ChangeX_4;
	// System.Boolean ResolutionFixer::ChangeY
	bool ___ChangeY_5;
	// System.Boolean ResolutionFixer::ChangeXRotated
	bool ___ChangeXRotated_6;
	// System.Boolean ResolutionFixer::ChangeYRotated
	bool ___ChangeYRotated_7;
	// UnityEngine.Transform ResolutionFixer::MainCamera
	Transform_t3275118058 * ___MainCamera_8;
	// System.Int32 ResolutionFixer::HeightStore
	int32_t ___HeightStore_9;
	// System.Int32 ResolutionFixer::WidthStore
	int32_t ___WidthStore_10;
	// System.Single ResolutionFixer::ratiox
	float ___ratiox_11;
	// System.Single ResolutionFixer::ratioy
	float ___ratioy_12;
	// System.Single ResolutionFixer::xdistance
	float ___xdistance_13;
	// System.Single ResolutionFixer::ydistance
	float ___ydistance_14;
	// System.Single ResolutionFixer::newxdistance
	float ___newxdistance_15;

public:
	inline static int32_t get_offset_of_WorkingXorRatio_2() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___WorkingXorRatio_2)); }
	inline int32_t get_WorkingXorRatio_2() const { return ___WorkingXorRatio_2; }
	inline int32_t* get_address_of_WorkingXorRatio_2() { return &___WorkingXorRatio_2; }
	inline void set_WorkingXorRatio_2(int32_t value)
	{
		___WorkingXorRatio_2 = value;
	}

	inline static int32_t get_offset_of_WorkingYorRatio_3() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___WorkingYorRatio_3)); }
	inline int32_t get_WorkingYorRatio_3() const { return ___WorkingYorRatio_3; }
	inline int32_t* get_address_of_WorkingYorRatio_3() { return &___WorkingYorRatio_3; }
	inline void set_WorkingYorRatio_3(int32_t value)
	{
		___WorkingYorRatio_3 = value;
	}

	inline static int32_t get_offset_of_ChangeX_4() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___ChangeX_4)); }
	inline bool get_ChangeX_4() const { return ___ChangeX_4; }
	inline bool* get_address_of_ChangeX_4() { return &___ChangeX_4; }
	inline void set_ChangeX_4(bool value)
	{
		___ChangeX_4 = value;
	}

	inline static int32_t get_offset_of_ChangeY_5() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___ChangeY_5)); }
	inline bool get_ChangeY_5() const { return ___ChangeY_5; }
	inline bool* get_address_of_ChangeY_5() { return &___ChangeY_5; }
	inline void set_ChangeY_5(bool value)
	{
		___ChangeY_5 = value;
	}

	inline static int32_t get_offset_of_ChangeXRotated_6() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___ChangeXRotated_6)); }
	inline bool get_ChangeXRotated_6() const { return ___ChangeXRotated_6; }
	inline bool* get_address_of_ChangeXRotated_6() { return &___ChangeXRotated_6; }
	inline void set_ChangeXRotated_6(bool value)
	{
		___ChangeXRotated_6 = value;
	}

	inline static int32_t get_offset_of_ChangeYRotated_7() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___ChangeYRotated_7)); }
	inline bool get_ChangeYRotated_7() const { return ___ChangeYRotated_7; }
	inline bool* get_address_of_ChangeYRotated_7() { return &___ChangeYRotated_7; }
	inline void set_ChangeYRotated_7(bool value)
	{
		___ChangeYRotated_7 = value;
	}

	inline static int32_t get_offset_of_MainCamera_8() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___MainCamera_8)); }
	inline Transform_t3275118058 * get_MainCamera_8() const { return ___MainCamera_8; }
	inline Transform_t3275118058 ** get_address_of_MainCamera_8() { return &___MainCamera_8; }
	inline void set_MainCamera_8(Transform_t3275118058 * value)
	{
		___MainCamera_8 = value;
		Il2CppCodeGenWriteBarrier(&___MainCamera_8, value);
	}

	inline static int32_t get_offset_of_HeightStore_9() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___HeightStore_9)); }
	inline int32_t get_HeightStore_9() const { return ___HeightStore_9; }
	inline int32_t* get_address_of_HeightStore_9() { return &___HeightStore_9; }
	inline void set_HeightStore_9(int32_t value)
	{
		___HeightStore_9 = value;
	}

	inline static int32_t get_offset_of_WidthStore_10() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___WidthStore_10)); }
	inline int32_t get_WidthStore_10() const { return ___WidthStore_10; }
	inline int32_t* get_address_of_WidthStore_10() { return &___WidthStore_10; }
	inline void set_WidthStore_10(int32_t value)
	{
		___WidthStore_10 = value;
	}

	inline static int32_t get_offset_of_ratiox_11() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___ratiox_11)); }
	inline float get_ratiox_11() const { return ___ratiox_11; }
	inline float* get_address_of_ratiox_11() { return &___ratiox_11; }
	inline void set_ratiox_11(float value)
	{
		___ratiox_11 = value;
	}

	inline static int32_t get_offset_of_ratioy_12() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___ratioy_12)); }
	inline float get_ratioy_12() const { return ___ratioy_12; }
	inline float* get_address_of_ratioy_12() { return &___ratioy_12; }
	inline void set_ratioy_12(float value)
	{
		___ratioy_12 = value;
	}

	inline static int32_t get_offset_of_xdistance_13() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___xdistance_13)); }
	inline float get_xdistance_13() const { return ___xdistance_13; }
	inline float* get_address_of_xdistance_13() { return &___xdistance_13; }
	inline void set_xdistance_13(float value)
	{
		___xdistance_13 = value;
	}

	inline static int32_t get_offset_of_ydistance_14() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___ydistance_14)); }
	inline float get_ydistance_14() const { return ___ydistance_14; }
	inline float* get_address_of_ydistance_14() { return &___ydistance_14; }
	inline void set_ydistance_14(float value)
	{
		___ydistance_14 = value;
	}

	inline static int32_t get_offset_of_newxdistance_15() { return static_cast<int32_t>(offsetof(ResolutionFixer_t357239262, ___newxdistance_15)); }
	inline float get_newxdistance_15() const { return ___newxdistance_15; }
	inline float* get_address_of_newxdistance_15() { return &___newxdistance_15; }
	inline void set_newxdistance_15(float value)
	{
		___newxdistance_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
