﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`2<System.Object,ChartboostSDK.CBImpressionError>
struct Action_2_t3988217506;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_ChartboostSDK_CBImpressionError4105614948.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Action`2<System.Object,ChartboostSDK.CBImpressionError>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1309431185_gshared (Action_2_t3988217506 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_2__ctor_m1309431185(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3988217506 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m1309431185_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Object,ChartboostSDK.CBImpressionError>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1868126680_gshared (Action_2_t3988217506 * __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
#define Action_2_Invoke_m1868126680(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3988217506 *, Il2CppObject *, int32_t, const MethodInfo*))Action_2_Invoke_m1868126680_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Object,ChartboostSDK.CBImpressionError>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m111925093_gshared (Action_2_t3988217506 * __this, Il2CppObject * ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Action_2_BeginInvoke_m111925093(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3988217506 *, Il2CppObject *, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m111925093_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Object,ChartboostSDK.CBImpressionError>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m4045295547_gshared (Action_2_t3988217506 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_2_EndInvoke_m4045295547(__this, ___result0, method) ((  void (*) (Action_2_t3988217506 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m4045295547_gshared)(__this, ___result0, method)
