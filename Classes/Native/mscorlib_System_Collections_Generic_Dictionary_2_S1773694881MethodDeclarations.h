﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>
struct ShimEnumerator_t1773694881;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1668570060;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4247058631_gshared (ShimEnumerator_t1773694881 * __this, Dictionary_2_t1668570060 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m4247058631(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1773694881 *, Dictionary_2_t1668570060 *, const MethodInfo*))ShimEnumerator__ctor_m4247058631_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m495099066_gshared (ShimEnumerator_t1773694881 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m495099066(__this, method) ((  bool (*) (ShimEnumerator_t1773694881 *, const MethodInfo*))ShimEnumerator_MoveNext_m495099066_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3042727434_gshared (ShimEnumerator_t1773694881 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3042727434(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1773694881 *, const MethodInfo*))ShimEnumerator_get_Entry_m3042727434_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3614959615_gshared (ShimEnumerator_t1773694881 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3614959615(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1773694881 *, const MethodInfo*))ShimEnumerator_get_Key_m3614959615_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m647599695_gshared (ShimEnumerator_t1773694881 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m647599695(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1773694881 *, const MethodInfo*))ShimEnumerator_get_Value_m647599695_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2478333013_gshared (ShimEnumerator_t1773694881 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2478333013(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1773694881 *, const MethodInfo*))ShimEnumerator_get_Current_m2478333013_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::Reset()
extern "C"  void ShimEnumerator_Reset_m4136098081_gshared (ShimEnumerator_t1773694881 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m4136098081(__this, method) ((  void (*) (ShimEnumerator_t1773694881 *, const MethodInfo*))ShimEnumerator_Reset_m4136098081_gshared)(__this, method)
