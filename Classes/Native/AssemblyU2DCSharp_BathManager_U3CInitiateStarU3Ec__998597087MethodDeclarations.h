﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BathManager/<InitiateStar>c__Iterator2
struct U3CInitiateStarU3Ec__Iterator2_t998597087;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BathManager/<InitiateStar>c__Iterator2::.ctor()
extern "C"  void U3CInitiateStarU3Ec__Iterator2__ctor_m1935710274 (U3CInitiateStarU3Ec__Iterator2_t998597087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BathManager/<InitiateStar>c__Iterator2::MoveNext()
extern "C"  bool U3CInitiateStarU3Ec__Iterator2_MoveNext_m3490082766 (U3CInitiateStarU3Ec__Iterator2_t998597087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BathManager/<InitiateStar>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInitiateStarU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m203780070 (U3CInitiateStarU3Ec__Iterator2_t998597087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BathManager/<InitiateStar>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInitiateStarU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m549233182 (U3CInitiateStarU3Ec__Iterator2_t998597087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager/<InitiateStar>c__Iterator2::Dispose()
extern "C"  void U3CInitiateStarU3Ec__Iterator2_Dispose_m2582415349 (U3CInitiateStarU3Ec__Iterator2_t998597087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BathManager/<InitiateStar>c__Iterator2::Reset()
extern "C"  void U3CInitiateStarU3Ec__Iterator2_Reset_m2731352795 (U3CInitiateStarU3Ec__Iterator2_t998597087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
