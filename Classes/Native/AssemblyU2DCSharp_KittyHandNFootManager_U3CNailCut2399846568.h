﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// KittyHandNFootManager
struct KittyHandNFootManager_t1686788969;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KittyHandNFootManager/<NailCuttingAnim>c__Iterator2
struct  U3CNailCuttingAnimU3Ec__Iterator2_t2399846568  : public Il2CppObject
{
public:
	// UnityEngine.GameObject KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::Obj
	GameObject_t1756533147 * ___Obj_0;
	// KittyHandNFootManager KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::$this
	KittyHandNFootManager_t1686788969 * ___U24this_1;
	// System.Object KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 KittyHandNFootManager/<NailCuttingAnim>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_Obj_0() { return static_cast<int32_t>(offsetof(U3CNailCuttingAnimU3Ec__Iterator2_t2399846568, ___Obj_0)); }
	inline GameObject_t1756533147 * get_Obj_0() const { return ___Obj_0; }
	inline GameObject_t1756533147 ** get_address_of_Obj_0() { return &___Obj_0; }
	inline void set_Obj_0(GameObject_t1756533147 * value)
	{
		___Obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CNailCuttingAnimU3Ec__Iterator2_t2399846568, ___U24this_1)); }
	inline KittyHandNFootManager_t1686788969 * get_U24this_1() const { return ___U24this_1; }
	inline KittyHandNFootManager_t1686788969 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(KittyHandNFootManager_t1686788969 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CNailCuttingAnimU3Ec__Iterator2_t2399846568, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CNailCuttingAnimU3Ec__Iterator2_t2399846568, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CNailCuttingAnimU3Ec__Iterator2_t2399846568, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
