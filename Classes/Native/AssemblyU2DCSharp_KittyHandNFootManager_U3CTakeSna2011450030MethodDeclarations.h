﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KittyHandNFootManager/<TakeSnap>c__IteratorB
struct U3CTakeSnapU3Ec__IteratorB_t2011450030;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KittyHandNFootManager/<TakeSnap>c__IteratorB::.ctor()
extern "C"  void U3CTakeSnapU3Ec__IteratorB__ctor_m1157678565 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KittyHandNFootManager/<TakeSnap>c__IteratorB::MoveNext()
extern "C"  bool U3CTakeSnapU3Ec__IteratorB_MoveNext_m1149126071 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<TakeSnap>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTakeSnapU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1427535143 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KittyHandNFootManager/<TakeSnap>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTakeSnapU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m4096029999 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<TakeSnap>c__IteratorB::Dispose()
extern "C"  void U3CTakeSnapU3Ec__IteratorB_Dispose_m43428456 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KittyHandNFootManager/<TakeSnap>c__IteratorB::Reset()
extern "C"  void U3CTakeSnapU3Ec__IteratorB_Reset_m2599214686 (U3CTakeSnapU3Ec__IteratorB_t2011450030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
