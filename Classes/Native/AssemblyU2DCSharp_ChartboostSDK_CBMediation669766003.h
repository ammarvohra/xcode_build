﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChartboostSDK.CBMediation
struct CBMediation_t669766003;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChartboostSDK.CBMediation
struct  CBMediation_t669766003  : public Il2CppObject
{
public:
	// System.String ChartboostSDK.CBMediation::name
	String_t* ___name_10;

public:
	inline static int32_t get_offset_of_name_10() { return static_cast<int32_t>(offsetof(CBMediation_t669766003, ___name_10)); }
	inline String_t* get_name_10() const { return ___name_10; }
	inline String_t** get_address_of_name_10() { return &___name_10; }
	inline void set_name_10(String_t* value)
	{
		___name_10 = value;
		Il2CppCodeGenWriteBarrier(&___name_10, value);
	}
};

struct CBMediation_t669766003_StaticFields
{
public:
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::AdMarvel
	CBMediation_t669766003 * ___AdMarvel_0;
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::Fuse
	CBMediation_t669766003 * ___Fuse_1;
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::Fyber
	CBMediation_t669766003 * ___Fyber_2;
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::HeyZap
	CBMediation_t669766003 * ___HeyZap_3;
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::MoPub
	CBMediation_t669766003 * ___MoPub_4;
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::Supersonic
	CBMediation_t669766003 * ___Supersonic_5;
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::AdMob
	CBMediation_t669766003 * ___AdMob_6;
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::HyprMX
	CBMediation_t669766003 * ___HyprMX_7;
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::AerServ
	CBMediation_t669766003 * ___AerServ_8;
	// ChartboostSDK.CBMediation ChartboostSDK.CBMediation::Other
	CBMediation_t669766003 * ___Other_9;

public:
	inline static int32_t get_offset_of_AdMarvel_0() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___AdMarvel_0)); }
	inline CBMediation_t669766003 * get_AdMarvel_0() const { return ___AdMarvel_0; }
	inline CBMediation_t669766003 ** get_address_of_AdMarvel_0() { return &___AdMarvel_0; }
	inline void set_AdMarvel_0(CBMediation_t669766003 * value)
	{
		___AdMarvel_0 = value;
		Il2CppCodeGenWriteBarrier(&___AdMarvel_0, value);
	}

	inline static int32_t get_offset_of_Fuse_1() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___Fuse_1)); }
	inline CBMediation_t669766003 * get_Fuse_1() const { return ___Fuse_1; }
	inline CBMediation_t669766003 ** get_address_of_Fuse_1() { return &___Fuse_1; }
	inline void set_Fuse_1(CBMediation_t669766003 * value)
	{
		___Fuse_1 = value;
		Il2CppCodeGenWriteBarrier(&___Fuse_1, value);
	}

	inline static int32_t get_offset_of_Fyber_2() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___Fyber_2)); }
	inline CBMediation_t669766003 * get_Fyber_2() const { return ___Fyber_2; }
	inline CBMediation_t669766003 ** get_address_of_Fyber_2() { return &___Fyber_2; }
	inline void set_Fyber_2(CBMediation_t669766003 * value)
	{
		___Fyber_2 = value;
		Il2CppCodeGenWriteBarrier(&___Fyber_2, value);
	}

	inline static int32_t get_offset_of_HeyZap_3() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___HeyZap_3)); }
	inline CBMediation_t669766003 * get_HeyZap_3() const { return ___HeyZap_3; }
	inline CBMediation_t669766003 ** get_address_of_HeyZap_3() { return &___HeyZap_3; }
	inline void set_HeyZap_3(CBMediation_t669766003 * value)
	{
		___HeyZap_3 = value;
		Il2CppCodeGenWriteBarrier(&___HeyZap_3, value);
	}

	inline static int32_t get_offset_of_MoPub_4() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___MoPub_4)); }
	inline CBMediation_t669766003 * get_MoPub_4() const { return ___MoPub_4; }
	inline CBMediation_t669766003 ** get_address_of_MoPub_4() { return &___MoPub_4; }
	inline void set_MoPub_4(CBMediation_t669766003 * value)
	{
		___MoPub_4 = value;
		Il2CppCodeGenWriteBarrier(&___MoPub_4, value);
	}

	inline static int32_t get_offset_of_Supersonic_5() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___Supersonic_5)); }
	inline CBMediation_t669766003 * get_Supersonic_5() const { return ___Supersonic_5; }
	inline CBMediation_t669766003 ** get_address_of_Supersonic_5() { return &___Supersonic_5; }
	inline void set_Supersonic_5(CBMediation_t669766003 * value)
	{
		___Supersonic_5 = value;
		Il2CppCodeGenWriteBarrier(&___Supersonic_5, value);
	}

	inline static int32_t get_offset_of_AdMob_6() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___AdMob_6)); }
	inline CBMediation_t669766003 * get_AdMob_6() const { return ___AdMob_6; }
	inline CBMediation_t669766003 ** get_address_of_AdMob_6() { return &___AdMob_6; }
	inline void set_AdMob_6(CBMediation_t669766003 * value)
	{
		___AdMob_6 = value;
		Il2CppCodeGenWriteBarrier(&___AdMob_6, value);
	}

	inline static int32_t get_offset_of_HyprMX_7() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___HyprMX_7)); }
	inline CBMediation_t669766003 * get_HyprMX_7() const { return ___HyprMX_7; }
	inline CBMediation_t669766003 ** get_address_of_HyprMX_7() { return &___HyprMX_7; }
	inline void set_HyprMX_7(CBMediation_t669766003 * value)
	{
		___HyprMX_7 = value;
		Il2CppCodeGenWriteBarrier(&___HyprMX_7, value);
	}

	inline static int32_t get_offset_of_AerServ_8() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___AerServ_8)); }
	inline CBMediation_t669766003 * get_AerServ_8() const { return ___AerServ_8; }
	inline CBMediation_t669766003 ** get_address_of_AerServ_8() { return &___AerServ_8; }
	inline void set_AerServ_8(CBMediation_t669766003 * value)
	{
		___AerServ_8 = value;
		Il2CppCodeGenWriteBarrier(&___AerServ_8, value);
	}

	inline static int32_t get_offset_of_Other_9() { return static_cast<int32_t>(offsetof(CBMediation_t669766003_StaticFields, ___Other_9)); }
	inline CBMediation_t669766003 * get_Other_9() const { return ___Other_9; }
	inline CBMediation_t669766003 ** get_address_of_Other_9() { return &___Other_9; }
	inline void set_Other_9(CBMediation_t669766003 * value)
	{
		___Other_9 = value;
		Il2CppCodeGenWriteBarrier(&___Other_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
