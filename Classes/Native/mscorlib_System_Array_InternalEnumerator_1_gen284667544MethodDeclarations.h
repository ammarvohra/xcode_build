﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen284667544.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23720882578.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m693451429_gshared (InternalEnumerator_1_t284667544 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m693451429(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t284667544 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m693451429_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m428834369_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m428834369(__this, method) ((  void (*) (InternalEnumerator_1_t284667544 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m428834369_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3211838505_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3211838505(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t284667544 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3211838505_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3400531782_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3400531782(__this, method) ((  void (*) (InternalEnumerator_1_t284667544 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3400531782_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4136778385_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4136778385(__this, method) ((  bool (*) (InternalEnumerator_1_t284667544 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4136778385_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::get_Current()
extern "C"  KeyValuePair_2_t3720882578  InternalEnumerator_1_get_Current_m1593252484_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1593252484(__this, method) ((  KeyValuePair_2_t3720882578  (*) (InternalEnumerator_1_t284667544 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1593252484_gshared)(__this, method)
