﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// KittyHandNFootManager
struct KittyHandNFootManager_t1686788969;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KittyHandNFootManager
struct  KittyHandNFootManager_t1686788969  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject KittyHandNFootManager::Talk
	GameObject_t1756533147 * ___Talk_2;
	// UnityEngine.Transform KittyHandNFootManager::Talk_Position
	Transform_t3275118058 * ___Talk_Position_3;
	// UnityEngine.Color[] KittyHandNFootManager::Colors
	ColorU5BU5D_t672350442* ___Colors_4;
	// UnityEngine.GameObject KittyHandNFootManager::LeftLegCollider
	GameObject_t1756533147 * ___LeftLegCollider_5;
	// UnityEngine.GameObject KittyHandNFootManager::RightLegCollider
	GameObject_t1756533147 * ___RightLegCollider_6;
	// UnityEngine.GameObject KittyHandNFootManager::LeftHandCollider
	GameObject_t1756533147 * ___LeftHandCollider_7;
	// UnityEngine.GameObject KittyHandNFootManager::RightHandCollider
	GameObject_t1756533147 * ___RightHandCollider_8;
	// UnityEngine.GameObject KittyHandNFootManager::NormalKitty
	GameObject_t1756533147 * ___NormalKitty_9;
	// UnityEngine.GameObject KittyHandNFootManager::EyeAnim
	GameObject_t1756533147 * ___EyeAnim_10;
	// UnityEngine.GameObject KittyHandNFootManager::HandIcon
	GameObject_t1756533147 * ___HandIcon_11;
	// UnityEngine.GameObject KittyHandNFootManager::HandIconTap
	GameObject_t1756533147 * ___HandIconTap_12;
	// UnityEngine.GameObject KittyHandNFootManager::NailCutter
	GameObject_t1756533147 * ___NailCutter_13;
	// UnityEngine.GameObject KittyHandNFootManager::NailPolishIcon
	GameObject_t1756533147 * ___NailPolishIcon_14;
	// UnityEngine.GameObject KittyHandNFootManager::FacePowderMirror
	GameObject_t1756533147 * ___FacePowderMirror_15;
	// UnityEngine.GameObject KittyHandNFootManager::FacePowderBrush
	GameObject_t1756533147 * ___FacePowderBrush_16;
	// UnityEngine.GameObject KittyHandNFootManager::Perfume
	GameObject_t1756533147 * ___Perfume_17;
	// UnityEngine.GameObject KittyHandNFootManager::NailPolishPanel
	GameObject_t1756533147 * ___NailPolishPanel_18;
	// UnityEngine.Vector2[] KittyHandNFootManager::CutterLeftHandPos
	Vector2U5BU5D_t686124026* ___CutterLeftHandPos_19;
	// UnityEngine.Vector2[] KittyHandNFootManager::CutterRightHandPos
	Vector2U5BU5D_t686124026* ___CutterRightHandPos_20;
	// UnityEngine.Vector2[] KittyHandNFootManager::CutterLeftLegPos
	Vector2U5BU5D_t686124026* ___CutterLeftLegPos_21;
	// UnityEngine.Vector2[] KittyHandNFootManager::CutterRightLegPos
	Vector2U5BU5D_t686124026* ___CutterRightLegPos_22;
	// UnityEngine.Vector3 KittyHandNFootManager::CutterLeftHandRot
	Vector3_t2243707580  ___CutterLeftHandRot_23;
	// UnityEngine.Vector3 KittyHandNFootManager::CutterRightHandRot
	Vector3_t2243707580  ___CutterRightHandRot_24;
	// UnityEngine.Vector3 KittyHandNFootManager::CutterLeftLegRot
	Vector3_t2243707580  ___CutterLeftLegRot_25;
	// UnityEngine.Vector3 KittyHandNFootManager::CutterRightLegRot
	Vector3_t2243707580  ___CutterRightLegRot_26;
	// UnityEngine.GameObject[] KittyHandNFootManager::NailPolishObjects
	GameObjectU5BU5D_t3057952154* ___NailPolishObjects_27;
	// UnityEngine.Color[] KittyHandNFootManager::NailPolishColors
	ColorU5BU5D_t672350442* ___NailPolishColors_28;
	// UnityEngine.Vector3[] KittyHandNFootManager::HandTapPos
	Vector3U5BU5D_t1172311765* ___HandTapPos_29;
	// UnityEngine.Sprite[] KittyHandNFootManager::WhiteNails
	SpriteU5BU5D_t3359083662* ___WhiteNails_30;
	// UnityEngine.GameObject[] KittyHandNFootManager::FaceGlow
	GameObjectU5BU5D_t3057952154* ___FaceGlow_31;
	// UnityEngine.GameObject KittyHandNFootManager::Star
	GameObject_t1756533147 * ___Star_32;
	// UnityEngine.GameObject KittyHandNFootManager::PerfumeSpray
	GameObject_t1756533147 * ___PerfumeSpray_33;
	// UnityEngine.GameObject KittyHandNFootManager::PowderEffect
	GameObject_t1756533147 * ___PowderEffect_34;
	// UnityEngine.GameObject KittyHandNFootManager::LocketStar
	GameObject_t1756533147 * ___LocketStar_35;
	// UnityEngine.GameObject KittyHandNFootManager::CurtainTassel
	GameObject_t1756533147 * ___CurtainTassel_36;
	// UnityEngine.GameObject KittyHandNFootManager::CurtainClose
	GameObject_t1756533147 * ___CurtainClose_37;
	// UnityEngine.GameObject KittyHandNFootManager::CurtainOpen
	GameObject_t1756533147 * ___CurtainOpen_38;
	// UnityEngine.GameObject KittyHandNFootManager::StylePanel
	GameObject_t1756533147 * ___StylePanel_39;
	// UnityEngine.GameObject KittyHandNFootManager::GameOverPanel
	GameObject_t1756533147 * ___GameOverPanel_40;
	// UnityEngine.GameObject KittyHandNFootManager::FbLikeButton
	GameObject_t1756533147 * ___FbLikeButton_41;
	// UnityEngine.GameObject KittyHandNFootManager::RestartButton
	GameObject_t1756533147 * ___RestartButton_42;
	// UnityEngine.GameObject KittyHandNFootManager::RateButton
	GameObject_t1756533147 * ___RateButton_43;
	// UnityEngine.GameObject KittyHandNFootManager::RatePanel
	GameObject_t1756533147 * ___RatePanel_44;
	// UnityEngine.GameObject KittyHandNFootManager::CurtainTop
	GameObject_t1756533147 * ___CurtainTop_45;
	// UnityEngine.GameObject KittyHandNFootManager::StyleManager
	GameObject_t1756533147 * ___StyleManager_46;
	// UnityEngine.GameObject KittyHandNFootManager::DoneButton
	GameObject_t1756533147 * ___DoneButton_47;
	// UnityEngine.GameObject KittyHandNFootManager::LastPanel
	GameObject_t1756533147 * ___LastPanel_48;
	// UnityEngine.GameObject KittyHandNFootManager::SnapTransparentBG
	GameObject_t1756533147 * ___SnapTransparentBG_49;
	// UnityEngine.GameObject KittyHandNFootManager::GameEndAnim
	GameObject_t1756533147 * ___GameEndAnim_50;
	// UnityEngine.GameObject KittyHandNFootManager::NailPolishType
	GameObject_t1756533147 * ___NailPolishType_51;
	// System.Boolean KittyHandNFootManager::LeftHandCollide
	bool ___LeftHandCollide_53;
	// System.Boolean KittyHandNFootManager::RightHandCollide
	bool ___RightHandCollide_54;
	// System.Boolean KittyHandNFootManager::LeftLegCollide
	bool ___LeftLegCollide_55;
	// System.Boolean KittyHandNFootManager::RightLegCollide
	bool ___RightLegCollide_56;
	// UnityEngine.Vector2 KittyHandNFootManager::CutterInitialPos
	Vector2_t2243707579  ___CutterInitialPos_57;
	// System.String KittyHandNFootManager::Talk1
	String_t* ___Talk1_58;
	// System.String KittyHandNFootManager::Talk2
	String_t* ___Talk2_59;
	// UnityEngine.GameObject KittyHandNFootManager::CurrentHand
	GameObject_t1756533147 * ___CurrentHand_60;
	// UnityEngine.Vector2 KittyHandNFootManager::InitialHandScale
	Vector2_t2243707579  ___InitialHandScale_61;
	// System.Boolean KittyHandNFootManager::LeftHandPolishModeOnly
	bool ___LeftHandPolishModeOnly_62;
	// System.Boolean KittyHandNFootManager::RightHandPolishModeOnly
	bool ___RightHandPolishModeOnly_63;
	// System.Boolean KittyHandNFootManager::LeftLegPolishModeOnly
	bool ___LeftLegPolishModeOnly_64;
	// System.Boolean KittyHandNFootManager::RightLegPolishModeOnly
	bool ___RightLegPolishModeOnly_65;
	// System.Boolean KittyHandNFootManager::StopTouchOnHand
	bool ___StopTouchOnHand_66;
	// System.Boolean KittyHandNFootManager::ManipulateRightHand1stNail
	bool ___ManipulateRightHand1stNail_67;
	// System.Boolean KittyHandNFootManager::isProcessing
	bool ___isProcessing_68;
	// System.String KittyHandNFootManager::destination
	String_t* ___destination_69;

public:
	inline static int32_t get_offset_of_Talk_2() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___Talk_2)); }
	inline GameObject_t1756533147 * get_Talk_2() const { return ___Talk_2; }
	inline GameObject_t1756533147 ** get_address_of_Talk_2() { return &___Talk_2; }
	inline void set_Talk_2(GameObject_t1756533147 * value)
	{
		___Talk_2 = value;
		Il2CppCodeGenWriteBarrier(&___Talk_2, value);
	}

	inline static int32_t get_offset_of_Talk_Position_3() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___Talk_Position_3)); }
	inline Transform_t3275118058 * get_Talk_Position_3() const { return ___Talk_Position_3; }
	inline Transform_t3275118058 ** get_address_of_Talk_Position_3() { return &___Talk_Position_3; }
	inline void set_Talk_Position_3(Transform_t3275118058 * value)
	{
		___Talk_Position_3 = value;
		Il2CppCodeGenWriteBarrier(&___Talk_Position_3, value);
	}

	inline static int32_t get_offset_of_Colors_4() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___Colors_4)); }
	inline ColorU5BU5D_t672350442* get_Colors_4() const { return ___Colors_4; }
	inline ColorU5BU5D_t672350442** get_address_of_Colors_4() { return &___Colors_4; }
	inline void set_Colors_4(ColorU5BU5D_t672350442* value)
	{
		___Colors_4 = value;
		Il2CppCodeGenWriteBarrier(&___Colors_4, value);
	}

	inline static int32_t get_offset_of_LeftLegCollider_5() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___LeftLegCollider_5)); }
	inline GameObject_t1756533147 * get_LeftLegCollider_5() const { return ___LeftLegCollider_5; }
	inline GameObject_t1756533147 ** get_address_of_LeftLegCollider_5() { return &___LeftLegCollider_5; }
	inline void set_LeftLegCollider_5(GameObject_t1756533147 * value)
	{
		___LeftLegCollider_5 = value;
		Il2CppCodeGenWriteBarrier(&___LeftLegCollider_5, value);
	}

	inline static int32_t get_offset_of_RightLegCollider_6() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___RightLegCollider_6)); }
	inline GameObject_t1756533147 * get_RightLegCollider_6() const { return ___RightLegCollider_6; }
	inline GameObject_t1756533147 ** get_address_of_RightLegCollider_6() { return &___RightLegCollider_6; }
	inline void set_RightLegCollider_6(GameObject_t1756533147 * value)
	{
		___RightLegCollider_6 = value;
		Il2CppCodeGenWriteBarrier(&___RightLegCollider_6, value);
	}

	inline static int32_t get_offset_of_LeftHandCollider_7() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___LeftHandCollider_7)); }
	inline GameObject_t1756533147 * get_LeftHandCollider_7() const { return ___LeftHandCollider_7; }
	inline GameObject_t1756533147 ** get_address_of_LeftHandCollider_7() { return &___LeftHandCollider_7; }
	inline void set_LeftHandCollider_7(GameObject_t1756533147 * value)
	{
		___LeftHandCollider_7 = value;
		Il2CppCodeGenWriteBarrier(&___LeftHandCollider_7, value);
	}

	inline static int32_t get_offset_of_RightHandCollider_8() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___RightHandCollider_8)); }
	inline GameObject_t1756533147 * get_RightHandCollider_8() const { return ___RightHandCollider_8; }
	inline GameObject_t1756533147 ** get_address_of_RightHandCollider_8() { return &___RightHandCollider_8; }
	inline void set_RightHandCollider_8(GameObject_t1756533147 * value)
	{
		___RightHandCollider_8 = value;
		Il2CppCodeGenWriteBarrier(&___RightHandCollider_8, value);
	}

	inline static int32_t get_offset_of_NormalKitty_9() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___NormalKitty_9)); }
	inline GameObject_t1756533147 * get_NormalKitty_9() const { return ___NormalKitty_9; }
	inline GameObject_t1756533147 ** get_address_of_NormalKitty_9() { return &___NormalKitty_9; }
	inline void set_NormalKitty_9(GameObject_t1756533147 * value)
	{
		___NormalKitty_9 = value;
		Il2CppCodeGenWriteBarrier(&___NormalKitty_9, value);
	}

	inline static int32_t get_offset_of_EyeAnim_10() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___EyeAnim_10)); }
	inline GameObject_t1756533147 * get_EyeAnim_10() const { return ___EyeAnim_10; }
	inline GameObject_t1756533147 ** get_address_of_EyeAnim_10() { return &___EyeAnim_10; }
	inline void set_EyeAnim_10(GameObject_t1756533147 * value)
	{
		___EyeAnim_10 = value;
		Il2CppCodeGenWriteBarrier(&___EyeAnim_10, value);
	}

	inline static int32_t get_offset_of_HandIcon_11() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___HandIcon_11)); }
	inline GameObject_t1756533147 * get_HandIcon_11() const { return ___HandIcon_11; }
	inline GameObject_t1756533147 ** get_address_of_HandIcon_11() { return &___HandIcon_11; }
	inline void set_HandIcon_11(GameObject_t1756533147 * value)
	{
		___HandIcon_11 = value;
		Il2CppCodeGenWriteBarrier(&___HandIcon_11, value);
	}

	inline static int32_t get_offset_of_HandIconTap_12() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___HandIconTap_12)); }
	inline GameObject_t1756533147 * get_HandIconTap_12() const { return ___HandIconTap_12; }
	inline GameObject_t1756533147 ** get_address_of_HandIconTap_12() { return &___HandIconTap_12; }
	inline void set_HandIconTap_12(GameObject_t1756533147 * value)
	{
		___HandIconTap_12 = value;
		Il2CppCodeGenWriteBarrier(&___HandIconTap_12, value);
	}

	inline static int32_t get_offset_of_NailCutter_13() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___NailCutter_13)); }
	inline GameObject_t1756533147 * get_NailCutter_13() const { return ___NailCutter_13; }
	inline GameObject_t1756533147 ** get_address_of_NailCutter_13() { return &___NailCutter_13; }
	inline void set_NailCutter_13(GameObject_t1756533147 * value)
	{
		___NailCutter_13 = value;
		Il2CppCodeGenWriteBarrier(&___NailCutter_13, value);
	}

	inline static int32_t get_offset_of_NailPolishIcon_14() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___NailPolishIcon_14)); }
	inline GameObject_t1756533147 * get_NailPolishIcon_14() const { return ___NailPolishIcon_14; }
	inline GameObject_t1756533147 ** get_address_of_NailPolishIcon_14() { return &___NailPolishIcon_14; }
	inline void set_NailPolishIcon_14(GameObject_t1756533147 * value)
	{
		___NailPolishIcon_14 = value;
		Il2CppCodeGenWriteBarrier(&___NailPolishIcon_14, value);
	}

	inline static int32_t get_offset_of_FacePowderMirror_15() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___FacePowderMirror_15)); }
	inline GameObject_t1756533147 * get_FacePowderMirror_15() const { return ___FacePowderMirror_15; }
	inline GameObject_t1756533147 ** get_address_of_FacePowderMirror_15() { return &___FacePowderMirror_15; }
	inline void set_FacePowderMirror_15(GameObject_t1756533147 * value)
	{
		___FacePowderMirror_15 = value;
		Il2CppCodeGenWriteBarrier(&___FacePowderMirror_15, value);
	}

	inline static int32_t get_offset_of_FacePowderBrush_16() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___FacePowderBrush_16)); }
	inline GameObject_t1756533147 * get_FacePowderBrush_16() const { return ___FacePowderBrush_16; }
	inline GameObject_t1756533147 ** get_address_of_FacePowderBrush_16() { return &___FacePowderBrush_16; }
	inline void set_FacePowderBrush_16(GameObject_t1756533147 * value)
	{
		___FacePowderBrush_16 = value;
		Il2CppCodeGenWriteBarrier(&___FacePowderBrush_16, value);
	}

	inline static int32_t get_offset_of_Perfume_17() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___Perfume_17)); }
	inline GameObject_t1756533147 * get_Perfume_17() const { return ___Perfume_17; }
	inline GameObject_t1756533147 ** get_address_of_Perfume_17() { return &___Perfume_17; }
	inline void set_Perfume_17(GameObject_t1756533147 * value)
	{
		___Perfume_17 = value;
		Il2CppCodeGenWriteBarrier(&___Perfume_17, value);
	}

	inline static int32_t get_offset_of_NailPolishPanel_18() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___NailPolishPanel_18)); }
	inline GameObject_t1756533147 * get_NailPolishPanel_18() const { return ___NailPolishPanel_18; }
	inline GameObject_t1756533147 ** get_address_of_NailPolishPanel_18() { return &___NailPolishPanel_18; }
	inline void set_NailPolishPanel_18(GameObject_t1756533147 * value)
	{
		___NailPolishPanel_18 = value;
		Il2CppCodeGenWriteBarrier(&___NailPolishPanel_18, value);
	}

	inline static int32_t get_offset_of_CutterLeftHandPos_19() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CutterLeftHandPos_19)); }
	inline Vector2U5BU5D_t686124026* get_CutterLeftHandPos_19() const { return ___CutterLeftHandPos_19; }
	inline Vector2U5BU5D_t686124026** get_address_of_CutterLeftHandPos_19() { return &___CutterLeftHandPos_19; }
	inline void set_CutterLeftHandPos_19(Vector2U5BU5D_t686124026* value)
	{
		___CutterLeftHandPos_19 = value;
		Il2CppCodeGenWriteBarrier(&___CutterLeftHandPos_19, value);
	}

	inline static int32_t get_offset_of_CutterRightHandPos_20() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CutterRightHandPos_20)); }
	inline Vector2U5BU5D_t686124026* get_CutterRightHandPos_20() const { return ___CutterRightHandPos_20; }
	inline Vector2U5BU5D_t686124026** get_address_of_CutterRightHandPos_20() { return &___CutterRightHandPos_20; }
	inline void set_CutterRightHandPos_20(Vector2U5BU5D_t686124026* value)
	{
		___CutterRightHandPos_20 = value;
		Il2CppCodeGenWriteBarrier(&___CutterRightHandPos_20, value);
	}

	inline static int32_t get_offset_of_CutterLeftLegPos_21() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CutterLeftLegPos_21)); }
	inline Vector2U5BU5D_t686124026* get_CutterLeftLegPos_21() const { return ___CutterLeftLegPos_21; }
	inline Vector2U5BU5D_t686124026** get_address_of_CutterLeftLegPos_21() { return &___CutterLeftLegPos_21; }
	inline void set_CutterLeftLegPos_21(Vector2U5BU5D_t686124026* value)
	{
		___CutterLeftLegPos_21 = value;
		Il2CppCodeGenWriteBarrier(&___CutterLeftLegPos_21, value);
	}

	inline static int32_t get_offset_of_CutterRightLegPos_22() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CutterRightLegPos_22)); }
	inline Vector2U5BU5D_t686124026* get_CutterRightLegPos_22() const { return ___CutterRightLegPos_22; }
	inline Vector2U5BU5D_t686124026** get_address_of_CutterRightLegPos_22() { return &___CutterRightLegPos_22; }
	inline void set_CutterRightLegPos_22(Vector2U5BU5D_t686124026* value)
	{
		___CutterRightLegPos_22 = value;
		Il2CppCodeGenWriteBarrier(&___CutterRightLegPos_22, value);
	}

	inline static int32_t get_offset_of_CutterLeftHandRot_23() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CutterLeftHandRot_23)); }
	inline Vector3_t2243707580  get_CutterLeftHandRot_23() const { return ___CutterLeftHandRot_23; }
	inline Vector3_t2243707580 * get_address_of_CutterLeftHandRot_23() { return &___CutterLeftHandRot_23; }
	inline void set_CutterLeftHandRot_23(Vector3_t2243707580  value)
	{
		___CutterLeftHandRot_23 = value;
	}

	inline static int32_t get_offset_of_CutterRightHandRot_24() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CutterRightHandRot_24)); }
	inline Vector3_t2243707580  get_CutterRightHandRot_24() const { return ___CutterRightHandRot_24; }
	inline Vector3_t2243707580 * get_address_of_CutterRightHandRot_24() { return &___CutterRightHandRot_24; }
	inline void set_CutterRightHandRot_24(Vector3_t2243707580  value)
	{
		___CutterRightHandRot_24 = value;
	}

	inline static int32_t get_offset_of_CutterLeftLegRot_25() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CutterLeftLegRot_25)); }
	inline Vector3_t2243707580  get_CutterLeftLegRot_25() const { return ___CutterLeftLegRot_25; }
	inline Vector3_t2243707580 * get_address_of_CutterLeftLegRot_25() { return &___CutterLeftLegRot_25; }
	inline void set_CutterLeftLegRot_25(Vector3_t2243707580  value)
	{
		___CutterLeftLegRot_25 = value;
	}

	inline static int32_t get_offset_of_CutterRightLegRot_26() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CutterRightLegRot_26)); }
	inline Vector3_t2243707580  get_CutterRightLegRot_26() const { return ___CutterRightLegRot_26; }
	inline Vector3_t2243707580 * get_address_of_CutterRightLegRot_26() { return &___CutterRightLegRot_26; }
	inline void set_CutterRightLegRot_26(Vector3_t2243707580  value)
	{
		___CutterRightLegRot_26 = value;
	}

	inline static int32_t get_offset_of_NailPolishObjects_27() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___NailPolishObjects_27)); }
	inline GameObjectU5BU5D_t3057952154* get_NailPolishObjects_27() const { return ___NailPolishObjects_27; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_NailPolishObjects_27() { return &___NailPolishObjects_27; }
	inline void set_NailPolishObjects_27(GameObjectU5BU5D_t3057952154* value)
	{
		___NailPolishObjects_27 = value;
		Il2CppCodeGenWriteBarrier(&___NailPolishObjects_27, value);
	}

	inline static int32_t get_offset_of_NailPolishColors_28() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___NailPolishColors_28)); }
	inline ColorU5BU5D_t672350442* get_NailPolishColors_28() const { return ___NailPolishColors_28; }
	inline ColorU5BU5D_t672350442** get_address_of_NailPolishColors_28() { return &___NailPolishColors_28; }
	inline void set_NailPolishColors_28(ColorU5BU5D_t672350442* value)
	{
		___NailPolishColors_28 = value;
		Il2CppCodeGenWriteBarrier(&___NailPolishColors_28, value);
	}

	inline static int32_t get_offset_of_HandTapPos_29() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___HandTapPos_29)); }
	inline Vector3U5BU5D_t1172311765* get_HandTapPos_29() const { return ___HandTapPos_29; }
	inline Vector3U5BU5D_t1172311765** get_address_of_HandTapPos_29() { return &___HandTapPos_29; }
	inline void set_HandTapPos_29(Vector3U5BU5D_t1172311765* value)
	{
		___HandTapPos_29 = value;
		Il2CppCodeGenWriteBarrier(&___HandTapPos_29, value);
	}

	inline static int32_t get_offset_of_WhiteNails_30() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___WhiteNails_30)); }
	inline SpriteU5BU5D_t3359083662* get_WhiteNails_30() const { return ___WhiteNails_30; }
	inline SpriteU5BU5D_t3359083662** get_address_of_WhiteNails_30() { return &___WhiteNails_30; }
	inline void set_WhiteNails_30(SpriteU5BU5D_t3359083662* value)
	{
		___WhiteNails_30 = value;
		Il2CppCodeGenWriteBarrier(&___WhiteNails_30, value);
	}

	inline static int32_t get_offset_of_FaceGlow_31() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___FaceGlow_31)); }
	inline GameObjectU5BU5D_t3057952154* get_FaceGlow_31() const { return ___FaceGlow_31; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_FaceGlow_31() { return &___FaceGlow_31; }
	inline void set_FaceGlow_31(GameObjectU5BU5D_t3057952154* value)
	{
		___FaceGlow_31 = value;
		Il2CppCodeGenWriteBarrier(&___FaceGlow_31, value);
	}

	inline static int32_t get_offset_of_Star_32() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___Star_32)); }
	inline GameObject_t1756533147 * get_Star_32() const { return ___Star_32; }
	inline GameObject_t1756533147 ** get_address_of_Star_32() { return &___Star_32; }
	inline void set_Star_32(GameObject_t1756533147 * value)
	{
		___Star_32 = value;
		Il2CppCodeGenWriteBarrier(&___Star_32, value);
	}

	inline static int32_t get_offset_of_PerfumeSpray_33() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___PerfumeSpray_33)); }
	inline GameObject_t1756533147 * get_PerfumeSpray_33() const { return ___PerfumeSpray_33; }
	inline GameObject_t1756533147 ** get_address_of_PerfumeSpray_33() { return &___PerfumeSpray_33; }
	inline void set_PerfumeSpray_33(GameObject_t1756533147 * value)
	{
		___PerfumeSpray_33 = value;
		Il2CppCodeGenWriteBarrier(&___PerfumeSpray_33, value);
	}

	inline static int32_t get_offset_of_PowderEffect_34() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___PowderEffect_34)); }
	inline GameObject_t1756533147 * get_PowderEffect_34() const { return ___PowderEffect_34; }
	inline GameObject_t1756533147 ** get_address_of_PowderEffect_34() { return &___PowderEffect_34; }
	inline void set_PowderEffect_34(GameObject_t1756533147 * value)
	{
		___PowderEffect_34 = value;
		Il2CppCodeGenWriteBarrier(&___PowderEffect_34, value);
	}

	inline static int32_t get_offset_of_LocketStar_35() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___LocketStar_35)); }
	inline GameObject_t1756533147 * get_LocketStar_35() const { return ___LocketStar_35; }
	inline GameObject_t1756533147 ** get_address_of_LocketStar_35() { return &___LocketStar_35; }
	inline void set_LocketStar_35(GameObject_t1756533147 * value)
	{
		___LocketStar_35 = value;
		Il2CppCodeGenWriteBarrier(&___LocketStar_35, value);
	}

	inline static int32_t get_offset_of_CurtainTassel_36() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CurtainTassel_36)); }
	inline GameObject_t1756533147 * get_CurtainTassel_36() const { return ___CurtainTassel_36; }
	inline GameObject_t1756533147 ** get_address_of_CurtainTassel_36() { return &___CurtainTassel_36; }
	inline void set_CurtainTassel_36(GameObject_t1756533147 * value)
	{
		___CurtainTassel_36 = value;
		Il2CppCodeGenWriteBarrier(&___CurtainTassel_36, value);
	}

	inline static int32_t get_offset_of_CurtainClose_37() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CurtainClose_37)); }
	inline GameObject_t1756533147 * get_CurtainClose_37() const { return ___CurtainClose_37; }
	inline GameObject_t1756533147 ** get_address_of_CurtainClose_37() { return &___CurtainClose_37; }
	inline void set_CurtainClose_37(GameObject_t1756533147 * value)
	{
		___CurtainClose_37 = value;
		Il2CppCodeGenWriteBarrier(&___CurtainClose_37, value);
	}

	inline static int32_t get_offset_of_CurtainOpen_38() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CurtainOpen_38)); }
	inline GameObject_t1756533147 * get_CurtainOpen_38() const { return ___CurtainOpen_38; }
	inline GameObject_t1756533147 ** get_address_of_CurtainOpen_38() { return &___CurtainOpen_38; }
	inline void set_CurtainOpen_38(GameObject_t1756533147 * value)
	{
		___CurtainOpen_38 = value;
		Il2CppCodeGenWriteBarrier(&___CurtainOpen_38, value);
	}

	inline static int32_t get_offset_of_StylePanel_39() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___StylePanel_39)); }
	inline GameObject_t1756533147 * get_StylePanel_39() const { return ___StylePanel_39; }
	inline GameObject_t1756533147 ** get_address_of_StylePanel_39() { return &___StylePanel_39; }
	inline void set_StylePanel_39(GameObject_t1756533147 * value)
	{
		___StylePanel_39 = value;
		Il2CppCodeGenWriteBarrier(&___StylePanel_39, value);
	}

	inline static int32_t get_offset_of_GameOverPanel_40() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___GameOverPanel_40)); }
	inline GameObject_t1756533147 * get_GameOverPanel_40() const { return ___GameOverPanel_40; }
	inline GameObject_t1756533147 ** get_address_of_GameOverPanel_40() { return &___GameOverPanel_40; }
	inline void set_GameOverPanel_40(GameObject_t1756533147 * value)
	{
		___GameOverPanel_40 = value;
		Il2CppCodeGenWriteBarrier(&___GameOverPanel_40, value);
	}

	inline static int32_t get_offset_of_FbLikeButton_41() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___FbLikeButton_41)); }
	inline GameObject_t1756533147 * get_FbLikeButton_41() const { return ___FbLikeButton_41; }
	inline GameObject_t1756533147 ** get_address_of_FbLikeButton_41() { return &___FbLikeButton_41; }
	inline void set_FbLikeButton_41(GameObject_t1756533147 * value)
	{
		___FbLikeButton_41 = value;
		Il2CppCodeGenWriteBarrier(&___FbLikeButton_41, value);
	}

	inline static int32_t get_offset_of_RestartButton_42() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___RestartButton_42)); }
	inline GameObject_t1756533147 * get_RestartButton_42() const { return ___RestartButton_42; }
	inline GameObject_t1756533147 ** get_address_of_RestartButton_42() { return &___RestartButton_42; }
	inline void set_RestartButton_42(GameObject_t1756533147 * value)
	{
		___RestartButton_42 = value;
		Il2CppCodeGenWriteBarrier(&___RestartButton_42, value);
	}

	inline static int32_t get_offset_of_RateButton_43() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___RateButton_43)); }
	inline GameObject_t1756533147 * get_RateButton_43() const { return ___RateButton_43; }
	inline GameObject_t1756533147 ** get_address_of_RateButton_43() { return &___RateButton_43; }
	inline void set_RateButton_43(GameObject_t1756533147 * value)
	{
		___RateButton_43 = value;
		Il2CppCodeGenWriteBarrier(&___RateButton_43, value);
	}

	inline static int32_t get_offset_of_RatePanel_44() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___RatePanel_44)); }
	inline GameObject_t1756533147 * get_RatePanel_44() const { return ___RatePanel_44; }
	inline GameObject_t1756533147 ** get_address_of_RatePanel_44() { return &___RatePanel_44; }
	inline void set_RatePanel_44(GameObject_t1756533147 * value)
	{
		___RatePanel_44 = value;
		Il2CppCodeGenWriteBarrier(&___RatePanel_44, value);
	}

	inline static int32_t get_offset_of_CurtainTop_45() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CurtainTop_45)); }
	inline GameObject_t1756533147 * get_CurtainTop_45() const { return ___CurtainTop_45; }
	inline GameObject_t1756533147 ** get_address_of_CurtainTop_45() { return &___CurtainTop_45; }
	inline void set_CurtainTop_45(GameObject_t1756533147 * value)
	{
		___CurtainTop_45 = value;
		Il2CppCodeGenWriteBarrier(&___CurtainTop_45, value);
	}

	inline static int32_t get_offset_of_StyleManager_46() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___StyleManager_46)); }
	inline GameObject_t1756533147 * get_StyleManager_46() const { return ___StyleManager_46; }
	inline GameObject_t1756533147 ** get_address_of_StyleManager_46() { return &___StyleManager_46; }
	inline void set_StyleManager_46(GameObject_t1756533147 * value)
	{
		___StyleManager_46 = value;
		Il2CppCodeGenWriteBarrier(&___StyleManager_46, value);
	}

	inline static int32_t get_offset_of_DoneButton_47() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___DoneButton_47)); }
	inline GameObject_t1756533147 * get_DoneButton_47() const { return ___DoneButton_47; }
	inline GameObject_t1756533147 ** get_address_of_DoneButton_47() { return &___DoneButton_47; }
	inline void set_DoneButton_47(GameObject_t1756533147 * value)
	{
		___DoneButton_47 = value;
		Il2CppCodeGenWriteBarrier(&___DoneButton_47, value);
	}

	inline static int32_t get_offset_of_LastPanel_48() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___LastPanel_48)); }
	inline GameObject_t1756533147 * get_LastPanel_48() const { return ___LastPanel_48; }
	inline GameObject_t1756533147 ** get_address_of_LastPanel_48() { return &___LastPanel_48; }
	inline void set_LastPanel_48(GameObject_t1756533147 * value)
	{
		___LastPanel_48 = value;
		Il2CppCodeGenWriteBarrier(&___LastPanel_48, value);
	}

	inline static int32_t get_offset_of_SnapTransparentBG_49() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___SnapTransparentBG_49)); }
	inline GameObject_t1756533147 * get_SnapTransparentBG_49() const { return ___SnapTransparentBG_49; }
	inline GameObject_t1756533147 ** get_address_of_SnapTransparentBG_49() { return &___SnapTransparentBG_49; }
	inline void set_SnapTransparentBG_49(GameObject_t1756533147 * value)
	{
		___SnapTransparentBG_49 = value;
		Il2CppCodeGenWriteBarrier(&___SnapTransparentBG_49, value);
	}

	inline static int32_t get_offset_of_GameEndAnim_50() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___GameEndAnim_50)); }
	inline GameObject_t1756533147 * get_GameEndAnim_50() const { return ___GameEndAnim_50; }
	inline GameObject_t1756533147 ** get_address_of_GameEndAnim_50() { return &___GameEndAnim_50; }
	inline void set_GameEndAnim_50(GameObject_t1756533147 * value)
	{
		___GameEndAnim_50 = value;
		Il2CppCodeGenWriteBarrier(&___GameEndAnim_50, value);
	}

	inline static int32_t get_offset_of_NailPolishType_51() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___NailPolishType_51)); }
	inline GameObject_t1756533147 * get_NailPolishType_51() const { return ___NailPolishType_51; }
	inline GameObject_t1756533147 ** get_address_of_NailPolishType_51() { return &___NailPolishType_51; }
	inline void set_NailPolishType_51(GameObject_t1756533147 * value)
	{
		___NailPolishType_51 = value;
		Il2CppCodeGenWriteBarrier(&___NailPolishType_51, value);
	}

	inline static int32_t get_offset_of_LeftHandCollide_53() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___LeftHandCollide_53)); }
	inline bool get_LeftHandCollide_53() const { return ___LeftHandCollide_53; }
	inline bool* get_address_of_LeftHandCollide_53() { return &___LeftHandCollide_53; }
	inline void set_LeftHandCollide_53(bool value)
	{
		___LeftHandCollide_53 = value;
	}

	inline static int32_t get_offset_of_RightHandCollide_54() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___RightHandCollide_54)); }
	inline bool get_RightHandCollide_54() const { return ___RightHandCollide_54; }
	inline bool* get_address_of_RightHandCollide_54() { return &___RightHandCollide_54; }
	inline void set_RightHandCollide_54(bool value)
	{
		___RightHandCollide_54 = value;
	}

	inline static int32_t get_offset_of_LeftLegCollide_55() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___LeftLegCollide_55)); }
	inline bool get_LeftLegCollide_55() const { return ___LeftLegCollide_55; }
	inline bool* get_address_of_LeftLegCollide_55() { return &___LeftLegCollide_55; }
	inline void set_LeftLegCollide_55(bool value)
	{
		___LeftLegCollide_55 = value;
	}

	inline static int32_t get_offset_of_RightLegCollide_56() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___RightLegCollide_56)); }
	inline bool get_RightLegCollide_56() const { return ___RightLegCollide_56; }
	inline bool* get_address_of_RightLegCollide_56() { return &___RightLegCollide_56; }
	inline void set_RightLegCollide_56(bool value)
	{
		___RightLegCollide_56 = value;
	}

	inline static int32_t get_offset_of_CutterInitialPos_57() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CutterInitialPos_57)); }
	inline Vector2_t2243707579  get_CutterInitialPos_57() const { return ___CutterInitialPos_57; }
	inline Vector2_t2243707579 * get_address_of_CutterInitialPos_57() { return &___CutterInitialPos_57; }
	inline void set_CutterInitialPos_57(Vector2_t2243707579  value)
	{
		___CutterInitialPos_57 = value;
	}

	inline static int32_t get_offset_of_Talk1_58() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___Talk1_58)); }
	inline String_t* get_Talk1_58() const { return ___Talk1_58; }
	inline String_t** get_address_of_Talk1_58() { return &___Talk1_58; }
	inline void set_Talk1_58(String_t* value)
	{
		___Talk1_58 = value;
		Il2CppCodeGenWriteBarrier(&___Talk1_58, value);
	}

	inline static int32_t get_offset_of_Talk2_59() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___Talk2_59)); }
	inline String_t* get_Talk2_59() const { return ___Talk2_59; }
	inline String_t** get_address_of_Talk2_59() { return &___Talk2_59; }
	inline void set_Talk2_59(String_t* value)
	{
		___Talk2_59 = value;
		Il2CppCodeGenWriteBarrier(&___Talk2_59, value);
	}

	inline static int32_t get_offset_of_CurrentHand_60() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___CurrentHand_60)); }
	inline GameObject_t1756533147 * get_CurrentHand_60() const { return ___CurrentHand_60; }
	inline GameObject_t1756533147 ** get_address_of_CurrentHand_60() { return &___CurrentHand_60; }
	inline void set_CurrentHand_60(GameObject_t1756533147 * value)
	{
		___CurrentHand_60 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentHand_60, value);
	}

	inline static int32_t get_offset_of_InitialHandScale_61() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___InitialHandScale_61)); }
	inline Vector2_t2243707579  get_InitialHandScale_61() const { return ___InitialHandScale_61; }
	inline Vector2_t2243707579 * get_address_of_InitialHandScale_61() { return &___InitialHandScale_61; }
	inline void set_InitialHandScale_61(Vector2_t2243707579  value)
	{
		___InitialHandScale_61 = value;
	}

	inline static int32_t get_offset_of_LeftHandPolishModeOnly_62() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___LeftHandPolishModeOnly_62)); }
	inline bool get_LeftHandPolishModeOnly_62() const { return ___LeftHandPolishModeOnly_62; }
	inline bool* get_address_of_LeftHandPolishModeOnly_62() { return &___LeftHandPolishModeOnly_62; }
	inline void set_LeftHandPolishModeOnly_62(bool value)
	{
		___LeftHandPolishModeOnly_62 = value;
	}

	inline static int32_t get_offset_of_RightHandPolishModeOnly_63() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___RightHandPolishModeOnly_63)); }
	inline bool get_RightHandPolishModeOnly_63() const { return ___RightHandPolishModeOnly_63; }
	inline bool* get_address_of_RightHandPolishModeOnly_63() { return &___RightHandPolishModeOnly_63; }
	inline void set_RightHandPolishModeOnly_63(bool value)
	{
		___RightHandPolishModeOnly_63 = value;
	}

	inline static int32_t get_offset_of_LeftLegPolishModeOnly_64() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___LeftLegPolishModeOnly_64)); }
	inline bool get_LeftLegPolishModeOnly_64() const { return ___LeftLegPolishModeOnly_64; }
	inline bool* get_address_of_LeftLegPolishModeOnly_64() { return &___LeftLegPolishModeOnly_64; }
	inline void set_LeftLegPolishModeOnly_64(bool value)
	{
		___LeftLegPolishModeOnly_64 = value;
	}

	inline static int32_t get_offset_of_RightLegPolishModeOnly_65() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___RightLegPolishModeOnly_65)); }
	inline bool get_RightLegPolishModeOnly_65() const { return ___RightLegPolishModeOnly_65; }
	inline bool* get_address_of_RightLegPolishModeOnly_65() { return &___RightLegPolishModeOnly_65; }
	inline void set_RightLegPolishModeOnly_65(bool value)
	{
		___RightLegPolishModeOnly_65 = value;
	}

	inline static int32_t get_offset_of_StopTouchOnHand_66() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___StopTouchOnHand_66)); }
	inline bool get_StopTouchOnHand_66() const { return ___StopTouchOnHand_66; }
	inline bool* get_address_of_StopTouchOnHand_66() { return &___StopTouchOnHand_66; }
	inline void set_StopTouchOnHand_66(bool value)
	{
		___StopTouchOnHand_66 = value;
	}

	inline static int32_t get_offset_of_ManipulateRightHand1stNail_67() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___ManipulateRightHand1stNail_67)); }
	inline bool get_ManipulateRightHand1stNail_67() const { return ___ManipulateRightHand1stNail_67; }
	inline bool* get_address_of_ManipulateRightHand1stNail_67() { return &___ManipulateRightHand1stNail_67; }
	inline void set_ManipulateRightHand1stNail_67(bool value)
	{
		___ManipulateRightHand1stNail_67 = value;
	}

	inline static int32_t get_offset_of_isProcessing_68() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___isProcessing_68)); }
	inline bool get_isProcessing_68() const { return ___isProcessing_68; }
	inline bool* get_address_of_isProcessing_68() { return &___isProcessing_68; }
	inline void set_isProcessing_68(bool value)
	{
		___isProcessing_68 = value;
	}

	inline static int32_t get_offset_of_destination_69() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969, ___destination_69)); }
	inline String_t* get_destination_69() const { return ___destination_69; }
	inline String_t** get_address_of_destination_69() { return &___destination_69; }
	inline void set_destination_69(String_t* value)
	{
		___destination_69 = value;
		Il2CppCodeGenWriteBarrier(&___destination_69, value);
	}
};

struct KittyHandNFootManager_t1686788969_StaticFields
{
public:
	// KittyHandNFootManager KittyHandNFootManager::instance
	KittyHandNFootManager_t1686788969 * ___instance_52;

public:
	inline static int32_t get_offset_of_instance_52() { return static_cast<int32_t>(offsetof(KittyHandNFootManager_t1686788969_StaticFields, ___instance_52)); }
	inline KittyHandNFootManager_t1686788969 * get_instance_52() const { return ___instance_52; }
	inline KittyHandNFootManager_t1686788969 ** get_address_of_instance_52() { return &___instance_52; }
	inline void set_instance_52(KittyHandNFootManager_t1686788969 * value)
	{
		___instance_52 = value;
		Il2CppCodeGenWriteBarrier(&___instance_52, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
